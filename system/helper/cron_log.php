<?php

// PHP for testing

// Allowed running only if included in CRON.XML
if ( isset($cron_status) ) {

  // For testing function
  if ( isset($log_file) AND isset($log_line) ) {
  
    $handle = fopen($log_file, 'a+');     // Open file
    $save   = fwrite($handle, $log_line); // Write string to file
    fclose($handle);                      // Close file

  } // if file & line

} // if cron_status

?>