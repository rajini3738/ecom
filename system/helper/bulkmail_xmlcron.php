<?php

//
// Bulk Mail to run with cron *** This BULKMAIL script is modified! 
//

// Allowed running only if included in CRON.XML
if (isset($cron_status)) {

  // Configuration
  //require_once '../../config.php';
  
  // Startup
  //require_once(DIR_SYSTEM . 'startup.php');
  
  // Application Classes
  //require_once(DIR_SYSTEM . 'library/mail.php');
  
  // Registry
  //$registry = new Registry();
  
  // Loader
  //$loader = new Loader($registry);
  //$registry->set('load', $loader);
  
  // Config
  //$config = new Config();
  //$registry->set('config', $config);
  $config = $this->config;
  
  // Database 
  //$db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
  //$registry->set('db', $db);
  $db = $this->db;
  
  // Settings
  //$query = $db->query("SELECT * FROM " . DB_PREFIX . "setting");
  
  //foreach ($query->rows as $setting) {
  //	$config->set($setting['key'], $setting['value']);
  //}
  
  // Limite in every launch
  $limit    = $config->get('bulk_mail_sendlimit');
  
  // sleep
  $sleep    = $config->get('bulk_mail_sleep');
  
  // Select rows in queue ready for processing
  $query = $db->query("SELECT * FROM " . DB_PREFIX . "mail_messages WHERE sended = 0 LIMIT 0," . $limit);
  
  $store_name = $config->get('config_name');
  
  foreach ($query->rows as $result) {
  	
  	$message  = '<html dir="ltr" lang="cs">' . "\n";
  	$message .= '<head>' . "\n";
  	$message .= '<title>' . html_entity_decode($result['subject']) . '</title>' . "\n";
  	$message .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . "\n";
  	$message .= '</head>' . "\n";
  	$message .= '<body>' . html_entity_decode($result['message'], ENT_QUOTES, 'UTF-8') . '</body>' . "\n";
  			
  	$message .= '</html>' . "\n";
  	
  	$mail = new Mail();	
  	
  	$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->hostname = $this->config->get('config_mail_smtp_hostname');
		$mail->username = $this->config->get('config_mail_smtp_username');
		$mail->password = $this->config->get('config_mail_smtp_password');
		$mail->port = $this->config->get('config_mail_smtp_port');
		$mail->timeout = $this->config->get('config_mail_smtp_timeout');				
  	$mail->setTo($result['mailto']);
  	$mail->setFrom($config->get('config_email'));
  	$mail->setSender($store_name);
  	$mail->setSubject($result['subject']);					
  					
  	// foreach ($attachments as $attachment) {
  		// $mail->addAttachment($attachment['path'], $attachment['filename']);
  	// }
  		
  	$mail->setHtml($message);
  	$mail->send();
  	
  	$db->query("UPDATE " . DB_PREFIX . "mail_messages 
  			SET sended = 1,
  				  user_id_sended = 1,
  				  date_sended = now()
  			WHERE message_id = " . (int)$result['message_id']);
  	
  	// pause-timer
  	if ($sleep < 1) {
  		usleep($sleep * 1000000); // microseconds
  	} else {
  		sleep($sleep); // seconds
  	}  
  	/* prevod mikrosec >> sec
  	 25000 = 0.025 sec.
  	250000 = 0.250 sec.
  	500000 = 0.500 sec.
  	*/
    
  } // foreach

} // if cron_status

?>