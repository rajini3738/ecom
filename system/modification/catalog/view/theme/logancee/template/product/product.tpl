﻿<?php echo $header; 
$theme_options = $this->registry->get('theme_options');
$config = $this->registry->get('config'); 
$page_direction = $theme_options->get( 'page_direction' );
include('catalog/view/theme/'.$config->get('config_template').'/template/new_elements/wrapper_top.tpl');

$product_detail = $theme_options->getDataProduct( $product_id );
$text_new = 'جديد';
if($theme_options->get( 'latest_text', $config->get( 'config_language_id' ) ) != '') {
    $text_new = $theme_options->get( 'latest_text', $config->get( 'config_language_id' ) );
} ?>

<div itemscope itemtype="http://data-vocabulary.org/Product">
  <span itemprop="name" class="hidden"><?php echo $heading_title; ?></span>
  <div class="product-info">
  	<div class="row">
  	     <?php $product_custom_block = $modules->getModules('product_custom_block'); ?>
  		<div class="col-sm-<?php if($theme_options->get( 'custom_block', 'product_page', $config->get( 'config_language_id' ), 'status' ) == 1 || count($product_custom_block)) { echo 9; } else { echo 12; } ?> col-sm-12">
  			<div class="row" id="quickview_product">
			    <?php if($theme_options->get( 'product_image_zoom' ) != 2) { ?>
			    <!-- <script>
			    	$(document).ready(function(){
                  var configzoom = {zoomType:'lens',lensSize:300,lensShape:'round',borderSize:1,containLensZoom:1,cursor:'crosshair',gallery:'gallery_01',galleryActiveClass:'active',responsive:1};
			    	     if($(window).width() > 992) {
     			    		<?php if($theme_options->get( 'product_image_zoom' ) == 1) { ?>
     			    			$('#image').elevateZoom({
     			    				zoomType: "inner",
     			    				cursor: "pointer",
     			    				zoomWindowFadeIn: 500,
     			    				zoomWindowFadeOut: 750
     			    			});
     			    		<?php } else { ?>
     				    		$('#image').elevateZoom({
                    zoomType:'lens',lensSize:300,lensShape:'round',borderSize:1,containLensZoom:1,cursor:'crosshair',gallery:'gallery_01',galleryActiveClass:'active',responsive:1
     								/*zoomWindowFadeIn: 500,
     								zoomWindowFadeOut: 500,
     								zoomWindowOffetx: 20,
     								zoomWindowOffety: -1,
     								cursor: "pointer",
     								lensFadeIn: 500,
     								lensFadeOut: 500,*/
     				    		});
     			    		<?php } ?>
     			    		
     			    		var z_index = 0;
     			    		
     			    		$(document).on('click', '.open-popup-image', function () {
     			    		  $('.popup-gallery').magnificPopup('open', z_index);
     			    		  return false;
     			    		});
			    		
     			    		$('.thumbnails a, .thumbnails-carousel a').click(function() {
     			    			var smallImage = $(this).attr('data-image');
     			    			var largeImage = $(this).attr('data-zoom-image');
     			    			var ez =   $('#image').data('elevateZoom');	
     			    			$('#ex1').attr('href', largeImage);  
     			    			ez.swaptheimage(smallImage, largeImage); 
     			    			z_index = $(this).index('.thumbnails a, .thumbnails-carousel a');
     			    			return false;
     			    		});
			    		} else {
			    			$(document).on('click', '.open-popup-image', function () {
			    			  $('.popup-gallery').magnificPopup('open', 0);
			    			  return false;
			    			});
			    		}
			    	});
			    </script> -->
			    <?php } ?>
			    <?php $image_grid = 6; $product_center_grid = 6; 
			    if ($theme_options->get( 'product_image_size' ) == 1) {
			    	$image_grid = 4; $product_center_grid = 8;
			    }
			    
			    if ($theme_options->get( 'product_image_size' ) == 3) {
			    	$image_grid = 8; $product_center_grid = 4;
			    }
			    ?>

			    <div class="col-sm-<?php echo $product_center_grid; ?> product-center clearfix">
			     <div itemprop="offerDetails" itemscope itemtype="http://data-vocabulary.org/Offer">
			     <h2 class="product-name"><?php echo $heading_title; ?></h2>
			      <?php 
			      $product_options_top = $modules->getModules('product_options_top');
			      if( count($product_options_top) ) { 
			      	foreach ($product_options_top as $module) {
			      		echo $module;
			      	}
			      } ?>
			      
			      <?php if ($review_status) { ?>
			      <div class="review" style="display:none">
			      	<?php if($rating > 0) { ?>
			      	<span itemprop="review" class="hidden" itemscope itemtype="http://data-vocabulary.org/Review-aggregate">
			      		<span itemprop="itemreviewed"><?php echo $heading_title; ?></span>
			      		<span itemprop="rating"><?php echo $rating; ?></span>
			      		<span itemprop="votes"><?php preg_match_all('/\(([0-9]+)\)/', $tab_review, $wyniki);
			      		if(isset($wyniki[1][0])) { echo $wyniki[1][0]; } else { echo 0; } ?></span>
			      	</span>
			      	<?php } ?>
			        <div class="rating"><i class="fa fa-star<?php if($rating >= 1) { echo ' active'; } ?>"></i><i class="fa fa-star<?php if($rating >= 2) { echo ' active'; } ?>"></i><i class="fa fa-star<?php if($rating >= 3) { echo ' active'; } ?>"></i><i class="fa fa-star<?php if($rating >= 4) { echo ' active'; } ?>"></i><i class="fa fa-star<?php if($rating >= 5) { echo ' active'; } ?>"></i>
			        <p class="rating-links"><a onclick="$('a[href=\'#tab-review\']').trigger('click'); $('html, body').animate({scrollTop:$('#tab-review').offset().top}, '500', 'swing');"><?php echo $reviews; ?></a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a onclick="$('a[href=\'#tab-review\']').trigger('click'); $('html, body').animate({scrollTop:$('#tab-review').offset().top}, '500', 'swing');"><?php echo $text_write; ?></a></p></div>
			      </div>
			      <?php } ?>
			      
			       
      <?php if ($price && $buynow_status == 0) { ?>   
      
    			       <div class="price" style="border: 0;">
    			            <div class="main-price">
    			       	      <?php if (!$discount_price) { ?>
    			       	        <span class="price-new"><span itemprop="price" id="price-old"><?php echo $detailsprice; ?></span></span>
    			       	      <?php } else { ?>
    			       	        <span class="price-old" id="price-old"><?php echo $price; ?></span><span class="price-new"><span itemprop="price" id="price-special"><?php echo $discount_price; ?></span></span>
    			       	      <?php } ?>
                        <?php if ($discount) { ?>
                        <span class="price-new" style="color:red">
                          (<?php echo $discount; ?>)</span>
                        <?php } ?>
    			              </div>    			              
    			              <div class="other-price" style="display: none;">
    			       	        <?php if ($tax) { ?>
    			       	        <span class="price-tax"  style="display: none;"><?php echo $text_tax; ?> <span id="price-tax"><?php echo $tax; ?></span></span>
    			       	        <?php } ?>
    			       	        <?php if ($points) { ?>
    			       	        <span class="reward" style="display: none;"><small><?php echo $text_points; ?> <?php echo $points; ?></small></span>
    			       	        <?php } ?>
    			       	        <?php if ($discounts) { ?>
    			       	        <div class="discount">
    			       	          <?php foreach ($discounts as $discount) { ?>
    			       	          <?php echo $discount['quantity']; ?> <?php echo $text_discount; ?> <?php echo $discount['price']; ?><br />
    			       	          <?php } ?>
    			       	        </div>
    			       	        <?php } ?>
    			              </div>
                      <div>
                      <?php if ($stock!="0") { ?>
                      	<!-- <span><i class="icon-layers"></i> الكمية المتوفرة :</span>  -->
                      	<!-- <strong><?php echo $stock;?> <?php if ($weight!="milliliter") { ?> &nbsp;<?php echo $weight;  ?> <?php } ?></strong> | --> 
                      <?php } ?>

                      <?php if ($stock != "0") { ?>
                        <span><?php // echo $text_stock; ?></span> <strong <?php if($stock_text == 'متوفر') { echo 'style="color: #99cc00;"'; } ?>><?php echo $stock_text; ?></strong>
                       <?php } else {?>
                        <?php if ($stock=="0") { ?><span><?php echo $text_stock; ?></span> 
                        	<strong style="color:red">نفدت الكمية</strong><?php } ?>
                      <?php } ?>
    			       </div>
			       <?php } ?>
			      </div>
			      
         <div class="description" style="display:none">
			        <?php if ($manufacturer) { ?>
			        <span><?php echo $text_manufacturer; ?></span> <a href="<?php echo $manufacturers; ?>" itemprop="brand"><?php echo $manufacturer; ?></a><br />
			        <?php } ?>
			        <span><?php echo $text_model; ?></span> <?php echo $model; ?><br />
			        <?php if ($reward) { ?>
			        <span><?php echo $text_reward; ?></span> <?php echo $reward; ?><br />
			        <?php } ?>
			        <span><?php echo $text_stock; ?></span> <strong <?php if($stock == 'In Stock') { echo 'style="color: #99cc00;"'; } ?>><?php echo $stock; ?></strong>    
              
           </div>
            <div style="margin-top: 10px;">
              <?php echo $description; ?>
            </div>
			     <div id="product">
			      <?php 
			      $product_options_center = $modules->getModules('product_options_center');
			      if( count($product_options_center) ) { 
			      	foreach ($product_options_center as $module) {
			      		echo $module;
			      	}
			      } ?>




<div class="cart">

					<div class="add-to-cart clearfix">
						<span style="color:red;">* </span>
							<span style="font-weight: 600; width: 70px; display: inline-block; overflow: hidden; vertical-align: top; white-space: nowrap;">
								<?php echo $text_amount; ?>
							</span>
						<br/>
						
						<div class="quantity">							
							<input type="text" name="quantity" id="quantity_wanted" size="2" value="<?php echo $minimum; ?>" />
							<a href="#" id="q_up"><i aria-hidden="true" class="icon_plus"></i></a>
							<a href="#" id="q_down"><i aria-hidden="true" class="icon_minus-06"></i></a>
						</div>

					</div>
					
				  </div>





			      <?php if ($options) { ?>
			      <div class="options">
			        <h2 style="display:none"><?php echo $text_option; ?></h2>
			        <?php foreach ($options as $option) { ?>
			        <?php if ($option['type'] == 'select') { ?>
			        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
			          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
			          <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
			            <!--<option value=""><?php echo $text_select; ?></option> -->
			            <?php foreach ($option['product_option_value'] as $option_value) { ?>
			            <option value="<?php echo $option_value['product_option_value_id']; ?>">
						<?php if ($option_value['quantity']) { ?><?php // echo $option_value['quantity']; ?><?php } ?> 
						<?php echo $option_value['name']; ?>
			            <?php if ($option_value['price']) { ?>
			            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
			            <?php } ?>
			            </option>
			            <?php } ?>
			          </select>
			        </div>
			        <?php } ?>
			        <?php if ($option['type'] == 'radio') { ?>
			        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
			          <label class="control-label"><?php echo $option['name']; ?></label>
			          <div id="input-option<?php echo $option['product_option_id']; ?>">
			            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                   <?php if ($option_value['quantity'] > 0) { ?>
			              <div class="radio <?php if($theme_options->get( 'product_page_radio_style' ) == 1) { echo 'radio-type-button2'; } ?>">
			                <label>
			                  <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                          <?php if (!$option_value['discount_price']) { ?>
                            <span><?php echo $option_value['name']; ?> <?php if ($option_value['price']) { ?>(<?php echo $option_value['price']; ?>)<?php } ?></span>
                          <?php } else { ?>
                            <span><?php echo $option_value['name']; ?> <?php if ($option_value['price']) { ?>(<?php echo $option_value['price']; ?> | <label style="color: red;"><?php echo $option_value['discount']; ?></label> )<?php } ?></span>
                          <?php } ?>			                
			                </label>
			              </div>
                   <?php } ?>
			            <?php } ?>
			            
			            <?php if($theme_options->get( 'product_page_radio_style' ) == 1) { ?>
			            <script type="text/javascript">
			                 $(document).ready(function(){
			                      $('#input-option<?php echo $option['product_option_id']; ?>').on('click', 'span', function () {
			                           $('#input-option<?php echo $option['product_option_id']; ?> span').removeClass("active");
			                           $(this).addClass("active");
			                      });
			                 });
			            </script>
			            <?php } ?>
			          </div>
			        </div>
			        <?php } ?>
			        <?php if ($option['type'] == 'checkbox') { ?>
			        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
			          <label class="control-label"><?php echo $option['name']; ?></label>
			          <div id="input-option<?php echo $option['product_option_id']; ?>">
			            <?php foreach ($option['product_option_value'] as $option_value) { ?>
			            <div class="checkbox <?php if($theme_options->get( 'product_page_checkbox_style' ) == 1) { echo 'radio-type-button2'; } ?>">
			              <label>
			                <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
			                <span><?php echo $option_value['name']; ?>
			                <?php if($theme_options->get( 'product_page_checkbox_style' ) != 1) { ?><?php if ($option_value['price']) { ?>
			                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
			                <?php } ?><?php } ?></span>
			              </label>
			            </div>
			            <?php } ?>
			            
			            <?php if($theme_options->get( 'product_page_checkbox_style' ) == 1) { ?>
			            <script type="text/javascript">
			                 $(document).ready(function(){
			                      $('#input-option<?php echo $option['product_option_id']; ?>').on('click', 'span', function () {
			                           if($(this).hasClass("active") == true) {
			                                $(this).removeClass("active");
			                           } else {
			                                $(this).addClass("active");
			                           }
			                      });
			                 });
			            </script>
			            <?php } ?>
			          </div>
			        </div>
			        <?php } ?>
			        <?php if ($option['type'] == 'image') { ?>
			        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
			          <label class="control-label"><?php echo $option['name']; ?></label>
			          <div id="input-option<?php echo $option['product_option_id']; ?>">
			            <?php foreach ($option['product_option_value'] as $option_value) { ?>
			            <div class="radio <?php if($theme_options->get( 'product_page_radio_style' ) == 1) { echo 'radio-type-button'; } ?>">
			              <label>
			                <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
			                <span <?php if($theme_options->get( 'product_page_radio_style' ) == 1) { ?>data-toggle="tooltip" data-placement="top" title="<?php echo $option_value['name']; ?> <?php if ($option_value['price']) { ?>(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)<?php } ?>"<?php } ?>><img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" <?php if($theme_options->get( 'product_page_radio_style' ) == 1) { ?>width="<?php if($theme_options->get( 'product_page_radio_image_width' ) > 0) { echo $theme_options->get( 'product_page_radio_image_width' ); } else { echo 25; } ?>px" height="<?php if($theme_options->get( 'product_page_radio_image_height' ) > 0) { echo $theme_options->get( 'product_page_radio_image_height' ); } else { echo 25; } ?>px"<?php } ?> /> <?php if($theme_options->get( 'product_page_radio_style' ) != 1) { ?><?php echo $option_value['name']; ?>
			                <?php if ($option_value['price']) { ?>
			                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
			                <?php } ?><?php } ?></span>
			              </label>
			            </div>
			            <?php } ?>
			            <?php if($theme_options->get( 'product_page_radio_style' ) == 1) { ?>
			            <script type="text/javascript">
			                 $(document).ready(function(){
			                      $('#input-option<?php echo $option['product_option_id']; ?>').on('click', 'span', function () {
			                           $('#input-option<?php echo $option['product_option_id']; ?> span').removeClass("active");
			                           $(this).addClass("active");
			                      });
			                 });
			            </script>
			            <?php } ?>
			          </div>
			        </div>
			        <?php } ?>
			        <?php if ($option['type'] == 'text') { ?>
			        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
			          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
			          <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
			        </div>
			        <?php } ?>
			        <?php if ($option['type'] == 'textarea') { ?>
			        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
			          <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
			          <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
			        </div>
			        <?php } ?>
			        <?php if ($option['type'] == 'file') { ?>
			        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
			          <label class="control-label"><?php echo $option['name']; ?></label>
			          <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" class="btn btn-default btn-block" style="margin-top: 7px"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
			          <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
			        </div>
			        <?php } ?>
			       	<?php if ($option['type'] == 'date') { ?>
			       	<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
			       	  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
			       	  <div class="input-group date">
			       	    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
			       	    <span class="input-group-btn">
			       	    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
			       	    </span></div>
			       	</div>
			       	<?php } ?>
			       	<?php if ($option['type'] == 'datetime') { ?>
			       	<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
			       	  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
			       	  <div class="input-group datetime">
			       	    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
			       	    <span class="input-group-btn">
			       	    <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
			       	    </span></div>
			       	</div>
			       	<?php } ?>
			       	<?php if ($option['type'] == 'time') { ?>
			       	<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
			       	  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
			       	  <div class="input-group time">
			       	    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
			       	    <span class="input-group-btn">
			       	    <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
			       	    </span></div>
			       	</div>
			       	<?php } ?>
			        <?php } ?>
			      </div>
			      <?php } ?>
			      
				  
			      <?php if ($recurrings) { ?>
			      <div class="options">
			          <h2><?php echo $text_payment_recurring ?></h2>
			          <div class="form-group required">
			            <select name="recurring_id" class="form-control">
			              <option value=""><?php echo $text_select; ?></option>
			              <?php foreach ($recurrings as $recurring) { ?>
			              <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
			              <?php } ?>
			            </select>
			            <div class="help-block" id="recurring-description"></div>
			          </div>
			      </div>
				  
			      <?php } ?>

			      
			      <div class="cart">
			        <div class="add-to-cart clearfix">
			          <?php 
			          $product_enquiry = $modules->getModules('product_enquiry');
			          if( count($product_enquiry) ) { 
			          	foreach ($product_enquiry as $module) {
			          		echo $module;
			          	}
			          } else { ?>
			          <br/>
						
						<!--
						<span style="color:red;">* </span>
						<span style="font-weight: 600; width: 70px; display: inline-block; overflow: hidden; vertical-align: top; white-space: nowrap;">
							<?php echo $text_amount; ?>
						</span>
						<br/>
						
						<div class="quantity">							
							<input type="text" name="quantity" id="quantity_wanted" size="2" value="<?php echo $minimum; ?>" />
							<a href="#" id="q_up"><i aria-hidden="true" class="icon_plus"></i></a>
							<a href="#" id="q_down"><i aria-hidden="true" class="icon_minus-06"></i></a>
						</div>
						-->
						  
						   
     			          <input type="hidden" name="product_id" size="2" value="<?php echo $product_id; ?>" />
                      <span class="input1"><a style="border-radius: 25px;height: 50px; margin-right:10px;margin-top: 10px;height: 50px;" href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></span>
                      <?php if($product_detail['quantity'] <= '0') {?>
                        <span class="input2"><input style="border-radius: 25px;" type="button" value="<?php echo $button_cart; ?>" id="button-cart-disable" rel="<?php echo $product_id; ?>" data-loading-text="<?php echo $text_loading; ?>" class="button"/></span>
                      <?php } else {?>
                          <span class="input2"><input style="border-radius: 25px;" type="button" value="<?php echo $button_cart; ?>" id="button-cart" rel="<?php echo $product_id; ?>" data-loading-text="<?php echo $text_loading; ?>" class="button"/></span>
                       <?php } ?>
					   <?php if ($auc_status == 1) { ?>
                <?php if ($auc_endstatus > 0 || $currentdate > $enddate) { ?>
                 <div class="alert alert-info"><?php echo $text_aucclosed; ?></div>
                <?php } else { ?>
                <div class="alert alert-info" style="width: 100%;"><div id="timedown" style="width: 100%;text-align: center;"></div></div>                 
              <input type="hidden" name="quantity" value="<?php echo $minimum; ?>" id="input-quantity" class="form-control" />
              <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
              <br />
              <?php if ($buynow_status == 0) { ?>                           
              <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary btn-lg btn-block"><?php echo $button_buynow; ?></button>
              <?php } ?> 
               <br />
              <div class="alert alert-info" style="width: 100%;text-align: center;"><?php echo $text_totalbids; ?> : <?php echo $total_bids; ?></div>
              <form id="form-bid" style="width: 100%;text-align: center;">              
              <label class="control-label" for="input-bid"><?php echo $entry_bid; ?></label>
              
              <div class="input-group" style="width: 100%;text-align: center;">
                    <span class="input-group-btn">
              <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="bid">
                <span class="glyphicon glyphicon-minus"></span>
              </button>
          </span>
          
                    <input type="text" name="bid" class="form-control input-number" value="<?php echo $last_bid; ?>" min="<?php echo $last_bid; ?>" max="10000000000" />
                  
                  <span class="input-group-btn">
              <button type="button" class="btn btn-danger btn-number" data-type="plus" data-field="bid">
                  <span class="glyphicon glyphicon-plus"></span>
              </button>
          </span>
          </div>
              
              <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
              <input type="hidden" name="customer_id" value="<?php echo $customer_id; ?>" />
              <br />
              <a id="button-bid" class="btn btn-primary btn-lg btn-block"><?php echo $button_bid; ?></a>
              </form>              
               <?php } ?>
               <?php } else { ?>
               <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary btn-lg btn-block"><?php echo $button_cart; ?></button>
               <?php } ?>
                        <!--<input type="button" value=""<?php echo $button_cart; ?>" id="button-cart" rel="<?php echo $product_id; ?>" data-loading-text="<?php echo $text_loading; ?>" class="button" />-->
                    <?php 
     			          $product_question = $modules->getModules('product_question');
     			          if( count($product_question) ) { 
     			          	foreach ($product_question as $module) {
     			          		echo $module;
     			          	}
     			          } ?>
			          <?php } ?>
			        </div>
			        
			        <div class="links" style="display:none">
			        	<a onclick="wishlist.add('<?php echo $product_id; ?>');"><?php echo $button_wishlist; ?></a>
			        	<a onclick="compare.add('<?php echo $product_id; ?>');"><?php echo $button_compare; ?></a>
			        </div>
			         
			        <?php if ($minimum > 1) { ?>
			        <div class="minimum"><?php echo $text_minimum; ?></div>
			        <?php } ?>
			      </div>
			     </div><!-- End #product -->
			     
			     <?php if($theme_options->get( 'product_social_share' ) != '0') { ?>
			     
                 <div class="social-share">
                    <div class="title">للمشاركة </div>

                        <ul class="social-listing">
                            <li class="facebook">
                                <a data-toggle="tooltip" data-placement="top" class="trasition-all" title="" href="#" target="_blank" rel="nofollow" onclick="window.open('//www.facebook.com/sharer/sharer.php?u='+window.location.href );return false;" data-original-title=""><i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li class="twitter">
                                <a data-toggle="tooltip" data-placement="top" class="trasition-all" href="#" title="" rel="nofollow" target="_blank" onclick="window.open('//twitter.com/share?url='+window.location.href);return false;" data-original-title=""><i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li class="linkedin">
                                <a data-toggle="tooltip" data-placement="top" class="trasition-all" href="#" title="" rel="nofollow" target="_blank" onclick="window.open('//www.linkedin.com/shareArticle?mini=true&amp;url='+window.location.href);return false;" data-original-title=""><i class="fa fa-linkedin"></i>
                                </a>
                            </li>
                            <li class="tumblr">
                                <a data-toggle="tooltip" data-placement="top" class="trasition-all" href="#" title="" rel="nofollow" target="_blank" onclick="window.open('//www.tumblr.com/share/link?url='+window.location.href);return false;" data-original-title=""><i class="fa fa-tumblr"></i>
                                </a>
                            </li>
                            <li class="google-plus">
                                <a data-toggle="tooltip" data-placement="top" class="trasition-all" href="#" title="" rel="nofollow" target="_blank" onclick="window.open('//plus.google.com/share?url='+window.location.href);return false;" data-original-title=""><i class="fa fa-google-plus"></i>
                                </a>
                            </li>
                            <li class="pinterest">
                                <a data-toggle="tooltip" data-placement="top" class="trasition-all" href="#" title="" rel="nofollow" target="_blank" onclick="window.open('//pinterest.com/pin/create/button/?url='+window.location.href);return false;" data-original-title=""><i class="fa fa-pinterest"></i>
                                </a>
                            </li>                                 
                            <li class="email">
                                <a data-toggle="tooltip" data-placement="top" class="trasition-all" href="#" target="_blank" onclick="window.open('//mail.google.com/mail/u/0/?body='+window.location.href);return false;" title="" data-original-title=""><i class="fa fa-envelope"></i>
                                </a>
                            </li>
                        </ul>                                
                   </div>
                 <div class="share" style="display:none">
			     	      <!-- AddThis Button BEGIN -->
			     	        <div class="addthis_toolbox addthis_default_style">
                            <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                            <a class="addthis_button_tweet"></a> 
                            <a class="addthis_button_pinterest_pinit"></a> 
                            <a class="addthis_counter addthis_pill_style"></a>
                            </div>
			     	        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script> 
			     	      <!-- AddThis Button END --> 
			           </div>
			     <?php } ?>
			      
			      <?php 
			      $product_options_bottom = $modules->getModules('product_options_bottom');
			      if( count($product_options_bottom) ) { 
			      	foreach ($product_options_bottom as $module) {
			      		echo $module;
			      	}
			      } ?>
		    	</div>
		    </div>
			    <div class="col-sm-<?php echo $image_grid; ?> popup-gallery">
			      <?php 
			      $product_image_top = $modules->getModules('product_image_top');
			      if( count($product_image_top) ) { 
			      	foreach ($product_image_top as $module) {
			      		echo $module;
			      	}
			      } ?>
			         
			      <div class="row">
			      	  <?php if (($images || $theme_options->get( 'product_image_zoom' ) != 2) && $theme_options->get( 'position_image_additional' ) == 2) { ?>
			      	  <div class="col-sm-2">
						<div class="thumbnails thumbnails-left clearfix">
							<ul>
							  <?php if($theme_options->get( 'product_image_zoom' ) != 2 && $thumb) { ?>
						      <li><p><a href="<?php echo $popup; ?>" class="popup-image" data-image="<?php echo $thumb; ?>" data-zoom-image="<?php echo $popup; ?>"><img src="<?php echo $theme_options->productImageThumb($product_id, $config->get('config_image_additional_width'), $config->get('config_image_additional_height')); ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></p></li>
							  <?php } ?>
						      <?php foreach ($images as $image) { ?>
						      <li><p><a href="<?php echo $image['popup']; ?>" class="popup-image" data-image="<?php echo $image['popup']; ?>" data-zoom-image="<?php echo $image['popup']; ?>"><img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></p></li>
						      <?php } ?>
						  </ul>
						</div>
			      	  </div>
			      	  <?php } ?>
			      	  
				      <div class="col-lg-12 col-md-12 col-sm-<?php if($theme_options->get( 'position_image_additional' ) == 2) { echo 10; } else { echo 12; } ?>">
				      	<?php if ($thumb) { ?>
					      <div class="product-image <?php if($theme_options->get( 'product_image_zoom' ) != 2) { if($theme_options->get( 'product_image_zoom' ) == 1) { echo 'inner-cloud-zoom'; } else { echo 'cloud-zoom'; } } ?>">
					      	 <?php if($special && $theme_options->get( 'display_text_sale' ) != '0') { ?>
					      	 	<?php $text_sale = 'Sale';
					      	 	if($theme_options->get( 'sale_text', $config->get( 'config_language_id' ) ) != '') {
					      	 		$text_sale = $theme_options->get( 'sale_text', $config->get( 'config_language_id' ) );
					      	 	} ?>
					      	 	<?php if($theme_options->get( 'type_sale' ) == '1') { ?>
					      	 	<?php $product_detail = $theme_options->getDataProduct( $product_id );
					      	 	$roznica_ceny = $product_detail['price']-$product_detail['special'];
					      	 	$procent = ($roznica_ceny*100)/$product_detail['price']; ?>
					      	 	<div class="sale">-<?php echo round($procent); ?>%</div>
					      	 	<?php } else { ?>
					      	 	<div class="sale"><?php echo $text_sale; ?></div>
					      	 	<?php } ?>
					      	 <?php } ?>
					      	 
					      	 <?php if($product_detail['is_latest'] && $theme_options->get( 'display_text_latest' ) != '0'):?>
					      	     <div class="new-label"><span><?php echo $text_new; ?></span></div>
					      	 <?php endif; ?>
                  <?php if($product_detail['quantity'] <= '0') {?>
                    <div class="new-label"><span>فدت الكمية  </span></div>
                  <?php } ?>
					     	 <a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" id="ex1" <?php if($theme_options->get( 'product_image_zoom' ) == 2) { ?>class="popup-image"<?php } else { echo 'class="open-popup-image"'; } ?>><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" id="image" itemprop="image" data-zoom-image="<?php echo $popup; ?>" /></a>
					      </div>
					  	 <?php } else { ?>
					  	 <div class="product-image">
					  	 	 <img src="image/no_image.jpg" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" id="image" itemprop="image" />
                   <?php if($product_detail['quantity'] <= '0') {?>
                    <div class="new-label"><span>فدت الكمية  </span></div>
                   <?php } ?>
					  	 </div>
					  	 <?php } ?>
				      </div>
				      
				       <?php if (($images || $theme_options->get( 'product_image_zoom' ) != 2) && $theme_options->get( 'position_image_additional' ) != 2) { ?>
					   <?php if(sizeof($images) > 1) {?>
						  <div class="col-sm-12">
							   <div class="overflow-thumbnails-carousel">
     							  <div class="thumbnails-carousel owl-carousel">
     					      		<?php if($theme_options->get( 'product_image_zoom' ) != 2 && $thumb) { ?>
     					      			 <div class="item"><a href="<?php echo $popup; ?>" class="popup-image" data-image="<?php echo $thumb; ?>" data-zoom-image="<?php echo $popup; ?>"><img src="<?php echo $theme_options->productImageThumb($product_id, $config->get('config_image_additional_width'), $config->get('config_image_additional_height')); ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></div>
     					      		<?php } ?>
     								 <?php foreach ($images as $image) { ?>
     									 <div class="item"><a href="<?php echo $image['popup']; ?>" class="popup-image" data-image="<?php echo $image['popup']; ?>" data-zoom-image="<?php echo $image['popup']; ?>"><img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></div>
     								 <?php } ?>
     							  </div>
							  </div>					      
							  <script type="text/javascript">
								   $(document).ready(function() {
									 $(".thumbnails-carousel").owlCarousel({
										 autoPlay: 6000, //Set AutoPlay to 3 seconds
										 navigation: true,
										 navigationText: ['', ''],
										 itemsCustom : [
										   [0, 4],
										   [450, 5],
										   [550, 6],
										   [768, 3],
										   [1200, 4]
										 ],
										 <?php if($page_direction[$config->get( 'config_language_id' )] == 'RTL'): ?>
										 direction: 'rtl'
										 <?php endif; ?>
									 });
								   });
							  </script>
						  </div>
						<?php } ?>
				      <?php } ?>
			      </div>
			      
			      <?php 
			      $product_image_bottom = $modules->getModules('product_image_bottom');
			      if( count($product_image_bottom) ) { 
			      	foreach ($product_image_bottom as $module) {
			      		echo $module;
			      	}
			      } ?>
			    </div>

			    
    	</div>
    	
    	<?php if($theme_options->get( 'custom_block', 'product_page', $config->get( 'config_language_id' ), 'status' ) == 1 || count($product_custom_block)) { ?>
    	<div class="col-sm-3">
    	     <?php if($theme_options->get( 'custom_block', 'product_page', $config->get( 'config_language_id' ), 'status' ) == 1) { ?>
    		<div class="product-block">
    			<?php if($theme_options->get( 'custom_block', 'product_page', $config->get( 'config_language_id' ), 'heading' ) != '') { ?>
    			<h4 class="title-block"><?php echo $theme_options->get( 'custom_block', 'product_page', $config->get( 'config_language_id' ), 'heading' ); ?></h4>
    			<div class="strip-line"></div>
    			<?php } ?>
    			<div class="block-content">
    				<?php echo html_entity_decode($theme_options->get( 'custom_block', 'product_page', $config->get( 'config_language_id' ), 'text' )); ?>
    			</div>
    		</div>
    		<?php } ?>
    		
    		<?php foreach ($product_custom_block as $module) { echo $module; } ?>
    	</div>
    	<?php } ?>
    </div>
  </div>
  
  <?php 
  $product_over_tabs = $modules->getModules('product_over_tabs');
  if( count($product_over_tabs) ) { 
  	foreach ($product_over_tabs as $module) {
  		echo $module;
  	}
  } ?>
  
  <?php 
  	  $language_id = $config->get( 'config_language_id' );
	  $tabs = array();
	  
	  $tabs[] = array(
	  	'heading' => $tab_description,
	  	'content' => 'description',
	  	'sort' => 1
	  );
	  
	  if ($attribute_groups) { 
		  $tabs[] = array(
		  	'heading' => $tab_attribute,
		  	'content' => 'attribute',
		  	'sort' => 3
		  );
	  }
	  
	  if ($review_status) { 
	  	  $tabs[] = array(
	  	  	'heading' => $tab_review,
	  	  	'content' => 'review',
	  	  	'sort' => 5
	  	  );
	  }
	  	  	  
	  if(is_array($config->get('product_tabs'))) {
		  foreach($config->get('product_tabs') as $tab) {
		  	if($tab['status'] == 1 || $tab['product_id'] == $product_id) {
		  		foreach($tab['tabs'] as $zakladka) {
		  			if($zakladka['status'] == 1) {
		  				$heading = false; $content = false;
		  				if(isset($zakladka[$language_id])) {
		  					$heading = $zakladka[$language_id]['name'];
		  					$content = html_entity_decode($zakladka[$language_id]['html']);
		  				}
		  				$tabs[] = array(
		  					'heading' => $heading,
		  					'content' => $content,
		  					'sort' => $zakladka['sort_order']
		  				);
		  			}
		  		}
		  	}
		  }
	  }
	  
	  usort($tabs, "cmp_by_optionNumber");
  ?>
  
  <?php if ($tags) { ?>
  <div class="tags_product"><b><?php echo $text_tags; ?></b>
    <?php for ($i = 0; $i < count($tags); $i++) { ?>
    <?php if ($i < (count($tags) - 1)) { ?>
    <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
    <?php } else { ?>
    <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
    <?php } ?>
    <?php } ?>
  </div>
  <?php } ?>
  
  <?php if ($products && $theme_options->get( 'product_related_status' ) != '0') { ?>
  <?php 
  $class = 3; 
  $id = rand(0, 5000)*rand(0, 5000); 
  $all = 4; 
  $row = 4; 
  
  if($theme_options->get( 'product_per_pow' ) == 6) { $class = 2; }
  if($theme_options->get( 'product_per_pow' ) == 5) { $class = 25; }
  if($theme_options->get( 'product_per_pow' ) == 3) { $class = 4; }
  
  if($theme_options->get( 'product_per_pow' ) > 1) { $row = $theme_options->get( 'product_per_pow' ); $all = $theme_options->get( 'product_per_pow' ); } 
  ?>
  <div class="box clearfix <?php if($theme_options->get( 'product_scroll_related' ) != '0') { echo 'with-scroll'; } ?>">
    <?php if($theme_options->get( 'product_scroll_related' ) != '0') { ?>
    <!-- Carousel nav -->
    <a class="next" href="#myCarousel<?php echo $id; ?>" id="myCarousel<?php echo $id; ?>_next"><span></span></a>
    <a class="prev" href="#myCarousel<?php echo $id; ?>" id="myCarousel<?php echo $id; ?>_prev"><span></span></a>
    <?php } ?>
  	
    <div class="box-heading"><?php echo $text_related; ?></div>
    <div class="strip-line"></div>
    <div class="box-content products related-products">
      <div class="box-product">
      	<div id="myCarousel<?php echo $id; ?>" <?php if($theme_options->get( 'product_scroll_related' ) != '0') { ?>class="carousel slide"<?php } ?>>
      		<!-- Carousel items -->
      		<div class="carousel-inner">
      			<?php $i = 0; $row_fluid = 0; $item = 0; foreach ($products as $product) { $row_fluid++; ?>
  	    			<?php if($i == 0) { $item++; echo '<div class="active item"><div class="product-grid"><div class="row">'; } ?>
  	    			<?php $r=$row_fluid-floor($row_fluid/$all)*$all; if($row_fluid>$all && $r == 1) { if($theme_options->get( 'product_scroll_related' ) != '0') { echo '</div></div></div><div class="item"><div class="product-grid"><div class="row">'; $item++; } else { echo '</div><div class="row">'; } } else { $r=$row_fluid-floor($row_fluid/$row)*$row; if($row_fluid>$row && $r == 1) { echo '</div><div class="row">'; } } ?>
  	    			<div class="col-sm-<?php echo $class; ?> col-xs-6">
  	    				<?php include('catalog/view/theme/'.$config->get('config_template').'/template/new_elements/product.tpl'); ?>
  	    			</div>
      			<?php $i++; } ?>
      			<?php if($i > 0) { echo '</div></div></div>'; } ?>
      		</div>
  	  </div>
      </div>
    </div>
  </div>
  
  <?php if($theme_options->get( 'product_scroll_related' ) != '0') { ?>
  <script type="text/javascript">
  $(document).ready(function() {
    var owl<?php echo $id; ?> = $(".box #myCarousel<?php echo $id; ?> .carousel-inner");
  	
    $("#myCarousel<?php echo $id; ?>_next").click(function(){
        owl<?php echo $id; ?>.trigger('owl.next');
        return false;
      })
    $("#myCarousel<?php echo $id; ?>_prev").click(function(){
        owl<?php echo $id; ?>.trigger('owl.prev');
        return false;
    });
      
    owl<?php echo $id; ?>.owlCarousel({
    	  slideSpeed : 500,
        singleItem:true,
        <?php if($page_direction[$config->get( 'config_language_id' )] == 'RTL'): ?>
        direction: 'rtl'
        <?php endif; ?>
     });
  });
  </script>
  <?php } ?>
  <?php } ?>
  
</div>
<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			
			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
//--></script> 
<script type="text/javascript">
          <!--
          $('#button-cart-disable').on('click', function() {
            alert('الكمية نفدت');
            return false;
          });
$('#button-cart').on('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-cart').button('loading');
		},
		complete: function() {
			$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			if (json['error']) {
        if(json['error']['stock']){
            $.notify({
					  message: json['error']['stock'],
					  target: '_blank'
				  },{
					  // settings
					  element: 'body',
					  position: null,
					  type: "info",
					  allow_dismiss: true,
					  newest_on_top: false,
					  placement: {
						  from: "top",
						  align: "right"
					  },
					  offset: 20,
					  spacing: 10,
					  z_index: 2031,
					  delay: 5000,
					  timer: 1000,
					  url_target: '_blank',
					  mouse_over: null,
					  animate: {
						  enter: 'animated fadeInDown',
						  exit: 'animated fadeOutUp'
					  },
					  onShow: null,
					  onShown: null,
					  onClose: null,
					  onClosed: null,
					  icon_type: 'class',
					  template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-danger" role="alert">' +
						  '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
						  '<span data-notify="message"><i class="fa fa-check-circle"></i>&nbsp; {2}</span>' +
						  '<div class="progress" data-notify="progressbar">' +
							  '<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
						  '</div>' +
						  '<a href="{3}" target="{4}" data-notify="url"></a>' +
					  '</div>' 
				  });
        }
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));
						
						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}
				
				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}
				
				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}
			
			if (json['success']) {
				$.notify({
					message: json['success'],
					target: '_blank'
				},{
					// settings
					element: 'body',
					position: null,
					type: "info",
					allow_dismiss: true,
					newest_on_top: false,
					placement: {
						from: "top",
						align: "right"
					},
					offset: 20,
					spacing: 10,
					z_index: 2031,
					delay: 5000,
					timer: 1000,
					url_target: '_blank',
					mouse_over: null,
					animate: {
						enter: 'animated fadeInDown',
						exit: 'animated fadeOutUp'
					},
					onShow: null,
					onShown: null,
					onClose: null,
					onClosed: null,
					icon_type: 'class',
					template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-success" role="alert">' +
						'<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
						'<span data-notify="message"><i class="fa fa-check-circle"></i>&nbsp; {2}</span>' +
						'<div class="progress" data-notify="progressbar">' +
							'<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
						'</div>' +
						'<a href="{3}" target="{4}" data-notify="url"></a>' +
					'</div>' 
				});
				
				$('#cart_block #cart_content').load('index.php?route=common/cart/info #cart_content_ajax');
				$('#cart_block #total_price_ajax').load('index.php?route=common/cart/info #total_price');
				$('#cart_block #total_item_ajax').load('index.php?route=common/cart/info #total_item');
			}
		},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
});
//--></script> 
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});
		
$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;
	
	$('#form-upload').remove();
	
	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');
	
	$('#form-upload input[name=\'file\']').trigger('click');
	
	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);
			
			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();
					
					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}
					
					if (json['success']) {
						alert(json['success']);
						
						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script> 
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
	e.preventDefault();
	
    $('#review').fadeOut('slow');
        
    $('#review').load(this.href);
    
    $('#review').fadeIn('slow');
});         

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
    $.ajax({
        url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
        type: 'post',
        dataType: 'json',
        data: $("#form-review").serialize(),
        beforeSend: function() {
            $('#button-review').button('loading');
        },
        complete: function() {
            $('#button-review').button('reset');
        },
        success: function(json) {
			$('.alert-success, .alert-danger').remove();
            
			if (json['error']) {
                $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
            }
            
            if (json['success']) {
                $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
                                
                $('input[name=\'name\']').val('');
                $('textarea[name=\'text\']').val('');
                $('input[name=\'rating\']:checked').prop('checked', false);
            }
        }
    });
});
</script>

<script type="text/javascript"><!--
$(document).ready(function() {     
	$('.popup-gallery').magnificPopup({
		delegate: 'a.popup-image',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-with-zoom',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
		image: {
			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
			titleSrc: function(item) {
				return item.el.attr('title');
			}
		}
	});
});
//--></script> 

<script type="text/javascript">
var ajax_price = function() {
	$.ajax({
		type: 'POST',
		url: 'index.php?route=product/liveprice/index',
		data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),
		dataType: 'json',
			success: function(json) {
			if (json.success) {
				change_price('#price-special', json.new_price.special);
				change_price('#price-tax', json.new_price.tax);
				//change_price('#price-old', json.new_price.price);
        if(json.new_price.discount){
          var html = '<span class="price-old" id="price-old">'+json.new_price.price+'</span><span class="price-new"><span itemprop="price" id="price-special">'+json.new_price.discount_price+'</span></span> <span class="price-new" style="color:red">('+json.new_price.discount+'%)</span>';
          $('.main-price').html(html);
          //change_price('#price-old', json.new_price.price);
          //change_price('#price-special', json.new_price.discount_price);
        }
        else{
           var html = '<span class="price-new"><span itemprop="price" id="price-old">'+json.new_price.price+'</span></span>';
          $('.main-price').html(html);
        }
			}
		}
	});
}

var change_price = function(id, new_price) {
	$(id).html(new_price);
}

$('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\'], .product-info input[type=\'checkbox\'], .product-info select, .product-info textarea, .product-info input[name=\'quantity\']').on('change', function() {
	ajax_price();
});
</script>

<script type="text/javascript">
$.fn.tabs = function() {
	var selector = this;
	
	this.each(function() {
		var obj = $(this); 
		
		$(obj.attr('href')).hide();
		
		$(obj).click(function() {
			$(selector).removeClass('selected');
			
			$(selector).each(function(i, element) {
				$($(element).attr('href')).hide();
			});
			
			$(this).addClass('selected');
			
			$($(this).attr('href')).show();
			
			return false;
		});
	});

	$(this).show();
	
	$(this).first().click();
};
</script>

<script type="text/javascript">
  <!--
$('#tabs a').tabs();
function showWarningMessage(){
	alert('الكمية نفدت');
	return false;
}
//--></script> 

<?php if($theme_options->get( 'product_image_zoom' ) != 2) { 
echo '<script type="text/javascript" src="catalog/view/theme/' . $config->get( 'config_template' ) . '/js/jquery.elevateZoom-3.0.3.min.js"></script>';
} ?>

<?php include('catalog/view/theme/'.$config->get('config_template').'/template/new_elements/wrapper_bottom.tpl'); ?>

     <script type="text/javascript"><!--
$('.btn-number').click(function(e){
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
                input.val(currentVal - <?php echo $krok_bid; ?>).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + <?php echo $krok_bid; ?>).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    
    
});
$(".input-number").keydown(function (e) {

        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||

            (e.keyCode == 65 && e.ctrlKey === true) || 

            (e.keyCode >= 35 && e.keyCode <= 39)) {

                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    //--></script>
<script type="text/javascript"><!--     
$('#button-bid').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/addbid&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: $("#form-bid").serialize(),
		beforeSend: function() {
			$('#button-bid').button('loading');
		},
		complete: function() {
			$('#button-bid').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error_minbid']) {
				$('#form-bid').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error_minbid'] + '</div>');
			}
      
      if (json['error_low']) {
				$('#form-bid').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error_low'] + '</div>');
			}
      
      if (json['error_login']) {
				$('#form-bid').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error_login'] + '</div>');
			}
      
      if (json['error_enddate']) {
				$('#form-bid').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error_enddate'] + '</div>');
			}

			if (json['success']) {
				$('#form-bid').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--    
var target_datetimedown = new Date('<?php echo $date_ende ?>').getTime();
var days, hours, minutes, seconds;
var countdowntimedown = document.getElementById('timedown');
setInterval(function () {
    var current_date = new Date('<?php echo $date_now ?>').getTime();
    var seconds_left = (target_datetimedown - current_date) / 1000;

    days = parseInt(seconds_left / 86400);
    seconds_left = seconds_left % 86400;
     
    hours = parseInt(seconds_left / 3600);
    seconds_left = seconds_left % 3600;
     
    minutes = parseInt(seconds_left / 60);
    seconds = parseInt(seconds_left % 60);

    countdowntimedown.innerHTML = '<span class="days"><?php echo $text_timeleft ?> : ' + days +  ' <b><?php echo $text_days ?></b></span> <span class="hours">' + hours + ' <b><?php echo $text_hours ?></b></span> <span class="minutes">'
    + minutes + ' <b><?php echo $text_minutes ?></b></span> <span class="seconds">' + seconds + ' <b><?php echo $text_seconds ?></b></span>';  
 
}, 1000);
//--></script> 
      
<?php echo $footer; ?>