<?php
class ControllerModuleFeatured extends Controller {
	public function index($setting) {
		$this->load->language('module/featured');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');


    $data['text_currentbid'] = $this->language->get('text_currentbid');
    $data['text_yourcurrent'] = $this->language->get('text_yourcurrent');  
    $data['text_timeleft'] = $this->language->get('text_timeleft');
    $data['text_days'] = $this->language->get('text_days');
    $data['text_hours'] = $this->language->get('text_hours');
    $data['text_minutes'] = $this->language->get('text_minutes');
    $data['text_seconds'] = $this->language->get('text_seconds');
    $data['button_buynow'] = $this->language->get('button_buynow');
    $data['button_bid'] = $this->language->get('button_bid');
    $data['text_aucclosed'] = $this->language->get('text_aucclosed');
      
		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

		if (!$setting['limit']) {
			$setting['limit'] = 4;
		}

		if (!empty($setting['product'])) {
			$products = array_slice($setting['product'], 0, (int)$setting['limit']);

			foreach ($products as $product_id) {
				$product_info = $this->model_catalog_product->getProduct($product_id);

				if ($product_info) {
					if ($product_info['image']) {
						$image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
					}

					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$price = false;
					}

					if ((float)$product_info['special']) {
						$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$special = false;
					}

					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
					} else {
						$tax = false;
					}

					if ($this->config->get('config_review_status')) {
						$rating = $product_info['rating'];
					} else {
						$rating = false;
					}


      $auc_status = $product_info['auc_status']; 
      $this->load->model('account/yourbids');
      $high_infa = $this->model_catalog_product->getHighbid($product_info['product_id']);
          if ($high_infa['bid']) {
          $current_bid = $this->currency->format($this->tax->calculate($high_infa['bid'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
          } else {
          $current_bid = $this->currency->format($this->tax->calculate($product_info['start_bid'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
          }
         
          if ($high_infa['bid']) {
          $tax_bid = $high_infa['bid'];
          } else {
          $tax_bid = $product_info['start_bid'];
          }
         
          if ($this->config->get('config_tax')) {
						$taxcurrent_bid = $this->currency->format((float)$tax_bid, $this->session->data['currency']);
					} else {
						$taxcurrent_bid = false;
					}
          
      $date_ende = $product_info['date_ende'];
      $datenow = date('Y-m-d h:m:s');
      $currentdate = strtotime($datenow);
      $date_ende = $product_info['date_ende'];
      $enddate = strtotime($date_ende);          
      
					$data['products'][] = array(
						'product_id'  => $product_info['product_id'],
						'thumb'       => $image,
						'name'        => $product_info['name'],
						'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',

       'auc_status' => $auc_status,
       'currentdate' => $currentdate,
       'enddate' => $enddate,
       'buynow_status' => $product_info['buynow_status'],
       'current_bid' => $current_bid,
       'datenow' => $datenow,
       'date_ende'   => $product_info['date_ende'],
       'auc_endstatus' => $product_info['auc_customer_id'],
       'taxcurrent_bid'         => $taxcurrent_bid,
      
						'price'       => $price,
						'special'     => $special,
						'tax'         => $tax,
						'rating'      => $rating,
						'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
					);
				}
			}
		}

		if ($data['products']) {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/featured.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/featured.tpl', $data);
			} else {
				return $this->load->view('default/template/module/featured.tpl', $data);
			}
		}
	}
}