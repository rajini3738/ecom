<?php
class ControllerReportProductPurchased extends Controller {

          public function export() {
		
				

				$url = '';

				if (isset($this->request->get['filter_date_start'])) {
					$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
				}

				if (isset($this->request->get['filter_date_end'])) {
					$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
				}

				if (isset($this->request->get['filter_order_status_id'])) {
					$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
				}

				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}


				$this->load->model('report/product');

				$products_list= array();
        
        if (isset($this->request->get['filter_date_start'])) {
			    $filter_date_start = $this->request->get['filter_date_start'];
		    } else {
			    $filter_date_start = '';
		    }

		    if (isset($this->request->get['filter_date_end'])) {
			    $filter_date_end = $this->request->get['filter_date_end'];
		    } else {
			    $filter_date_end = '';
		    }

		    if (isset($this->request->get['filter_order_status_id'])) {
			    $filter_order_status_id = $this->request->get['filter_order_status_id'];
		    } else {
			    $filter_order_status_id = 0;
		    }

		    if (isset($this->request->get['filter_name'])) {
			    $filter_name = $this->request->get['filter_name'];
		    } else {
			    $filter_name = null;
		    }

		    if (isset($this->request->get['page'])) {
			    $page = $this->request->get['page'];
		    } else {
			    $page = 1;
		    }
        if (isset($this->request->get['sort'])) {
			    $sort = $this->request->get['sort'];
		    } else {
			    $sort = 'total';
		    }
		    if (isset($this->request->get['order'])) {
			    $order = $this->request->get['order'];
		    } else {
			    $order = 'DESC';
		    }

				$data = array(
					'filter_date_start'	     => $filter_date_start,
			    'filter_date_end'	     => $filter_date_end,
			    'filter_order_status_id' => $filter_order_status_id,
			    'filter_name'	         => $filter_name,
			    'sort'                   => $sort,
			    'order'                  => $order,
			   // 'start'                  => ($page - 1) * $this->config->get('config_limit_admin'),
			   // 'limit'                  => $this->config->get('config_limit_admin')
				);

				$results = $this->model_report_product->getPurchased($data);
        $this->load->model('catalog/product');
				foreach ($results as $result) {
            $category = "";
            $product_categories = $this->model_catalog_product->getProductCategories1($result['product_id']);
            foreach ($product_categories  as $product_category) {
                $category = $category.', '.$product_category['name'];
            }
            $category=ltrim($category,",");
            $category=rtrim($category,",");
            
					$products_list[]= array(
						'name'       => $result['name'],
						'model'      => $category,
						'quantity'   => $result['quantity'],
						'total'      => $this->currency->format($result['total'], $this->config->get('config_currency'))
					);
				}
				
				$products = array();
				
				$products_column=array();
				
				$products_column = array('Product Name', 'Category', 'Quantity', 'Total');
					
				$products[0]=   $products_column;   
				
				foreach($products_list as $products_row)
				{
					$products[]=   $products_row;            
				}
				require_once(DIR_SYSTEM . 'library/PHPExcel.php');
				$filename='product_purchased_report_'.date('Y-m-d _ H:i:s');
				$filename = preg_replace('/[^aA-zZ0-9\_\-]/', '', $filename);
				// Create new PHPExcel object

				$objPHPExcel = new PHPExcel();

								
				$objPHPExcel->getActiveSheet()->fromArray($products, null, 'A1');
				// Set active sheet index to the first sheet, so Excel opens this as the first sheet
				$objPHPExcel->setActiveSheetIndex(0);

				// Save Excel 95 file

				$callStartTime = microtime(true);
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
								
				//Setting the header type
				header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
				header("Content-Disposition: attachment; filename=\"" . $filename . ".xls\"");
								
				header('Cache-Control: max-age=0');

				$objWriter->save('php://output');

			}
public function export1() {
		
				

				$url = '';

				if (isset($this->request->get['filter_date_start'])) {
					$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
				}

				if (isset($this->request->get['filter_date_end'])) {
					$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
				}

				if (isset($this->request->get['filter_order_status_id'])) {
					$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
				}

				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}


				$this->load->model('report/product');

				$products_list= array();

				$data = array(
					
				);

				$results = $this->model_report_product->getPurchased($data);

				foreach ($results as $result) {
					$products_list[]= array(
						'name'       => $result['name'],
						'model'      => $result['model'],
						'quantity'   => $result['quantity'],
						'total'      => $this->currency->format($result['total'], $this->config->get('config_currency'))
					);
				}
				
				$products = array();
				
				$products_column=array();
				
				$products_column = array('Product Name', 'Model', 'Quantity', 'Total');
					
				$products[0]=   $products_column;   
				
				foreach($products_list as $products_row)
				{
					$products[]=   $products_row;            
				}     
				require_once(DIR_SYSTEM . 'library/excel_xml.php');
				$xls = new Excel_XML('UTF-8', false, 'Products Purchased Report');
				
				$xls->addArray($products);
				
				$xls->generateXML('product_purchased_report_'.date('Y-m-d _ H:i:s'));	

			}
	public function index() {
		$this->load->language('report/product_purchased');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['filter_date_start'])) {
			$filter_date_start = $this->request->get['filter_date_start'];
		} else {
			$filter_date_start = '';
		}

		if (isset($this->request->get['filter_date_end'])) {
			$filter_date_end = $this->request->get['filter_date_end'];
		} else {
			$filter_date_end = '';
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$filter_order_status_id = $this->request->get['filter_order_status_id'];
		} else {
			$filter_order_status_id = "Sales";
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'total';
		}
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('report/product_purchased', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$this->load->model('report/product');

		$data['products'] = array();

		$filter_data = array(
			'filter_date_start'	     => $filter_date_start,
			'filter_date_end'	     => $filter_date_end,
			'filter_order_status_id' => $filter_order_status_id,
			'filter_name'	         => $filter_name,
			'sort'                   => $sort,
			'order'                  => $order,
			'start'                  => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                  => $this->config->get('config_limit_admin')
		);

		$product_total = $this->model_report_product->getTotalPurchased($filter_data);

		$results = $this->model_report_product->getPurchased($filter_data);

        $this->load->model('catalog/product');
		foreach ($results as $result) {
            $category = "";
            $product_categories = $this->model_catalog_product->getProductCategories1($result['product_id']);
            foreach ($product_categories  as $product_category) {
                $category = $category.', '.$product_category['name'];
            }
            $category=ltrim($category,",");
            $category=rtrim($category,",");

			$data['products'][] = array(
				'name'       => $result['name'],
				'model'      => $category,
				'quantity'   => $result['quantity'],
				'total'      => $this->currency->format($result['total'], $this->config->get('config_currency'))
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_all_status'] = $this->language->get('text_all_status');
		$data['entry_name'] = $this->language->get('entry_name');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_model'] = $this->language->get('column_model');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_total'] = $this->language->get('column_total');

		$data['entry_date_start'] = $this->language->get('entry_date_start');
		$data['entry_date_end'] = $this->language->get('entry_date_end');
		$data['entry_status'] = $this->language->get('entry_status');

$data['button_export'] = $this->language->get('button_export');
			$data['export'] = $this->url->link('report/product_purchased/export', 'token=' . $this->session->data['token'] . $url, 'SSL');
			$data['exportalldatafree_status']=$this->config->get('exportalldatafree_status');
			
$data['button_export'] = $this->language->get('button_export');
			$data['export'] = $this->url->link('report/product_purchased/export', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$data['button_filter'] = $this->language->get('button_filter');

		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('report/product_purchased', 'token=' . $this->session->data['token'] . '&sort=op.name' . $url, 'SSL');
		$data['sort_model'] = $this->url->link('report/product_purchased', 'token=' . $this->session->data['token'] . '&sort=op.model' . $url, 'SSL');
		$data['sort_quantity'] = $this->url->link('report/product_purchased', 'token=' . $this->session->data['token'] . '&sort=quantity' . $url, 'SSL');
		$data['sort_total'] = $this->url->link('report/product_purchased', 'token=' . $this->session->data['token'] . '&sort=total' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['filter_date_start'])) {
			$url .= '&filter_date_start=' . $this->request->get['filter_date_start'];
		}

		if (isset($this->request->get['filter_date_end'])) {
			$url .= '&filter_date_end=' . $this->request->get['filter_date_end'];
		}

		if (isset($this->request->get['filter_order_status_id'])) {
			$url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('report/product_purchased', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($product_total - $this->config->get('config_limit_admin'))) ? $product_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $product_total, ceil($product_total / $this->config->get('config_limit_admin')));

		$data['filter_date_start'] = $filter_date_start;
		$data['filter_date_end'] = $filter_date_end;
		$data['filter_order_status_id'] = $filter_order_status_id;
		$data['filter_name'] = $filter_name;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$data['sort'] = $sort;
		$data['order'] = $order;

		$this->response->setOutput($this->load->view('report/product_purchased.tpl', $data));
	}
}