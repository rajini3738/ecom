<?php
class ModelCheckoutOrder extends Model {
	public function addOrder($data, $is_admin_order="N") {
		$this->event->trigger('pre.order.add', $data);

		$this->db->query("INSERT INTO `" . DB_PREFIX . "order` SET invoice_prefix = '" . $this->db->escape($data['invoice_prefix']) . "', is_admin_order = '" . $is_admin_order . "', store_id = '" . (int)$data['store_id'] . "', store_name = '" . $this->db->escape($data['store_name']) . "', store_url = '" . $this->db->escape($data['store_url']) . "', customer_id = '" . (int)$data['customer_id'] . "', customer_group_id = '" . (int)$data['customer_group_id'] . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? serialize($data['custom_field']) : '') . "', payment_firstname = '" . $this->db->escape($data['payment_firstname']) . "', payment_lastname = '" . $this->db->escape($data['payment_lastname']) . "', payment_company = '" . $this->db->escape($data['payment_company']) . "', payment_address_1 = '" . $this->db->escape($data['payment_address_1']) . "', payment_address_2 = '" . $this->db->escape($data['payment_address_2']) . "', payment_city = '" . $this->db->escape($data['payment_city']) . "', payment_postcode = '" . $this->db->escape($data['payment_postcode']) . "', payment_country = '" . $this->db->escape($data['payment_country']) . "', payment_country_id = '" . (int)$data['payment_country_id'] . "', payment_zone = '" . $this->db->escape($data['payment_zone']) . "', payment_zone_id = '" . (int)$data['payment_zone_id'] . "', payment_address_format = '" . $this->db->escape($data['payment_address_format']) . "', payment_custom_field = '" . $this->db->escape(isset($data['payment_custom_field']) ? serialize($data['payment_custom_field']) : '') . "', payment_method = '" . $this->db->escape($data['payment_method']) . "', payment_code = '" . $this->db->escape($data['payment_code']) . "', shipping_firstname = '" . $this->db->escape($data['shipping_firstname']) . "', shipping_lastname = '" . $this->db->escape($data['shipping_lastname']) . "', shipping_company = '" . $this->db->escape($data['shipping_company']) . "', shipping_address_1 = '" . $this->db->escape($data['shipping_address_1']) . "', shipping_address_2 = '" . $this->db->escape($data['shipping_address_2']) . "', shipping_city = '" . $this->db->escape($data['shipping_city']) . "', shipping_postcode = '" . $this->db->escape($data['shipping_postcode']) . "', shipping_telephone = '" . $this->db->escape($data['shipping_telephone']) . "', shipping_country = '" . $this->db->escape($data['shipping_country']) . "', shipping_country_id = '" . (int)$data['shipping_country_id'] . "', shipping_zone = '" . $this->db->escape($data['shipping_zone']) . "', shipping_zone_id = '" . (int)$data['shipping_zone_id'] . "', shipping_address_format = '" . $this->db->escape($data['shipping_address_format']) . "', shipping_custom_field = '" . $this->db->escape(isset($data['shipping_custom_field']) ? serialize($data['shipping_custom_field']) : '') . "', shipping_method = '" . $this->db->escape($data['shipping_method']) . "', shipping_code = '" . $this->db->escape($data['shipping_code']) . "', comment = '" . $this->db->escape($data['comment']) . "', total = '" . (float)$data['total'] . "', affiliate_id = '" . (int)$data['affiliate_id'] . "', commission = '" . (float)$data['commission'] . "', marketing_id = '" . (int)$data['marketing_id'] . "', tracking = '" . $this->db->escape($data['tracking']) . "', language_id = '" . (int)$data['language_id'] . "', currency_id = '" . (int)$data['currency_id'] . "', currency_code = '" . $this->db->escape($data['currency_code']) . "', currency_value = '" . (float)$data['currency_value'] . "', ip = '" . $this->db->escape($data['ip']) . "', forwarded_ip = '" .  $this->db->escape($data['forwarded_ip']) . "', user_agent = '" . $this->db->escape($data['user_agent']) . "', accept_language = '" . $this->db->escape($data['accept_language']) . "', date_added = NOW(), date_modified = NOW()");

		$order_id = $this->db->getLastId();

		// updating invoice number

		$invoice = date('ymd');
		$query2 = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order` order by order_id desc limit 2");

		$date_added1 = date('ymd',strtotime($query2->rows[1]['date_added']));

		if(strtotime($invoice) == strtotime($date_added1))
			$invoiceno = ((int)$query2->rows[1]['invoice_no'])+1;
		else
			$invoiceno='1';
		if(strlen($invoiceno)==1)
			$invoice = $invoice.'00';
		else if(strlen($invoiceno)==2)
			$invoice = $invoice.'0';

		$customerquery = $this->db->query("SELECT file_number FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$data['customer_id'] . "'");
		$file_number = $customerquery->row['file_number'];

		$this->db->query("UPDATE `" . DB_PREFIX . "order` SET invoice_prefix = '" . $invoice . "', invoice_no = '" . $invoiceno . "', file_number = '" . $file_number . "' WHERE order_id = '" . (int)$order_id . "'");

		// Products
        $is_product_discount =false;
		if (isset($data['products'])) {
			foreach ($data['products'] as $product) {
                if(!$is_product_discount)
                    $is_product_discount = $this->getIsProductDiscount($product['product_id']);
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_product SET order_id = '" . (int)$order_id . "', product_id = '" . (int)$product['product_id'] . "', name = '" . $this->db->escape($product['name']) . "', model = '" . $this->db->escape($product['model']) . "', quantity = '" . (int)$product['quantity'] . "', price = '" . (float)$product['price'] . "', total = '" . (float)$product['total'] . "', discount = '" . (float)$product['discount'] . "', unit_price = '" . (float)$product['unit_price'] . "', intial_total = '" . (float)$product['intial_total'] . "', tax_value = '" . (float)$product['tax'] . "', total_tax_value = '" . (float)$product['total_tax'] . "', reward = '" . (int)$product['reward'] . "', weight = '" . (int)$product['weight'] . "'");

				$order_product_id = $this->db->getLastId();

				foreach ($product['option'] as $option) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "order_option SET order_id = '" . (int)$order_id . "', order_product_id = '" . (int)$order_product_id . "', product_option_id = '" . (int)$option['product_option_id'] . "', product_option_value_id = '" . (int)$option['product_option_value_id'] . "', name = '" . $this->db->escape($option['name']) . "', `value` = '" . $this->db->escape($option['value']) . "', `type` = '" . $this->db->escape($option['type']) . "'");
				}
			}
		}

		// Gift Voucher
		$this->load->model('checkout/voucher');

		// Vouchers
		if (isset($data['vouchers'])) {
			foreach ($data['vouchers'] as $voucher) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_voucher SET order_id = '" . (int)$order_id . "', description = '" . $this->db->escape($voucher['description']) . "', code = '" . $this->db->escape($voucher['code']) . "', from_name = '" . $this->db->escape($voucher['from_name']) . "', from_email = '" . $this->db->escape($voucher['from_email']) . "', to_name = '" . $this->db->escape($voucher['to_name']) . "', to_email = '" . $this->db->escape($voucher['to_email']) . "', voucher_theme_id = '" . (int)$voucher['voucher_theme_id'] . "', message = '" . $this->db->escape($voucher['message']) . "', amount = '" . (float)$voucher['amount'] . "'");

				$order_voucher_id = $this->db->getLastId();

				$voucher_id = $this->model_checkout_voucher->addVoucher($order_id, $voucher);

				$this->db->query("UPDATE " . DB_PREFIX . "order_voucher SET voucher_id = '" . (int)$voucher_id . "' WHERE order_voucher_id = '" . (int)$order_voucher_id . "'");
			}
		}

		// Totals
		if (isset($data['totals'])) {
			foreach ($data['totals'] as $total) {
                if($is_product_discount){
                    if($total['code']!="total_customer_group_discount"){
                        $this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_id = '" . (int)$order_id . "', code = '" . $this->db->escape($total['code']) . "', title = '" . $this->db->escape($total['title']) . "', `value` = '" . (float)$total['value'] . "', sort_order = '" . (int)$total['sort_order'] . "'");
                    }
                }
                else
                    $this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_id = '" . (int)$order_id . "', code = '" . $this->db->escape($total['code']) . "', title = '" . $this->db->escape($total['title']) . "', `value` = '" . (float)$total['value'] . "', sort_order = '" . (int)$total['sort_order'] . "'");
			}
		}

		$this->event->trigger('post.order.add', $order_id);

		return $order_id;
	}

	public function editOrder($order_id, $data) {
		$this->event->trigger('pre.order.edit', $data);

		// Void the order first
		$this->addOrderHistory($order_id, 0);

		// updating invoice number
		//$invoice = date('ymd');

		$this->db->query("UPDATE `" . DB_PREFIX . "order` SET invoice_prefix = '" . $invoice . "', store_id = '" . (int)$data['store_id'] . "', store_name = '" . $this->db->escape($data['store_name']) . "', store_url = '" . $this->db->escape($data['store_url']) . "', customer_id = '" . (int)$data['customer_id'] . "', customer_group_id = '" . (int)$data['customer_group_id'] . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', custom_field = '" . $this->db->escape(serialize($data['custom_field'])) . "', payment_firstname = '" . $this->db->escape($data['payment_firstname']) . "', payment_lastname = '" . $this->db->escape($data['payment_lastname']) . "', payment_company = '" . $this->db->escape($data['payment_company']) . "', payment_address_1 = '" . $this->db->escape($data['payment_address_1']) . "', payment_address_2 = '" . $this->db->escape($data['payment_address_2']) . "', payment_city = '" . $this->db->escape($data['payment_city']) . "', payment_postcode = '" . $this->db->escape($data['payment_postcode']) . "', payment_country = '" . $this->db->escape($data['payment_country']) . "', payment_country_id = '" . (int)$data['payment_country_id'] . "', payment_zone = '" . $this->db->escape($data['payment_zone']) . "', payment_zone_id = '" . (int)$data['payment_zone_id'] . "', payment_address_format = '" . $this->db->escape($data['payment_address_format']) . "', payment_custom_field = '" . $this->db->escape(serialize($data['payment_custom_field'])) . "', payment_method = '" . $this->db->escape($data['payment_method']) . "', payment_code = '" . $this->db->escape($data['payment_code']) . "', shipping_firstname = '" . $this->db->escape($data['shipping_firstname']) . "', shipping_lastname = '" . $this->db->escape($data['shipping_lastname']) . "', shipping_company = '" . $this->db->escape($data['shipping_company']) . "', shipping_address_1 = '" . $this->db->escape($data['shipping_address_1']) . "', shipping_address_2 = '" . $this->db->escape($data['shipping_address_2']) . "', shipping_city = '" . $this->db->escape($data['shipping_city']) . "', shipping_postcode = '" . $this->db->escape($data['shipping_postcode']) . "', shipping_telephone = '" . $this->db->escape($data['shipping_telephone']) . "', shipping_country = '" . $this->db->escape($data['shipping_country']) . "', shipping_country_id = '" . (int)$data['shipping_country_id'] . "', shipping_zone = '" . $this->db->escape($data['shipping_zone']) . "', shipping_zone_id = '" . (int)$data['shipping_zone_id'] . "', shipping_address_format = '" . $this->db->escape($data['shipping_address_format']) . "', shipping_custom_field = '" . $this->db->escape(serialize($data['shipping_custom_field'])) . "', shipping_method = '" . $this->db->escape($data['shipping_method']) . "', shipping_code = '" . $this->db->escape($data['shipping_code']) . "', comment = '" . $this->db->escape($data['comment']) . "', total = '" . (float)$data['total'] . "', affiliate_id = '" . (int)$data['affiliate_id'] . "', commission = '" . (float)$data['commission'] . "', date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "'");

		// Products
        $is_product_discount =false;
		if (isset($data['products'])) {
			foreach ($data['products'] as $product) {
                if(!$is_product_discount)
                    $is_product_discount = $this->getIsProductDiscount($product['product_id']);

				$this->db->query("INSERT INTO " . DB_PREFIX . "order_product SET order_id = '" . (int)$order_id . "', product_id = '" . (int)$product['product_id'] . "', name = '" . $this->db->escape($product['name']) . "', model = '" . $this->db->escape($product['model']) . "', quantity = '" . (int)$product['quantity'] . "', price = '" . (float)$product['price'] . "', total = '" . (float)$product['total'] . "', tax = '" . (float)$product['tax'] . "', tax = '" . (float)$product['tax'] . "', reward = '" . (int)$product['reward'] . "', weight = '" . (int)$product['weight'] . "'");

				$order_product_id = $this->db->getLastId();

				foreach ($product['option'] as $option) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "order_option SET order_id = '" . (int)$order_id . "', order_product_id = '" . (int)$order_product_id . "', product_option_id = '" . (int)$option['product_option_id'] . "', product_option_value_id = '" . (int)$option['product_option_value_id'] . "', name = '" . $this->db->escape($option['name']) . "', `value` = '" . $this->db->escape($option['value']) . "', `type` = '" . $this->db->escape($option['type']) . "'");
				}
			}
		}

		// Gift Voucher
		$this->load->model('checkout/voucher');

		$this->model_checkout_voucher->disableVoucher($order_id);

		// Vouchers
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");

		if (isset($data['vouchers'])) {
			foreach ($data['vouchers'] as $voucher) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_voucher SET order_id = '" . (int)$order_id . "', description = '" . $this->db->escape($voucher['description']) . "', code = '" . $this->db->escape($voucher['code']) . "', from_name = '" . $this->db->escape($voucher['from_name']) . "', from_email = '" . $this->db->escape($voucher['from_email']) . "', to_name = '" . $this->db->escape($voucher['to_name']) . "', to_email = '" . $this->db->escape($voucher['to_email']) . "', voucher_theme_id = '" . (int)$voucher['voucher_theme_id'] . "', message = '" . $this->db->escape($voucher['message']) . "', amount = '" . (float)$voucher['amount'] . "'");

				$order_voucher_id = $this->db->getLastId();

				$voucher_id = $this->model_checkout_voucher->addVoucher($order_id, $voucher);

				$this->db->query("UPDATE " . DB_PREFIX . "order_voucher SET voucher_id = '" . (int)$voucher_id . "' WHERE order_voucher_id = '" . (int)$order_voucher_id . "'");
			}
		}

		// Totals
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int)$order_id . "'");

		if (isset($data['totals'])) {
			foreach ($data['totals'] as $total) {
                if($is_product_discount){
                    if($total['code']!="total_customer_group_discount"){
                        $this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_id = '" . (int)$order_id . "', code = '" . $this->db->escape($total['code']) . "', title = '" . $this->db->escape($total['title']) . "', `value` = '" . (float)$total['value'] . "', sort_order = '" . (int)$total['sort_order'] . "'");
                    }
                }
                else
                    $this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_id = '" . (int)$order_id . "', code = '" . $this->db->escape($total['code']) . "', title = '" . $this->db->escape($total['title']) . "', `value` = '" . (float)$total['value'] . "', sort_order = '" . (int)$total['sort_order'] . "'");
            }
		}

		$this->event->trigger('post.order.edit', $order_id);
	}

	public function deleteOrder($order_id) {
		$this->event->trigger('pre.orselder.delete', $order_id);

		// Void the order first
		$this->addOrderHistory($order_id, 0);

		$this->db->query("DELETE FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "order_product` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "order_option` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "order_voucher` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "order_history` WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("DELETE `or`, ort FROM `" . DB_PREFIX . "order_recurring` `or`, `" . DB_PREFIX . "order_recurring_transaction` `ort` WHERE order_id = '" . (int)$order_id . "' AND ort.order_recurring_id = `or`.order_recurring_id");
		$this->db->query("DELETE FROM `" . DB_PREFIX . "affiliate_transaction` WHERE order_id = '" . (int)$order_id . "'");

		// Gift Voucher
		$this->load->model('checkout/voucher');

		$this->model_checkout_voucher->disableVoucher($order_id);

		$this->event->trigger('post.order.delete', $order_id);
	}

	public function getOrder($order_id) {
		$order_query = $this->db->query("SELECT *, (SELECT os.name FROM `" . DB_PREFIX . "order_status` os WHERE os.order_status_id = o.order_status_id AND os.language_id = o.language_id) AS order_status FROM `" . DB_PREFIX . "order` o WHERE o.order_id = '" . (int)$order_id . "'");

		if ($order_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['payment_country_id'] . "'");

			if ($country_query->num_rows) {
				$payment_iso_code_2 = $country_query->row['iso_code_2'];
				$payment_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$payment_iso_code_2 = '';
				$payment_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['payment_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$payment_zone_code = $zone_query->row['code'];
			} else {
				$payment_zone_code = '';
			}

			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

			if ($country_query->num_rows) {
				$shipping_iso_code_2 = $country_query->row['iso_code_2'];
				$shipping_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$shipping_iso_code_2 = '';
				$shipping_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$shipping_zone_code = $zone_query->row['code'];
			} else {
				$shipping_zone_code = '';
			}

			$this->load->model('localisation/language');

			$language_info = $this->model_localisation_language->getLanguage($order_query->row['language_id']);

			if ($language_info) {
				$language_code = $language_info['code'];
				$language_directory = $language_info['directory'];
			} else {
				$language_code = '';
				$language_directory = '';
			}

			return array(
				'order_id'                => $order_query->row['order_id'],
				'invoice_no'              => $order_query->row['invoice_no'],
				'invoice_prefix'          => $order_query->row['invoice_prefix'],
				'store_id'                => $order_query->row['store_id'],
				'store_name'              => $order_query->row['store_name'],
				'store_url'               => $order_query->row['store_url'],
				'customer_id'             => $order_query->row['customer_id'],
				'customer_group_id'       => $order_query->row['customer_group_id'],
				'firstname'               => $order_query->row['firstname'],
				'lastname'                => $order_query->row['lastname'],
				'email'                   => $order_query->row['email'],
				'telephone'               => $order_query->row['telephone'],
				'fax'                     => $order_query->row['fax'],
				'custom_field'            => unserialize($order_query->row['custom_field']),
				'payment_firstname'       => $order_query->row['payment_firstname'],
				'payment_lastname'        => $order_query->row['payment_lastname'],
				'payment_company'         => $order_query->row['payment_company'],
				'payment_address_1'       => $order_query->row['payment_address_1'],
				'payment_address_2'       => $order_query->row['payment_address_2'],
				'payment_postcode'        => $order_query->row['payment_postcode'],
				'payment_city'            => $order_query->row['payment_city'],
				'payment_zone_id'         => $order_query->row['payment_zone_id'],
				'payment_zone'            => $order_query->row['payment_zone'],
				'payment_zone_code'       => $payment_zone_code,
				'payment_country_id'      => $order_query->row['payment_country_id'],
				'payment_country'         => $order_query->row['payment_country'],
				'payment_iso_code_2'      => $payment_iso_code_2,
				'payment_iso_code_3'      => $payment_iso_code_3,
				'payment_address_format'  => $order_query->row['payment_address_format'],
				'payment_custom_field'    => unserialize($order_query->row['payment_custom_field']),
				'payment_method'          => $order_query->row['payment_method'],
				'payment_code'            => $order_query->row['payment_code'],
				'shipping_firstname'      => $order_query->row['shipping_firstname'],
				'shipping_lastname'       => $order_query->row['shipping_lastname'],
				'shipping_company'        => $order_query->row['shipping_company'],
				'shipping_address_1'      => $order_query->row['shipping_address_1'],
				'shipping_address_2'      => $order_query->row['shipping_address_2'],
				'shipping_postcode'       => $order_query->row['shipping_postcode'],
				'shipping_telephone'      => $order_query->row['shipping_telephone'],
				'shipping_city'           => $order_query->row['shipping_city'],
				'shipping_zone_id'        => $order_query->row['shipping_zone_id'],
				'shipping_zone'           => $order_query->row['shipping_zone'],
				'shipping_zone_code'      => $shipping_zone_code,
				'shipping_country_id'     => $order_query->row['shipping_country_id'],
				'shipping_country'        => $order_query->row['shipping_country'],
				'shipping_iso_code_2'     => $shipping_iso_code_2,
				'shipping_iso_code_3'     => $shipping_iso_code_3,
				'shipping_address_format' => $order_query->row['shipping_address_format'],
				'shipping_custom_field'   => unserialize($order_query->row['shipping_custom_field']),
				'shipping_method'         => $order_query->row['shipping_method'],
				'shipping_code'           => $order_query->row['shipping_code'],
				'comment'                 => $order_query->row['comment'],
				'total'                   => $order_query->row['total'],
				'order_status_id'         => $order_query->row['order_status_id'],
				'order_status'            => $order_query->row['order_status'],
				'affiliate_id'            => $order_query->row['affiliate_id'],
				'commission'              => $order_query->row['commission'],
				'language_id'             => $order_query->row['language_id'],
				'language_code'           => $language_code,
				'language_directory'      => $language_directory,
				'currency_id'             => $order_query->row['currency_id'],
				'currency_code'           => $order_query->row['currency_code'],
				'currency_value'          => $order_query->row['currency_value'],
				'ip'                      => $order_query->row['ip'],
				'forwarded_ip'            => $order_query->row['forwarded_ip'],
				'user_agent'              => $order_query->row['user_agent'],
				'accept_language'         => $order_query->row['accept_language'],
				'date_modified'           => $order_query->row['date_modified'],
				'date_added'              => $order_query->row['date_added'],
                'is_admin_order'          => $order_query->row['is_admin_order'],
                'platform'                => $order_query->row['platform']
			);
		} else {
			return false;
		}
	}

	public function addOrderHistory($order_id, $order_status_id, $comment = '', $notify = false, $detect_quantity = false, $notifyhistory=false) {
		$this->event->trigger('pre.order.history.add', $order_id);

		$order_info = $this->getOrder($order_id);

		if ($order_info) {
			// Fraud Detection
			$this->load->model('account/customer');

			$customer_info = $this->model_account_customer->getCustomer($order_info['customer_id']);

			if ($customer_info && $customer_info['safe']) {
				$safe = true;
			} else {
				$safe = false;
			}

			if (!$safe) {
				// Ban IP
				$status = false;

				if ($order_info['customer_id']) {
					$results = $this->model_account_customer->getIps($order_info['customer_id']);

					foreach ($results as $result) {
						if ($this->model_account_customer->isBanIp($result['ip'])) {
							$status = true;

							break;
						}
					}
				} else {
					$status = $this->model_account_customer->isBanIp($order_info['ip']);
				}

				if ($status) {
					$order_status_id = $this->config->get('config_order_status_id');
				}

				// Anti-Fraud
				$this->load->model('extension/extension');

				$extensions = $this->model_extension_extension->getExtensions('fraud');

				foreach ($extensions as $extension) {
					if ($this->config->get($extension['code'] . '_status')) {
						$this->load->model('fraud/' . $extension['code']);

						$fraud_status_id = $this->{'model_fraud_' . $extension['code']}->check($order_info);

						if ($fraud_status_id) {
							$order_status_id = $fraud_status_id;
						}
					}
				}
			}

			$this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '" . (int)$order_status_id . "', date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");

			$this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET order_id = '" . (int)$order_id . "', order_status_id = '" . (int)$order_status_id . "', notify = '" . (int)$notify . "', comment = '" . $this->db->escape($comment) . "', date_added = NOW()");
			// If current order status is not processing or complete but new status is processing or complete then commence completing the order
			if($detect_quantity){
				// Stock subtraction
				$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

				foreach ($order_product_query->rows as $order_product) {

					$selectedProduct = $this->db->query("SELECT * FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$order_product['product_id'] . "'");

					if($selectedProduct->row['quantity'] > 0 && $selectedProduct->row['subtract'] != 0 ){

						$total_quantity = ((int)$order_product['quantity']) * ((int)$order_product['weight'] == 0 ? 1 : (int)$order_product['weight']);

						$this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = (quantity - " . $total_quantity . ") WHERE product_id = '" . (int)$order_product['product_id'] . "' AND subtract = '1'");
						
						$this->db->query("UPDATE " . DB_PREFIX . "stock_manager SET quantity = (quantity - " . $total_quantity . ") WHERE product_id = '" . (int)$order_product['product_id'] . "'");

						$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$order_product['order_product_id'] . "'");

						foreach ($order_option_query->rows as $option) {
							//if($order_product['weight'] <= 0){
                            $this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity - " . $total_quantity . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'");
							//}
						}
					}
				}
			}
            else if($order_info['is_admin_order']=="Y" && $order_status_id !="0" ) {
				// Stock subtraction
				$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

				foreach ($order_product_query->rows as $order_product) {

					$selectedProduct = $this->db->query("SELECT * FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$order_product['product_id'] . "'");

					if($selectedProduct->row['adminquantity'] > 0 && $selectedProduct->row['subtract'] != 0 ){

						$total_quantity = ((int)$order_product['quantity']) * ((int)$order_product['weight'] == 0 ? 1 : (int)$order_product['weight']);

						$this->db->query("UPDATE " . DB_PREFIX . "product SET adminquantity = (adminquantity - " . $total_quantity . ") WHERE product_id = '" . (int)$order_product['product_id'] . "' AND subtract = '1'");
						
						$this->db->query("UPDATE " . DB_PREFIX . "stock_manager SET adminquantity = (adminquantity - " . $total_quantity . ") WHERE product_id = '" . (int)$order_product['product_id'] . "'");

                        //$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$order_product['order_product_id'] . "'");

                        //foreach ($order_option_query->rows as $option) {
                        //    if($order_product['weight'] <= 0){
                        //        $this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity - " . $total_quantity . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'");
                        //    }
                        //}
					}
				}
			}
			else if (!in_array($order_info['order_status_id'], array_merge([1],$this->config->get('config_processing_status'), $this->config->get('config_complete_status'))) && in_array($order_status_id, array_merge([1],$this->config->get('config_processing_status'), $this->config->get('config_complete_status')))) {
				// Stock subtraction
				$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

				foreach ($order_product_query->rows as $order_product) {

					$selectedProduct = $this->db->query("SELECT * FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$order_product['product_id'] . "'");

					if($selectedProduct->row['quantity'] > 0 && $selectedProduct->row['subtract'] != 0 ){

						$total_quantity = ((int)$order_product['quantity']) * ((int)$order_product['weight'] == 0 ? 1 : (int)$order_product['weight']);

						$this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = (quantity - " . $total_quantity . ") WHERE product_id = '" . (int)$order_product['product_id'] . "' AND subtract = '1'");
						
						$this->db->query("UPDATE " . DB_PREFIX . "stock_manager SET quantity = (quantity - " . $total_quantity . ") WHERE product_id = '" . (int)$order_product['product_id'] . "'");

						$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$order_product['order_product_id'] . "'");

						foreach ($order_option_query->rows as $option) {
                            //if($order_product['weight'] <= 0){
                            $this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity - " . $total_quantity . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'");
                            //}
						}
					}
				}

				// Redeem coupon, vouchers and reward points
				$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order ASC");

				foreach ($order_total_query->rows as $order_total) {

		       if(strpos($order_total['code'],'xfee')!==false) continue;
			
					$this->load->model('total/' . $order_total['code']);

					if (method_exists($this->{'model_total_' . $order_total['code']}, 'confirm')) {
						$this->{'model_total_' . $order_total['code']}->confirm($order_info, $order_total);
					}
				}

				// Add commission if sale is linked to affiliate referral.
				if ($order_info['affiliate_id'] && $this->config->get('config_affiliate_auto')) {
					$this->load->model('affiliate/affiliate');

					$this->model_affiliate_affiliate->addTransaction($order_info['affiliate_id'], $order_info['commission'], $order_id);
				}
			}


			// If old order status is the processing or complete status but new status is not then commence restock, and remove coupon, voucher and reward history
            if($order_info['is_admin_order']=="Y" && $order_status_id =="0" ) {
				// Stock subtraction
				$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

				foreach ($order_product_query->rows as $order_product) {

					$selectedProduct = $this->db->query("SELECT * FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$order_product['product_id'] . "'");

					if($selectedProduct->row['adminquantity'] > 0 && $selectedProduct->row['subtract'] != 0 ){

						$total_quantity = ((int)$order_product['quantity']) * ((int)$order_product['weight'] == 0 ? 1 : (int)$order_product['weight']);

						$this->db->query("UPDATE " . DB_PREFIX . "product SET adminquantity = (adminquantity + " . $total_quantity . ") WHERE product_id = '" . (int)$order_product['product_id'] . "' AND subtract = '1'");
						
						$this->db->query("UPDATE " . DB_PREFIX . "stock_manager SET adminquantity = (adminquantity + " . $total_quantity . ") WHERE product_id = '" . (int)$order_product['product_id'] . "'");

                        //$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$order_product['order_product_id'] . "'");

                        //foreach ($order_option_query->rows as $option) {
                        //    if($order_product['weight'] <= 0){
                        //        $this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity - " . $total_quantity . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'");
                        //    }
                        //}
					}
				}
			}
			else if (in_array($order_info['order_status_id'], array_merge([1],$this->config->get('config_processing_status'), $this->config->get('config_complete_status'))) && !in_array($order_status_id, array_merge([1],$this->config->get('config_processing_status'), $this->config->get('config_complete_status')))) {
				// Restock
				$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

				foreach($product_query->rows as $product) {

					$selectedProduct = $this->db->query("SELECT * FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product['product_id'] . "'");

					if($selectedProduct->row['quantity'] > 0 && $selectedProduct->row['subtract'] != 0 ){

						$total_quantity = ((int)$product['quantity']) * ((int)$product['weight'] == 0 ? 1 : (int)$product['weight']);

						$this->db->query("UPDATE `" . DB_PREFIX . "product` SET quantity = (quantity + " . $total_quantity . ") WHERE product_id = '" . (int)$product['product_id'] . "' AND subtract = '1'");
						
						$this->db->query("UPDATE " . DB_PREFIX . "stock_manager SET quantity = (quantity + " . $total_quantity . ") WHERE product_id = '" . (int)$product['product_id'] . "'");

						$option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");

						foreach ($option_query->rows as $option) {
							//if($product['weight'] <= 0){
                            $this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity + " . $total_quantity . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'");
							//}
						}
					}
				}

				// Remove coupon, vouchers and reward points history
				$this->load->model('account/order');

				$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order ASC");

				foreach ($order_total_query->rows as $order_total) {

		       if(strpos($order_total['code'],'xfee')!==false) continue;
			
					$this->load->model('total/' . $order_total['code']);

					if (method_exists($this->{'model_total_' . $order_total['code']}, 'unconfirm')) {
						$this->{'model_total_' . $order_total['code']}->unconfirm($order_id);
					}
				}

				// Remove commission if sale is linked to affiliate referral.
				if ($order_info['affiliate_id']) {
					$this->load->model('affiliate/affiliate');

					$this->model_affiliate_affiliate->deleteTransaction($order_id);
				}
			}

			$this->cache->delete('product');

			// If order status is 0 then becomes greater than 0 send main html email
			if (!$order_info['order_status_id'] && $order_status_id) {
				// Check for any downloadable products
				$download_status = false;

				$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

				foreach ($order_product_query->rows as $order_product) {
					// Check if there are any linked downloads
					$product_download_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "product_to_download` WHERE product_id = '" . (int)$order_product['product_id'] . "'");

					if ($product_download_query->row['total']) {
						$download_status = true;
					}
				}

				// Load the language for any mails that might be required to be sent out
				$language = new Language($order_info['language_directory']);
				$language->load($order_info['language_directory']);
				$language->load('mail/order');

				$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_status_id . "' AND language_id = '" . (int)$order_info['language_id'] . "'");

				if ($order_status_query->num_rows) {
					$order_status = $order_status_query->row['name'];
				} else {
					$order_status = '';
				}

				$subject = sprintf($language->get('text_new_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_info['invoice_prefix'].$order_info['invoice_no']);

				// HTML Mail
				$data = array();

				$data['title'] = sprintf($language->get('text_new_subject'), $order_info['store_name'], $order_id);

				$data['text_greeting'] = sprintf($language->get('text_new_greeting'), $order_info['store_name']);
                $data['text_greeting1'] = $language->get('text_new_greeting1');
				$data['text_link'] = $language->get('text_new_link');
				$data['text_download'] = $language->get('text_new_download');
				$data['text_order_detail'] = $language->get('text_new_order_detail');
				$data['text_instruction'] = $language->get('text_new_instruction');
				$data['text_order_id'] = $language->get('text_new_order_id');
				$data['text_date_added'] = $language->get('text_new_date_added');
				$data['text_payment_method'] = $language->get('text_new_payment_method');
				$data['text_shipping_method'] = $language->get('text_new_shipping_method');
				$data['text_email'] = $language->get('text_new_email');
				$data['text_telephone'] = $language->get('text_new_telephone');
				$data['text_ip'] = $language->get('text_new_ip');
				$data['text_order_status'] = $language->get('text_new_order_status');
				$data['text_payment_address'] = $language->get('text_new_payment_address');
				$data['text_shipping_address'] = $language->get('text_new_shipping_address');
				$data['text_product'] = $language->get('text_new_product');
				$data['text_model'] = $language->get('text_new_model');
				$data['text_quantity'] = $language->get('text_new_quantity');
				$data['text_price'] = $language->get('text_new_price');
				$data['text_total'] = $language->get('text_new_total');
				$data['text_footer'] = $language->get('text_new_footer');
				$data['text_tax'] = $language->get('text_new_tax');
				$data['text_total_tax'] = $language->get('text_new_total_tax');
				$data['text_discount'] = $language->get('text_new_discount');
				$data['text_intial_total'] = $language->get('text_new_intial_total');

				$data['text_tax_details'] = $language->get('text_tax_details');
				$data['text_tax_number'] = $language->get('text_tax_number');

				$data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');
				$data['store_name'] = $order_info['store_name'];
				$data['store_url'] = $order_info['store_url'];
				$data['customer_id'] = $order_info['customer_id'];
				$data['link'] = $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id;
				$data['account_link'] = $order_info['store_url'] . 'index.php?route=account/login';
				$data['customer_group_name'] = $order_info['customer_group_id'];

                $is_bank_transfer='N';
                if($order_info['payment_code']=="bank_transfer")
                    $is_bank_transfer='Y';
                $data['is_bank_transfer'] = $is_bank_transfer;

				if ($download_status) {
					$data['download'] = $order_info['store_url'] . 'index.php?route=account/download';
				} else {
					$data['download'] = '';
				}

				$data['order_id'] = $order_id;
				$data['order_id1'] = $order_info['invoice_prefix'].$order_info['invoice_no'];
				$data['date_added'] = date($language->get('date_format_short'), strtotime($order_info['date_added']));
				$data['payment_method'] = $order_info['payment_method'];
				$data['shipping_method'] = $order_info['shipping_method'];
				$data['email'] = $order_info['email'];
				$data['telephone'] = $order_info['telephone'];
				$data['ip'] = $order_info['ip'];
				$data['order_status'] = $order_status;

				if ($comment && $notify) {
					$data['comment'] = nl2br($comment);
				} else {
					$data['comment'] = '';
				}

				if ($order_info['payment_address_format']) {
					$format = $order_info['payment_address_format'];
				} else {
					$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
				}

				$find = array(
					'{firstname}',
					'{lastname}',
					'{company}',
					'{address_1}',
					'{address_2}',
					'{city}',
					'{postcode}',
					'{zone}',
					'{zone_code}',
					'{country}'
				);

				$replace = array(
					'firstname' => $order_info['payment_firstname'],
					'lastname'  => $order_info['payment_lastname'],
					'company'   => $order_info['payment_company'],
					'address_1' => $order_info['payment_address_1'],
					'address_2' => $order_info['payment_address_2'],
					'city'      => $order_info['payment_city'],
					'postcode'  => $order_info['payment_postcode'],
					'zone'      => $order_info['payment_zone'],
					'zone_code' => $order_info['payment_zone_code'],
					'country'   => $order_info['payment_country']
				);

				$data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

				if ($order_info['shipping_address_format']) {
					$format = $order_info['shipping_address_format'];
				} else {
					$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
				}

				$find = array(
					'{firstname}',
					'{lastname}',
					'{company}',
					'{address_1}',
					'{address_2}',
					'{city}',
					'{postcode}',
					'{zone}',
					'{zone_code}',
					'{country}'
				);

				$replace = array(
					'firstname' => $order_info['shipping_firstname'],
					'lastname'  => $order_info['shipping_lastname'],
					'company'   => $order_info['shipping_company'],
					'address_1' => $order_info['shipping_address_1'],
					'address_2' => $order_info['shipping_address_2'],
					'city'      => $order_info['shipping_city'],
					'postcode'  => $order_info['shipping_postcode'],
					'telephone' => $order_info['shipping_telephone'],
					'zone'      => $order_info['shipping_zone'],
					'zone_code' => $order_info['shipping_zone_code'],
					'country'   => $order_info['shipping_country']
				);

				$data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

				$this->load->model('tool/upload');

				// Products
				$data['products'] = array();
				$total_intial=0;
				$total_discount=0;
				$total_total=0;
				$total_tax=0;
				$total_grand_total=0;
				foreach ($order_product_query->rows as $product) {
					$option_data = array();

					$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");

					foreach ($order_option_query->rows as $option) {
						if ($option['type'] != 'file') {
							$value = $option['value'];
						} else {
							$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

							if ($upload_info) {
								$value = $upload_info['name'];
							} else {
								$value = '';
							}
						}

						$option_data[] = array(
							'name'  => $option['name'],
							'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
						);
					}
					$total_intial = $total_intial+ $product['intial_total'];
					$total_discount = $total_discount+ $product['discount'];
					$total_total = $total_total+ $product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0);
					$total_tax = $total_tax+ $product['tax_value'];
					$total_grand_total = $total_grand_total+ $product['total_tax_value'];
					$data['products'][] = array(
						'name'			=> $product['name'],
						'model'			=> $product['model'],
						'option'		=> $option_data,
						'quantity'		=> $product['quantity'],
						'price'			=> $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
						'total'			=> $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
						'tax'			=> $this->currency->format($product['tax_value']),
						'total_tax'		=> $this->currency->format($product['total_tax_value']),
						'discount'		=> $product['discount'].'%',
						'unit_price'	=> $this->currency->format($product['unit_price']),
						'intial_total'	=> $this->currency->format($product['intial_total'])
					);
				}

				$data['total_last_datas'][] = array(
						'total_intial'		=> $this->currency->format($total_intial),
						'total_discount'	=> $total_discount.'%',
						'total_total'		=> $this->currency->format($total_total),
						'total_tax'			=> $this->currency->format($total_tax),
						'total_grand_total' => $this->currency->format($total_grand_total),
				);

				// Vouchers
				$data['vouchers'] = array();

				$order_voucher_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");

				foreach ($order_voucher_query->rows as $voucher) {
					$data['vouchers'][] = array(
						'description' => $voucher['description'],
						'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']),
					);
				}

				// Order Totals
				$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order ASC");

				foreach ($order_total_query->rows as $total) {
					$data['totals'][] = array(
						'title' => $total['title'],
						'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
					);
				}

				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/order.tpl')) {
					$html = $this->load->view($this->config->get('config_template') . '/template/mail/order.tpl', $data);
				} else {
					$html = $this->load->view('default/template/mail/order.tpl', $data);
				}

				// Can not send confirmation emails for CBA orders as email is unknown
				$this->load->model('payment/amazon_checkout');

				if (!$this->model_payment_amazon_checkout->isAmazonOrder($order_info['order_id'])) {
					// Text Mail
					$text  = sprintf($language->get('text_new_greeting'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8')) . "\n\n";
					$text .= $language->get('text_new_order_id') . ' ' . $order_id . "\n";
					$text .= $language->get('text_new_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n";
					$text .= $language->get('text_new_order_status') . ' ' . $order_status . "\n\n";

					if ($comment && $notify) {
						$text .= $language->get('text_new_instruction') . "\n\n";
						$text .= $comment . "\n\n";
					}

					// Products
					$text .= $language->get('text_new_products') . "\n";

					foreach ($order_product_query->rows as $product) {
						$text .= $product['quantity'] . 'x ' . $product['name'] . ' (' . $product['model'] . ') ' . html_entity_decode($this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";

						$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . $product['order_product_id'] . "'");

						foreach ($order_option_query->rows as $option) {
							if ($option['type'] != 'file') {
								$value = $option['value'];
							} else {
								$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

								if ($upload_info) {
									$value = $upload_info['name'];
								} else {
									$value = '';
								}
							}

							$text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value) . "\n";
						}
					}

					foreach ($order_voucher_query->rows as $voucher) {
						$text .= '1x ' . $voucher['description'] . ' ' . $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']);
					}

					$text .= "\n";

					$text .= $language->get('text_new_order_total') . "\n";

					foreach ($order_total_query->rows as $total) {
						$text .= $total['title'] . ': ' . html_entity_decode($this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";
					}

					$text .= "\n";

					if ($order_info['customer_id']) {
						$text .= $language->get('text_new_link') . "\n";
						$text .= $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id . "\n\n";
					}

					if ($download_status) {
						$text .= $language->get('text_new_download') . "\n";
						$text .= $order_info['store_url'] . 'index.php?route=account/download' . "\n\n";
					}

					// Comment
					if ($order_info['comment']) {
						$text .= $language->get('text_new_comment') . "\n\n";
						$text .= $order_info['comment'] . "\n\n";
					}

					$text .= $language->get('text_new_footer') . "\n\n";
					

          
			// Send SMS
				$message_order = $this->config->get('jossms_message_order');
				$message_order_toadmin = $this->config->get('jossms_config_alert_sms');
				$storename = $this->config->get('config_name');
				$produk = '';
				
				foreach ($order_product_query->rows as $product) {
					$produk .= $product['quantity'] . 'x ' . $product['name'] . ' (' . $product['model'] . ') ' . html_entity_decode($this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";
					
					$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . $product['order_product_id'] . "'");
					
					foreach ($order_option_query->rows as $option) {
						if ($option['type'] != 'file') {
							$value = $option['value'];
						} else {
							$value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
						}
											
						$produk .= ' -' . $option['name'] . ' ' . (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value) . "\n";
					}
				}
				
				foreach ($order_voucher_query->rows as $voucher) {
					$produk .= '1x ' . $voucher['description'] . ' ' . $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']);
				}
				
				$produk .= "\n";
				$produk .= $language->get('text_new_order_total') . "\n";

				foreach ($order_total_query->rows as $total) {
					$produk .= $total['title'] . ': ' . html_entity_decode($this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";
				}

				if ($order_info['comment']) {
					$produk .= $language->get('text_new_comment') . "\n";
					$produk .= $order_info['comment'];
				}
				
				foreach ($order_total_query->rows as $total) {
					$totalnya = html_entity_decode($this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8');
				}

        $query = $this->db->query("SELECT oh.date_added, os.name AS status, oh.comment, oh.notify FROM " . DB_PREFIX . "order_history oh LEFT JOIN " . DB_PREFIX . "order_status os ON oh.order_status_id = os.order_status_id WHERE oh.comment LIKE '%Order No%' AND oh.order_id = '" . (int)$order_id . "' AND os.language_id = '2' ORDER BY oh.date_added ASC");
			  $shipments = $query->rows;
			  if($shipments)
			  {
						  foreach($shipments as $key=>$comment)
						  {
							  $cmnt_txt = ($comment['comment'])?$comment['comment']:'';
							  if (version_compare(PHP_VERSION, '5.3.0') <= 0) {
								  $awbno = substr($cmnt_txt,0, strpos($cmnt_txt,"- Order No")); 
							  }
							  else{				
								  $awbno = strstr($cmnt_txt,"- Order No",true);
							  }
								  $awb_no=trim($awbno,"AWB No.");					
								  break;
						  }
			  }else{
					  $awb_no= 0;
			  }
        $order_invoice_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order WHERE order_id = '" . (int)$order_id . "'");
        $order_no = $order_invoice_query->row['invoice_prefix'].$order_invoice_query->row['invoice_no'];
        $shipping_code = $order_invoice_query->row['shipping_code'];
        
				if($order_status=="قيد التجهيز"){
          if($shipping_code=="pickup.pickup"){
            $message_order = "أهلاً بك في سكة الطيب، طلبكم رقم ".$order_no." ينتظركم في محلنا بالدمام";
          }
          else{
            $message_order="مرحبا بك في سكة الطيب تم استلام طلبك رقم ".$order_no." سيتم التنفيذ خلال خمس إلى سبعة أيام عمل.";
          }
				}
				else if($order_status=="تمت معالجته" || $order_status=="قيد الاجراء"){
					$message_order = "تم تعميد الطلب رقم ".$order_no." وسيتم الشحن قريبا";
				}
				else if($order_status=="تم الشحن"){
					$message_order = " تم شحن الطلب رقم ".$order_no." عبر ارامكس برقم شحن ".$awb_no." ولتتبع الشحنة قم بزيارة www.aramex.com";
				}
				else if($order_status=="انتهى"){
					$message_order = "تم الغاء طلبك رقم ".$order_no." وذلك بسبب انتهاء فترة السداد";
				}
				else if($order_status=="إبطال ألغاء"){
					$message_order = "تم الغاء طلبك رقم ".$order_no." بسبب التكرار";
				}
				else if($order_status=="تم الإلغاء"){
					$message_order = "تم الغاء طلبك رقم ".$order_no." بناءا علي طلب العميل";
				}
        
					
				if($message_order != ""){
					$this->load->model('libraries/jossms');
					$gateway = $this->config->get('jossms_gateway');
					
					$parsing = array ( 
						'{order_date}',
						'{products_ordered}',
						'{firstname}',
						'{lastname}',
						'{email}',
						'{telephone}',
						'{orderid}',
						'{orderstatus}',
						'{shippingmethod}',
						'{shipping_firstname}',
						'{shipping_lastname}',
						'{shipping_company}',
						'{shipping_address}',
						'{shipping_city}',
						'{shipping_postcode}',
						'{shipping_state}',
						'{shipping_country}',
						'{paymentmethod}',
						'{payment_firstname}',
						'{payment_lastname}',
						'{payment_company}',
						'{payment_address}',
						'{payment_city}',
						'{payment_postcode}',
						'{payment_state}',
						'{payment_country}',
						'{total}',
						'{comment}',
						'{storename}'
					);
					$replace = array (
						date($language->get('date_format_short'), strtotime($order_info['date_added'])),
						$produk,
						$order_info['firstname'],
						$order_info['lastname'],
						$order_info['email'],
						$order_info['telephone'],
						$order_info['order_id'],
						$order_status,
						$order_info['shipping_method'],
						$order_info['shipping_firstname'],
						$order_info['shipping_lastname'],
						$order_info['shipping_company'],
						$order_info['shipping_address_1']." ".$order_info['shipping_address_2'],
						$order_info['shipping_city'],
						$order_info['shipping_postcode'],
						$order_info['shipping_zone'],
						$order_info['shipping_country'],
						$order_info['payment_method'],
						$order_info['payment_firstname'],
						$order_info['payment_lastname'],
						$order_info['payment_company'],
						$order_info['payment_address_1']." ".$order_info['payment_address_2'],
						$order_info['payment_city'],
						$order_info['payment_postcode'],
						$order_info['payment_zone'],
						$order_info['payment_country'],
						$totalnya,
						$order_info['comment'],
						$storename
					);
					
					$pesan = str_replace( $parsing, $replace, $message_order );
					
					$query = $this->db->query("SELECT `iso_code_2` FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$order_info['payment_country_id'] . "'");
					$isoc = $query->row['iso_code_2'];
					$phone_number = $this->model_libraries_jossms->getConvertPhonePrefix($order_info['telephone'],$isoc);
					$getresponse = $this->model_libraries_jossms->send_message($phone_number, $pesan, $gateway);
				}
				
				if ($message_order_toadmin != "") {
					$this->load->model('libraries/jossms');
					$gateway = $this->config->get('jossms_gateway');
					$countryid = $this->config->get('config_country_id');
					
					$parsing = array ( 
						'{order_date}',
						'{products_ordered}',
						'{firstname}',
						'{lastname}',
						'{email}',
						'{telephone}',
						'{orderid}',
						'{orderstatus}',
						'{shippingmethod}',
						'{shipping_firstname}',
						'{shipping_lastname}',
						'{shipping_company}',
						'{shipping_address}',
						'{shipping_city}',
						'{shipping_postcode}',
						'{shipping_state}',
						'{shipping_country}',
						'{paymentmethod}',
						'{payment_firstname}',
						'{payment_lastname}',
						'{payment_company}',
						'{payment_address}',
						'{payment_city}',
						'{payment_postcode}',
						'{payment_state}',
						'{payment_country}',
						'{total}',
						'{comment}',
						'{storename}'
					);
					$replace = array (
						date($language->get('date_format_short'), strtotime($order_info['date_added'])),
						$produk,
						$order_info['firstname'],
						$order_info['lastname'],
						$order_info['email'],
						$order_info['telephone'],
						$order_info['order_id'],
						$order_status,
						$order_info['shipping_method'],
						$order_info['shipping_firstname'],
						$order_info['shipping_lastname'],
						$order_info['shipping_company'],
						$order_info['shipping_address_1']." ".$order_info['shipping_address_2'],
						$order_info['shipping_city'],
						$order_info['shipping_postcode'],
						$order_info['shipping_zone'],
						$order_info['shipping_country'],
						$order_info['payment_method'],
						$order_info['payment_firstname'],
						$order_info['payment_lastname'],
						$order_info['payment_company'],
						$order_info['payment_address_1']." ".$order_info['payment_address_2'],
						$order_info['payment_city'],
						$order_info['payment_postcode'],
						$order_info['payment_zone'],
						$order_info['payment_country'],
						$totalnya,
						$order_info['comment'],
						$storename
					);
					
					$pesan = str_replace( $parsing, $replace, $message_order_toadmin );
					
					$query = $this->db->query("SELECT `iso_code_2` FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$countryid . "'");
					$isoc = $query->row['iso_code_2'];
					$phone_number = $this->model_libraries_jossms->getConvertPhonePrefix($this->config->get('config_telephone'),$isoc);
					$getresponse = $this->model_libraries_jossms->send_message($phone_number, $pesan, $gateway);
					
					$nohps = explode(',', $this->config->get('jossms_message_alert'));
					foreach ($nohps as $nohp) {
						if ($nohp) {
							$phone_number = $this->model_libraries_jossms->getConvertPhonePrefix($nohp,$isoc);
							$getresponse = $this->model_libraries_jossms->send_message($phone_number, $pesan, $gateway);
						}
					}
				}
			
					if($order_info['email']){
						$mail = new Mail();
						$mail->protocol = $this->config->get('config_mail_protocol');
						$mail->parameter = $this->config->get('config_mail_parameter');
						$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
						$mail->smtp_username = $this->config->get('config_mail_smtp_username');
						$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
						$mail->smtp_port = $this->config->get('config_mail_smtp_port');
						$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

						$mail->setTo($order_info['email']);
						$mail->setFrom($this->config->get('config_email'));
						$mail->setSender(html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
						$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
						$mail->setHtml($html);
						$mail->setText($text);
						$mail->send();
					}
				}


			//Competition Auto Entry Start:
			$this->load->model('competition/competition');
			$order_value_query = $this->db->query("SELECT value FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' AND code = 'sub_total'");

			$competition_data = array(
				'value' => $order_value_query->row['value'],
				'email' => $order_info['email'],
				'name'	=> $order_info['payment_firstname'] . ' ' . $order_info['payment_lastname']
			);
			
			$today = strtotime(date("d-m-Y"));
			$competitions = $this->model_competition_competition->getCompetition();

			if ($competitions) {
			  foreach ($competitions as $competition) {
				if ($competition['auto'] && $competition['status'] && (isset($this->session->data['comp_entry'])) &&  (strtotime($this->model_competition_competition->getCompetitionDate($competition['competition_id'])) > $today) && ($order_value_query->row['value'] >= $competition['auto_entry_value'])){
				  $email_check = $this->model_competition_competition->getAutoEntryEmailCheck($order_info['email'], $competition['competition_id']);
				  if (!$email_check) {
						$this->model_competition_competition->setAutoEntry($competition_data, $competition['competition_id']);
				  }
				}
			  }
			}
			//Competition Auto Entry End:
			
				// Admin Alert Mail

								if($this->config->get('aramex_auto_create_shipment')==1 && $this->config->get('aramex_status') ==1){
								$this->load->model('aramex/aramex');
								$this->model_aramex_aramex->create($order_id);
								}
								
				if ($this->config->get('config_order_mail')) {
					$subject = sprintf($language->get('text_new_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'), $order_info['invoice_prefix'].$order_info['invoice_no']);

					// HTML Mail
					$data['text_greeting'] = $language->get('text_new_received');

					if ($comment) {
						if ($order_info['comment']) {
							$data['comment'] = nl2br($comment) . '<br/><br/>' . $order_info['comment'];
						} else {
							$data['comment'] = nl2br($comment);
						}
					} else {
						if ($order_info['comment']) {
							$data['comment'] = $order_info['comment'];
						} else {
							$data['comment'] = '';
						}
					}

					$data['text_download'] = '';

					$data['text_footer'] = '';

					$data['text_link'] = '';
					$data['link'] = '';
					$data['download'] = '';

					if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/order.tpl')) {
						$html = $this->load->view($this->config->get('config_template') . '/template/mail/order.tpl', $data);
					} else {
						$html = $this->load->view('default/template/mail/order.tpl', $data);
					}

					// Text
					$text  = $language->get('text_new_received') . "\n\n";
					$text .= $language->get('text_new_order_id') . ' ' . $order_id . "\n";
					$text .= $language->get('text_new_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n";
					$text .= $language->get('text_new_order_status') . ' ' . $order_status . "\n\n";
					$text .= $language->get('text_new_products') . "\n";

					foreach ($order_product_query->rows as $product) {
						$text .= $product['quantity'] . 'x ' . $product['name'] . ' (' . $product['model'] . ') ' . html_entity_decode($this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";

						$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . $product['order_product_id'] . "'");

						foreach ($order_option_query->rows as $option) {
							if ($option['type'] != 'file') {
								$value = $option['value'];
							} else {
								$value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
							}

							$text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value) . "\n";
						}
					}

					foreach ($order_voucher_query->rows as $voucher) {
						$text .= '1x ' . $voucher['description'] . ' ' . $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']);
					}

					$text .= "\n";

					$text .= $language->get('text_new_order_total') . "\n";

					foreach ($order_total_query->rows as $total) {
						$text .= $total['title'] . ': ' . html_entity_decode($this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";
					}

					$text .= "\n";

					if ($order_info['comment']) {
						$text .= $language->get('text_new_comment') . "\n\n";
						$text .= $order_info['comment'] . "\n\n";
					}

					//echo $html;
					$mail = new Mail();
					$mail->protocol = $this->config->get('config_mail_protocol');
					$mail->parameter = $this->config->get('config_mail_parameter');
					$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
					$mail->smtp_username = $this->config->get('config_mail_smtp_username');
					$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
					$mail->smtp_port = $this->config->get('config_mail_smtp_port');
					$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

					$mail->setTo($this->config->get('config_email'));
					$mail->setFrom($this->config->get('config_email'));
					$mail->setSender(html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
					$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
					$mail->setHtml($html);
					$mail->setText($text);
					$mail->send();

					// Send to additional alert emails
					$emails = explode(',', $this->config->get('config_mail_alert'));

					foreach ($emails as $email) {
						if ($email && preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $email)) {
							$mail->setTo($email);
							$mail->send();
						}
					}
				}
			}

			// If order status is 0 then becomes greater than 0 send main html email
			if ($notifyhistory) {
				// Check for any downloadable products
				$download_status = false;

				$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

				foreach ($order_product_query->rows as $order_product) {
					// Check if there are any linked downloads
					$product_download_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "product_to_download` WHERE product_id = '" . (int)$order_product['product_id'] . "'");

					if ($product_download_query->row['total']) {
						$download_status = true;
					}
				}

				// Load the language for any mails that might be required to be sent out
				$language = new Language($order_info['language_directory']);
				$language->load($order_info['language_directory']);
				$language->load('mail/order');

				$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_status_id . "' AND language_id = '" . (int)$order_info['language_id'] . "'");

				if ($order_status_query->num_rows) {
					$order_status = $order_status_query->row['name'];
				} else {
					$order_status = '';
				}

				$subject = sprintf($language->get('text_new_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_info['invoice_prefix'].$order_info['invoice_no']);

				// HTML Mail
				$data = array();

				$data['title'] = sprintf($language->get('text_new_subject'), $order_info['store_name'], $order_id);

				$data['text_greeting'] = sprintf($language->get('text_new_greeting'), $order_info['store_name']);
                $data['text_greeting1'] = $language->get('text_new_greeting1');
				$data['text_link'] = $language->get('text_new_link');
				$data['text_download'] = $language->get('text_new_download');
				$data['text_order_detail'] = $language->get('text_new_order_detail');
				$data['text_instruction'] = $language->get('text_new_instruction');
				$data['text_order_id'] = $language->get('text_new_order_id');
				$data['text_date_added'] = $language->get('text_new_date_added');
				$data['text_payment_method'] = $language->get('text_new_payment_method');
				$data['text_shipping_method'] = $language->get('text_new_shipping_method');
				$data['text_email'] = $language->get('text_new_email');
				$data['text_telephone'] = $language->get('text_new_telephone');
				$data['text_ip'] = $language->get('text_new_ip');
				$data['text_order_status'] = $language->get('text_new_order_status');
				$data['text_payment_address'] = $language->get('text_new_payment_address');
				$data['text_shipping_address'] = $language->get('text_new_shipping_address');
				$data['text_product'] = $language->get('text_new_product');
				$data['text_model'] = $language->get('text_new_model');
				$data['text_quantity'] = $language->get('text_new_quantity');
				$data['text_price'] = $language->get('text_new_price');
				$data['text_total'] = $language->get('text_new_total');
				$data['text_footer'] = $language->get('text_new_footer');
				$data['text_tax'] = $language->get('text_new_tax');
				$data['text_total_tax'] = $language->get('text_new_total_tax');
				$data['text_discount'] = $language->get('text_new_discount');
				$data['text_intial_total'] = $language->get('text_new_intial_total');

				$data['text_tax_details'] = $language->get('text_tax_details');
				$data['text_tax_number'] = $language->get('text_tax_number');

				$data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');
				$data['store_name'] = $order_info['store_name'];
				$data['store_url'] = $order_info['store_url'];
				$data['customer_id'] = $order_info['customer_id'];
				$data['link'] = $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id;
				$data['account_link'] = $order_info['store_url'] . 'index.php?route=account/login';
				$data['customer_group_name'] = $order_info['customer_group_id'];

                $is_bank_transfer='N';
                if($order_info['payment_code']=="bank_transfer")
                    $is_bank_transfer='Y';
                $data['is_bank_transfer'] = $is_bank_transfer;

				if ($download_status) {
					$data['download'] = $order_info['store_url'] . 'index.php?route=account/download';
				} else {
					$data['download'] = '';
				}

				$data['order_id'] = $order_id;
				$data['order_id1'] = $order_info['invoice_prefix'].$order_info['invoice_no'];
				$data['date_added'] = date($language->get('date_format_short'), strtotime($order_info['date_added']));
				$data['payment_method'] = $order_info['payment_method'];
				$data['shipping_method'] = $order_info['shipping_method'];
				$data['email'] = $order_info['email'];
				$data['telephone'] = $order_info['telephone'];
				$data['ip'] = $order_info['ip'];
				$data['order_status'] = $order_status;

				if ($comment && $notify) {
					$data['comment'] = nl2br($comment);
				} else {
					$data['comment'] = '';
				}

				if ($order_info['payment_address_format']) {
					$format = $order_info['payment_address_format'];
				} else {
					$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
				}

				$find = array(
					'{firstname}',
					'{lastname}',
					'{company}',
					'{address_1}',
					'{address_2}',
					'{city}',
					'{postcode}',
					'{zone}',
					'{zone_code}',
					'{country}'
				);

				$replace = array(
					'firstname' => $order_info['payment_firstname'],
					'lastname'  => $order_info['payment_lastname'],
					'company'   => $order_info['payment_company'],
					'address_1' => $order_info['payment_address_1'],
					'address_2' => $order_info['payment_address_2'],
					'city'      => $order_info['payment_city'],
					'postcode'  => $order_info['payment_postcode'],
					'zone'      => $order_info['payment_zone'],
					'zone_code' => $order_info['payment_zone_code'],
					'country'   => $order_info['payment_country']
				);

				$data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

				if ($order_info['shipping_address_format']) {
					$format = $order_info['shipping_address_format'];
				} else {
					$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
				}

				$find = array(
					'{firstname}',
					'{lastname}',
					'{company}',
					'{address_1}',
					'{address_2}',
					'{city}',
					'{postcode}',
					'{telephone}',
					'{zone}',
					'{zone_code}',
					'{country}'
				);

				$replace = array(
					'firstname' => $order_info['shipping_firstname'],
					'lastname'  => $order_info['shipping_lastname'],
					'company'   => $order_info['shipping_company'],
					'address_1' => $order_info['shipping_address_1'],
					'address_2' => $order_info['shipping_address_2'],
					'city'      => $order_info['shipping_city'],
					'postcode'  => $order_info['shipping_postcode'],
					'telephone' => $order_info['shipping_telephone'],
					'zone'      => $order_info['shipping_zone'],
					'zone_code' => $order_info['shipping_zone_code'],
					'country'   => $order_info['shipping_country']
				);

				$data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

				$this->load->model('tool/upload');

				// Products
				$data['products'] = array();
				$total_intial=0;
				$total_discount=0;
				$total_total=0;
				$total_tax=0;
				$total_grand_total=0;

				foreach ($order_product_query->rows as $product) {
					$option_data = array();

					$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");

					foreach ($order_option_query->rows as $option) {
						if ($option['type'] != 'file') {
							$value = $option['value'];
						} else {
							$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

							if ($upload_info) {
								$value = $upload_info['name'];
							} else {
								$value = '';
							}
						}

						$option_data[] = array(
							'name'  => $option['name'],
							'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
						);
					}
					$total_intial = $total_intial+ $product['intial_total'];
					$total_discount = $total_discount+ $product['discount'];
					$total_total = $total_total+ $product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0);
					$total_tax = $total_tax+ $product['tax_value'];
					$total_grand_total = $total_grand_total+ $product['total_tax_value'];
					$data['products'][] = array(
						'name'			=> $product['name'],
						'model'			=> $product['model'],
						'option'		=> $option_data,
						'quantity'		=> $product['quantity'],
						'price'			=> $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
						'total'			=> $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
						'tax'			=> $this->currency->format($product['tax_value']),
						'total_tax'		=> $this->currency->format($product['total_tax_value']),
						'discount'		=> $product['discount'].'%',
						'unit_price'	=> $this->currency->format($product['unit_price']),
						'intial_total'	=> $this->currency->format($product['intial_total'])
					);
				}
				$data['total_last_datas'][] = array(
						'total_intial'		=> $this->currency->format($total_intial),
						'total_discount'	=> $total_discount.'%',
						'total_total'		=> $this->currency->format($total_total),
						'total_tax'			=> $this->currency->format($total_tax),
						'total_grand_total' => $this->currency->format($total_grand_total),
				);

				// Vouchers
				$data['vouchers'] = array();

				$order_voucher_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");

				foreach ($order_voucher_query->rows as $voucher) {
					$data['vouchers'][] = array(
						'description' => $voucher['description'],
						'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']),
					);
				}

				// Order Totals
				$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order ASC");

				foreach ($order_total_query->rows as $total) {
					$data['totals'][] = array(
						'title' => $total['title'],
						'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
					);
				}

				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/order.tpl')) {
					$html = $this->load->view($this->config->get('config_template') . '/template/mail/order.tpl', $data);
				} else {
					$html = $this->load->view('default/template/mail/order.tpl', $data);
				}

				// Can not send confirmation emails for CBA orders as email is unknown
				$this->load->model('payment/amazon_checkout');

				if (!$this->model_payment_amazon_checkout->isAmazonOrder($order_info['order_id'])) {
					// Text Mail
					$text  = sprintf($language->get('text_new_greeting'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8')) . "\n\n";
					$text .= $language->get('text_new_order_id') . ' ' . $order_id . "\n";
					$text .= $language->get('text_new_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n";
					$text .= $language->get('text_new_order_status') . ' ' . $order_status . "\n\n";

					if ($comment && $notify) {
						$text .= $language->get('text_new_instruction') . "\n\n";
						$text .= $comment . "\n\n";
					}

					// Products
					$text .= $language->get('text_new_products') . "\n";

					foreach ($order_product_query->rows as $product) {
						$text .= $product['quantity'] . 'x ' . $product['name'] . ' (' . $product['model'] . ') ' . html_entity_decode($this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";

						$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . $product['order_product_id'] . "'");

						foreach ($order_option_query->rows as $option) {
							if ($option['type'] != 'file') {
								$value = $option['value'];
							} else {
								$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

								if ($upload_info) {
									$value = $upload_info['name'];
								} else {
									$value = '';
								}
							}

							$text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value) . "\n";
						}
					}

					foreach ($order_voucher_query->rows as $voucher) {
						$text .= '1x ' . $voucher['description'] . ' ' . $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']);
					}

					$text .= "\n";

					$text .= $language->get('text_new_order_total') . "\n";

					foreach ($order_total_query->rows as $total) {
						$text .= $total['title'] . ': ' . html_entity_decode($this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";
					}

					$text .= "\n";

					if ($order_info['customer_id']) {
						$text .= $language->get('text_new_link') . "\n";
						$text .= $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id . "\n\n";
					}

					if ($download_status) {
						$text .= $language->get('text_new_download') . "\n";
						$text .= $order_info['store_url'] . 'index.php?route=account/download' . "\n\n";
					}

					// Comment
					if ($order_info['comment']) {
						$text .= $language->get('text_new_comment') . "\n\n";
						$text .= $order_info['comment'] . "\n\n";
					}

					$text .= $language->get('text_new_footer') . "\n\n";
					if($order_info['email']){

          
			// Send SMS
				$message_order = $this->config->get('jossms_message_order');
				$message_order_toadmin = $this->config->get('jossms_config_alert_sms');
				$storename = $this->config->get('config_name');
				$produk = '';
				
				foreach ($order_product_query->rows as $product) {
					$produk .= $product['quantity'] . 'x ' . $product['name'] . ' (' . $product['model'] . ') ' . html_entity_decode($this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";
					
					$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . $product['order_product_id'] . "'");
					
					foreach ($order_option_query->rows as $option) {
						if ($option['type'] != 'file') {
							$value = $option['value'];
						} else {
							$value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
						}
											
						$produk .= ' -' . $option['name'] . ' ' . (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value) . "\n";
					}
				}
				
				foreach ($order_voucher_query->rows as $voucher) {
					$produk .= '1x ' . $voucher['description'] . ' ' . $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']);
				}
				
				$produk .= "\n";
				$produk .= $language->get('text_new_order_total') . "\n";

				foreach ($order_total_query->rows as $total) {
					$produk .= $total['title'] . ': ' . html_entity_decode($this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";
				}

				if ($order_info['comment']) {
					$produk .= $language->get('text_new_comment') . "\n";
					$produk .= $order_info['comment'];
				}
				
				foreach ($order_total_query->rows as $total) {
					$totalnya = html_entity_decode($this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8');
				}

        $query = $this->db->query("SELECT oh.date_added, os.name AS status, oh.comment, oh.notify FROM " . DB_PREFIX . "order_history oh LEFT JOIN " . DB_PREFIX . "order_status os ON oh.order_status_id = os.order_status_id WHERE oh.comment LIKE '%Order No%' AND oh.order_id = '" . (int)$order_id . "' AND os.language_id = '2' ORDER BY oh.date_added ASC");
			  $shipments = $query->rows;
			  if($shipments)
			  {
						  foreach($shipments as $key=>$comment)
						  {
							  $cmnt_txt = ($comment['comment'])?$comment['comment']:'';
							  if (version_compare(PHP_VERSION, '5.3.0') <= 0) {
								  $awbno = substr($cmnt_txt,0, strpos($cmnt_txt,"- Order No")); 
							  }
							  else{				
								  $awbno = strstr($cmnt_txt,"- Order No",true);
							  }
								  $awb_no=trim($awbno,"AWB No.");					
								  break;
						  }
			  }else{
					  $awb_no= 0;
			  }
        $order_invoice_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order WHERE order_id = '" . (int)$order_id . "'");
        $order_no = $order_invoice_query->row['invoice_prefix'].$order_invoice_query->row['invoice_no'];
        $shipping_code = $order_invoice_query->row['shipping_code'];
        
				if($order_status=="قيد التجهيز"){
          if($shipping_code=="pickup.pickup"){
            $message_order = "أهلاً بك في سكة الطيب، طلبكم رقم ".$order_no." ينتظركم في محلنا بالدمام";
          }
          else{
            $message_order="مرحبا بك في سكة الطيب تم استلام طلبك رقم ".$order_no." سيتم التنفيذ خلال خمس إلى سبعة أيام عمل.";
          }
				}
				else if($order_status=="تمت معالجته" || $order_status=="قيد الاجراء"){
					$message_order = "تم تعميد الطلب رقم ".$order_no." وسيتم الشحن قريبا";
				}
				else if($order_status=="تم الشحن"){
					$message_order = " تم شحن الطلب رقم ".$order_no." عبر ارامكس برقم شحن ".$awb_no." ولتتبع الشحنة قم بزيارة www.aramex.com";
				}
				else if($order_status=="انتهى"){
					$message_order = "تم الغاء طلبك رقم ".$order_no." وذلك بسبب انتهاء فترة السداد";
				}
				else if($order_status=="إبطال ألغاء"){
					$message_order = "تم الغاء طلبك رقم ".$order_no." بسبب التكرار";
				}
				else if($order_status=="تم الإلغاء"){
					$message_order = "تم الغاء طلبك رقم ".$order_no." بناءا علي طلب العميل";
				}
        
					
				if($message_order != ""){
					$this->load->model('libraries/jossms');
					$gateway = $this->config->get('jossms_gateway');
					
					$parsing = array ( 
						'{order_date}',
						'{products_ordered}',
						'{firstname}',
						'{lastname}',
						'{email}',
						'{telephone}',
						'{orderid}',
						'{orderstatus}',
						'{shippingmethod}',
						'{shipping_firstname}',
						'{shipping_lastname}',
						'{shipping_company}',
						'{shipping_address}',
						'{shipping_city}',
						'{shipping_postcode}',
						'{shipping_state}',
						'{shipping_country}',
						'{paymentmethod}',
						'{payment_firstname}',
						'{payment_lastname}',
						'{payment_company}',
						'{payment_address}',
						'{payment_city}',
						'{payment_postcode}',
						'{payment_state}',
						'{payment_country}',
						'{total}',
						'{comment}',
						'{storename}'
					);
					$replace = array (
						date($language->get('date_format_short'), strtotime($order_info['date_added'])),
						$produk,
						$order_info['firstname'],
						$order_info['lastname'],
						$order_info['email'],
						$order_info['telephone'],
						$order_info['order_id'],
						$order_status,
						$order_info['shipping_method'],
						$order_info['shipping_firstname'],
						$order_info['shipping_lastname'],
						$order_info['shipping_company'],
						$order_info['shipping_address_1']." ".$order_info['shipping_address_2'],
						$order_info['shipping_city'],
						$order_info['shipping_postcode'],
						$order_info['shipping_zone'],
						$order_info['shipping_country'],
						$order_info['payment_method'],
						$order_info['payment_firstname'],
						$order_info['payment_lastname'],
						$order_info['payment_company'],
						$order_info['payment_address_1']." ".$order_info['payment_address_2'],
						$order_info['payment_city'],
						$order_info['payment_postcode'],
						$order_info['payment_zone'],
						$order_info['payment_country'],
						$totalnya,
						$order_info['comment'],
						$storename
					);
					
					$pesan = str_replace( $parsing, $replace, $message_order );
					
					$query = $this->db->query("SELECT `iso_code_2` FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$order_info['payment_country_id'] . "'");
					$isoc = $query->row['iso_code_2'];
					$phone_number = $this->model_libraries_jossms->getConvertPhonePrefix($order_info['telephone'],$isoc);
					$getresponse = $this->model_libraries_jossms->send_message($phone_number, $pesan, $gateway);
				}
				
				if ($message_order_toadmin != "") {
					$this->load->model('libraries/jossms');
					$gateway = $this->config->get('jossms_gateway');
					$countryid = $this->config->get('config_country_id');
					
					$parsing = array ( 
						'{order_date}',
						'{products_ordered}',
						'{firstname}',
						'{lastname}',
						'{email}',
						'{telephone}',
						'{orderid}',
						'{orderstatus}',
						'{shippingmethod}',
						'{shipping_firstname}',
						'{shipping_lastname}',
						'{shipping_company}',
						'{shipping_address}',
						'{shipping_city}',
						'{shipping_postcode}',
						'{shipping_state}',
						'{shipping_country}',
						'{paymentmethod}',
						'{payment_firstname}',
						'{payment_lastname}',
						'{payment_company}',
						'{payment_address}',
						'{payment_city}',
						'{payment_postcode}',
						'{payment_state}',
						'{payment_country}',
						'{total}',
						'{comment}',
						'{storename}'
					);
					$replace = array (
						date($language->get('date_format_short'), strtotime($order_info['date_added'])),
						$produk,
						$order_info['firstname'],
						$order_info['lastname'],
						$order_info['email'],
						$order_info['telephone'],
						$order_info['order_id'],
						$order_status,
						$order_info['shipping_method'],
						$order_info['shipping_firstname'],
						$order_info['shipping_lastname'],
						$order_info['shipping_company'],
						$order_info['shipping_address_1']." ".$order_info['shipping_address_2'],
						$order_info['shipping_city'],
						$order_info['shipping_postcode'],
						$order_info['shipping_zone'],
						$order_info['shipping_country'],
						$order_info['payment_method'],
						$order_info['payment_firstname'],
						$order_info['payment_lastname'],
						$order_info['payment_company'],
						$order_info['payment_address_1']." ".$order_info['payment_address_2'],
						$order_info['payment_city'],
						$order_info['payment_postcode'],
						$order_info['payment_zone'],
						$order_info['payment_country'],
						$totalnya,
						$order_info['comment'],
						$storename
					);
					
					$pesan = str_replace( $parsing, $replace, $message_order_toadmin );
					
					$query = $this->db->query("SELECT `iso_code_2` FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$countryid . "'");
					$isoc = $query->row['iso_code_2'];
					$phone_number = $this->model_libraries_jossms->getConvertPhonePrefix($this->config->get('config_telephone'),$isoc);
					$getresponse = $this->model_libraries_jossms->send_message($phone_number, $pesan, $gateway);
					
					$nohps = explode(',', $this->config->get('jossms_message_alert'));
					foreach ($nohps as $nohp) {
						if ($nohp) {
							$phone_number = $this->model_libraries_jossms->getConvertPhonePrefix($nohp,$isoc);
							$getresponse = $this->model_libraries_jossms->send_message($phone_number, $pesan, $gateway);
						}
					}
				}
			
						$mail = new Mail();
						$mail->protocol = $this->config->get('config_mail_protocol');
						$mail->parameter = $this->config->get('config_mail_parameter');
						$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
						$mail->smtp_username = $this->config->get('config_mail_smtp_username');
						$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
						$mail->smtp_port = $this->config->get('config_mail_smtp_port');
						$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

						$mail->setTo($order_info['email']);
						$mail->setFrom($this->config->get('config_email'));
						$mail->setSender(html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
						$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
						$mail->setHtml($html);
						$mail->setText($text);
						$mail->send();
					}
				}


			//Competition Auto Entry Start:
			$this->load->model('competition/competition');
			$order_value_query = $this->db->query("SELECT value FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' AND code = 'sub_total'");

			$competition_data = array(
				'value' => $order_value_query->row['value'],
				'email' => $order_info['email'],
				'name'	=> $order_info['payment_firstname'] . ' ' . $order_info['payment_lastname']
			);
			
			$today = strtotime(date("d-m-Y"));
			$competitions = $this->model_competition_competition->getCompetition();

			if ($competitions) {
			  foreach ($competitions as $competition) {
				if ($competition['auto'] && $competition['status'] && (isset($this->session->data['comp_entry'])) &&  (strtotime($this->model_competition_competition->getCompetitionDate($competition['competition_id'])) > $today) && ($order_value_query->row['value'] >= $competition['auto_entry_value'])){
				  $email_check = $this->model_competition_competition->getAutoEntryEmailCheck($order_info['email'], $competition['competition_id']);
				  if (!$email_check) {
						$this->model_competition_competition->setAutoEntry($competition_data, $competition['competition_id']);
				  }
				}
			  }
			}
			//Competition Auto Entry End:
			
				// Admin Alert Mail

								if($this->config->get('aramex_auto_create_shipment')==1 && $this->config->get('aramex_status') ==1){
								$this->load->model('aramex/aramex');
								$this->model_aramex_aramex->create($order_id);
								}
								
				if ($this->config->get('config_order_mail')) {
					$subject = sprintf($language->get('text_new_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'), $order_info['invoice_prefix'].$order_info['invoice_no']);

					// HTML Mail
					$data['text_greeting'] = $language->get('text_new_received');

					if ($comment) {
						if ($order_info['comment']) {
							$data['comment'] = nl2br($comment) . '<br/><br/>' . $order_info['comment'];
						} else {
							$data['comment'] = nl2br($comment);
						}
					} else {
						if ($order_info['comment']) {
							$data['comment'] = $order_info['comment'];
						} else {
							$data['comment'] = '';
						}
					}

					$data['text_download'] = '';

					$data['text_footer'] = '';

					$data['text_link'] = '';
					$data['link'] = '';
					$data['download'] = '';

					if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/order.tpl')) {
						$html = $this->load->view($this->config->get('config_template') . '/template/mail/order.tpl', $data);
					} else {
						$html = $this->load->view('default/template/mail/order.tpl', $data);
					}

					// Text
					$text  = $language->get('text_new_received') . "\n\n";
					$text .= $language->get('text_new_order_id') . ' ' . $order_id . "\n";
					$text .= $language->get('text_new_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n";
					$text .= $language->get('text_new_order_status') . ' ' . $order_status . "\n\n";
					$text .= $language->get('text_new_products') . "\n";

					foreach ($order_product_query->rows as $product) {
						$text .= $product['quantity'] . 'x ' . $product['name'] . ' (' . $product['model'] . ') ' . html_entity_decode($this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";

						$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . $product['order_product_id'] . "'");

						foreach ($order_option_query->rows as $option) {
							if ($option['type'] != 'file') {
								$value = $option['value'];
							} else {
								$value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
							}

							$text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value) . "\n";
						}
					}

					foreach ($order_voucher_query->rows as $voucher) {
						$text .= '1x ' . $voucher['description'] . ' ' . $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']);
					}

					$text .= "\n";

					$text .= $language->get('text_new_order_total') . "\n";

					foreach ($order_total_query->rows as $total) {
						$text .= $total['title'] . ': ' . html_entity_decode($this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";
					}

					$text .= "\n";

					if ($order_info['comment']) {
						$text .= $language->get('text_new_comment') . "\n\n";
						$text .= $order_info['comment'] . "\n\n";
					}

					//echo $html;
					$mail = new Mail();
					$mail->protocol = $this->config->get('config_mail_protocol');
					$mail->parameter = $this->config->get('config_mail_parameter');
					$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
					$mail->smtp_username = $this->config->get('config_mail_smtp_username');
					$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
					$mail->smtp_port = $this->config->get('config_mail_smtp_port');
					$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

					$mail->setTo($this->config->get('config_email'));
					$mail->setFrom($this->config->get('config_email'));
					$mail->setSender(html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
					$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
					$mail->setHtml($html);
					$mail->setText($text);
					$mail->send();

					// Send to additional alert emails
					$emails = explode(',', $this->config->get('config_mail_alert'));

					foreach ($emails as $email) {
						if ($email && preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $email)) {
							$mail->setTo($email);
							$mail->send();
						}
					}
				}
			}

			// If order status is not 0 then send update text email
			if ($order_info['order_status_id'] && $order_status_id && $notify && !$notifyhistory) {
				$language = new Language($order_info['language_directory']);
				$language->load($order_info['language_directory']);
				$language->load('mail/order');

				$subject = sprintf($language->get('text_update_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);

				$message  = $language->get('text_update_order') . ' ' . $order_id . "\n";
				$message .= $language->get('text_update_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n\n";

				$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_status_id . "' AND language_id = '" . (int)$order_info['language_id'] . "'");

				if ($order_status_query->num_rows) {
					$message .= $language->get('text_update_order_status') . "\n\n";
					$message .= $order_status_query->row['name'] . "\n\n";
				}

				if ($order_info['customer_id']) {
					$message .= $language->get('text_update_link') . "\n";
					$message .= $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id . "\n\n";
				}

				if ($comment) {
					$message .= $language->get('text_update_comment') . "\n\n";
					$message .= strip_tags($comment) . "\n\n";
				}

				$message .= $language->get('text_update_footer');
				if($order_info['email']){
					$mail = new Mail();
					$mail->protocol = $this->config->get('config_mail_protocol');
					$mail->parameter = $this->config->get('config_mail_parameter');
					$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
					$mail->smtp_username = $this->config->get('config_mail_smtp_username');
					$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
					$mail->smtp_port = $this->config->get('config_mail_smtp_port');
					$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

					$mail->setTo($order_info['email']);
					$mail->setFrom($this->config->get('config_email'));
					$mail->setSender(html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
					$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
					$mail->setText($message);
					$mail->send();
				}
			}

			// If order status in the complete range create any vouchers that where in the order need to be made available.
			if (in_array($order_info['order_status_id'], $this->config->get('config_complete_status'))) {
				// Send out any gift voucher mails
				$this->load->model('checkout/voucher');

				$this->model_checkout_voucher->confirm($order_id);
			}
		}

		$this->event->trigger('post.order.history.add', $order_id);
	}

	public function getOrders($data = array()) {
		$sql = "SELECT o.order_id, o.invoice_no, o.invoice_prefix, CONCAT(o.firstname, ' ', o.lastname) AS customer, (SELECT os.name FROM " . DB_PREFIX . "order_status os WHERE os.order_status_id = o.order_status_id AND os.language_id = '" . (int)$this->config->get('config_language_id') . "') AS status, o.shipping_code, o.payment_method, o.total, o.currency_code, o.currency_value, o.date_added, o.date_modified, o.customer_group_id, o.payment_code, o.proof_of_payment FROM " . DB_PREFIX . "order o";

		// if (isset($data['reciept_filter'])) {
		// 		$sql .= " WHERE o.proof_of_payment IS NOT NULL";
		// }
		// else{
		// 	if (isset($data['filter_order_status'])) {
		// 		$implode = array();

		// 		$order_statuses = explode(',', $data['filter_order_status']);

		// 		foreach ($order_statuses as $order_status_id) {
		// 			$implode[] = "o.order_status_id = '" . (int)$order_status_id . "'";
		// 		}

		// 		if ($implode) {
		// 			$sql .= " WHERE (" . implode(" OR ", $implode) . ")";
		// 		} else {

		// 		}
		// 	} else {
		// 		$sql .= " WHERE o.order_status_id > '0'";
		// 	}
		// }

		if (isset($data['filter_order_status'])) {
			$implode = array();

			$order_statuses = explode(',', $data['filter_order_status']);

			foreach ($order_statuses as $order_status_id) {
				$implode[] = "o.order_status_id = '" . (int)$order_status_id . "'";
			}

			if ($implode) {
				$sql .= " WHERE (" . implode(" OR ", $implode) . ")";
			} else {

			}
		} else {
			$sql .= " WHERE o.order_status_id > '0'";
		}

		if (!empty($data['filter_payment_method'])) {
			$sql .= " AND o.payment_code = '" . $data['filter_payment_method'] . "'";
		}

		if (!empty($data['filter_order_id'])) {
			$sql .= " AND o.order_id = '" . (int)$data['filter_order_id'] . "'";
		}

		if (!empty($data['filter_customer'])) {
			$sql .= " AND CONCAT(o.firstname, ' ', o.lastname) LIKE '%" . $this->db->escape($data['filter_customer']) . "%'";
		}

		if (!empty($data['filter_date_added'])) {
			$sql .= " AND DATE(o.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		if (!empty($data['filter_date_modified'])) {
			$sql .= " AND DATE(o.date_modified) = DATE('" . $this->db->escape($data['filter_date_modified']) . "')";
		}

		if (!empty($data['filter_bankdeposit'])) {
			$sql .= " AND o.proof_of_payment IS NOT NULL";
		}

		if (!empty($data['filter_total'])) {
			$sql .= " AND o.total = '" . (float)$data['filter_total'] . "'";
		}

		$sort_data = array(
			'o.order_id',
			'customer',
			'status',
			'o.date_added',
			'o.date_modified',
			'o.total'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY o.order_id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function addOrderComments($order_id, $order_status_id, $comment = '', $notify = false, $detect_quantity = false, $notifyhistory=false) {
		//$this->event->trigger('pre.order.history.add', $order_id);

		$order_info = $this->getOrder($order_id);

		if ($order_info) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET order_id = '" . (int)$order_id . "', order_status_id = '" . (int)$order_status_id . "', notify = '" . (int)$notify . "', comment = '" . $this->db->escape($comment) . "', date_added = NOW()");
		}

		//$this->event->trigger('post.order.history.add', $order_id);
	}

    public function getSettingFreeShipping($store_id = 0) {
		$setting_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `code` = 'config_free_shipping'");

		foreach ($query->rows as $result) {
			if (!$result['serialized']) {
				$setting_data[$result['key']] = $result['value'];
			} else {
				$setting_data[$result['key']] = unserialize($result['value']);
			}
		}

		return $setting_data;
	}

    public function getSettingFreeShipping1($store_id = 0) {
		$setting_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `code` = 'config_free_shipping_order'");

		foreach ($query->rows as $result) {
			if (!$result['serialized']) {
				$setting_data[$result['key']] = $result['value'];
			} else {
				$setting_data[$result['key']] = unserialize($result['value']);
			}
		}

		return $setting_data;
	}

    public function getOrderbyCustomer($customer_id) {
		$order_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order` o WHERE o.customer_id = '" . (int)$customer_id . "' AND o.order_status_id > 0 ORDER BY order_id DESC LIMIT 1");

		if ($order_query->num_rows) {
            $today_date = date("Y-m-d H:i:s");
            $order_date = date("Y-m-d H:i:s", strtotime($order_query->row['date_added']));
            $seconds = strtotime($today_date) - strtotime($order_date);
			$days = $seconds / 86400;
            return ceil(abs($days));
		} else {
			return false;
		}
	}

    public function getIsProductDiscount($product_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_option_value po LEFT JOIN " . DB_PREFIX . "product_discount pd on po.product_id=pd.product_id WHERE po.product_id = '" . (int)$product_id . "' and (po.discount > 0 OR pd.price > 0)");

        if($query->row['total'] > 0)
            return true;
		return false;
	}

	public function getOrderbyInvoice($order_id) {
		$order_query = $this->db->query("SELECT *, (SELECT os.name FROM `" . DB_PREFIX . "order_status` os WHERE os.order_status_id = o.order_status_id AND os.language_id = o.language_id) AS order_status FROM `" . DB_PREFIX . "order` o WHERE CONCAT(o.invoice_prefix, '',o.invoice_no) = '" . (int)$order_id . "'");

		if ($order_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['payment_country_id'] . "'");

			if ($country_query->num_rows) {
				$payment_iso_code_2 = $country_query->row['iso_code_2'];
				$payment_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$payment_iso_code_2 = '';
				$payment_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['payment_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$payment_zone_code = $zone_query->row['code'];
			} else {
				$payment_zone_code = '';
			}

			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

			if ($country_query->num_rows) {
				$shipping_iso_code_2 = $country_query->row['iso_code_2'];
				$shipping_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$shipping_iso_code_2 = '';
				$shipping_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$shipping_zone_code = $zone_query->row['code'];
			} else {
				$shipping_zone_code = '';
			}

			$this->load->model('localisation/language');

			$language_info = $this->model_localisation_language->getLanguage($order_query->row['language_id']);

			if ($language_info) {
				$language_code = $language_info['code'];
				$language_directory = $language_info['directory'];
			} else {
				$language_code = '';
				$language_directory = '';
			}

			return array(
				'order_id'                => $order_query->row['order_id'],
				'invoice_no'              => $order_query->row['invoice_no'],
				'invoice_prefix'          => $order_query->row['invoice_prefix'],
				'store_id'                => $order_query->row['store_id'],
				'store_name'              => $order_query->row['store_name'],
				'store_url'               => $order_query->row['store_url'],
				'customer_id'             => $order_query->row['customer_id'],
				'customer_group_id'       => $order_query->row['customer_group_id'],
				'firstname'               => $order_query->row['firstname'],
				'lastname'                => $order_query->row['lastname'],
				'email'                   => $order_query->row['email'],
				'telephone'               => $order_query->row['telephone'],
				'fax'                     => $order_query->row['fax'],
				'custom_field'            => unserialize($order_query->row['custom_field']),
				'payment_firstname'       => $order_query->row['payment_firstname'],
				'payment_lastname'        => $order_query->row['payment_lastname'],
				'payment_company'         => $order_query->row['payment_company'],
				'payment_address_1'       => $order_query->row['payment_address_1'],
				'payment_address_2'       => $order_query->row['payment_address_2'],
				'payment_postcode'        => $order_query->row['payment_postcode'],
				'payment_city'            => $order_query->row['payment_city'],
				'payment_zone_id'         => $order_query->row['payment_zone_id'],
				'payment_zone'            => $order_query->row['payment_zone'],
				'payment_zone_code'       => $payment_zone_code,
				'payment_country_id'      => $order_query->row['payment_country_id'],
				'payment_country'         => $order_query->row['payment_country'],
				'payment_iso_code_2'      => $payment_iso_code_2,
				'payment_iso_code_3'      => $payment_iso_code_3,
				'payment_address_format'  => $order_query->row['payment_address_format'],
				'payment_custom_field'    => unserialize($order_query->row['payment_custom_field']),
				'payment_method'          => $order_query->row['payment_method'],
				'payment_code'            => $order_query->row['payment_code'],
				'shipping_firstname'      => $order_query->row['shipping_firstname'],
				'shipping_lastname'       => $order_query->row['shipping_lastname'],
				'shipping_company'        => $order_query->row['shipping_company'],
				'shipping_address_1'      => $order_query->row['shipping_address_1'],
				'shipping_address_2'      => $order_query->row['shipping_address_2'],
				'shipping_postcode'       => $order_query->row['shipping_postcode'],
				'shipping_telephone'      => $order_query->row['shipping_telephone'],
				'shipping_city'           => $order_query->row['shipping_city'],
				'shipping_zone_id'        => $order_query->row['shipping_zone_id'],
				'shipping_zone'           => $order_query->row['shipping_zone'],
				'shipping_zone_code'      => $shipping_zone_code,
				'shipping_country_id'     => $order_query->row['shipping_country_id'],
				'shipping_country'        => $order_query->row['shipping_country'],
				'shipping_iso_code_2'     => $shipping_iso_code_2,
				'shipping_iso_code_3'     => $shipping_iso_code_3,
				'shipping_address_format' => $order_query->row['shipping_address_format'],
				'shipping_custom_field'   => unserialize($order_query->row['shipping_custom_field']),
				'shipping_method'         => $order_query->row['shipping_method'],
				'shipping_code'           => $order_query->row['shipping_code'],
				'comment'                 => $order_query->row['comment'],
				'total'                   => $order_query->row['total'],
				'order_status_id'         => $order_query->row['order_status_id'],
				'order_status'            => $order_query->row['order_status'],
				'affiliate_id'            => $order_query->row['affiliate_id'],
				'commission'              => $order_query->row['commission'],
				'language_id'             => $order_query->row['language_id'],
				'language_code'           => $language_code,
				'language_directory'      => $language_directory,
				'currency_id'             => $order_query->row['currency_id'],
				'currency_code'           => $order_query->row['currency_code'],
				'currency_value'          => $order_query->row['currency_value'],
				'ip'                      => $order_query->row['ip'],
				'forwarded_ip'            => $order_query->row['forwarded_ip'],
				'user_agent'              => $order_query->row['user_agent'],
				'accept_language'         => $order_query->row['accept_language'],
				'date_modified'           => $order_query->row['date_modified'],
				'date_added'              => $order_query->row['date_added'],
                'is_admin_order'          => $order_query->row['is_admin_order'],
                'platform'                => $order_query->row['platform']
			);
		} else {
			return false;
		}
	}
}
