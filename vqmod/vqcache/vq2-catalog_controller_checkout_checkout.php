<?php
class ControllerCheckoutCheckout extends Controller {
	public function index() {

		$this->session->data['comp_entry'] = 1;
		
			
		// Validate cart has products and has stock.
		if (((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) && $this->cart->hasSubtract()) {
			$this->response->redirect($this->url->link('checkout/cart'));
		}

		// Validate minimum quantity requirements.
		$products = $this->cart->getProducts();

		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total && $this->cart->hasSubtract()) {
				$this->response->redirect($this->url->link('checkout/cart'));
			}
		}

		$this->load->language('checkout/checkout');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

		// Required by klarna
		if ($this->config->get('klarna_account') || $this->config->get('klarna_invoice')) {
			$this->document->addScript('http://cdn.klarna.com/public/kitt/toc/v1.0/js/klarna.terms.min.js');
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_cart'),
			'href' => $this->url->link('checkout/cart')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('checkout/checkout', '', 'SSL')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_checkout_option'] = $this->language->get('text_checkout_option');
		$data['text_checkout_account'] = $this->language->get('text_checkout_account');
		$data['text_checkout_payment_address'] = $this->language->get('text_checkout_payment_address');
		$data['text_checkout_shipping_address'] = $this->language->get('text_checkout_shipping_address');
		$data['text_checkout_shipping_method'] = $this->language->get('text_checkout_shipping_method');
		$data['text_checkout_payment_method'] = $this->language->get('text_checkout_payment_method');
		$data['text_checkout_confirm'] = $this->language->get('text_checkout_confirm');

        $data['text_extr_title'] = $this->language->get('text_extr_title');
        $data['text_extr_amount'] = $this->language->get('text_extr_amount');
        $data['text_extr_button'] = $this->language->get('text_extr_button');
        $data['text_extr_button1'] = $this->language->get('text_extr_button1');

		if (isset($this->session->data['error'])) {
			$data['error_warning'] = $this->session->data['error'];
			unset($this->session->data['error']);
		} else {
			$data['error_warning'] = '';
		}

		$data['logged'] = $this->customer->isLogged();

		if (isset($this->session->data['account'])) {
			$data['account'] = $this->session->data['account'];
		} else {
			$data['account'] = '';
		}

		$data['shipping_required'] = $this->cart->hasShipping();

			//need to add enabled check
		$cart_subtotal = $this->cart->getSubTotal();
		$this->load->model('competition/competition');
		$this->load->model('catalog/information');
		$competitions = $this->model_competition_competition->getCompetition();
		$today = strtotime(date("d-m-Y"));
		$data['competition'] = false;
		
		if ($competitions) {
		  foreach ($competitions as $competition) {
			if ($competition['auto'] && $competition['status'] &&  (strtotime($this->model_competition_competition->getCompetitionDate($competition['competition_id'])) > $today) && ($cart_subtotal >= $competition['auto_entry_value'])){
				$data['competition'] = true;
				break;
			}
		  }
		}
		
		$information_info = $this->model_catalog_information->getInformation($this->config->get('config_competition_id'));
		
		if ($information_info) {
			$data['text_auto_entry'] = sprintf($this->language->get('text_auto_agree'), $this->url->link('information/information/agree', 'information_id=' . $this->config->get('config_competition_id')), $information_info['title'], $information_info['title']);
		} else {
			$data['text_auto_entry'] = $this->language->get('text_auto_entry');
		}
		
			

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

        $this->load->model('checkout/order');
        $data['days_order']='No';
        if ($this->customer->isLogged()) {
            $freeshipping=$this->model_checkout_order->getSettingFreeShipping1();
             if($freeshipping&& $freeshipping['config_free_shipping_order']==1){
                 $config_order_days = $freeshipping['config_free_shipping_order_days'];
                 $order_days = $this->model_checkout_order->getOrderbyCustomer($this->customer->getId());
                 if($order_days!=null && $order_days <= $config_order_days){
                     $data['days_order']=$this->language->get('text_congrats_message');;
                 }
             }
        }

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/checkout.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/checkout/checkout.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/checkout/checkout.tpl', $data));
		}
	}


	public function enter() {
		$this->session->data['comp_entry'] = 1;
		$this->language->load('checkout/checkout');
		$json = $this->language->get('text_will');
		
		$this->response->setOutput(json_encode($json));
	}
	
	public function no_enter() {
		unset($this->session->data['comp_entry']);
		$this->language->load('checkout/checkout');

		$json = $this->language->get('text_wont');
		
		$this->response->setOutput(json_encode($json));
	}
            
	public function country() {
		$json = array();

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function customfield() {
		$json = array();

		$this->load->model('account/custom_field');

		// Customer Group
		if (isset($this->request->get['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->get['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $this->request->get['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$custom_fields = $this->model_account_custom_field->getCustomFields($customer_group_id);

		foreach ($custom_fields as $custom_field) {
			$json[] = array(
				'custom_field_id' => $custom_field['custom_field_id'],
				'required'        => $custom_field['required']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}