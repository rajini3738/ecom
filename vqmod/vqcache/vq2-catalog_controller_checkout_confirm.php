<?php
class ControllerCheckoutConfirm extends Controller {
	public function index() {
		$redirect = '';

		if ($this->cart->hasShipping()) {
			// Validate if shipping address has been set.
			if (!isset($this->session->data['shipping_address'])) {
				$redirect = $this->url->link('checkout/checkout', '', 'SSL');
			}

			// Validate if shipping method has been set.
			if (!isset($this->session->data['shipping_method'])) {
				$redirect = $this->url->link('checkout/checkout', '', 'SSL');
			}
		} else {
			unset($this->session->data['shipping_address']);
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
		}

		// Validate if payment address has been set.
		if (!isset($this->session->data['payment_address'])) {
			$redirect = $this->url->link('checkout/checkout', '', 'SSL');
		}

		// Validate if payment method has been set.
		if (!isset($this->session->data['payment_method'])) {
			$redirect = $this->url->link('checkout/checkout', '', 'SSL');
		}

		// Validate cart has products and has stock.
		if (((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout')))&& $this->cart->hasSubtract()) {
			$redirect = $this->url->link('checkout/cart');
		}

		// Validate minimum quantity requirements.
		$products = $this->cart->getProducts();
        $product_total_cost = 0;
		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
                    $product_total_cost += $product_2['total'];
				}
			}

			if ($product['minimum'] > $product_total) {
				$redirect = $this->url->link('checkout/cart');

				break;
			}
		}

		if (!$redirect) {
			$order_data = array();

			$order_data['totals'] = array();
			$total = 0;
			$taxes = $this->cart->getTaxes1();
			//print_r($taxes);
			$this->load->model('extension/extension');

			$sort_order = array();

			$results = $this->model_extension_extension->getExtensions('total', $this->cart->hasDiscount());

			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

            $this->load->model('checkout/order');

            /*if($this->session->data['payment_method']['code'] == "cod" && $this->session->data['shipping_method']['code']=="aramex.aramex"){
                $shippingcost = $this->session->data['shipping_method']['cost'];
                if($this->session->data['shipping_address']['iso_code_2']=="SA"){
                    $this->session->data['shipping_method']['cost']=$shippingcost+20;
                }
                else{
                    $this->session->data['shipping_method']['cost']=$shippingcost+40;
                }
            }
            else{
                $this->session->data['shipping_method']['cost'] =  $this->session->data['shipping_method']['originalcost'];
            }*/



            $order_data['free_shipping']=0;
            if($this->session->data['payment_method']['code'] == "bank_transfer"){
                $freeshipping=$this->model_checkout_order->getSettingFreeShipping();
                if($freeshipping){
                    $value = (int)$freeshipping['config_free_shipping_amount'];
                    if($freeshipping['config_free_shipping_status_id']==1 && $product_total_cost >=$value){
                        $this->session->data['shipping_method']['cost']=0;
                    }
                }
            }
            if ($this->customer->isLogged()) {
                $freeshipping=$this->model_checkout_order->getSettingFreeShipping1();
                if($freeshipping && $freeshipping['config_free_shipping_order']==1){
                    $config_order_days = $freeshipping['config_free_shipping_order_days'];
                    $order_days = $this->model_checkout_order->getOrderbyCustomer($this->customer->getId());
                    if($order_days!=null && $order_days <= $config_order_days){
                        $this->session->data['shipping_method']['cost']=0;
                    }
                }
            }

			/*if($this->session->data['shipping_address']['iso_code_2']=="SA" && $product_total_cost >= 300){
				if($this->session->data['payment_method']['code'] == "bank_transfer" || $this->session->data['payment_method']['code'] == "payfort_fort"){
					$this->session->data['shipping_method']['cost']=0;
				}
            }*/

            $isCoupon=false;
            if (isset($this->session->data['coupon'])) {
                $isCoupon=true;
            }

			foreach ($results as $result) {
                if($isCoupon){
                    if($result['code']!="total_customer_group_discount"){
                        if ($this->config->get($result['code'] . '_status')) {
                            $this->load->model('total/' . $result['code']);

                            $this->{'model_total_' . $result['code']}->getTotal($order_data['totals'], $total, $taxes);
                        }
                    }
                }
                else{
                    if ($this->config->get($result['code'] . '_status')) {
                        $this->load->model('total/' . $result['code']);
						//print_r($result['code']);
                        $this->{'model_total_' . $result['code']}->getTotal($order_data['totals'], $total, $taxes);
                    }
                }
			}

			$sort_order = array();

			foreach ($order_data['totals'] as $key => $value) {
				unset($order_data['totals'][3]);
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $order_data['totals']);

			$this->load->language('checkout/checkout');

			$order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
			$order_data['store_id'] = $this->config->get('config_store_id');
			$order_data['store_name'] = $this->config->get('config_name');

			if ($order_data['store_id']) {
				$order_data['store_url'] = $this->config->get('config_url');
			} else {
				$order_data['store_url'] = HTTP_SERVER;
			}

			if ($this->customer->isLogged()) {
				$this->load->model('account/customer');

				$customer_info = $this->model_account_customer->getCustomer($this->customer->getId());

				$order_data['customer_id'] = $this->customer->getId();
				$order_data['customer_group_id'] = $customer_info['customer_group_id'];
				$order_data['firstname'] = $customer_info['firstname'];
				$order_data['lastname'] = $customer_info['lastname'];
				$order_data['email'] = $customer_info['email'];
				$order_data['telephone'] = $customer_info['telephone'];
				$order_data['fax'] = $customer_info['fax'];
				$order_data['custom_field'] = unserialize($customer_info['custom_field']);
			} elseif (isset($this->session->data['guest'])) {
				$order_data['customer_id'] = 0;
				$order_data['customer_group_id'] = $this->session->data['guest']['customer_group_id'];
				$order_data['firstname'] = $this->session->data['guest']['firstname'];
				$order_data['lastname'] = $this->session->data['guest']['lastname'];
				$order_data['email'] = $this->session->data['guest']['email'];
				$order_data['telephone'] = $this->session->data['guest']['telephone'];
				$order_data['fax'] = $this->session->data['guest']['fax'];
				$order_data['custom_field'] = $this->session->data['guest']['custom_field'];
			}

			$order_data['payment_firstname'] = $this->session->data['payment_address']['firstname'];
			$order_data['payment_lastname'] = $this->session->data['payment_address']['lastname'];
			$order_data['payment_company'] = $this->session->data['payment_address']['company'];
			$order_data['payment_address_1'] = $this->session->data['payment_address']['address_1'];
			$order_data['payment_address_2'] = $this->session->data['payment_address']['address_2'];
			$order_data['payment_city'] = $this->session->data['payment_address']['city'];
			$order_data['payment_postcode'] = $this->session->data['payment_address']['postcode'];
			$order_data['payment_zone'] = $this->session->data['payment_address']['zone'];
			$order_data['payment_zone_id'] = $this->session->data['payment_address']['zone_id'];
			$order_data['payment_country'] = $this->session->data['payment_address']['country'];
			$order_data['payment_country_id'] = $this->session->data['payment_address']['country_id'];
			$order_data['payment_address_format'] = $this->session->data['payment_address']['address_format'];
			$order_data['payment_custom_field'] = (isset($this->session->data['payment_address']['custom_field']) ? $this->session->data['payment_address']['custom_field'] : array());

			if (isset($this->session->data['payment_method']['title'])) {
				$order_data['payment_method'] = $this->session->data['payment_method']['title'];
			} else {
				$order_data['payment_method'] = '';
			}

			if (isset($this->session->data['payment_method']['code'])) {
				$order_data['payment_code'] = $this->session->data['payment_method']['code'];
			} else {
				$order_data['payment_code'] = '';
			}

			if ($this->cart->hasShipping()) {
				$order_data['shipping_firstname'] = $this->session->data['shipping_address']['firstname'];
				$order_data['shipping_lastname'] = $this->session->data['shipping_address']['lastname'];
				$order_data['shipping_company'] = $this->session->data['shipping_address']['company'];
				$order_data['shipping_address_1'] = $this->session->data['shipping_address']['address_1'];
				$order_data['shipping_address_2'] = $this->session->data['shipping_address']['address_2'];
				$order_data['shipping_city'] = $this->session->data['shipping_address']['city'];
				$order_data['shipping_postcode'] = $this->session->data['shipping_address']['postcode'];
				$order_data['shipping_telephone'] = $this->session->data['shipping_address']['telephone'];
				$order_data['shipping_zone'] = $this->session->data['shipping_address']['zone'];
				$order_data['shipping_zone_id'] = $this->session->data['shipping_address']['zone_id'];
				$order_data['shipping_country'] = $this->session->data['shipping_address']['country'];
				$order_data['shipping_country_id'] = $this->session->data['shipping_address']['country_id'];
				$order_data['shipping_address_format'] = $this->session->data['shipping_address']['address_format'];
				$order_data['shipping_custom_field'] = (isset($this->session->data['shipping_address']['custom_field']) ? $this->session->data['shipping_address']['custom_field'] : array());

				if (isset($this->session->data['shipping_method']['title'])) {
					$order_data['shipping_method'] = $this->session->data['shipping_method']['title'];
				} else {
					$order_data['shipping_method'] = '';
				}

				if (isset($this->session->data['shipping_method']['code'])) {
					$order_data['shipping_code'] = $this->session->data['shipping_method']['code'];
				} else {
					$order_data['shipping_code'] = '';
				}
			} else {
				$order_data['shipping_firstname'] = '';
				$order_data['shipping_lastname'] = '';
				$order_data['shipping_company'] = '';
				$order_data['shipping_address_1'] = '';
				$order_data['shipping_address_2'] = '';
				$order_data['shipping_city'] = '';
				$order_data['shipping_postcode'] = '';
				$order_data['shipping_zone'] = '';
				$order_data['shipping_zone_id'] = '';
				$order_data['shipping_country'] = '';
				$order_data['shipping_country_id'] = '';
				$order_data['shipping_address_format'] = '';
				$order_data['shipping_custom_field'] = array();
				$order_data['shipping_method'] = '';
				$order_data['shipping_code'] = '';
			}

			$order_data['products'] = array();
			$this->load->model('total/tax');
			foreach ($this->cart->getProducts() as $product) {
				$option_data = array();
				$this->{'model_total_tax'}->getTotal1($product['total'], $total_tax, $taxes, $taxes_value);
				foreach ($product['option'] as $option) {
					$option_data[] = array(
						'product_option_id'       => $option['product_option_id'],
						'product_option_value_id' => $option['product_option_value_id'],
						'option_id'               => $option['option_id'],
						'option_value_id'         => $option['option_value_id'],
						'name'                    => $option['name'],
						'value'                   => $option['value'],
						'type'                    => $option['type']
					);
				}

				$order_data['products'][] = array(
					'product_id'	=> $product['product_id'],
					'name'			=> $product['name'],
					'model'			=> $product['model'],
					'option'		=> $option_data,
					'download'		=> $product['download'],
					'quantity'		=> $product['quantity'],
					'weight'		=> $product['weight'],
					'subtract'		=> $product['subtract'],
					'price'			=> $product['price'],
					'total'			=> $product['total'],
					'tax'			=> $taxes_value,//$this->tax->getTax($product['price'], $product['tax_class_id']),
					'total_tax'		=> $total_tax,
					'discount'		=> $product['discount'],
					'unit_price'	=> $product['unit_price'],
					'intial_total'	=> $product['intial_total'],
					'reward'		=> $product['reward']
				);
			}

			// Gift Voucher
			$order_data['vouchers'] = array();

			if (!empty($this->session->data['vouchers'])) {
				foreach ($this->session->data['vouchers'] as $voucher) {
					$order_data['vouchers'][] = array(
						'description'      => $voucher['description'],
						'code'             => substr(md5(mt_rand()), 0, 10),
						'to_name'          => $voucher['to_name'],
						'to_email'         => $voucher['to_email'],
						'from_name'        => $voucher['from_name'],
						'from_email'       => $voucher['from_email'],
						'voucher_theme_id' => $voucher['voucher_theme_id'],
						'message'          => $voucher['message'],
						'amount'           => $voucher['amount']
					);
				}
			}

			$order_data['comment'] = $this->session->data['comment'];
			$order_data['total'] = $total;

			if (isset($this->request->cookie['tracking'])) {
				$order_data['tracking'] = $this->request->cookie['tracking'];

				$subtotal = $this->cart->getSubTotal();

				// Affiliate
				$this->load->model('affiliate/affiliate');

				$affiliate_info = $this->model_affiliate_affiliate->getAffiliateByCode($this->request->cookie['tracking']);

				if ($affiliate_info) {
					$order_data['affiliate_id'] = $affiliate_info['affiliate_id'];
					$order_data['commission'] = ($subtotal / 100) * $affiliate_info['commission'];
				} else {
					$order_data['affiliate_id'] = 0;
					$order_data['commission'] = 0;
				}

				// Marketing
				$this->load->model('checkout/marketing');

				$marketing_info = $this->model_checkout_marketing->getMarketingByCode($this->request->cookie['tracking']);

				if ($marketing_info) {
					$order_data['marketing_id'] = $marketing_info['marketing_id'];
				} else {
					$order_data['marketing_id'] = 0;
				}
			} else {
				$order_data['affiliate_id'] = 0;
				$order_data['commission'] = 0;
				$order_data['marketing_id'] = 0;
				$order_data['tracking'] = '';
			}

			$order_data['language_id'] = $this->config->get('config_language_id');
			$order_data['currency_id'] = $this->currency->getId();
			$order_data['currency_code'] = $this->currency->getCode();
			$order_data['currency_value'] = $this->currency->getValue($this->currency->getCode());
			$order_data['ip'] = $this->request->server['REMOTE_ADDR'];

			if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
				$order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
			} elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
				$order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
			} else {
				$order_data['forwarded_ip'] = '';
			}

			if (isset($this->request->server['HTTP_USER_AGENT'])) {
				$order_data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
			} else {
				$order_data['user_agent'] = '';
			}

			if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
				$order_data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
			} else {
				$order_data['accept_language'] = '';
			}

			$this->load->model('checkout/order');
			print_r($order_data);
			$this->session->data['order_id'] = $this->model_checkout_order->addOrder($order_data);

			$data['text_recurring_item'] = $this->language->get('text_recurring_item');
			$data['text_payment_recurring'] = $this->language->get('text_payment_recurring');

			$data['column_name'] = $this->language->get('column_name');
			$data['column_model'] = $this->language->get('column_model');
			$data['column_quantity'] = $this->language->get('column_quantity');
			$data['column_price'] = $this->language->get('column_price');
			$data['column_total'] = $this->language->get('column_total');
			$data['column_tax'] = $this->language->get('column_tax');
			$data['column_total_tax'] = $this->language->get('column_total_tax');

			$this->load->model('tool/upload');

			$data['products'] = array();
			$this->load->model('total/tax');
			foreach ($this->cart->getProducts() as $product) {
				$option_data = array();

				foreach ($product['option'] as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);
				}

				$recurring = '';

				if ($product['recurring']) {
					$frequencies = array(
						'day'        => $this->language->get('text_day'),
						'week'       => $this->language->get('text_week'),
						'semi_month' => $this->language->get('text_semi_month'),
						'month'      => $this->language->get('text_month'),
						'year'       => $this->language->get('text_year'),
					);

					if ($product['recurring']['trial']) {
						$recurring = sprintf($this->language->get('text_trial_description'), $this->currency->format($this->tax->calculate($product['recurring']['trial_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax'))), $product['recurring']['trial_cycle'], $frequencies[$product['recurring']['trial_frequency']], $product['recurring']['trial_duration']) . ' ';
					}

					if ($product['recurring']['duration']) {
						$recurring .= sprintf($this->language->get('text_payment_description'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax'))), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
					} else {
						$recurring .= sprintf($this->language->get('text_payment_cancel'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax'))), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
					}
				}
				$ptotal = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'];
				$this->{'model_total_tax'}->getTotal1($ptotal, $total_tax, $taxes, $taxes_value);
				$data['products'][] = array(
					'cart_id'		=> $product['cart_id'],
					'product_id'	=> $product['product_id'],
					'name'			=> $product['name'],
					'model'			=> $product['model'],
					'option'		=> $option_data,
					'recurring'		=> $recurring,
					'quantity'		=> $product['quantity'],
					'subtract'		=> $product['subtract'],
					'price'			=> $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'))),
					'total'			=> $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']),
					'tax'			=> $this->currency->format($taxes_value),
					'total_tax'		=> $this->currency->format($total_tax),
					'unit_price'	=> $product['unit_price'],
					'intial_total'	=> $product['intial_total'],
					'href'			=> $this->url->link('product/product', 'product_id=' . $product['product_id']),
				);
			}

			// Gift Voucher
			$data['vouchers'] = array();

			if (!empty($this->session->data['vouchers'])) {
				foreach ($this->session->data['vouchers'] as $voucher) {
					$data['vouchers'][] = array(
						'description' => $voucher['description'],
						'amount'      => $this->currency->format($voucher['amount'])
					);
				}
			}

			$data['totals'] = array();

			foreach ($order_data['totals'] as $total) {

				
				

			
				$isDiscount = false;
				if($total['code'] =='total_customer_group_discount'){
					if((int)$total['value'] == 0 ){
						$isDiscount = true;
					}
					else{
						$isDiscount = false;
					}
				}
				if(!$isDiscount && $total['code']!='tax'){
					// unset($arr[$i]);
					// unset($order_data['totals'][3]);
					// echo "<pre>";
					// 	print_r($data['totals']);
					// echo "</pre>";

					$data['totals'][] = array(
						'title' => $total['title'],
						'text'  => $this->currency->format($total['value']),
					);			
				}


			}
			

			
				// Add from vqmod
				$verified = 0;
				$groupid = 0;
				
				if ($this->customer->isLogged()) {
					$groupid = $this->customer->getGroupId();
				}
				
				if (isset($this->session->data['phone_verified'])) {
					$verified = $this->session->data['phone_verified'];
				}
				
				$bypass_group = $this->config->get('jossms_skip_group_id');
				if ($bypass_group){
					if (@in_array($groupid,$bypass_group) && !@in_array(0,$bypass_group)) {
						$verified = 1;
					}
				}
				
				// Skip verification if payment method is in data
				$payment_method = $this->session->data['payment_method']['code'];
				$bypass_payment_method = $this->config->get('jossms_skip_payment_method');
				if ($bypass_payment_method){
					if (@in_array($payment_method,$bypass_payment_method) && !@in_array("none",$bypass_payment_method)){
						$verified = 1;
					}
				}			
				
				if (!$verified && $this->config->get('jossms_verify') == 1 && $this->config->get('jossms_order_verify') == 1) {
					$data['payment'] = $this->load->controller('module/jossmsverify');
				} else {
					if ($verified == 2) {
						$data['payment'] = "This transaction cannot be completed...";
					}	else {
						unset($this->session->data['phone_verified']);
						$data['payment'] = $this->load->controller('payment/' . $this->session->data['payment_method']['code']);
					}
				}
				//
			
		} else {
			$data['redirect'] = $redirect;
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/confirm.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/checkout/confirm.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/checkout/confirm.tpl', $data));
		}
	}
}