<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">

			<?php if($exportalldatafree_status=="1") { ?>
			<a href="<?php echo $export; ?>" data-toggle="tooltip" title="<?php echo "Export"; ?>" class="btn btn-success"><i class="fa fa-download"></i></a>
			<?php } else { ?>
			<button type="submit" id="button-shipping" form="form-order" formaction="<?php echo $shipping; ?>" data-toggle="tooltip" title="<?php echo $button_shipping_print; ?>" class="btn btn-info"><i class="fa fa-truck"></i></button>
			<?php } ?>
			
        <button type="submit" id="button-shipping" form="form-order" formaction="<?php echo $shipping; ?>" data-toggle="tooltip" title="<?php echo $button_shipping_print; ?>" class="btn btn-info"><i class="fa fa-truck"></i></button>
        <button type="submit" id="button-invoice" form="form-order" formaction="<?php echo $invoice; ?>" data-toggle="tooltip" title="<?php echo $button_invoice_print; ?>" class="btn btn-info"><i class="fa fa-print"></i></button>
        <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="submit" id="button-sendsms" form="form-order" formaction="<?php echo $sendsms; ?>" data-toggle="tooltip" title="Send Message" class="btn btn-info">Send Message</button>
		<a href="<?php echo $changeorderstatus; ?>" data-toggle="tooltip" title="Change Order Status" class="btn btn-primary">Change Order Status</a>
       </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
	<div id="history"></div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-order-id"><?php echo $entry_order_id; ?></label>
                <input type="text" name="filter_order_id" value="<?php echo $filter_order_id; ?>" placeholder="<?php echo $entry_order_id; ?>" id="input-order-id" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-customer"><?php echo $entry_customer; ?></label>
                <input type="text" name="filter_customer" value="<?php echo $filter_customer; ?>" placeholder="<?php echo $entry_customer; ?>" id="input-customer" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-order-status">
                  Payment Type
                </label>
                <select name="filter_payment_method" id="input-payment-method" class="form-control">
                  <option value="*"></option>                  
                  <?php foreach ($payment_methods as $payment_method) { ?>
                  <?php if ($payment_method['code'] == $filter_payment_method) { ?>
                  <option value="<?php echo $payment_method['code']; ?>" selected="selected"><?php echo $payment_method['title']; ?>
                  </option>
                  <?php } else { ?>
                  <option value="<?php echo $payment_method['code']; ?>"><?php echo $payment_method['title']; ?>
                  </option>
                  <?php } ?>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-order-status">
                  Bank Transfer Payment Status
                </label>
                <select name="filter_banktransfer" id="input-banktransfer" class="form-control">
                  <option value="*"></option>
                  <option value="Y">Yes</option>
                  <option value="N">No</option>
                </select>
              </div>
            <div class="form-group">
                <label class="control-label" for="input-filenumber"><?php echo $results1; ?></label>
            </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-order-status"><?php echo $entry_order_status; ?></label>
                <select name="filter_order_status" id="input-order-status" class="form-control">
                  <option value="*"></option>
                  <?php if ($filter_order_status == '0') { ?>
                  <option value="0" selected="selected"><?php echo $text_missing; ?></option>
                  <?php } else { ?>
                  <option value="0"><?php echo $text_missing; ?></option>
                  <?php } ?>
                  <?php foreach ($order_statuses as $order_status) { ?>
                  <?php if ($order_status['order_status_id'] == $filter_order_status) { ?>
                  <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-filenumber">Country</label>
                <input type="text" name="filter_country" value="<?php echo $filter_country; ?>" placeholder="Customer Country" id="input-country" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-total"><?php echo $entry_total; ?></label>
                <input type="text" name="filter_total" value="<?php echo $filter_total; ?>" placeholder="<?php echo $entry_total; ?>" id="input-total" class="form-control" />
              </div>
              <div class="form-group" style="margin-top: 40px;">
                <?php if ($filter_bankdeposit == 'bankdeposit') { ?>
                <label>
                  <input name="filter_bankdeposit" type="checkbox" value="bankdeposit" checked="checked"></input>Bank Deposit Slip
                </label>
                <?php } else { ?>
                <label>
                  <input name="filter_bankdeposit" type="checkbox" value="bankdeposit"></input>Bank Deposit Slip
                </label>
                <?php } ?>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-date-added"><?php echo $entry_date_added; ?></label>
                <div class="input-group date">
                  <input type="text" name="filter_date_added" value="<?php echo $filter_date_added; ?>" placeholder="<?php echo $entry_date_added; ?>" data-date-format="YYYY-MM-DD" id="input-date-added" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-date-end">Date End</label>
                <div class="input-group date">
                  <input type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" placeholder="Date End" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-date-modified"><?php echo $entry_date_modified; ?></label>
                <div class="input-group date">
                  <input type="text" name="filter_date_modified" value="<?php echo $filter_date_modified; ?>" placeholder="<?php echo $entry_date_modified; ?>" data-date-format="YYYY-MM-DD" id="input-date-modified" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                  </span></div>
              </div>              
              <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
            </div>
          </div>		 
        </div>
		<div class="well">
		 <div class="row">
		   <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-order-status">Select Status</label>
                <select name="chnage_order_status" id="input-order-status-chnage" class="form-control">
                  <option value="*"></option>
                  <?php if ($filter_order_status == '0') { ?>
                  <option value="0" selected="selected"><?php echo $text_missing; ?></option>
                  <?php } else { ?>
                  <option value="0"><?php echo $text_missing; ?></option>
                  <?php } ?>
                  <?php foreach ($order_statuses as $order_status) { ?>
                  <?php if ($order_status['order_status_id'] == $filter_order_status) { ?>
                  <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
              </div>
            </div>
			<div class="col-sm-4">
				<button type="button" id="button-change-status" class="btn btn-primary" data-toggle="tooltip" title="Change Status" class="btn btn-info" style="margin-top: 23px;">Submit</button>
			</div>
		  </div>
		</div>
        <form method="post" enctype="multipart/form-data" id="form-order">
		  <input type='hidden' name='chnageorderstatus' id='chnageorderstatus'/>
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td class="text-center" style="width: 7%;"><?php if ($sort == 'o.order_id') { ?>
                    <a href="<?php echo $sort_order; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_order_id; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_order; ?>"><?php echo $column_order_id; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'customer') { ?>
                    <a href="<?php echo $sort_customer; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_customer; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_customer; ?>"><?php echo $column_customer; ?></a>
                    <?php } ?></td>
                  <td class="text-center" style="width: 4%;"><a href="javascript:return false;">CTRY</a></td>
                  <td class="text-center" style="width: 4%;"><?php if ($sort == 'status') { ?>
                    <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>">Stat</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_status; ?>">Stat</a>
                    <?php } ?></td>
                  <td class="text-center" style="width: 4%;">
                    <?php if ($sort == 'o.payment_method') { ?>
                    <a href="<?php echo $sort_payment_type; ?>" class="<?php echo strtolower($order); ?>">PT</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_payment_type; ?>">PT</a>
                    <?php } ?></td>
                  <td class="text-center" style="width: 3%;"><a href="javascript:return false;">PO</a></td>
				  <td class="text-center" style="width: 4%;"><a href="javascript:return false;">COM</a></td>
                  <td class="text-right" style="width: 6%;"><?php if ($sort == 'o.total') { ?>
                    <a href="<?php echo $sort_payment_type; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_total; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_total; ?>"><?php echo $column_total; ?></a>
                    <?php } ?></td>
                  <td class="text-center" style="width: 8%;"><?php if ($sort == 'o.date_added') { ?>
                    <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                    <?php } ?></td>
                  <td class="text-center" style="width: 8%;"><?php if ($sort == 'o.date_modified') { ?>
                    <a href="<?php echo $sort_date_modified; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_modified; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_date_modified; ?>"><?php echo $column_date_modified; ?></a>
                    <?php } ?></td>
                  <td class="text-right" style="width: 14%;"><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($orders) { ?>
                <?php foreach ($orders as $order) { ?>
                <tr>
                  <td class="text-center"><?php if (in_array($order['order_id'], $selected)) { ?>
                    <input type="checkbox" class="multiselect" name="selected[]" value="<?php echo $order['order_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" class="multiselect" name="selected[]" value="<?php echo $order['order_id']; ?>" />
                    <?php } ?>
                    <input type="hidden" name="shipping_code[]" value="<?php echo $order['shipping_code']; ?>" /></td>
                  <td class="text-center"><?php echo $order['order_no']; ?></td>
                  <td class="text-left">
                    <a href="<?php echo $order['customer_link']; ?>" target="_blank"><?php echo $order['customer']; ?>
                    </a>
                  </td>
                  <td class="text-center"><?php echo $order['country_code']; ?></td>
                  <td class="text-center"><img title='<?php echo $order["status"]; ?>' src='<?php echo $order["status_color"]; ?>'/></td>
                  <td class="text-center"><?php echo $order['payment_code']; ?></td>
                  <td class="text-center"><?php echo $order['pending_orders']; ?></td>
				  <td class="text-center"><?php echo $order['comment']; ?></td>
                  <td class="text-right"><?php echo $order['total']; ?></td>
                  <td class="text-center"><?php echo $order['date_added']; ?></td>
                  <td class="text-center"><?php echo $order['date_modified']; ?></td>
                  <td class="text-right">
					  <a href="<?php echo $order['view']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-info"><i class="fa fa-eye"></i></a> 
					  <a href="<?php echo $order['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a> 
					  <a href="<?php echo $order['invoice']; ?>" data-toggle="tooltip" title="<?php echo $button_invoice_print; ?>" class="btn btn-primary" target='_blank'><i class="fa fa-print"></i></a> 
					  <?php if(!$order['is_shipment']) { ?>
						<a href="<?php echo $order['aramex']; ?>" data-toggle="tooltip" title="<?php echo $button_aramex; ?>" class="btn btn-primary" target='_blank'><img src="image/ecommerce.png"/></a>
					  <?php } else { ?>	
						<a href="javascript:return false;" onclick="javascript:alert('Aramex document already created.')" data-toggle="tooltip" title="<?php echo $button_aramex; ?>" class="btn btn-primary" target='_blank'><img src="image/ecommerce.png"/></a>
					  <?php } ?>
					  <a href="<?php echo $order['delete']; ?>" id="button-delete<?php echo $order['order_id']; ?>" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
				  </td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="11"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    <!--
$('#button-filter').on('click', function() {
	url = 'index.php?route=sale/order&token=<?php echo $token; ?>';
	
	var filter_order_id = $('input[name=\'filter_order_id\']').val();
	
	if (filter_order_id) {
		url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
	}
	
	var filter_customer = $('input[name=\'filter_customer\']').val();
	
	if (filter_customer) {
		url += '&filter_customer=' + encodeURIComponent(filter_customer);
	}
  
  var filter_country = $('input[name=\'filter_country\']').val();
	
	if (filter_country) {
		url += '&filter_country=' + encodeURIComponent(filter_country);
	}
	
	var filter_order_status = $('select[name=\'filter_order_status\']').val();
	
	if (filter_order_status != '*') {
		url += '&filter_order_status=' + encodeURIComponent(filter_order_status);
	}	
  
  var filter_payment_method = $('select[name=\'filter_payment_method\']').val();
	
	if (filter_payment_method != '*') {
		url += '&filter_payment_method=' + encodeURIComponent(filter_payment_method);
	}	

	var filter_total = $('input[name=\'filter_total\']').val();

	if (filter_total) {
		url += '&filter_total=' + encodeURIComponent(filter_total);
	}	
	
	var filter_date_added = $('input[name=\'filter_date_added\']').val();
	
	if (filter_date_added) {
		url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
	}
  
  var filter_date_end = $('input[name=\'filter_date_end\']').val();
	
	if (filter_date_end) {
		url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
	}
	
	var filter_date_modified = $('input[name=\'filter_date_modified\']').val();
	
	if (filter_date_modified) {
		url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
	}
  
  var filter_bankdeposit = $('input[name=\'filter_bankdeposit\']:checked').val();//$('input[name=\'filter_bankdeposit\']'):checked;
	
	if (filter_bankdeposit) {
		url += '&filter_bankdeposit=' + encodeURIComponent(filter_bankdeposit);
	}
  
  var filter_banktransfer = $('select[name=\'filter_banktransfer\']').val();
	
	if (filter_banktransfer != '*') {
		url += '&filter_banktransfer=' + encodeURIComponent(filter_banktransfer);
	}
				
	location = url;
});
//--></script> 
  <script type="text/javascript"><!--
  $('#input-order-status-chnage').change(function(){
		$('#chnageorderstatus').val($(this).val());
  });
$('input[name=\'filter_customer\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=sale/customer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',			
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['customer_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_customer\']').val(item['label']);
	}	
});
//--></script> 
  <script type="text/javascript"><!--
$('input[name^=\'selected\']').on('change', function() {
	$('#button-shipping, #button-invoice, #button-sendsms').prop('disabled', true);
	
	var selected = $('input[name^=\'selected\']:checked');
	
	if (selected.length) {
		$('#button-invoice').prop('disabled', false);
    $('#button-sendsms').prop('disabled', false);
	}
	
	for (i = 0; i < selected.length; i++) {
		if ($(selected[i]).parent().find('input[name^=\'shipping_code\']').val()) {
			$('#button-shipping').prop('disabled', false);
			
			break;
		}
	}
});

$('input[name^=\'selected\']:first').trigger('change');

$('a[id^=\'button-delete\']').on('click', function(e) {
	e.preventDefault();
	
	if (confirm('<?php echo $text_confirm; ?>')) {
		location = $(this).attr('href');
	}
});
//--></script> 
  <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
$('#button-change-status').on('click', function() {
	var selectedIds =$('.multiselect:checked').map(function() {return this.value;}).get().join(',')
	$.ajax({
		url: 'index.php?route=sale/order/api&token=<?php echo $token; ?>&api=api/order/changeOrderStatus',
		type: 'post',
		dataType: 'json',
		data: 'order_status_id=' + encodeURIComponent($('select[name=\'chnage_order_status\']').val()) + '&notify= 1 &append = 0 &order_ids=' + encodeURIComponent(selectedIds),
		beforeSend: function() {
			$('#button-change-status').button('loading');			
		},
		complete: function() {
			$('#button-change-status').button('reset');	
		},
		success: function(json) {
			$('.alert').remove();
			
			if (json['error']) {
				$('#history').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			} 
		
			if (json['success']) {
				//$('#history').load('index.php?route=sale/order/history&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>');
				
				$('#history').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				//location.reload();
			}			
		},			
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

//--></script></div>
<?php echo $footer; ?>