<?php
class ModelShippingDHL extends Model {
	public function getQuote($address) {
		$this->load->language('shipping/dhl');

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('dhl_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

		if (!$this->config->get('dhl_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}		

		$error = '';

		$quote_data = array();
		
		if ($status) {

			$this->load->model('localisation/country');

			$countries = $this->model_localisation_country->getCountries();

			foreach($countries as $cnt){
				if($cnt['country_id'] == $this->config->get('dhl_country')){
					$country = $cnt['iso_code_2'];
					break;
				}
			}

			$pieces = $this->wf_get_package_piece();

			$mailing_date = date('Y-m-d', time());
			$mailing_datetime = date('Y-m-d', time()) . 'T' . date('H:i:s', time());

			$url = 'https://xmlpi-ea.dhl.com/XMLShippingServlet';

			$is_dutiable = ($address['iso_code_2'] == $country || $this->wf_dhl_is_eu_country($country, $address['iso_code_2'])) ? "N" : "Y";

        	$dutiable_content = $is_dutiable == "Y" ? "<Dutiable><DeclaredCurrency>USD</DeclaredCurrency><DeclaredValue>{$this->cart->getSubTotal()}</DeclaredValue></Dutiable>" : "";

        	if($address['postcode'] != 0){
        		$to = "<Postalcode>{$address['postcode']}</Postalcode>";
        	} else {
        		$to = "<City>{$address['city']}</City>";
        	}

$xmlRequest = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<p:DCTRequest xmlns:p="http://www.dhl.com" xmlns:p1="http://www.dhl.com/datatypes" xmlns:p2="http://www.dhl.com/DCTRequestdatatypes" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dhl.com DCT-req.xsd ">
  <GetQuote>
    <Request>
		<ServiceHeader>
			<MessageTime>{$mailing_datetime}</MessageTime>
			<MessageReference>1234567890123456789012345678901</MessageReference>
			<SiteID>{$this->config->get('dhl_site_id')}</SiteID>
			<Password>{$this->config->get('dhl_site_pwd')}</Password>
		</ServiceHeader>
    </Request>
    <From>
	  <CountryCode>{$country}</CountryCode>
	  <Postalcode>{$this->config->get('dhl_postcode')}</Postalcode>
	  <City>{$this->config->get('dhl_city')}</City>
    </From>
    <BkgDetails>
      <PaymentCountryCode>{$country}</PaymentCountryCode>
      <Date>{$mailing_date}</Date>
      <ReadyTime>PT10H21M</ReadyTime>
      <DimensionUnit>CM</DimensionUnit>
      <WeightUnit>KG</WeightUnit>
      <Pieces>{$pieces}</Pieces>
	  <IsDutiable>{$is_dutiable}</IsDutiable>
	  <NetworkTypeCode>AL</NetworkTypeCode>
	  </BkgDetails>
    <To>
      <CountryCode>{$address['iso_code_2']}</CountryCode>
	  {$to}
    </To>
	{$dutiable_content}
  </GetQuote>
</p:DCTRequest>
XML;

			$curl = curl_init($url);

			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $xmlRequest);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_TIMEOUT, 60);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

			$xml = curl_exec($curl);

			curl_close($curl);

			$result = simplexml_load_string($xml);
		
			if(isset($result->GetQuoteResponse->BkgDetails->QtdShp)){

				foreach ($result->GetQuoteResponse->BkgDetails->QtdShp as $shipping) {
					$cost = 0;
					if(isset($shipping->QtdSInAdCur)){
						$title = (string)$shipping->ProductShortName[0];

						$code = str_replace(' ', '', strtolower((string)$shipping->ProductShortName[0]));

						$days = (string)$shipping->TotalTransitDays[0] + (string)$shipping->PickupPostalLocAddDays[0];
				
						$costs = $shipping->QtdSInAdCur;
						
						foreach ($costs as $key => $value) {
							if($value->CurrencyCode[0] == 'USD'){
								$cost = (string)$value->TotalAmount[0];
								$currency = 'USD';
							}
						}
						if(!$cost){
							if($value->CurrencyCode[0] == 'EUR'){
								$cost = (string)$value->TotalAmount[0];
								$currency = 'EUR';
							}
						}
						if(!$cost){
							if($value->CurrencyCode[0] == 'GBP'){
								$cost = (string)$value->TotalAmount[0];
								$currency = 'GBP';
							}
						}
						$quote_data[$code] = array(
							'code'         => 'dhl.'.$code,
							'title'        => $title,
							'cost'         => $this->currency->convert($cost, $currency, $this->session->data['currency']),
							'tax_class_id' => $this->config->get('dhl_tax_class_id'),
							'text'         => $this->currency->format($this->currency->convert($cost, $currency, $this->session->data['currency']), $this->session->data['currency'], 1.0000000)
						);
					}
				}

			} else {
				$error = 'error';	
			}
			
		}
		
		$method_data = array();
		if ($quote_data) {
			$method_data = array(
				'code'       => 'dhl',
				'title'      => $this->language->get('text_title'),
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('dhl_sort_order'),
				'error'      => $error
			);
		}

		return $method_data;
	}

	private function wf_dhl_is_eu_country ($countrycode, $destinationcode) {

		$eu_countrycodes = array(

			'AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 

			'ES', 'FI', 'FR', 'GB', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV',

			'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK',

			'HR', 'GR'



			);

		return(in_array($countrycode, $eu_countrycodes) && in_array($destinationcode, $eu_countrycodes));

	}
	private function wf_get_package_piece() {

        $pieces = "";
        $products = $this->cart->getProducts();
        foreach ($products as $key => $parcel) {
        		
                $index = $key + 1;
                $pieces .= '<Piece><PieceID>' . $index . '</PieceID>';
                $pieces .= '<PackageTypeCode>BOX</PackageTypeCode>';
                if( !empty($parcel['length']) && !empty($parcel['width']) && !empty($parcel['height']) ){

                	if($parcel['length_class_id'] == 2){
	                	$parcel['height'] = $parcel['height'] / 10;
	                	$parcel['length'] = $parcel['length'] / 10;
	                	$parcel['width'] = $parcel['width'] / 10;
	                } elseif($parcel['length_class_id'] == 3){
	                	$parcel['height'] = $parcel['height'] * 2.54;
	                	$parcel['length'] = $parcel['length'] * 2.54;
	                	$parcel['width'] = $parcel['width'] * 2.54;
	                }
                    $pieces .= '<Height>' . round($parcel['height'] * $parcel['quantity'], 3) . '</Height>';
                    $pieces .= '<Depth>' . round($parcel['length'], 3) . '</Depth>';
                    $pieces .= '<Width>' . round($parcel['width'], 3) . '</Width>';
                }
                if($parcel['weight_class_id'] == 1){
                	$weight = $parcel['weight'];
                } elseif($parcel['weight_class_id'] == 2){
                	$weight = $parcel['weight'] * 1000;
                } elseif($parcel['weight_class_id'] == 5){
                	$weight = $parcel['weight'] * 0.45359;
                } elseif($parcel['weight_class_id'] == 6){
                	$weight = $parcel['weight'] * 0.028349;
                }
                $pieces .= '<Weight>' . round($weight, 3) . '</Weight></Piece>';
        }
        
        return $pieces;
    }
}