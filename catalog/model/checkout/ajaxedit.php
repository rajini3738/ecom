<?php
class ModelCheckoutAjaxedit extends Model {
		
		public function addDiscount($discount_data) {
			
			$json = array();
			$code = 'customdiscount';
			
			$this->language->load('sale/order');
			
			$order_id = $discount_data['order_id'];
			
			$this->createCustomDiscountTable();
			
			$discount_value = $this->cleanValue($discount_data['value'], $order_id);
			
			/*
			$order_totals = $this->getOrderTotalCodes($order_id);
			$old_order_totals = $this->getOldTotals($order_totals, $order_id);
			$order_products = $this->getOrderProducts2($order_id);
			
			if (empty($discount_data['remove'])) {
				$discount_value = $this->cleanValue($discount_data['value'], $order_id);
			} else {
				$discount_value = -$old_order_totals[$code];
			}
			$taxes = in_array('tax', $order_totals) ? $old_order_totals['tax'] : 0 ;
			$discount_total = 0;
			
			if ($discount_data['type'] == 'P') {
			
				foreach ($order_products as $product) {
					$discount = 0;
			
					$discount = $product['total'] / 100 * $discount_value;
							
					if (in_array('tax', $order_totals) && $product['tax_class_id']) {
						$tax_rates = $this->tax->getRates($product['total'] - ($product['total'] - $discount), $product['tax_class_id']);

						foreach ($tax_rates as $tax_rate) {
							if ($tax_rate['type'] == 'P') {
								$taxes -= $tax_rate['amount'];
							}
						}
					}

					$discount_total += $discount;
				}
			
			} elseif ($discount_data['type'] == 'F' || (empty($discount_data['type']) && $discount_data['remove'])) {
		
				$discount = $discount_value;
				$discount_partial = $discount/(count($order_products));
				
				foreach ($order_products as $product) {
							
					if (in_array('tax', $order_totals) && $product['tax_class_id']) {
						$tax_rates = $this->tax->getRates($product['total'] - ($product['total'] - $discount_partial), $product['tax_class_id']);

						foreach ($tax_rates as $tax_rate) {
							if ($tax_rate['type'] == 'P') {
								if (empty($discount_data['remove'])) {
									$taxes -= $tax_rate['amount'];
								} else {
									$taxes += $tax_rate['amount'];
								}
								
							}
						}
					}
				}
				$discount_total = $discount;
			}
			
			$old_tax = in_array('tax', $order_totals) ? $old_order_totals['tax'] : 0;
			$new_tax = in_array('tax', $order_totals) ? $taxes : 0 ;
			
			if (empty($discount_data['remove'])) {
				foreach ($order_totals as $key) {
					$new_order_totals[$key] = $old_order_totals[$key];
				}
			} else {
				foreach ($order_totals as $key) {
					if ($key != $code ) {
						$new_order_totals[$key] = $old_order_totals[$key];
					}
				}
			}
			
			if (isset($new_order_totals['tax'])) {
				$new_order_totals['tax'] = in_array('tax', $order_totals) ? $old_order_totals['tax'] - $old_tax + $new_tax : 0;
			}
			
			if ($discount_data['remove']) {
				$new_order_totals['total'] =  $old_order_totals['total'] - $old_tax + $new_tax + $discount_total;
			} elseif (in_array($code, $order_totals)) {
				$new_order_totals['total'] =  $old_order_totals['total'] - $old_tax + $new_tax + $old_order_totals[$code] - $discount_total;
			} else {
				$new_order_totals['total'] =  $old_order_totals['total'] - $old_tax + $new_tax - $discount_total;
			}	
			
			$discount_total = -$discount_total;
			*/
			
			
			if ($discount_data['remove']) {
				//$this->db->query("DELETE FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' AND code = '" . $code . "' ");
				$this->db->query("DELETE FROM `" . DB_PREFIX . "order_custom_discount` WHERE order_id = '" . (int)$order_id . "'");
			//} elseif (in_array($code, $order_totals)) {
			//	$this->db->query("UPDATE `" . DB_PREFIX . "order_total` SET title = '" . $this->db->escape($discount_data['title']) . "', value = '" . (float)$discount_total . "' WHERE order_id = '" . (int)$order_id . "' AND code = '" . $code . "' ");
			} else {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "order_custom_discount` SET order_id = '" . (int)$order_id . "', type = '" . $this->db->escape($discount_data['type']) . "', title = '" . $this->db->escape($discount_data['title']) . "', discount = '" . (float)$discount_value . "', sort_order = '" . (int)$discount_data['sort_order'] . "'");
				//$this->db->query("INSERT INTO `" . DB_PREFIX . "order_total` SET order_id = '" . (int)$order_id . "', code = '" . $code . "', title = '" . $this->db->escape($discount_data['title']) . "', value = '" . (float)$discount_total . "', sort_order = '" . (int)$discount_data['sort_order'] . "' ");
			}
			
			/*
			$currency = $this->getCurrency($order_id);
			$new_order_totals['currency_code'] = $currency['currency_code'];
			$new_order_totals['currency_value'] = $currency['currency_value'];
			*/
			
			//return $this->rewriteTotals($new_order_totals);
			return true;
		}
		
		private function createCustomDiscountTable() {
		
			$sql  = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX ."order_custom_discount`";
			$sql .= " (`order_custom_discount_id` int(11) NOT NULL AUTO_INCREMENT, `order_id` int(11) NOT NULL, `type` char(1) NOT NULL,";
  			$sql .= " `discount` decimal(15,4) NOT NULL DEFAULT '0.0000', `title` varchar(255) NOT NULL, `sort_order` INT(3) NOT NULL DEFAULT '2',";
  			$sql .= " PRIMARY KEY (`order_custom_discount_id`), KEY `order_id` (`order_id`) ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;";
			$this->db->query($sql);
			return true;
		}
		
		public function getCustomDiscount() {
			if (!empty($_SESSION['ajaxedit_order_id'])) {
				$query = $this->db->query("SELECT * FROM `" . DB_PREFIX. "order_custom_discount` WHERE `order_id` = '" . (int)$_SESSION['ajaxedit_order_id'] . "'");
				return !empty($query->num_rows) ? $query->row : false ;
			} else {
			 	return false;
			}
		}
		
		public function setOrderTotals($order_totals, $new_order_totals, $order_id) {
			
			foreach ($order_totals as $total) {
				if (!empty($new_order_totals[$total])) {
					$sql = "UPDATE `" . DB_PREFIX . "order_total` SET `value` = '" . $new_order_totals[$total] . "' WHERE `order_id` = '" . (int)$order_id . "' AND `code` = '" . $total . "'";
					$this->db->query($sql);
				}
			}
			
			if (isset($new_order_totals['total'])) {
				$this->db->query("UPDATE `" . DB_PREFIX . "order` SET `total` = '" . $new_order_totals['total'] . "', date_modified = NOW() WHERE `order_id` = '" . (int)$order_id . "'");
			}
		}
		
		public function editOrderTotals ($order_totals, $order_id) {
		
			$this->db->query("DELETE FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int)$order_id . "'");

			if (isset($order_totals)) {
				foreach ($order_totals as $total) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET order_id = '" . (int)$order_id . "', code = '" . $this->db->escape($total['code']) . "', title = '" . $this->db->escape($total['title']) . "', `value` = '" . (float)$total['value'] . "', sort_order = '" . (int)$total['sort_order'] . "'");
					
					if ($total['code'] == 'total') {
						$this->db->query("UPDATE `" . DB_PREFIX . "order` SET `total` = '" . $total['value'] . "', date_modified = NOW() WHERE `order_id` = '" . (int)$order_id . "'");
					}
				
				}
			}
		}
		
		/*
		public function getCustomDiscount($order_id){
			$query = $this->db->query("SELECT * FROM `" . DB_PREFIX. "order_total` WHERE `code` = 'customdiscount' AND `order_id` = '" . (int)$order_id . "'");
			return !empty($query->num_rows) ? $query->row : false ;
		}
		*/
		
		protected function getCurrency($order_id){
			$query = $this->db->query("SELECT currency_code, currency_value FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$order_id . "'");
			
			return $query->row;
		}
		
		protected function getOrderProducts2($order_id) {
			$query = $this->db->query("SELECT op.*, p.tax_class_id FROM " . DB_PREFIX . "order_product op INNER JOIN " . DB_PREFIX . "product p ON op.product_id = p.product_id WHERE order_id = '" . (int)$order_id . "'");

			return $query->rows;
		}
		
		protected function getOrderTotalCodes($order_id) {
			$query = $this->db->query("SELECT code FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "'");
			
			$order_totals = array();
			foreach ($query->rows as $code) {
				foreach ($code as $value) {
					$order_totals[] = $value;
				}
			}
			return $order_totals;
		}
		
		protected function cleanValue($price, $order_id) {
			
			$query = $this->db->query("SELECT o.currency_id, c.symbol_left, c.symbol_right FROM `" . DB_PREFIX . "order` o INNER JOIN `" . DB_PREFIX . "currency` c ON o.currency_id = c.currency_id WHERE order_id = '" . (int)$order_id . "'");		
			$currency_data = $query->row;
			
			$currency_symbol = empty($currency_data['symbol_left']) ? $currency_data['symbol_right'] : $currency_data['symbol_left'] ;
			$regex = '/\\' . $currency_symbol . '[\$\€\£]/';
			
			$price = preg_replace($regex, '' , $price);
			 
			return $price;
		}
		
		protected function getOldTotals($order_totals, $order_id) {
			
			// Get old Order Total values
			
			$old_order_totals = array();
			
			foreach ($order_totals as $total) {
				$query = $this->db->query("SELECT value FROM `" . DB_PREFIX . "order_total` WHERE `order_id` = '" . (int)$order_id . "' AND `code` = '" . $total . "'");
				$old_order_totals[$total] = $query->row['value'];
			}
			
			return $old_order_totals;
		}

		public function resendConfirmEmail ($order_id, $order_status_id, $comment = '', $notify = true) {
			$this->event->trigger('pre.order.confirmation.resend', $order_id);
			
			$this->load->model('checkout/order');
			$order_info = $this->model_checkout_order->getOrder($order_id);

			if ($order_info) {
				// Fraud Detection
				$this->load->model('account/customer');

				$customer_info = $this->model_account_customer->getCustomer($order_info['customer_id']);

				if ($customer_info && $customer_info['safe']) {
					$safe = true;
				} else {
					$safe = false;
				}

				if ($this->config->get('config_fraud_detection')) {
					$this->load->model('checkout/fraud');

					$risk_score = $this->model_checkout_fraud->getFraudScore($order_info);

					if (!$safe && $risk_score > $this->config->get('config_fraud_score')) {
						$order_status_id = $this->config->get('config_fraud_status_id');
					}
				}

				// Ban IP
				if (!$safe) {
					$status = false;

					if ($order_info['customer_id']) {
						$results = $this->model_account_customer->getIps($order_info['customer_id']);

						foreach ($results as $result) {
							if ($this->model_account_customer->isBanIp($result['ip'])) {
								$status = true;

								break;
							}
						}
					} else {
						$status = $this->model_account_customer->isBanIp($order_info['ip']);
					}

					if ($status) {
						$order_status_id = $this->config->get('config_order_status_id');
					}
				}

				$this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '" . (int)$order_status_id . "', date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");

				$this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET order_id = '" . (int)$order_id . "', order_status_id = '" . (int)$order_status_id . "', notify = '" . (int)$notify . "', comment = '" . $this->db->escape($comment) . "', date_added = NOW()");

				if ($order_status_id) {
					// Check for any downloadable products
					$download_status = false;

					$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

					foreach ($order_product_query->rows as $order_product) {
						// Check if there are any linked downloads
						$product_download_query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "product_to_download` WHERE product_id = '" . (int)$order_product['product_id'] . "'");

						if ($product_download_query->row['total']) {
							$download_status = true;
						}
					}

					// Load the language for any mails that might be required to be sent out
					$language = new Language($order_info['language_directory']);
					$language->load('default');
					$language->load('mail/order');

					$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_status_id . "' AND language_id = '" . (int)$order_info['language_id'] . "'");

					if ($order_status_query->num_rows) {
						$order_status = $order_status_query->row['name'];
					} else {
						$order_status = '';
					}

					$subject = sprintf($language->get('text_update_subject'), $order_info['store_name'], $order_id);

					// HTML Mail
					$data = array();

					$data['title'] = sprintf($language->get('text_new_subject'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'), $order_id);

					$data['text_greeting'] = sprintf($language->get('text_new_greeting'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8'));
					$data['text_link'] = $language->get('text_new_link');
					$data['text_download'] = $language->get('text_new_download');
					$data['text_order_detail'] = $language->get('text_new_order_detail');
					$data['text_instruction'] = $language->get('text_new_instruction');
					$data['text_order_id'] = $language->get('text_new_order_id');
					$data['text_date_added'] = $language->get('text_new_date_added');
					$data['text_payment_method'] = $language->get('text_new_payment_method');
					$data['text_shipping_method'] = $language->get('text_new_shipping_method');
					$data['text_email'] = $language->get('text_new_email');
					$data['text_telephone'] = $language->get('text_new_telephone');
					$data['text_ip'] = $language->get('text_new_ip');
					$data['text_order_status'] = $language->get('text_new_order_status');
					$data['text_payment_address'] = $language->get('text_new_payment_address');
					$data['text_shipping_address'] = $language->get('text_new_shipping_address');
					$data['text_product'] = $language->get('text_new_product');
					$data['text_model'] = $language->get('text_new_model');
					$data['text_quantity'] = $language->get('text_new_quantity');
					$data['text_price'] = $language->get('text_new_price');
					$data['text_total'] = $language->get('text_new_total');
					$data['text_footer'] = $language->get('text_new_footer');

					$data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');
					$data['store_name'] = $order_info['store_name'];
					$data['store_url'] = $order_info['store_url'];
					$data['customer_id'] = $order_info['customer_id'];
					$data['link'] = $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id;

					if ($download_status) {
						$data['download'] = $order_info['store_url'] . 'index.php?route=account/download';
					} else {
						$data['download'] = '';
					}

					$data['order_id'] = $order_id;
					$data['date_added'] = date($language->get('date_format_short'), strtotime($order_info['date_added']));
					$data['payment_method'] = $order_info['payment_method'];
					$data['shipping_method'] = $order_info['shipping_method'];
					$data['email'] = $order_info['email'];
					$data['telephone'] = $order_info['telephone'];
					$data['ip'] = $order_info['ip'];
					$data['order_status'] = $order_status;

					if ($comment && $notify) {
						$data['comment'] = nl2br($comment);
					} else {
						$data['comment'] = '';
					}

					if ($order_info['payment_address_format']) {
						$format = $order_info['payment_address_format'];
					} else {
						$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
					}

					$find = array(
						'{firstname}',
						'{lastname}',
						'{company}',
						'{address_1}',
						'{address_2}',
						'{city}',
						'{postcode}',
						'{zone}',
						'{zone_code}',
						'{country}'
					);

					$replace = array(
						'firstname' => $order_info['payment_firstname'],
						'lastname'  => $order_info['payment_lastname'],
						'company'   => $order_info['payment_company'],
						'address_1' => $order_info['payment_address_1'],
						'address_2' => $order_info['payment_address_2'],
						'city'      => $order_info['payment_city'],
						'postcode'  => $order_info['payment_postcode'],
						'zone'      => $order_info['payment_zone'],
						'zone_code' => $order_info['payment_zone_code'],
						'country'   => $order_info['payment_country']
					);

					$data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

					if ($order_info['shipping_address_format']) {
						$format = $order_info['shipping_address_format'];
					} else {
						$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
					}

					$find = array(
						'{firstname}',
						'{lastname}',
						'{company}',
						'{address_1}',
						'{address_2}',
						'{city}',
						'{postcode}',
						'{zone}',
						'{zone_code}',
						'{country}'
					);

					$replace = array(
						'firstname' => $order_info['shipping_firstname'],
						'lastname'  => $order_info['shipping_lastname'],
						'company'   => $order_info['shipping_company'],
						'address_1' => $order_info['shipping_address_1'],
						'address_2' => $order_info['shipping_address_2'],
						'city'      => $order_info['shipping_city'],
						'postcode'  => $order_info['shipping_postcode'],
						'zone'      => $order_info['shipping_zone'],
						'zone_code' => $order_info['shipping_zone_code'],
						'country'   => $order_info['shipping_country']
					);

					$data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

					$this->load->model('tool/upload');

					// Products
					$data['products'] = array();

					foreach ($order_product_query->rows as $product) {
						$option_data = array();

						$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");

						foreach ($order_option_query->rows as $option) {
							if ($option['type'] != 'file') {
								$value = $option['value'];
							} else {
								$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

								if ($upload_info) {
									$value = $upload_info['name'];
								} else {
									$value = '';
								}
							}

							$option_data[] = array(
								'name'  => $option['name'],
								'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
							);
						}

						$data['products'][] = array(
							'name'     => $product['name'],
							'model'    => $product['model'],
							'option'   => $option_data,
							'quantity' => $product['quantity'],
							'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
							'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
						);
					}

					// Vouchers
					$data['vouchers'] = array();

					$order_voucher_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");

					foreach ($order_voucher_query->rows as $voucher) {
						$data['vouchers'][] = array(
							'description' => $voucher['description'],
							'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']),
						);
					}

					// Order Totals
					$order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order ASC");

					foreach ($order_total_query->rows as $total) {
						$data['totals'][] = array(
							'title' => $total['title'],
							'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
						);
					}

					if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/order.tpl')) {
						$html = $this->load->view($this->config->get('config_template') . '/template/mail/order.tpl', $data);
					} else {
						$html = $this->load->view('default/template/mail/order.tpl', $data);
					}

					// Can not send confirmation emails for CBA orders as email is unknown
					$this->load->model('payment/amazon_checkout');

					if (!$this->model_payment_amazon_checkout->isAmazonOrder($order_info['order_id'])) {
						// Text Mail
						$text  = sprintf($language->get('text_new_greeting'), html_entity_decode($order_info['store_name'], ENT_QUOTES, 'UTF-8')) . "\n\n";
						$text .= $language->get('text_new_order_id') . ' ' . $order_id . "\n";
						$text .= $language->get('text_new_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n";
						$text .= $language->get('text_new_order_status') . ' ' . $order_status . "\n\n";

						if ($comment && $notify) {
							$text .= $language->get('text_new_instruction') . "\n\n";
							$text .= $comment . "\n\n";
						}

						// Products
						$text .= $language->get('text_new_products') . "\n";

						foreach ($order_product_query->rows as $product) {
							$text .= $product['quantity'] . 'x ' . $product['name'] . ' (' . $product['model'] . ') ' . html_entity_decode($this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";

							$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . $product['order_product_id'] . "'");

							foreach ($order_option_query->rows as $option) {
								if ($option['type'] != 'file') {
									$value = $option['value'];
								} else {
									$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

									if ($upload_info) {
										$value = $upload_info['name'];
									} else {
										$value = '';
									}
								}

								$text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value) . "\n";
							}
						}

						foreach ($order_voucher_query->rows as $voucher) {
							$text .= '1x ' . $voucher['description'] . ' ' . $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']);
						}

						$text .= "\n";

						$text .= $language->get('text_new_order_total') . "\n";

						foreach ($order_total_query->rows as $total) {
							$text .= $total['title'] . ': ' . html_entity_decode($this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";
						}

						$text .= "\n";

						if ($order_info['customer_id']) {
							$text .= $language->get('text_new_link') . "\n";
							$text .= $order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id . "\n\n";
						}

						if ($download_status) {
							$text .= $language->get('text_new_download') . "\n";
							$text .= $order_info['store_url'] . 'index.php?route=account/download' . "\n\n";
						}

						// Comment
						if ($order_info['comment']) {
							$text .= $language->get('text_new_comment') . "\n\n";
							$text .= $order_info['comment'] . "\n\n";
						}

						$text .= $language->get('text_new_footer') . "\n\n";

						$mail = new Mail();
						$mail->protocol = $this->config->get('config_mail_protocol');
						$mail->parameter = $this->config->get('config_mail_parameter');
						$mail->smtp_hostname = $this->config->get('config_mail_smtp_host');
						$mail->smtp_username = $this->config->get('config_mail_smtp_username');
						$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
						$mail->smtp_port = $this->config->get('config_mail_smtp_port');
						$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
			
						$mail->setTo($order_info['email']);
						$mail->setFrom($this->config->get('config_email'));
						$mail->setSender($order_info['store_name']);
						$mail->setSubject($subject);
						$mail->setHtml($html);
						$mail->setText($text);
						$mail->send();
					}

					// Admin Alert Mail
					if ($this->config->get('config_order_mail')) {
						$subject = sprintf($language->get('text_new_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'), $order_id);

						// HTML Mail
						$data['text_greeting'] = $language->get('text_new_received');

						if ($comment) {
							if ($order_info['comment']) {
								$data['comment'] = nl2br($comment) . '<br/><br/>' . $order_info['comment'];
							} else {
								$data['comment'] = nl2br($comment);
							}
						} else {
							if ($order_info['comment']) {
								$data['comment'] = $order_info['comment'];
							} else {
								$data['comment'] = '';
							}
						}
						$data['text_download'] = '';

						$data['text_footer'] = '';

						$data['text_link'] = '';
						$data['link'] = '';
						$data['download'] = '';

						if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/mail/order.tpl')) {
							$html = $this->load->view($this->config->get('config_template') . '/template/mail/order.tpl', $data);
						} else {
							$html = $this->load->view('default/template/mail/order.tpl', $data);
						}

						// Text
						$text  = $language->get('text_new_received') . "\n\n";
						$text .= $language->get('text_new_order_id') . ' ' . $order_id . "\n";
						$text .= $language->get('text_new_date_added') . ' ' . date($language->get('date_format_short'), strtotime($order_info['date_added'])) . "\n";
						$text .= $language->get('text_new_order_status') . ' ' . $order_status . "\n\n";
						$text .= $language->get('text_new_products') . "\n";

						foreach ($order_product_query->rows as $product) {
							$text .= $product['quantity'] . 'x ' . $product['name'] . ' (' . $product['model'] . ') ' . html_entity_decode($this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";

							$order_option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . $product['order_product_id'] . "'");

							foreach ($order_option_query->rows as $option) {
								if ($option['type'] != 'file') {
									$value = $option['value'];
								} else {
									$value = utf8_substr($option['value'], 0, utf8_strrpos($option['value'], '.'));
								}

								$text .= chr(9) . '-' . $option['name'] . ' ' . (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value) . "\n";
							}
						}

						foreach ($order_voucher_query->rows as $voucher) {
							$text .= '1x ' . $voucher['description'] . ' ' . $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']);
						}

						$text .= "\n";

						$text .= $language->get('text_new_order_total') . "\n";

						foreach ($order_total_query->rows as $total) {
							$text .= $total['title'] . ': ' . html_entity_decode($this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']), ENT_NOQUOTES, 'UTF-8') . "\n";
						}

						$text .= "\n";

						if ($order_info['comment']) {
							$text .= $language->get('text_new_comment') . "\n\n";
							$text .= $order_info['comment'] . "\n\n";
						}

						$mail = new Mail();
						$mail->protocol = $this->config->get('config_mail_protocol');
						$mail->parameter = $this->config->get('config_mail_parameter');
						$mail->smtp_hostname = $this->config->get('config_mail_smtp_host');
						$mail->smtp_username = $this->config->get('config_mail_smtp_username');
						$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
						$mail->smtp_port = $this->config->get('config_mail_smtp_port');
						$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
			
						$mail->setTo($this->config->get('config_email'));
						$mail->setFrom($this->config->get('config_email'));
						$mail->setReplyTo($order_info['email']);
						$mail->setSender($order_info['store_name']);
						$mail->setSubject($subject);
						$mail->setHtml($html);
						$mail->setText($text);
						$mail->send();

						// Send to additional alert emails
						$emails = explode(',', $this->config->get('config_mail_alert'));

						foreach ($emails as $email) {
							if ($email && preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $email)) {
								$mail->setTo($email);
								$mail->send();
							}
						}
					}
				}
			}

			$this->event->trigger('post.order.confirmation.resend', $order_id);
			return 'Confirmation email was sent successfully!';
			//return $html;
		}
		
		protected function rewriteTotals($totals) {
			
		//$this->language->load('sale/order');
		$json = array();
		
		if (isset($totals['currency_code']) && isset($totals['currency_value'])) {
			if (isset($totals['price'])) {
				$json['price'] = $this->currency->format($totals['price'], $totals['currency_code'], $totals['currency_value']);
			}
			if (isset($totals['product_total'])) {
				$json['product_total'] = $this->currency->format($totals['product_total'], $totals['currency_code'], $totals['currency_value']);
			}
			if (isset($totals['sub_total'])) {
				$json['sub_total'] = $this->currency->format($totals['sub_total'], $totals['currency_code'], $totals['currency_value']);
			}
			if (isset($totals['shipping'])) {
				$json['shipping'] = $this->currency->format($totals['shipping'], $totals['currency_code'], $totals['currency_value']);
			}
			if (isset($totals['tax'])) {
				$json['tax'] = $this->currency->format($totals['tax'], $totals['currency_code'], $totals['currency_value']);
			}
			if (isset($totals['total'])) {
				$json['total'] = $this->currency->format($totals['total'], $totals['currency_code'], $totals['currency_value']);
			}
			if (isset($totals['discount'])) {
				$json['discount'] = $this->currency->format($totals['discount'], $totals['currency_code'], $totals['currency_value']);
			}
			if (isset($totals['debug'])) {
				$json['debug'] = $totals['debug'];
			}
			//$json['msg'] = $this->language->get('text_success');
		} else {
			//$json['error'] = $this->language->get('error_currency_format');
		}
		return $json;
	}
}