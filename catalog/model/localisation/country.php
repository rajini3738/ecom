<?php
class ModelLocalisationCountry extends Model {
	public function getCountry($country_id) {
		//$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$country_id . "' AND status = '1'");
		$query = $this->db->query("SELECT cd.country_id,cd.language_id,cd.name,c.iso_code_2,c.iso_code_3,c.address_format,c.postcode_required,c.sort_order,c.status FROM " . DB_PREFIX . "country_description cd LEFT JOIN " . DB_PREFIX . "country c ON (cd.country_id = c.country_id) WHERE c.country_id = '" . (int)$country_id . "' AND c.status = '1' AND cd.language_id = '2' ORDER BY cd.name ASC");

		return $query->row;
	}

	public function getCountries() {
		///$country_data = $this->cache->get('country.status');

		//if (!$country_data) {
			//$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "country WHERE status = '1' ORDER BY name ASC");
			$query = $this->db->query("SELECT cd.country_id,cd.language_id,cd.name,c.iso_code_2,c.iso_code_3,c.address_format,c.postcode_required,c.sort_order,c.status FROM oc_country_description cd LEFT JOIN oc_country c ON (cd.country_id = c.country_id) WHERE c.status = '1' AND cd.language_id = '2' ORDER BY FIELD(cd.country_id, 17, 114, 161, 173, 184, 221) DESC");
			$country_data = $query->rows;
			$this->cache->set('country.status', $country_data);
		//}
		return $country_data;
	}
}