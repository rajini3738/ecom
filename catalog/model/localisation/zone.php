<?php
class ModelLocalisationZone extends Model {
	public function getZone($zone_id) {
		//$query = $this->db->query("SELECT zd.zone_id, zd.language_id, zd.name, z.country_id, z.code, z.sort_order, z.status FROM " . DB_PREFIX . "zone_description zd LEFT JOIN " . DB_PREFIX . "zone z ON (zd.zone_id = z.zone_id) WHERE z.zone_id = '" . (int)$zone_id . "' AND z.status = '1' AND zd.language_id = '2' ORDER BY zd.name ASC");
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone WHERE zone_id = '" . (int)$zone_id . "' AND status = '1'");

		return $query->row;
	}

	public function getZonesByCountryId($country_id) {
		//$zone_data = $this->cache->get('zone.' . (int)$country_id);

		//if (!$zone_data) {
			//$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone WHERE country_id = '" . (int)$country_id . "' AND status = '1' ORDER BY name");

			$query = $this->db->query("SELECT zd.zone_id, zd.language_id, zd.name, z.country_id, z.code, z.sort_order, z.status FROM " . DB_PREFIX . "zone_description zd LEFT JOIN " . DB_PREFIX . "zone z ON (zd.zone_id = z.zone_id) WHERE z.country_id = '" . (int)$country_id . "' AND z.status = '1' AND zd.language_id = '2' ORDER BY zd.name ASC");
			if (!$query->num_rows) {
				$query = $this->db->query("SELECT zd.zone_id, zd.language_id, zd.name, z.country_id, z.code, z.sort_order, z.status FROM " . DB_PREFIX . "zone_description zd LEFT JOIN " . DB_PREFIX . "zone z ON (zd.zone_id = z.zone_id) WHERE z.country_id = '" . (int)$country_id . "' AND z.status = '1' AND zd.language_id = '1' ORDER BY zd.name ASC");
			}
			$zone_data = $query->rows;

			//$this->cache->set('zone.' . (int)$country_id, $zone_data);
		//}

		return $zone_data;
	}
	public function getZone1($zone_id) {
		$query = $this->db->query("SELECT zd.zone_id, zd.language_id, zd.name, z.country_id, z.code, z.sort_order, z.status FROM " . DB_PREFIX . "zone_description zd LEFT JOIN " . DB_PREFIX . "zone z ON (zd.zone_id = z.zone_id) WHERE z.zone_id = '" . (int)$zone_id . "' AND z.status = '1' AND zd.language_id = '2' ORDER BY zd.name ASC");
		//$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone WHERE zone_id = '" . (int)$zone_id . "' AND status = '1' AND language_id = '2'");

		return $query->row;
	}
}