<?php
class ModelCompetitionCompetition extends Model {
	public function getCompetitionDetails($competition_id) {
	//	$query = $this->db->query("SELECT DISTINCT *, (SELECT name FROM " . DB_PREFIX . "competition_winners WHERE competition_id='" . (int)$competition_id . "') AS winner FROM " . DB_PREFIX . "competition c LEFT JOIN " . DB_PREFIX . "competition_description cd ON (c.competition_id = cd.competition_id) LEFT JOIN " . DB_PREFIX . "competition_to_store c2s ON (c.competition_id = c2s.competition_id) WHERE c.competition_id = '" . (int)$competition_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1'");
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "competition c LEFT JOIN " . DB_PREFIX . "competition_description cd ON (c.competition_id = cd.competition_id) LEFT JOIN " . DB_PREFIX . "competition_to_store c2s ON (c.competition_id = c2s.competition_id) WHERE c.competition_id = '" . (int)$competition_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1'");
		return $query->row;
	}
	
	public function getWinners($competition_id) {
		$query = $this->db->query("SELECT name FROM " . DB_PREFIX . "competition_winners WHERE competition_id='" . (int)$competition_id . "'");
		return $query->rows;
	}

	public function getCompetitionAnswer($competition_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "competition c LEFT JOIN " . DB_PREFIX . "competition_description cd ON (c.competition_id = cd.competition_id) LEFT JOIN " . DB_PREFIX . "competition_to_store c2s ON (c.competition_id = c2s.competition_id) WHERE c.competition_id = '" . (int)$competition_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1'");
		return $query->row['answer_is'];
	}
	
	public function getCompetitionQuestion($competition_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "competition c LEFT JOIN " . DB_PREFIX . "competition_description cd ON (c.competition_id = cd.competition_id) LEFT JOIN " . DB_PREFIX . "competition_to_store c2s ON (c.competition_id = c2s.competition_id) WHERE c.competition_id = '" . (int)$competition_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1'");
		return $query->row['question'];
	}
	
	public function getCompetitionDate($competition_id) {
		$query = $this->db->query("SELECT end_date FROM " . DB_PREFIX . "competition WHERE competition_id = '" . (int)$competition_id . "'");
		return $query->row['end_date'];
	}
	
	public function getCompetitionNewsletter($competition_id) {
		$query = $this->db->query("SELECT newsletter FROM " . DB_PREFIX . "competition WHERE competition_id = '" . (int)$competition_id . "'");
		return $query->row['newsletter'];
	}
	
	public function addNewsletter($data) {
	
		$newsletter = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer c LEFT JOIN " . DB_PREFIX . "c_newsletter cn ON (c.email = cn.email) WHERE c.email = '" . $this->db->escape($data['entered_email']) . "' AND newsletter = '1'");
		$email_check = $this->db->query("SELECT * FROM " . DB_PREFIX . "c_newsletter WHERE email='" . $data['entered_email'] . "'");

		if (!$newsletter->num_rows && !$email_check->num_rows) {
      		$this->db->query("INSERT INTO " . DB_PREFIX . "c_newsletter SET email = '" . $this->db->escape($data['entered_email']) . "',name='" . $this->db->escape($data['entered_name']) . "'");       	
		}
	}
	
	public function getCompetition() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "competition c LEFT JOIN " . DB_PREFIX . "competition_description cd ON (c.competition_id = cd.competition_id) LEFT JOIN " . DB_PREFIX . "competition_to_store c2s ON (c.competition_id = c2s.competition_id) WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1' ORDER BY c.date_added DESC");
		return $query->rows;
	}

	public function getCompetitionTeaser() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "competition c LEFT JOIN " . DB_PREFIX . "competition_description cd ON (c.competition_id = cd.competition_id) LEFT JOIN " . DB_PREFIX . "competition_to_store c2s ON (c.competition_id = c2s.competition_id) WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1' ORDER BY c.date_added DESC LIMIT 1");
		return $query->rows;
	}

	public function checkmailid($email, $competition_id) {
	   $query=$this->db->query("SELECT * FROM " . DB_PREFIX . "competition_entrants WHERE email_id='" . $email . "' AND competition_id='" . $competition_id . "'");
	   return $query->num_rows;
	}
	
	public function checkNewsletterExisting($data) {
	   $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "c_newsletter WHERE email='" . $this->db->escape($data['unsubscribe_email']) . "'");
	   return $query->num_rows;
	}
	
	public function unsubscribe($data) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "c_newsletter WHERE email = '" . $this->db->escape($data['unsubscribe_email']) . "'");
	}
	
	public function submitEntry($data, $name, $email) {
		
		if (isset($data['entered_answer'])) {
			$answer = $this->db->escape($data['entered_answer']);
		} else {
			$answer = '';
		}
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "competition_entrants SET email_id='" . $email . "', name='" .$name . "', answer='" . $answer . "', competition_id='" . $this->db->escape($data['competition_id']) . "'");
	}
	
	public function getAutoEntryEmailCheck($email, $competition_id) {
	   $query=$this->db->query("SELECT * FROM " . DB_PREFIX . "competition_entrants WHERE email_id='" . $email . "' AND competition_id='" . $competition_id . "'");
	   return $query->num_rows;
	}
	
	public function setAutoEntry($data, $competition_id) {
		$email_check = $this->db->query("SELECT * FROM " . DB_PREFIX . "competition_entrants WHERE email_id='" . $data['email'] . "' AND competition_id='" . $competition_id . "'");
		if (!$email_check->num_rows) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "competition_entrants SET email_id='" . $this->db->escape($data['email']) . "', name='" . $this->db->escape($data['name']) . "', competition_id='" . $this->db->escape($competition_id) . "'");
		}
	}

}
?>
