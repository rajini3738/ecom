<?php
class ModelTotalTax extends Model {
	public function getTotal(&$total_data, &$total, &$taxes) {
		foreach ($taxes as $tax_rate) {
			$amount=0;
			$title ="";
		
			if ($tax_rate['type'] == 'F') {
				$title = $tax_rate['name'].' ('.intval($tax_rate['rate']).')';
				$amount += $tax_rate['rate'];
			} elseif ($tax_rate['type'] == 'P') {
				$title = $tax_rate['name'].' ('.intval($tax_rate['rate']).'%)';
				$amount += ($total / 100 * $tax_rate['rate']);
			}
		
			$total_data[] = array(
				'code'       => 'tax',
				'title'      => $title,
				'value'      => $amount,
				'sort_order' => $this->config->get('tax_sort_order')
			);
			$total += $amount;
		}
	}

	public function getTotal1(&$total, &$total_tax, &$taxes, &$taxes_value) {
		if(sizeof($taxes)>0){
			foreach ($taxes as $tax_rate) {
				$amount=0;		
				if ($tax_rate['type'] == 'F') {
					$amount += $tax_rate['rate'];
				} elseif ($tax_rate['type'] == 'P') {
					$amount += ($total / 100 * $tax_rate['rate']);
				}
				$total_tax = $total+$amount;
				$taxes_value=$amount;
			}
		}
		else{
			$total_tax = $total;
		}
	}
}