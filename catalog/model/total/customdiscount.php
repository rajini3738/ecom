<?php
class ModelTotalCustomdiscount extends Model {
	public function getTotal(&$total_data, &$total, &$taxes) {
		if ($this->cart->hasProducts()) {
			$this->load->model('checkout/ajaxedit');
			
			$discount_total = 0;
			
			$custom_discount = $this->model_checkout_ajaxedit->getCustomDiscount();

			if ($custom_discount) {
			
				if ($custom_discount['type'] == 'P') {
			
					foreach ($this->cart->getProducts() as $product) {
						$discount = 0;
						$discount = $product['total'] / 100 * $custom_discount['discount'];
									
						if ($product['tax_class_id']) {
							$tax_rates = $this->tax->getRates($product['total'] - ($product['total'] - $discount), $product['tax_class_id']);

							foreach ($tax_rates as $tax_rate) {
								if ($tax_rate['type'] == 'P') {
									$taxes[$tax_rate['tax_rate_id']] -= $tax_rate['amount'];
								}
							}
						}
						
						$discount_total += $discount;
					}
				} elseif ($custom_discount['type'] == 'F') {
				
					$discount = $custom_discount['discount'];
					$discount_partial = $discount/($this->cart->countProducts());
				
					foreach ($this->cart->getProducts() as $product) {
									
						if ($product['tax_class_id']) {
							$tax_rates = $this->tax->getRates($product['total'] - ($product['total'] - ($discount_partial * $product['quantity'])), $product['tax_class_id']);

							foreach ($tax_rates as $tax_rate) {
								if ($tax_rate['type'] == 'P') {
									$taxes[$tax_rate['tax_rate_id']] -= $tax_rate['amount'];
								}
							}
						}
					}
				
					$discount_total += $discount;
				}
			
			}
			
			$total_data[] = array(
				'code'       => 'customdiscount',
				'title'      =>  $custom_discount['title'],
				'value'      => -$discount_total,
				'sort_order' => $custom_discount['sort_order']
			);	
			
			$total -= $discount_total;
		}
	}
}