<?php
class ModelTotalProductTotal extends Model {
	public function getTotal(&$total_data, &$total, &$taxes) {
		$this->load->language('total/product_total');

		$product_total = $this->cart->getProductTotal();

		if (isset($this->session->data['vouchers']) && $this->session->data['vouchers']) {
			foreach ($this->session->data['vouchers'] as $voucher) {
				$product_total += $voucher['amount'];
			}
		}

		$total_data[] = array(
			'code'       => 'product_total',
			'title'      => $this->language->get('text_product_total'),
			'value'      => $product_total,
			'sort_order' => $this->config->get('product_total_sort_order')
		);

		$total += $product_total;
	}

	public function getExsitingTotal(&$total_data, &$total, &$taxes, &$product_total) {
		$this->load->language('total/product_total');

		if (isset($this->session->data['vouchers']) && $this->session->data['vouchers']) {
			foreach ($this->session->data['vouchers'] as $voucher) {
				$product_total += $voucher['amount'];
			}
		}

		$total_data[] = array(
			'code'       => 'product_total',
			'title'      => $this->language->get('text_product_total'),
			'value'      => $product_total,
			'sort_order' => $this->config->get('product_total_sort_order')
		);

		$total += $product_total;
	}
}