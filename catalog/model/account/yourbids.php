<?php
class ModelAccountYourbids extends Model {
  public function getYourbids() {
		$query = $this->db->query("SELECT product_id FROM " . DB_PREFIX . "customer_bid WHERE customer_id = '" . (int)$this->customer->getId() . "'");

		return $query->rows;
	}
  
  public function getYourbid($product_id) {
		$query = $this->db->query("SELECT bid FROM " . DB_PREFIX . "customer_bid WHERE product_id = '" . (int)$product_id . "' AND customer_id = '" . (int)$this->customer->getId() . "'");

		return $query->row['bid'];
	}
  
  public function getWinningbids() {
		$query = $this->db->query("SELECT product_id FROM " . DB_PREFIX . "product WHERE auc_customer_id = '" . (int)$this->customer->getId() . "'");

		return $query->rows;
	}

	public function getTotalbids($product_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "customer_bid` WHERE product_id = '" . (int)$product_id . "'");

		return $query->row['total'];
	}

}