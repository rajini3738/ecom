<?php
class ModelAccountComplaint extends Model {
	public function addComplaint($data) {
		$this->event->trigger('pre.return.add', $data);

        $this->db->query("INSERT INTO `" . DB_PREFIX . "complaint` SET order_id = '" . (int)$data['order_id'] . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', middlename = '" . $this->db->escape($data['middlename']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', opened = '1', reason_id = '" . (int)$data['reason_id'] . "', comment = '" . $this->db->escape($data['comment']) . "', uploadphoto = '" . $this->db->escape($data['code']) . "', date_added = NOW(), date_modified = NOW()");

		$return_id = $this->db->getLastId();

		$this->event->trigger('post.return.add', $return_id);

		return $return_id;
	}

	public function getComplaint($return_id) {
		$query = $this->db->query("SELECT r.return_id, r.order_id, r.firstname, r.lastname, r.middlename, r.email, r.telephone, r.product, r.model, r.quantity, r.opened, (SELECT rr.name FROM " . DB_PREFIX . "return_reason rr WHERE rr.return_reason_id = r.return_reason_id AND rr.language_id = '" . (int)$this->config->get('config_language_id') . "') AS reason, (SELECT ra.name FROM " . DB_PREFIX . "return_action ra WHERE ra.return_action_id = r.return_action_id AND ra.language_id = '" . (int)$this->config->get('config_language_id') . "') AS action, (SELECT rs.name FROM " . DB_PREFIX . "return_status rs WHERE rs.return_status_id = r.return_status_id AND rs.language_id = '" . (int)$this->config->get('config_language_id') . "') AS status, r.comment, r.date_ordered, r.date_added, r.date_modified FROM `" . DB_PREFIX . "return` r WHERE return_id = '" . (int)$return_id . "' AND customer_id = '" . $this->customer->getId() . "'");

		return $query->row;
	}

	public function getComplaints($start = 0, $limit = 20) {
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 20;
		}

		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "complaint` ORDER BY r.return_id DESC LIMIT " . (int)$start . "," . (int)$limit);

		return $query->rows;
	}

	public function getTotalComplaints() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "return`WHERE customer_id = '" . $this->customer->getId() . "'");

		return $query->row['total'];
	}
}