﻿<?php echo $header; 
$theme_options = $this->registry->get('theme_options');
$config = $this->registry->get('config'); 
include('catalog/view/theme/' . $config->get('config_template') . '/template/new_elements/wrapper_top.tpl'); ?>
<div class="col-md-4">
<h2>مرحبا <strong style="display: inline-block;direction: ltr;text-align: right;"><?php echo $welcome; ?></strong>,</h2>
<h2>رقم الحساب : <?php echo $account_number; ?></h2>
<h2>نوع العضوية : <?php echo $group_name; ?></h2>
<h2><?php echo $text_my_account; ?></h2>
<ul class="list-unstyled">
  <li><a href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
  <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
  <li><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
  <li style="display:none"><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
</ul>

<h2><?php echo $text_my_orders; ?></h2>
<ul class="list-unstyled">
  <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
  <li style="display:none"><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
  <?php if ($reward) { ?>
  <li style="display:none"><a href="<?php echo $reward; ?>"><?php echo $text_reward; ?></a></li>
  <?php } ?>
  <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
  <li style="display:none"><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
  <li style="display:none"><a href="<?php echo $recurring; ?>"><?php echo $text_recurring; ?></a></li>
</ul>

<h2 style="display:none"><?php echo $text_my_newsletter; ?></h2>
<ul class="list-unstyled" style="display:none">
  <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
</ul>
</div>
<div class="col-md-6">
    <h2>لإرفاق اللإيصال البنكي الرجاء أتباع الخطوات التالية :</h2>
  <ol>
    <li>
      أضغط على قائمة طلباتي أو عرض سجل الطلبات
    </li>
    <li>أضغط على “عرض” لمشاهدة تفاصيل الطلب</li>
    <li>إرفق الإيصال البنكي</li>
    <li>أضغط على“ إعتماد”</li>
  </ol>
</div>
<?php include('catalog/view/theme/' . $config->get('config_template') . '/template/new_elements/wrapper_bottom.tpl'); ?>
<?php echo $footer; ?>