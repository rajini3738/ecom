﻿<?php echo $header; 
$theme_options = $this->registry->get('theme_options');
$config = $this->registry->get('config'); 
include('catalog/view/theme/' . $config->get('config_template') . '/template/new_elements/wrapper_top.tpl'); ?>

<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
  <fieldset>
    <legend>
      <?php echo $text_order; ?>
    </legend>
    <div class="form-group required">
      <label class="col-sm-2 control-label" for="input-firstname">
        <?php echo $entry_firstname; ?>
      </label>
      <div class="col-sm-10">
        <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" />
        <?php if ($error_firstname) { ?>
        <div class="text-danger">
          <?php echo $error_firstname; ?>
        </div>
        <?php } ?>
      </div>
    </div>
    <div class="form-group required">
      <label class="col-sm-2 control-label" for="input-middlename">
        <?php echo $entry_middlename; ?>
      </label>
      <div class="col-sm-10">
        <input type="text" name="middlename" value="<?php echo $middlename; ?>" placeholder="<?php echo $entry_middlename; ?>" id="input-middlename" class="form-control" />
          <?php if ($error_middlename) { ?>
        <div class="text-danger">
          <?php echo $error_middlename; ?>
        </div>
        <?php } ?>
      </div>
    </div>
    <div class="form-group required">
      <label class="col-sm-2 control-label" for="input-lastname">
        <?php echo $entry_lastname; ?>
      </label>
      <div class="col-sm-10">
        <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />
        <?php if ($error_lastname) { ?>
        <div class="text-danger">
          <?php echo $error_lastname; ?>
        </div>
        <?php } ?>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label" for="input-email">
        <?php echo $entry_email; ?>
      </label>
      <div class="col-sm-10 required">
        <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
        <?php if ($error_email) { ?>
        <div class="text-danger">
          <?php echo $error_email; ?>
        </div>
        <?php } ?>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label" for="input-order-id">
        <?php echo $entry_order_id; ?>
      </label>
      <div class="col-sm-10">
        <input type="text" name="order_id" value="<?php echo $order_id; ?>" placeholder="<?php echo $entry_order_id; ?>" id="input-order-id" class="form-control" />
        <?php if ($error_order_id) { ?>
        <div class="text-danger">
          <?php echo $error_order_id; ?>
        </div>
        <?php } ?>
      </div>
    </div>  
	<div class="form-group required">
      <label class="col-sm-2 control-label" for="input-telephone">
        <?php echo $entry_telephone; ?>
      </label>
      <div class="col-sm-10">
        <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control" />
        <?php if ($error_telephone) { ?>
        <div class="text-danger">
          <?php echo $error_telephone; ?>
        </div>
        <?php } ?>
      </div>
    </div>	
	<div class="form-group required">
      <label class="col-sm-2 control-label">
        <?php echo $entry_reason; ?>
      </label>
      <div class="col-sm-10">
        <select id="input-reason" class="form-control" name="reason_id">
          <option value=""> --- الرجاء الاختيار --- </option>
          <option value="1">تتعلق بقسم المالية</option>
		  <option value="2">تتعلق بالإرجاع</option>
		  <option value="3">تتعلق بالشحن</option>
		  <option value="4">الطلبية فيها خطأ</option>
		  <option value="5">سبب آخر</option>
        </select>        
        <?php if ($error_reason) { ?>
        <div class="text-danger">
          <?php echo $error_reason; ?>
        </div>
        <?php } ?>
      </div>
    </div> 
	  <div class="row">
	  <div class="form-group">
		<label class="col-sm-2 control-label" for="button-upload">
		  <span data-toggle="tooltip" title="">الإيصال البنكي</span>
		</label>
		<div class="col-sm-5">
		  <input type="hidden" name="code"  id="code"/>
		  <button type="button" id="button-upload" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo $button_upload; ?>
		  </button>
			&nbsp;&nbsp;<label id="lblcode"></label>
		</div>
	  </div>
	</div>
  </fieldset>
  <fieldset> 
    <div class="form-group">
      <label class="col-sm-2 control-label" for="input-comment">
        <?php echo $entry_fault_detail; ?>
      </label>
      <div class="col-sm-10">
        <textarea name="comment" rows="10" placeholder="<?php echo $entry_fault_detail; ?>" id="input-comment" class="form-control"><?php echo $comment; ?></textarea>
      </div>
    </div>   
  </fieldset> 
  <div class="form-group">
    <label class="col-sm-2 control-label" for="input-comment">
      &nbsp;
    </label>
    <div class="col-sm-10">
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
          <a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-left">
          <input type="submit" value="<?php echo $button_submit; ?>" class="btn btn-primary pull-right" />
        </div>
      </div>
    </div>
  </div>
	<input type="hidden" id="hndreason" value="<?php echo $reason_id; ?>" />
</form>

<script type="text/javascript">
  <!--
$('.date').datetimepicker({
	pickTime: false
});
$('#input-reason').val($('#hndreason').val());
$('#button-upload').on('click', function() {
	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$('#button-upload').button('loading');
				},
				complete: function() {
					$('#button-upload').button('reset');
				},
				success: function(json) {
					var code = json['code'];
					$('#code').val(code);
					$('#lblcode').html(code);
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//-->
</script>

<?php include('catalog/view/theme/' . $config->get('config_template') . '/template/new_elements/wrapper_bottom.tpl'); ?>
<?php echo $footer; ?>