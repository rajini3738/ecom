﻿<?php echo $header; 
$theme_options = $this->registry->get('theme_options');
$config = $this->registry->get('config'); 
include('catalog/view/theme/' . $config->get('config_template') . '/template/new_elements/wrapper_top.tpl'); ?>
<?php if ($cart_order_message) { ?>
  <p><?php echo $cart_order_message; ?></p>
<?php } ?>
<h2>
  مرحبا <strong style="display: inline-block;direction: ltr;text-align: right;"><?php echo $welcome; ?></strong>,
</h2>
<h2>
  رقم الحساب : <?php echo $account_number; ?>
</h2>
<!-- <h2 style="border-bottom: 1px solid #ccc;">
  نوع العضوية : <?php echo $group_name; ?>
</h2> -->
<div style="width: 100%; height: 1px; border-bottom: 1px solid #ccc; margin-bottom: 20px;"></div>
<?php if ($orders) { ?>
<div class="table-responsive">
  <table class="table table-bordered table-hover">
    <thead style="font-weight: bold;">
      <tr>
        <td class="text-right"><?php echo $column_order_id; ?></td>
        <td class="text-left"><?php echo $column_status; ?></td>
        <td class="text-left"><?php echo $column_date_added; ?></td>
        <td class="text-right"><?php echo $column_product; ?></td>
        <td class="text-right"><?php echo $column_total; ?></td>
        <td></td>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($orders as $order) { ?>
      <tr>
        <td class="text-right"><?php echo $order['order_no']; ?></td>
        <td class="text-left"><?php echo $order['status']; ?></td>
        <td class="text-left"><?php echo $order['date_added']; ?></td>
        <td class="text-right"><?php echo $order['products']; ?></td>
        <td class="text-right"><?php echo $order['total']; ?></td>
        <td class="text-right"><a href="<?php echo $order['href']; ?>" class="btn btn-info"><?php echo $button_view; ?></a></td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
<div class="text-right"><?php echo $pagination; ?></div>
<?php } else { ?>
<p><?php echo $text_empty; ?></p>
<?php } ?>
<div class="buttons clearfix">
  <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary">رجوع</a></div>
</div>
  
<?php include('catalog/view/theme/' . $config->get('config_template') . '/template/new_elements/wrapper_bottom.tpl'); ?>
<?php echo $footer; ?>