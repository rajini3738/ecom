<?php echo $header; 
$theme_options = $this->registry->get('theme_options');
$config = $this->registry->get('config'); 
include('catalog/view/theme/' . $config->get('config_template') . '/template/new_elements/wrapper_top.tpl'); ?>

<h2><?php echo $text_address_book; ?></h2>
<?php if ($addresses) { ?>
<table class="table table-bordered table-hover">
  <?php foreach ($addresses as $result) { ?>
  <tr>
    <td class="text-left"><?php echo $result['address']; ?></td>
    <td class="text-right">
    
    <div class="row">
    
    <div class="col-md-2 col-sm-2 col-xs-6"><a href="<?php echo $result['update']; ?>" class="btn btn-info"><?php echo $button_edit; ?></a> </div>
     <div class="col-md-2 col-sm-2 col-xs-6"><a href="<?php echo $result['delete']; ?>" class="btn btn-danger"><?php echo $button_delete; ?></a></div>
    
    </div>
    
    </td>
  </tr>
  <?php } ?>
</table>
<?php } else { ?>
<p><?php echo $text_empty; ?></p>
<?php } ?>
<div class="buttons clearfix">
  <a style="margin-right:10px" href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a> <a href="<?php echo $add; ?>" class="btn btn-primary"><?php echo $button_new_address; ?></a>
</div>
  
<?php include('catalog/view/theme/' . $config->get('config_template') . '/template/new_elements/wrapper_bottom.tpl'); ?>
<?php echo $footer; ?>