﻿<?php echo $header; 
$theme_options = $this->registry->get('theme_options');
$config = $this->registry->get('config'); 
include('catalog/view/theme/' . $config->get('config_template') . '/template/new_elements/wrapper_top.tpl'); ?>

<div class="row">
  <div class="col-sm-6" style="display:none">
    <!--
    <div class="well">
      <h2><?php echo $text_new_customer; ?></h2>
      <!-- <p><?php echo $text_register; ?></p>
      <p style="padding-bottom: 10px"><?php echo $text_register_account; ?></p>
      <a href="<?php echo $register; ?>" class="btn btn-primary"><?php echo $button_continue1; ?></a>
    </div> -->
  </div>
  <div class="col-sm-6">
    <div class="well">
      <h2><?php echo $text_returning_customer; ?></h2>
      <p><?php echo $text_i_am_returning_customer; ?></p>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <div class="form-group">
          <label class="control-label" for="input-telephone"><?php echo $entry_telephone; ?><span style="color:red;font-size:14px;"> (يجب أن تشمل رمز البلد في رقم الجوال، ورقم يبدأ ب 00)</span> </label>
          <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="00971xxxxxxxxx" id="input-telephone" class="form-control" />
          </div>
        <div class="form-group" style="padding-bottom: 10px">
          <label class="control-label" for="input-password"><?php echo $entry_password; ?></label>
          <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
          </div>
          
          <div class="row">
          
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-8"><a style="line-height:50px;" href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></div>
          
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-4 text-left"><input type="submit" value="<?php echo $button_login; ?>" class="btn btn-primary pull-right" /></div>
          <?php if ($redirect) { ?>
           <div class="col-md-6">
           
           
        <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
        
           </div>
          <?php } ?>
          </div>
          
        
        
      </form>
    </div>
  </div>
</div>

<?php include('catalog/view/theme/' . $config->get('config_template') . '/template/new_elements/wrapper_bottom.tpl'); ?>
<?php echo $footer; ?>