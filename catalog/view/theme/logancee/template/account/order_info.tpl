<?php echo $header; 
$theme_options = $this->registry->get('theme_options');
$config = $this->registry->get('config'); 
include('catalog/view/theme/' . $config->get('config_template') . '/template/new_elements/wrapper_top.tpl'); ?>
<table class="table table-bordered table-hover">
  <thead>
    <tr>
      <td class="text-left" colspan="2"><?php echo $text_order_detail; ?></td>
    </tr>
  </thead>
  <tbody>
    <tr id="timmer">
      <td>المهلة المتاحة للسداد قبل الغاء الطلب:</td>
      <td>
        <div id="defaultCountdown"></div>
      </td>
    </tr>
    <tr>
      <td class="text-left"><?php if ($invoice_no) { ?>
        <b><?php echo $text_invoice_no; ?></b> <?php echo $invoice_no; ?><br />
        <?php } ?>        
        <b><?php echo $text_date_added; ?></b> <?php echo $date_added; ?>
        <input type="hidden" name="dateadded" id="dateadded" value="<?php echo $date_added1; ?>"/>
      </td>
      <td class="text-left"><?php if ($payment_method) { ?>
        <b><?php echo $text_payment_method; ?></b> <?php echo $payment_method; ?><br />
        <?php } ?>
        <?php if ($shipping_method) { ?>
        <b><?php echo $text_shipping_method; ?></b> <?php echo $shipping_method; ?>
        <?php } ?></td>
    </tr>
  </tbody>
</table>
<table class="table table-bordered table-hover">
  <thead>
    <tr>
      <td class="text-left" style="font-weight: bold;">تفاصيل الفاتورة</td>
      <?php if ($shipping_address) { ?>
      <td class="text-left" style="font-weight: bold;">تفاصيل الشحن</td>
      <?php } ?>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="text-left"><?php echo $payment_address; ?></td>
      <?php if ($shipping_address) { ?>
      <td class="text-left"><?php echo $shipping_address; ?></td>
      <?php } ?>
    </tr>
  </tbody>
</table>

<table class="table table-bordered table-hover">
    <thead class="theadme">
      <tr>
        <td class="text-left"><?php echo $column_name; ?></td>
        <td class="text-right"><?php echo $column_quantity; ?></td>
        <td class="text-right"><?php echo $column_price; ?></td>
        <td class="text-right"><?php echo $column_total; ?></td>
		<td class="text-right"><?php echo $column_tax; ?></td>
		<td class="text-right"><?php echo $column_total_tax; ?></td>
        <?php if ($products) { ?>
        <td style=""></td>
        <?php } ?>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($products as $product) { ?>
      <tr style="border-bottom:none;">
        <td class="text-left" style="border-bottom:none; padding-bottom:0px;"><?php echo $product['name']; ?>
          <?php foreach ($product['option'] as $option) { ?>
          <br />
          &nbsp;<small> - <?php echo $option['value']; ?></small>
          <?php } ?></td>
        <td class="text-right" style="border-bottom:none; padding-bottom:0px;"><?php echo $product['quantity']; ?></td>
        <td class="text-right" style="border-bottom:none; padding-bottom:0px;"><?php echo $product['price']; ?></td>
        <td class="text-right" style="border-bottom:none; padding-bottom:0px;"><?php echo $product['total']; ?></td>
        <td class="text-right" style="border-bottom:none; padding-bottom:0px;"><?php echo $product['tax']; ?></td>
		<td class="text-right" style="border-bottom:none; padding-bottom:0px;"><?php echo $product['total_tax']; ?></td>
      </tr>
      
      <tr>
      <td class="text-right" style="white-space: nowrap;"><?php if ($product['reorder']) { ?>
          <a href="<?php echo $product['reorder']; ?>" data-toggle="tooltip" title="<?php echo $button_reorder; ?>" class="btn btn-primary"><i class="fa fa-shopping-cart"></i></a>
          <?php } ?>
          <a href="<?php echo $product['return']; ?>" data-toggle="tooltip" title="<?php echo $button_return; ?>" class="btn btn-danger"><i class="fa fa-reply"></i></a>
          <a href="<?php echo $invoice; ?>" target="_blank" data-toggle="tooltip" title="<?php echo $button_invoice_print; ?>" class="btn btn-info"><i class="fa fa-print"></i></a></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      </tr>
      <?php } ?>
      <?php foreach ($vouchers as $voucher) { ?>
      <tr>
        <td class="text-left"><?php echo $voucher['description']; ?></td>
        <td class="text-left"></td>
        <td class="text-right">1</td>
        <td class="text-right"><?php echo $voucher['amount']; ?></td>
        <td class="text-right"><?php echo $voucher['amount']; ?></td>
        <?php if ($products) { ?>
        <td></td>
        <?php } ?>
      </tr>
      <?php } ?>
    </tbody>
    <tfoot>
      <?php foreach ($totals as $total) { ?>
      <tr>
        <td class="text-right"><b><?php echo $total['title']; ?></b></td>
        <td colspan="2" class="text-right"><?php echo $total['text']; ?></td>
        <td colspan="3"></td>
        <?php if ($products) { ?>
        <td></td>
        <?php } ?>
      </tr>
      <?php } ?>
    </tfoot>
  </table>
<?php if ($comment) { ?>
<table class="table table-bordered table-hover">
  <thead>
    <tr>
      <td class="text-left"><?php echo $text_comment; ?></td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="text-left"><?php echo $comment; ?></td>
    </tr>
  </tbody>
</table>
<?php } ?>
<?php if ($histories) { ?>
<h3><?php echo $text_history; ?></h3>
<table class="table table-bordered table-hover">
  <thead>
    <tr>
      <td class="text-left"><?php echo $column_date_added; ?></td>
      <td class="text-left"><?php echo $column_status; ?></td>
      <td class="text-left"><?php echo $column_comment; ?></td>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($histories as $history) { ?>
    <tr>
      <td class="text-left"><?php echo $history['date_added']; ?></td>
      <td class="text-left"><?php echo $history['status']; ?></td>
      <td class="text-left"><?php echo $history['comment']; ?></td>
    </tr>
    <?php } ?>
  </tbody>
</table>
<?php } ?>
<?php if (strcmp($payment_code,'bank_transfer')==0 ) { ?>
  <h3 style="margin-top:40px;">
    <?php echo $text_payment_proof; ?>
  </h3>
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
  <div class="form-group">
  <label class="col-sm-3 control-label" for="recieptupload">
    <span style="color:red">*</span> هل تم السداد؟
  </label>
  <div class="col-sm-8">
    <select name="recieptupload" id="recieptupload" class="form-control">
      <option value="">الرجاء الاختيار</option>
      <option value="Y">نعم</option>
      <option value="N">لا</option> 
    </select>
    <div style="display:none" id="recieptuploaderror">الرجاء تحديد حالة السداد.</div>
  </div>
</div>
  <div class="form-group">
      <label class="col-sm-3 control-label" for="membername">
        <span style="color:red" id="span1">*</span>اسم المودع
      </label>
      <div class="col-sm-8">
        <input type="text" name="membername" id="membername" value=""  class="form-control"/>
        <div style="display:none">الرجاء أدخال اسم المودع.</div>
      </div>
    </div>
  <div class="form-group">
      <label class="col-sm-3 control-label" for="accountno">
        <span style="color:red" id="span2">*</span>رقم الحساب البنكي
      </label>
      <div class="col-sm-8">
        <input type="text" name="accountno" id="accountno" value=""  class="form-control"/>
        <div style="display:none">الرجاء أدخال رقم الحساب البنكي.</div>
      </div>
    </div>
  <div class="form-group">
      <label class="col-sm-3 control-label" for="sukkatbank">
        <span style="color:red" id="span3">*</span>لاي بنك تم التحويل
      </label>
      <div class="col-sm-8">
        <select name="sukkatbank" id="sukkatbank" class="form-control">
          <option value="">الرجاء الاختيار</option>
          <option value="NCB">الأهلي (0102 7600 7483 04)</option>
          <option value="ARB">مصرف الراجحي (0205007 60801 170)</option>
        </select>
        <div style="display:none" id="sukkatbankerror">الرجاء أختيار اسم البنك.</div>
      </div>
    </div>  
  <div class="form-group">
      <label class="col-sm-3 control-label" for="button-upload">
        <span data-toggle="tooltip" title="">الإيصال البنكي</span>
      </label>
      <div class="col-sm-5">
        <input type="hidden" name="orderid"  id="hndid" value="<?php echo $order_id; ?>"/>
        <input type="hidden" name="code"  id="code"/>
        <button type="button" id="button-upload" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo $button_upload; ?>
        </button>  <?php if (strcmp($filename,'#')!=0 ) { ?> | <a href="<?php echo $filename; ?>" target="_blank" class="btn btn-primary"> إظهار الإيصال </a><?php } ?>
        <div style="display:none">الرجاء رفع اللإيصال البنكي.</div>
      </div>
    </div>
    <div class="buttons clearfix">
      <div class="pull-right">
        <button type="button" id="button-submit" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $button_continue; ?></button>      
      </div>
    </div>
  </form>
<?php } ?>
<div class="buttons clearfix">
  <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary">رجوع</a></div>
</div>  
<?php include('catalog/view/theme/' . $config->get('config_template') . '/template/new_elements/wrapper_bottom.tpl'); ?>
<script type="text/javascript">
  <!--
  $('#recieptupload').on('change', function() {
    if($('#recieptupload').val()=="N"){
      $('#span1').hide();
      $('#span2').hide();
      $('#span3').hide();
    }
    else{
      $('#span1').show();
      $('#span2').show();
      $('#span3').show();
    }
  });
$('#button-upload').on('click', function() {
//alert($('#hndid').val());
	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$('#button-upload').button('loading');
				},
				complete: function() {
					$('#button-upload').button('reset');
				},
				success: function(json) {
					var code = json['code'];
          $('#code').val(code);
          $('#button-submit').after('<div class="alert alert-success" style="width: 85%;"><i class="fa fa-check-circle"></i> قد تم تحميل إيصال البنك بنجاح. </div>');
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});

$('#button-submit').on('click', function() {
  var isValid=true;  
  if($('#recieptupload').val()=="Y"){
    $('#span1').show();
    $('#span2').show();
    $('#span3').show();
    if($('#membername').val()==""){
      $('#membername').next().show();
      $('#membername').next().addClass( "text-danger" );
      isValid=false;
    }
    else{
       $('#membername').next().hide();
    }
    if($('#accountno').val()==""){
        $('#accountno').next().show();
        $('#accountno').next().addClass( "text-danger" );
       isValid=false;
    }
    else{
      $('#accountno').next().hide();
    }
    if($('#sukkatbank').val()==""){
        $('#sukkatbankerror').show();
        $('#sukkatbankerror').addClass( "text-danger" );
       isValid=false;
    } 
    else{
      $('#sukkatbankerror').hide();
    }
  }
  else{
    $('#span1').show();
    $('#span2').show();
    $('#span3').show();
     if($('#recieptupload').val()==""){
      $('#recieptuploaderror').show();
      $('#recieptuploaderror').addClass( "text-danger" );
      isValid=false;
    }
    else{
       $('#recieptuploaderror').hide();
    }
  }
  if(isValid){
		  $.ajax({
          url: 'index.php?route=account/order/updatepayment',
				  type: 'post',
				  dataType: 'json',
				  data: 'code='+$('#code').val()+'&orderid='+$('#hndid').val()+'&account_name='+$('#membername').val()+'&account_number='+$('#accountno').val()+'&sukkat_bank='+$('#sukkatbank').val()+'&bank_transfer_made='+$('#recieptupload').val(),
          success: function(json) {
            $('#button-submit').after('<div class="alert alert-success" style="width: 85%;"><i class="fa fa-check-circle"></i> شكراً لقد تم استلام معلومات التحويل بنجاح </div>');
            //location.reload();            
          },
				  error: function(xhr, ajaxOptions, thrownError) {
					  alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				  }
    });	
  }
});
//-->
</script>

<script>
  $(function(){
  var now = new Date();
  var sdfsd = now.getTimezoneOffset();
  var value = $('#dateadded').val();
  if(value!=""){
  $('#timmer').hide()
  var newYear = new Date(value);
  $('#defaultCountdown').countdown({until: newYear});
  }
  else{
  $('#timmer').hide()
  }
  });
</script>
<?php echo $footer; ?>