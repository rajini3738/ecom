<?php echo $header; 
$theme_options = $this->registry->get('theme_options');
$config = $this->registry->get('config'); 
include('catalog/view/theme/' . $config->get('config_template') . '/template/new_elements/wrapper_top.tpl'); ?>

<p>
  <?php echo $text_description; ?>
</p>
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
  <fieldset>
    <legend>
      <?php echo $text_order; ?>
    </legend>
    <div class="form-group required">
      <label class="col-sm-2 control-label" for="input-firstname">
        <?php echo $entry_firstname; ?>
      </label>
      <div class="col-sm-10">
        <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" />
        <?php if ($error_firstname) { ?>
        <div class="text-danger">
          <?php echo $error_firstname; ?>
        </div>
        <?php } ?>
      </div>
    </div>
    <div class="form-group required" style="display:none">
      <label class="col-sm-2 control-label" for="input-middlename">
        <?php echo $entry_middlename; ?>
      </label>
      <div class="col-sm-10">
        <input type="text" name="middlename" value="<?php echo $middlename; ?>" placeholder="<?php echo $entry_middlename; ?>" id="input-middlename" class="form-control" />
          <?php if ($error_middlename) { ?>
        <div class="text-danger">
          <?php echo $error_middlename; ?>
        </div>
        <?php } ?>
      </div>
    </div>
    <div class="form-group required" style="display:none">
      <label class="col-sm-2 control-label" for="input-lastname">
        <?php echo $entry_lastname; ?>
      </label>
      <div class="col-sm-10">
        <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />
        <?php if ($error_lastname) { ?>
        <div class="text-danger">
          <?php echo $error_lastname; ?>
        </div>
        <?php } ?>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label" for="input-email">
        <?php echo $entry_email; ?>
      </label>
      <div class="col-sm-10 required">
        <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
        <?php if ($error_email) { ?>
        <div class="text-danger">
          <?php echo $error_email; ?>
        </div>
        <?php } ?>
      </div>
    </div>
    <div class="form-group required">
      <label class="col-sm-2 control-label" for="input-telephone">
        <?php echo $entry_telephone; ?>
      </label>
      <div class="col-sm-10">
        <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control" />
        <?php if ($error_telephone) { ?>
        <div class="text-danger">
          <?php echo $error_telephone; ?>
        </div>
        <?php } ?>
      </div>
    </div>
    <div class="form-group required" style="display:none">
      <label class="col-sm-2 control-label" for="input-order-id">
        <?php echo $entry_order_id; ?>
      </label>
      <div class="col-sm-10">
        <input type="text" name="order_id" value="<?php echo $order_id; ?>" placeholder="<?php echo $entry_order_id; ?>" id="input-order-id" class="form-control" />
        <?php if ($error_order_id) { ?>
        <div class="text-danger">
          <?php echo $error_order_id; ?>
        </div>
        <?php } ?>
      </div>
    </div>
    <div class="form-group" style="display:none">
      <label class="col-sm-2 control-label" for="input-date-ordered">
        <?php echo $entry_date_ordered; ?>
      </label>
      <div class="col-sm-5">
        <div class="input-group date">
          <input type="text" name="date_ordered" value="<?php echo $date_ordered; ?>" placeholder="<?php echo $entry_date_ordered; ?>" data-date-format="YYYY-MM-DD" id="input-date-ordered" class="form-control" /><span class="input-group-btn">
            <button type="button" class="btn btn-default">
              <i class="fa fa-calendar"></i>
            </button>
          </span>
        </div>
      </div>
    </div>
  </fieldset>
  <fieldset>
    <legend>
      <?php echo $text_product; ?>
    </legend>
    <div class="form-group required">
      <label class="col-sm-2 control-label" for="input-product">
        <?php echo $entry_product; ?>
      </label>
      <div class="col-sm-10">
        <input type="text" name="product" value="<?php echo $product; ?>" placeholder="<?php echo $entry_product; ?>" id="input-product" class="form-control" />
        <?php if ($error_product) { ?>
        <div class="text-danger">
          <?php echo $error_product; ?>
        </div>
        <?php } ?>
      </div>
    </div>
    <div class="form-group required" style="display:none">
      <label class="col-sm-2 control-label" for="input-model">
        <?php echo $entry_model; ?>
      </label>
      <div class="col-sm-10">
        <input type="text" name="model" value="<?php echo $model; ?>" placeholder="<?php echo $entry_model; ?>" id="input-model" class="form-control" />
        <?php if ($error_model) { ?>
        <div class="text-danger">
          <?php echo $error_model; ?>
        </div>
        <?php } ?>
      </div>
    </div>
    <div class="form-group" style="display:none">
      <label class="col-sm-2 control-label" for="input-quantity">
        <?php echo $entry_quantity; ?>
      </label>
      <div class="col-sm-10">
        <input type="text" name="quantity" value="<?php echo $quantity; ?>" placeholder="<?php echo $entry_quantity; ?>" id="input-quantity" class="form-control" />
      </div>
    </div>
    <div class="form-group required" style="display:none">
      <label class="col-sm-2 control-label">
        <?php echo $entry_opened; ?>
      </label>
      <div class="col-sm-10">
        <label class="radio-inline">
          <?php if ($opened) { ?>
          <input type="radio" name="opened" value="1" checked="checked" />
          <?php } else { ?>
          <input type="radio" name="opened" value="1" />
          <?php } ?>
          <?php echo $text_yes; ?>
        </label>
        <label class="radio-inline">
          <?php if (!$opened) { ?>
          <input type="radio" name="opened" value="0" checked="checked" />
          <?php } else { ?>
          <input type="radio" name="opened" value="0" />
          <?php } ?>
          <?php echo $text_no; ?>
        </label>
      </div>
    </div>
    <div class="form-group required" style="display:none">
      <label class="col-sm-2 control-label">
        <?php echo $entry_reason; ?>
      </label>
      <div class="col-sm-10">
        <select id="input-sort" class="form-control" name="return_reason_id">
          <?php foreach ($return_reasons as $return_reason) { ?>
            <?php if ($return_reason['return_reason_id'] == $return_reason_id) { ?>
              <option value="<?php echo $return_reason['return_reason_id']; ?>" selected="selected"><?php echo $return_reason['name']; ?>
              </option>
            <?php } else { ?>
              <option value="<?php echo $return_reason['return_reason_id']; ?>"><?php echo $return_reason['name']; ?>
              </option>
            <?php } ?>
          <?php  } ?>
        </select>        
        <?php if ($error_reason) { ?>
        <div class="text-danger">
          <?php echo $error_reason; ?>
        </div>
        <?php } ?>
      </div>
    </div>
    
    <div class="form-group">
      <label class="col-sm-2 control-label" for="input-comment">
        <?php echo $entry_fault_detail; ?>
      </label>
      <div class="col-sm-10">
        <textarea name="comment" rows="10" placeholder="<?php echo $entry_fault_detail; ?>" id="input-comment" class="form-control"><?php echo $comment; ?>
        </textarea>
      </div>
    </div>    
    <!--<?php echo $captcha; ?>-->
  </fieldset>
  <?php if ($text_agree) { ?>
  <div class="form-group">
    <label class="col-sm-2 control-label" for="input-comment">
      &nbsp;
    </label>
    <div class="col-sm-10">
      <div class="col-md-3 text-left" style="padding:0px; line-height:57px;width: 22%;">
        <?php echo $text_agree; ?>
        <?php if ($agree) { ?>
        <input type="checkbox" name="agree" value="1" checked="checked" style="margin-top:22px;" />
        <?php } else { ?>
        <input type="checkbox" name="agree" value="1" style="margin-top:22px;" />
        <?php } ?>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label" for="input-comment">
     &nbsp;
    </label>
    <div class="col-sm-10">
      <div class="buttons clearfix">
        <div class="clearfix">
          <div class="col-md-7 text-right" style="padding:0px;">
            <a style="float:none; display:inline-block;" href="<?php echo $back; ?>" class="btn btn-danger"><?php echo $button_back; ?>
            </a>
          </div>
          <div class="col-md-5 text-left" style="padding:0px; line-height:57px">
              <input style="margin-right:40px;" type="submit" value="<?php echo $button_submit; ?>" class="btn btn-primary" />
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php } else { ?>
  <div class="form-group">
    <label class="col-sm-2 control-label" for="input-comment">
      &nbsp;
    </label>
    <div class="col-sm-10">
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
          <a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-left">
          <input type="submit" value="<?php echo $button_submit; ?>" class="btn btn-primary pull-right" />
        </div>
      </div>
    </div>
  </div>
  <?php }  ?>
</form>

<script type="text/javascript">
  <!--
$('.date').datetimepicker({
	pickTime: false
});
//-->
</script>

<?php include('catalog/view/theme/' . $config->get('config_template') . '/template/new_elements/wrapper_bottom.tpl'); ?>
<?php echo $footer; ?>