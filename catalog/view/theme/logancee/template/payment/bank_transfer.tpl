<h2><?php echo $text_instruction; ?></h2>
<p><b><?php echo $text_description; ?></b></p>
<div class="well well-sm">
  <p><?php echo $bank; ?></p>
  <p style="color:red;display:none"><?php echo $text_payment; ?></p>
</div>
<div class="row">
  <div class="form-group">
    <label class="col-sm-2 control-label" for="recieptupload">
      <span style="color:red">*</span>هل تم السداد؟
    </label>
    <div class="col-sm-8">
      <select name="recieptupload" id="recieptupload" class="form-control">
        <option value="">الرجاء الاختيار</option>
        <option value="Y">نعم</option>
        <option value="N">لا</option>
      </select>
      <div style="display:none" id="recieptuploaderror">الرجاء تحديد حالة السداد.</div>
    </div>
  </div>
</div>
<div class="row">
  <div class="form-group">
    <label class="col-sm-2 control-label" for="membername">
      <span style="color:red" id="span1">*</span>اسم المودع
    </label>
    <div class="col-sm-8">
      <input type="text" name="membername" id="membername" value="<?php echo $user_f_name ?>"  class="form-control"/>
      <div style="display:none">الرجاء أدخال اسم المودع.</div>
    </div>
  </div>
</div>
<div class="row">
  <div class="form-group">
    <label class="col-sm-2 control-label" for="accountno">
      <span style="color:red" id="span2">*</span>رقم الحساب البنكي
    </label>
    <div class="col-sm-8">
      <input type="text" name="accountno" id="accountno" value=""  class="form-control"/>
      <div style="display:none">الرجاء أدخال رقم الحساب البنكي.</div>
    </div>
  </div>
</div>
 <div class="row">
  <div class="form-group">
    <label class="col-sm-2 control-label" for="sukkatbank">
      <span style="color:red" id="span3">*</span>لاي بنك تم التحويل
    </label>
    <div class="col-sm-8">
      <select name="sukkatbank" id="sukkatbank" class="form-control">
        <option value="">الرجاء الاختيار</option>
        <option value="EPT">ELEGANCE PARFUMS TRADING(18857902)</option>
      </select>
      <div style="display:none" id="sukkatbankerror">الرجاء أختيار اسم البنك.</div>
    </div>
  </div>
</div>
<div class="row">
  <div class="form-group">
    <label class="col-sm-2 control-label" for="button-upload">
      <span data-toggle="tooltip" title="">الإيصال البنكي</span>
    </label>
    <div class="col-sm-5">
      <!--<input type="hidden" name="orderid"  id="hndid" value="<?php echo $order_id; ?>"/>-->
      <input type="hidden" name="code"  id="code"/>
      <button type="button" id="button-upload" data-loading-text=""
        <?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-upload"></i> <?php echo $button_upload; ?>
      </button>
    </div>
  </div>
</div>
<div class="buttons">
  <div class="pull-right">
    <input type="button" value="<?php echo $button_confirm; ?>" id="button-confirm" class="btn btn-primary" data-loading-text="<?php echo $text_loading; ?>" />
  </div>
</div>
<script type="text/javascript">
  <!--
    $('#recieptupload').on('change', function() {
    if($('#recieptupload').val()=="N"){
      $('#span1').hide();
      $('#span2').hide();
      $('#span3').hide();
    }
    else{
      $('#span1').show();
      $('#span2').show();
      $('#span3').show();
    }
  });
  $('#button-upload').on('click', function() {
	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$('#button-upload').button('loading');
				},
				complete: function() {
					$('#button-upload').button('reset');
				},
				success: function(json) {
					var code = json['code'];
          $('#code').val(code);
          $('#button-submit').after('<div class="alert alert-success" style="width: 85%;"><i class="fa fa-check-circle"></i> قد تم تحميل إيصال البنك بنجاح. </div>');
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
$('#button-confirm').on('click', function() {
debugger;
  var isValid=true;  
  if($('#recieptupload').val()=="Y"){
    $('#span1').show();
    $('#span2').show();
    $('#span3').show();
    if($('#membername').val()==""){
      $('#membername').next().show();
      $('#membername').next().addClass( "text-danger" );
      isValid=false;
    }
    else{
       $('#membername').next().hide();
    }
    if($('#accountno').val()==""){
        $('#accountno').next().show();
        $('#accountno').next().addClass( "text-danger" );
       isValid=false;
    }
    else{
      $('#accountno').next().hide();
    }
    if($('#sukkatbank').val()==""){
        $('#sukkatbankerror').show();
        $('#sukkatbankerror').addClass( "text-danger" );
       isValid=false;
    } 
    else{
      $('#sukkatbankerror').hide();
    }
  }
  else{
    $('#span1').show();
    $('#span2').show();
    $('#span3').show();
     if($('#recieptupload').val()==""){
      $('#recieptuploaderror').show();
      $('#recieptuploaderror').addClass( "text-danger" );
      isValid=false;
    }
    else{
       $('#recieptuploaderror').hide();
    }
  }
  if(isValid){
	  $.ajax({
		  url: 'index.php?route=payment/bank_transfer/confirm',
      type: 'post',
		  dataType: 'json',
		  data: 'code='+$('#code').val()+'&account_name='+$('#membername').val()+'&account_number='+$('#accountno').val()+'&sukkat_bank='+$('#sukkatbank').val()+'&bank_transfer_made='+$('#recieptupload').val(),
		  cache: false,
		  beforeSend: function() {
			  $('#button-confirm').button('loading');      
		  },
		  complete: function() {
			  $('#button-confirm').button('reset');
		  },
		  success: function() {
			  location = '<?php echo $continue; ?>';
		  }
	  });
  }
});
//--></script>
