﻿<?php 
if($this->registry->has('theme_options') == true) { 
$class = 3; 
$id = rand(0, 5000)*rand(5000, 50000);
$all = 4; 
$row = 4; 

$theme_options = $this->registry->get('theme_options');
$config = $this->registry->get('config');
$page_direction = $theme_options->get('page_direction' );

if($theme_options->get( 'product_per_pow' ) == 6) { $class = 2; }
if($theme_options->get( 'product_per_pow' ) == 5) { $class = 25; }
if($theme_options->get( 'product_per_pow' ) == 3) { $class = 4; }

if($theme_options->get( 'product_per_pow' ) > 1) { $row = $theme_options->get( 'product_per_pow' ); $all = $theme_options->get( 'product_per_pow' ); } ?>
<div class="box clearfix box-with-products" <?php if($theme_options->get( 'product_scroll_specials' ) != '0') { echo 'with-scroll';  } ?>" id="categoryproduct">
  <?php if($theme_options->get( 'product_scroll_specials' ) != '0') { ?>
  <!-- Carousel nav
  <a class="next" href="#myCarousel"
    <?php echo $id; ?>" id="myCarousel<?php echo $id; ?>_next"><span></span>
  </a>
  <a class="prev" href="#myCarousel"
    <?php echo $id; ?>" id="myCarousel<?php echo $id; ?>_prev"><span></span>
  </a>-->
  <?php } ?>

  <div class="box-heading">
    <?php echo $heading_title; ?>
  </div>
  <div class="strip-line"></div>
  <div class="box-content products">
    <div class="box-product">
		<?php if ($products) { ?>
<!-- Filter -->
  <div class="product-filter clearfix" style="display:none">
  	<div class="options">
  		<div class="button-group display" data-toggle="buttons-radio">
  			<button id="grid" <?php if($theme_options->get('default_list_grid') == '1') { echo 'class="active"'; } ?> rel="tooltip" title="Grid" onclick="display('grid');"><i class="fa fa-th-large"></i></button>
  			<button id="list" <?php if($theme_options->get('default_list_grid') != '1') { echo 'class="active"'; } ?> rel="tooltip" title="List" onclick="display('list');"><i class="fa fa-th-list"></i></button>
  		</div>
  	</div>
	<div class="list-options">
  		<div class="sort">
			<div class="product-info">
				<?php foreach ($categories as $category) { ?>
					<div class="radio radio-type-button2">
						<label>
							<input type="radio" name="option" value="<?php echo $category['href']; ?>" onchange="location = this.value;"/>
							<?php if ($category['category_id'] == $category_id) { ?>
								<span class="active"><?php echo $category['name']; ?></span>
  							 <?php } else { ?>
								<span><?php echo $category['name']; ?></span>
  							 <?php } ?>
						</label>
					</div>
				<?php } ?>
			</div>
	</div></div>
  </div>
  <!-- Products list -->
  <div class="products-list product-list"<?php if(!($theme_options->get('default_list_grid') == '1')) { echo ' class="active"'; } ?>>
  	<?php foreach ($products as $product) { 
  	$product_detail = $theme_options->getDataProduct( $product['product_id'] );
  	$text_new = 'New';
  	if($theme_options->get( 'latest_text', $config->get( 'config_language_id' ) ) != '') {
  	    $text_new = $theme_options->get( 'latest_text', $config->get( 'config_language_id' ) );
  	} ?>
  	<div class="row" style="border-bottom:1px solid #d5d5d5">
  	     <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="display:none">
  	          <div class="product-list-img">
  	               <div class="product-show">
  	               	<?php if($product['special'] && $theme_options->get( 'display_text_sale' ) != '0') { ?>
  	               		<?php $text_sale = 'Sale';
  	               		if($theme_options->get( 'sale_text', $config->get( 'config_language_id' ) ) != '') {
  	               			$text_sale = $theme_options->get( 'sale_text', $config->get( 'config_language_id' ) );
  	               		} ?>
  	               		<?php if($theme_options->get( 'type_sale' ) == '1') { ?>
  	               		<?php $product_detail = $theme_options->getDataProduct( $product['product_id'] );
  	               		$roznica_ceny = $product_detail['price']-$product_detail['special'];
  	               		$procent = ($roznica_ceny*100)/$product_detail['price']; ?>
  	               		<div class="sale">-<?php echo round($procent); ?>%</div>
  	               		<?php } else { ?>
  	               		<div class="sale"><?php echo $text_sale; ?></div>
  	               		<?php } ?>
  	               	<?php } ?>
  	               	
  	               	<?php if($product_detail['is_latest'] && $theme_options->get( 'display_text_latest' ) != '0'):?>
  	               	    <div class="new-label"><span><?php echo $text_new; ?></span></div>
  	               	<?php endif; ?>
  	               	
  	                    <a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>" class="product-image">
  	                         <span class="front margin-image">
  	                         	<?php if($product['thumb']) { ?>
  	                         		<?php if($theme_options->get( 'lazy_loading_images' ) != '0') { ?>
  	                         		<img src="image/catalog/blank.gif" data-echo="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" />
  	                         		<?php } else { ?>
  	                         		<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" />
  	                         		<?php } ?>
  	                         	<?php } else { ?>
  	                         	<img src="image/no_image.jpg" alt="<?php echo $product['name']; ?>" />
  	                         	<?php } ?>
  	                         </span>
  	                         
  	                         <?php if($theme_options->get( 'product_image_effect' ) == '1') {
  	                         	$nthumb = str_replace(' ', "%20", ($product['thumb']));
  	                         	$nthumb = str_replace(HTTP_SERVER, "", $nthumb);
  	                         	$image_size = getimagesize($nthumb);
  	                         	$image_swap = $theme_options->productImageSwap($product['product_id'], $image_size[0], $image_size[1]);
  	                         	if($image_swap != '') echo '<span class="product-img-additional back margin-image"><img src="' . $image_swap . '" alt="' . $product['name'] . '" class="swap-image" /></span>';
  	                         } ?> 
  	                    </a>
  	                    
  	                    <?php if($theme_options->get( 'quick_view' ) == 1) { ?>
  	                    <div class="category-over product-show-box">
  	                         <div class="main-quickview quickview">  	                              
  	                              <a class="btn show-quickview" href="index.php?route=product/quickview&product_id=<?php echo $product['product_id']; ?>" title="<?php echo $product['name']; ?>"><span><i aria-hidden="true" class="arrow_expand"></i></span></a>
  	                         </div>
  	                    </div>
  	                    <?php } ?>
  	               </div>
  	          </div>
  	     </div>  	     
  	     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  	          <div class="product-shop">
  	               <h2 class="product-name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a><?php if($product_detail['quantity'] <= '0') {?><span style="color:red;font-weight: bold;">( نفدت الكمية )</span><?php } ?></h2>
				   <div style="display:none">
						<?php if ($product['quantity']) { ?>
  	                    	<span class="regular-price"><span class="price"><?php echo $product['quantity']; ?></span></span> 
  						<?php } ?>
				   </div>
				   <div style="display:none">
						<?php if ($product['weight']) { ?>
  	                    	<span class="regular-price"><span class="price"><?php echo $product['weight']; ?></span></span> | 
  						<?php } ?>
				   </div>					
  	               <div class="product-value">
  	                    <div class="price-box">
  	                    	<?php if (!$product['special']) { ?>
  	                    	<span class="regular-price"><span class="price" id="price-old_<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></span></span>
  	                    	<?php } else { ?>
  	                    	<p class="old-price">
  	                    	     <span class="price" id="price-old_<?php echo $product['product_id']; ?>"><?php echo $product['price']; ?></span>
  	                    	</p>  	                    	
  	                    	<p class="special-price">
  	                    	     <span class="price" id="price-old_<?php echo $product['product_id']; ?>"><?php echo $product['special']; ?></span>
  	                    	</p>
  	                    	<?php } ?>
  	                    </div>
  	                    
  	                    <?php if ($product['rating']) { ?>
  	                    <div class="rating-reviews clearfix">
  	                         <div class="rating"><i class="fa fa-star<?php if($product['rating'] >= 1) { echo ' active'; } ?>"></i><i class="fa fa-star<?php if($product['rating'] >= 2) { echo ' active'; } ?>"></i><i class="fa fa-star<?php if($product['rating'] >= 3) { echo ' active'; } ?>"></i><i class="fa fa-star<?php if($product['rating'] >= 4) { echo ' active'; } ?>"></i><i class="fa fa-star<?php if($product['rating'] >= 5) { echo ' active'; } ?>"></i></div>
  	                    </div>
  	                    <?php } ?>
  	               </div>
					<?php if ($product['quantity_per'] !=-1) { ?>
						<div>
							<?php if($product['quantity_per'] >=50) { ?>
								<span class="regular-price"><span class="price" style="font-size: 17px;color:#fff;background-color:<?php echo $product['quantity_color']; ?>">متوفر حالياً</span></span>
								
							<?php } else if($product['quantity_per'] < 50 && $product['quantity_per'] >0) { ?>
								<span class="regular-price"><span class="price" style="font-size: 17px;color:#fff;background-color:<?php echo $product['quantity_color']; ?>">شارف على الانتهاء</span></span> 
								
							<?php } else {?>
								<span class="regular-price"><span class="price" style="font-size: 17px;color:#fff;background-color:<?php echo $product['quantity_color']; ?>">الكمية نفدت</span></span>								
							<?php }?>
						</div>
					<?php } ?>
				   <div id="product" style="width:100%">
				   <div class="product-info listoption options" style="padding-bottom: 0px !important;">
				   		<?php if ($product['options']) {  ?>
						<?php foreach ($product['options'] as $option) { ?>
						   <?php if ($option['type'] == 'radio') { ?>
							<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
							  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
							  <div id="input-option<?php echo $option['product_option_id']; ?>">
								<?php foreach ($option['product_option_value'] as $option_value) { ?>
									<?php if ($option_value['quantity'] > 0) { ?>
										<div class="radio <?php if($theme_options->get( 'product_page_radio_style' ) == 1) { echo 'radio-type-button2'; } ?>">
										  <label>
											<input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" data-productid="<?php echo $product['product_id']; ?>"/>
											<span><?php echo $option_value['name']; ?>
											 <?php if ($option_value['price']) { ?>(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)<?php } ?>										
											  <?php if ($option_value['discount_price']) { ?>(<?php echo $option_value['discount_price']; ?>)<?php } ?>
											  <?php if ($option_value['discount']) { ?>(<?php echo $option_value['discount']; ?>)<?php } ?>
											  </span>
										  </label>
										</div>
									 <?php } ?>
								<?php } ?>
			            
								<?php if($theme_options->get( 'product_page_radio_style' ) == 1) { ?>
								<script type="text/javascript">
									 $(document).ready(function(){
										  $('#input-option<?php echo $option['product_option_id']; ?>').on('click', 'span', function () {
											   $('#input-option<?php echo $option['product_option_id']; ?> span').removeClass("active");
											   $(this).addClass("active");
											   var productId = $(this).prev().attr('data-productid');
											   $('#product_id').val(productId);
										  });
									 });
								</script>
								<?php } ?>
							  </div>
							</div>
							<?php } ?>
							<?php } ?>
						<?php } ?>						
  	               </div>
				   </div>
  	               <div class="desc std" style='display:none'><?php echo $product['description']; ?></div>
  	               <div class="typo-actions clearfix">
  	                    <div class="addtocart">
  	                           <?php $enquiry = false; if($config->get( 'product_blocks_module' ) != '') { $enquiry = $theme_options->productIsEnquiry($product['product_id']); }
  	                           if(is_array($enquiry)) { ?>
  	                           <a href="javascript:openPopup('<?php echo $enquiry['popup_module']; ?>', '<?php echo $product['product_id']; ?>')" class="button button-enquiry">
  	                                <?php if($enquiry['icon'] != '' && $enquiry['icon_position'] == 'left') { echo '<img src="image/' . $enquiry['icon']. '" align="left" class="icon-enquiry" alt="Icon">'; } ?>
  	                                <span class="text-enquiry"><?php echo $enquiry['block_name']; ?></span>
  	                                <?php if($enquiry['icon'] != '' && $enquiry['icon_position'] == 'right') { echo '<img src="image/' . $enquiry['icon']. '" align="right" class="icon-enquiry" alt="Icon">'; } ?>
  	                           </a>
  	                           <?php } else { ?>
							   <?php if($product_detail['quantity'] != '0') {?>
								 <?php if ($product['options']) {  ?>
  										<a class="button addtocartlist" data-productid="<?php echo $product['product_id']; ?>"><?php echo $button_cart; ?></a>
								   <?php } else { ?>
										<a onclick="cart.add('<?php echo $product['product_id']; ?>');" class="button"><?php echo $button_cart; ?></a>
									<?php } ?>
							   <?php } else {?>
									<a onclick="showWarningMessage()" class="button btn-cart"><?php echo $button_cart; ?></a>
								<?php } ?>								  
  	                           <?php } ?>
  	                    </div>

  	                    <div class="addtolist" style="display:none">
  	                         <div class="add-to-links">
  	                              <div class="wishlist">
  	                                   <a onclick="wishlist.add('<?php echo $product['product_id']; ?>');" title="" data-toggle="tooltip" data-placement="top" class="link-wishlist" data-original-title="<?php if($theme_options->get( 'add_to_wishlist_text', $config->get( 'config_language_id' ) ) != '') { echo $theme_options->get( 'add_to_wishlist_text', $config->get( 'config_language_id' ) ); } else { echo 'Add to wishlist'; } ?>"><i aria-hidden="true" class="icon_heart_alt"></i></a>
  	                              </div>
  	                              
  	                              <div class="compare">
  	                                   <a rel="nofollow" onclick="compare.add('<?php echo $product['product_id']; ?>');" title="" data-toggle="tooltip" data-placement="top" class="link-compare" data-original-title="<?php if($theme_options->get( 'add_to_compare_text', $config->get( 'config_language_id' ) ) != '') { echo $theme_options->get( 'add_to_compare_text', $config->get( 'config_language_id' ) ); } else { echo 'Add to compare'; } ?>"><i aria-hidden="true" class="icon_piechart"></i></a>
  	                              </div>
  	                         </div>
  	                    </div>
  	               </div>
  	          </div>
  	     </div>
  	</div>
  	<?php } ?>
	<div id="productid">
		<input type="hidden" name="product_id" id="product_id" size="2"/>
	</div>
  </div>
  <!-- Products grid -->
  <?php 
  $class = 3; 
  $row = 4; 
  $id = rand(0, 5000)*rand(5000, 50000);
  
  if($theme_options->get( 'product_per_pow2' ) == 6) { $class = 2; }
  if($theme_options->get( 'product_per_pow2' ) == 5) { $class = 25; }
  if($theme_options->get( 'product_per_pow2' ) == 3) { $class = 4; }
  
  if($theme_options->get( 'product_per_pow2' ) > 1) { $row = $theme_options->get( 'product_per_pow2' ); } 
  ?>
  <div class="products-grid product-grid"<?php if($theme_options->get('default_list_grid') == '1') { echo ' class="active"'; } ?>>
  	<div class="row">
	  	<?php $row_fluid = 0; foreach ($products as $product) { $row_fluid++; ?>
		  	<?php $r=$row_fluid-floor($row_fluid/$row)*$row; if($row_fluid>$row && $r == 1) { echo '</div><div class="row">'; } ?>
		  	<div class="col-sm-<?php echo $class; ?> col-xs-6">
		  	    <?php include('catalog/view/theme/'.$config->get('config_template').'/template/new_elements/product.tpl'); ?>
		  	</div>
	    <?php } ?>
    </div>
  </div>  
<?php } ?>
    </div>
  </div>
</div>
<?php 
if($this->registry->has('theme_options') == true) { 
$class = 3; 
$id = rand(0, 5000)*rand(5000, 50000);
$all = 4; 
$row = 4; 

$theme_options = $this->registry->get('theme_options');
$config = $this->registry->get('config');
$page_direction = $theme_options->get( 'page_direction' );

if($theme_options->get( 'product_per_pow' ) == 6) { $class = 2; }
if($theme_options->get( 'product_per_pow' ) == 5) { $class = 25; }
if($theme_options->get( 'product_per_pow' ) == 3) { $class = 4; }

if($theme_options->get( 'product_per_pow' ) > 1) { $row = $theme_options->get( 'product_per_pow' ); $all = $theme_options->get( 'product_per_pow' ); } 
?>
<div class="box clearfix box-with-products" with-scroll"="" id="categoryproduct1">
    <div class="box-heading">From The Store</div>
    <div class="strip-line"></div>
    <div class="box-content products">
        <div class="box-product">
            <div id="myCarousel<?php echo $id; ?>" <?php if($theme_options->get( 'product_scroll_latest' ) != '0') { ?>class="carousel slide"<?php } ?>>
    		    <!-- Carousel items <div class="carousel-inner">-->
    		    <div class="carousel-inner">
    			    <?php $i = 0; $row_fluid = 0; $item = 0; foreach ($categories as $category) { $row_fluid++; ?>
	    			    <?php if($i == 0) { $item++; echo '<div class="item active"><div class="product-grid"><div class="row" style="margin-bottom: 20px;">'; } ?>
	    			    <?php $r=$row_fluid-floor($row_fluid/$all)*$all; if($row_fluid>$all && $r == 1) { if($theme_options->get( 'product_scroll_latest' ) != '0') { echo '</div></div></div><div class="item"><div class="product-grid"><div class="row" style="margin-bottom: 20px;">'; $item++; } else { echo '</div><div class="row">'; } } else { $r=$row_fluid-floor($row_fluid/$row)*$row; if($row_fluid>$row && $r == 1) { echo '</div><div class="row">'; } } ?>
	    			    <div class="col-sm-<?php echo $class; ?> col-xs-6 <?php if($class == 2) { echo 'col-md-25 col-lg-2 col-sm-3 '; } if($class == 2 && $r == 0) { echo 'hidden-md hidden-sm'; } if($class == 2 && $r == 5) { echo 'hidden-sm'; } ?> <?php if($class == 25) { echo 'col-md-25 col-lg-25 col-sm-3 '; } if($class == 25 && $r == 0) { echo 'hidden-sm'; } ?> <?php if($class == 3) { echo 'col-md-4 col-lg-3 col-sm-4 '; } if($class == 3 && $r == 0) { echo 'hidden-sm hidden-md'; } ?>">
	    				    <div class="product product-layout clearfix product-hover">
                                <div class="left">
                                    <div class="image image-swap-effect">
                                        <?php if ($category['thumb']) { ?>
				                            <a href="#"><img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>" class=""></a>
                                        <?php } else { ?>
                                            <a href="#"><img src="image/no_image.jpg" alt="<?php echo $category['name']; ?>" class=""></a>
                                        <?php } ?>
			                        </div>
                                </div>
                                <div class="right">
                                    <div class="name"><a href="#"><?php echo $category['name']; ?>(<?php echo $category['count']; ?>)</a></div>
                                </div>
                           </div>
	    			    </div>
    			    <?php $i++; } ?>
    			    <?php if($i > 0) { echo '</div></div></div>'; } ?>
    		    </div>
		    </div>
        </div>
    </div>
</div>
<?php } ?>
<script type="text/javascript"><!--
function display(view) {
	if (view == 'list') {
		$('.products-grid').css("display", "none");//.removeClass("active");
		$('.products-list').css("display", "block");//addClass("active");

		$('.display').html('<button id="grid" rel="tooltip" title="Grid" onclick="display(\'grid\');"><i class="fa fa-th-large"></i></button> <button class="active" id="list" rel="tooltip" title="List" onclick="display(\'list\');"><i class="fa fa-th-list"></i></button>');
		
		localStorage.setItem('display', 'list');
	} else {
	
		$('.products-grid').css("display", "block");
		$('.products-list').css("display", "none");
		//$('.product-grid').addClass("active");
		//$('.product-list').removeClass("active");
					
		$('.display').html('<button class="active" id="grid" rel="tooltip" title="Grid" onclick="display(\'grid\');"><i class="fa fa-th-large"></i></button> <button id="list" rel="tooltip" title="List" onclick="display(\'list\');"><i class="fa fa-th-list"></i></button>');
		
		localStorage.setItem('display', 'grid');
	}
}

if (localStorage.getItem('display') == 'list') {
	display('list');
} else {
	display('grid');
}
--></script>
<script type="text/javascript">
          <!--
$('.addtocartlist').on('click', function() {
	 var productId = $(this).attr('data-productid');
	$('#product_id').val(productId);
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',		
		data: $('#productid input[type=\'hidden\'], #product input[type=\'radio\']:checked'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-cart').button('loading');
		},
		complete: function() {
			$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			if (json['error']) {
        if(json['error']['stock']){
            $.notify({
					  message: json['error']['stock'],
					  target: '_blank'
				  },{
					  // settings
					  element: 'body',
					  position: null,
					  type: "info",
					  allow_dismiss: true,
					  newest_on_top: false,
					  placement: {
						  from: "top",
						  align: "right"
					  },
					  offset: 20,
					  spacing: 10,
					  z_index: 2031,
					  delay: 5000,
					  timer: 1000,
					  url_target: '_blank',
					  mouse_over: null,
					  animate: {
						  enter: 'animated fadeInDown',
						  exit: 'animated fadeOutUp'
					  },
					  onShow: null,
					  onShown: null,
					  onClose: null,
					  onClosed: null,
					  icon_type: 'class',
					  template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-danger" role="alert">' +
						  '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
						  '<span data-notify="message"><i class="fa fa-check-circle"></i>&nbsp; {2}</span>' +
						  '<div class="progress" data-notify="progressbar">' +
							  '<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
						  '</div>' +
						  '<a href="{3}" target="{4}" data-notify="url"></a>' +
					  '</div>' 
				  });
        }
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));
						
						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}
				
				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}
				
				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}
			
			if (json['success']) {
				$('span').removeClass("active");
				$.notify({
					message: json['success'],
					target: '_blank'
				},{
					// settings
					element: 'body',
					position: null,
					type: "info",
					allow_dismiss: true,
					newest_on_top: false,
					placement: {
						from: "top",
						align: "right"
					},
					offset: 20,
					spacing: 10,
					z_index: 2031,
					delay: 5000,
					timer: 1000,
					url_target: '_blank',
					mouse_over: null,
					animate: {
						enter: 'animated fadeInDown',
						exit: 'animated fadeOutUp'
					},
					onShow: null,
					onShown: null,
					onClose: null,
					onClosed: null,
					icon_type: 'class',
					template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-success" role="alert">' +
						'<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
						'<span data-notify="message"><i class="fa fa-check-circle"></i>&nbsp; {2}</span>' +
						'<div class="progress" data-notify="progressbar">' +
							'<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
						'</div>' +
						'<a href="{3}" target="{4}" data-notify="url"></a>' +
					'</div>' 
				});
				
				$('#cart_block #cart_content').load('index.php?route=common/cart/info #cart_content_ajax');
				$('#cart_block #total_price_ajax').load('index.php?route=common/cart/info #total_price');
				$('#cart_block #total_item_ajax').load('index.php?route=common/cart/info #total_item');
			}
		},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
});
//--></script> 
<script type="text/javascript">
var ajax_price = function() {
	$.ajax({
		type: 'POST',
		url: 'index.php?route=product/liveprice/index',
		data: $('.product-info input[type=\'text\'], #productid input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),
		dataType: 'json',
			success: function(json) {
			if (json.success) {
				change_price('#price-special', json.new_price.special);
				change_price('#price-tax', json.new_price.tax);
				change_price('#price-old', json.new_price.price);
			}
		}
	});
}

var change_price = function(id, new_price) {
	var productid=$('#product_id').val();
	var newid = id+"_"+productid;
	$(newid).text(new_price);
}

$('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\'], .product-info input[type=\'checkbox\'], .product-info select, .product-info textarea, .product-info input[name=\'quantity\']').on('change', function() {
	ajax_price();
});
function showWarningMessage(){
	alert('الكمية نفدت');
	return false;
}
</script>
<script type="text/javascript">
$(document).ready(function() {
  var owl<?php echo $id; ?> = $(".box #myCarousel<?php echo $id; ?> .carousel-inner");
	
  $("#myCarousel<?php echo $id; ?>_next").click(function(){
      owl<?php echo $id; ?>.trigger('owl.next');
      return false;
    })
  $("#myCarousel<?php echo $id; ?>_prev").click(function(){
      owl<?php echo $id; ?>.trigger('owl.prev');
      return false;
  });
    
  owl<?php echo $id; ?>.owlCarousel({
  	  slideSpeed : 500,
      singleItem:true,
      <?php if($page_direction[$config->get( 'config_language_id' )] == 'RTL'): ?>
      direction: 'rtl'
      <?php endif; ?>
   });
});
</script>
<?php } ?>