<?php echo $header; 
if(isset($mfilter_json)) {
	if(!empty($mfilter_json)) { 
		echo '<div id="mfilter-json" style="display:none">' . base64_encode( $mfilter_json ) . '</div>'; 
	} 
}

$theme_options = $this->registry->get('theme_options');
$config = $this->registry->get('config'); 
include('catalog/view/theme/'.$config->get('config_template').'/template/new_elements/wrapper_top.tpl'); ?>
<div id="mfilter-content-container">
  <?php if ($thumb || $description) { ?>
  <div class="category-info clearfix">
    <?php if ($thumb) { ?>
    <div class="image"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" /></div>
    <?php } ?>
    <?php if ($description) { ?>
    <?php echo $description; ?>
    <?php } ?>
  </div>
  <?php } ?>
  <?php if ($categories && $theme_options->get('refine_search_style') != '2') { ?>
  <h2 class="refine_search"><?php echo $text_refine; ?></h2>
  <div class="category-list<?php if ($theme_options->get('refine_search_style') == '1') { echo ' category-list-text-only'; } ?>">
  	<div class="row">
  	  <?php 
  	  $class = 3; 
  	  $row = 4; 
  	  
  	  if($theme_options->get( 'refine_search_number' ) == 2) { $class = 62; }
  	  if($theme_options->get( 'refine_search_number' ) == 5) { $class = 25; }
  	  if($theme_options->get( 'refine_search_number' ) == 3) { $class = 4; }
  	  if($theme_options->get( 'refine_search_number' ) == 6) { $class = 2; }
  	  
  	  if($theme_options->get( 'refine_search_number' ) > 1) { $row = $theme_options->get( 'refine_search_number' ); } 
  	  ?>
	  <?php $row_fluid = 0; foreach ($theme_options->refineSearch() as $category) { $row_fluid++; ?>
	  	<?php 
	  	if ($theme_options->get('refine_search_style') != '1') {
	  		$width = 250;
	  		$height = 250;
	  		if($theme_options->get( 'refine_image_width' ) > 20) $width = $theme_options->get( 'refine_image_width' );
	  		if($theme_options->get( 'refine_image_height' ) > 20) $height = $theme_options->get( 'refine_image_height' );
	  		$model_tool_image = $this->registry->get('model_tool_image');
		  	if($category['thumb'] != '') { 
		  		$image = $model_tool_image->resize($category['thumb'], $width, $height); 
		  	} else { 
		  		$image = $model_tool_image->resize('no_image.jpg', $width, $height); 
		  	} 
	  	}
	  	?>
	  	<?php $r=$row_fluid-floor($row_fluid/$row)*$row; if($row_fluid>$row && $r == 1) { echo '</div><div class="row">'; } ?>
	  	<div class="col-sm-<?php echo $class; ?> col-xs-6">
	  		<?php if ($theme_options->get('refine_search_style') != '1') { ?>
		  	<a href="<?php echo $category['href']; ?>"><img src="<?php echo $image; ?>" alt="<?php echo $category['name']; ?>" /></a>
		  	<?php } ?>
		  	<a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
	  	</div>
	  <?php } ?>
	</div>
  </div>
  <?php } ?>
  <?php if ($products) { ?>
  <!-- Filter -->
  <div class="product-filter clearfix">
  	<div class="options">
  		<div class="product-compare" style="display:none"><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></div>
  		
  		<div class="button-group display" data-toggle="buttons-radio">
  			<button id="grid" <?php if($theme_options->get('default_list_grid') == '1') { echo 'class="active"'; } ?> rel="tooltip" title="Grid" onclick="display('grid');"><i class="fa fa-th-large"></i></button>
  			<button id="list" <?php if($theme_options->get('default_list_grid') != '1') { echo 'class="active"'; } ?> rel="tooltip" title="List" onclick="display('list');"><i class="fa fa-th-list"></i></button>
  		</div>
  	</div>
  	
  	<div class="list-options">
  		<div class="sort">
  			<span class="hideit"><?php echo $text_sort; ?></span>
  			<select onchange="location = this.value;" style="padding-right: 10px;">
  			  <?php foreach ($sorts as $sorts) { ?>
  			  <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
  			  <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
  			  <?php } else { ?>
  			  <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
  			  <?php } ?>
  			  <?php } ?>
  			</select>
  		</div>
  		
  		<div class="limit">
  			<?php echo $text_limit; ?>
  			<select onchange="location = this.value;" style="padding-right: 10px;width:100px">
  			  <?php foreach ($limits as $limits) { ?>
  			  <?php if ($limits['value'] == $limit) { ?>
  			  <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
  			  <?php } else { ?>
  			  <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
  			  <?php } ?>
  			  <?php } ?>
  			</select>
  		</div>
  	</div>
  </div>
  
  <!-- Products list -->
  <div class="product-list clearfix"<?php if(!($theme_options->get('default_list_grid') == '1')) { echo ' class="active"'; } ?>>
  	<?php foreach ($products as $product) { 
  	$product_detail = $theme_options->getDataProduct( $product['product_id'] );
  	$text_new = 'New';
  	if($theme_options->get( 'latest_text', $config->get( 'config_language_id' ) ) != '') {
  	    $text_new = $theme_options->get( 'latest_text', $config->get( 'config_language_id' ) );
  	} ?>
  	<div class="row" style="border-bottom:1px solid #d5d5d5">
  	     <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" style="display:none">
  	          <div class="product-list-img">
  	               <div class="product-show">
  	               	<?php if($product['special'] && $theme_options->get( 'display_text_sale' ) != '0') { ?>
  	               		<?php $text_sale = 'Sale';
  	               		if($theme_options->get( 'sale_text', $config->get( 'config_language_id' ) ) != '') {
  	               			$text_sale = $theme_options->get( 'sale_text', $config->get( 'config_language_id' ) );
  	               		} ?>
  	               		<?php if($theme_options->get( 'type_sale' ) == '1') { ?>
  	               		<?php $product_detail = $theme_options->getDataProduct( $product['product_id'] );
  	               		$roznica_ceny = $product_detail['price']-$product_detail['special'];
  	               		$procent = ($roznica_ceny*100)/$product_detail['price']; ?>
  	               		<div class="sale">-<?php echo round($procent); ?>%</div>
  	               		<?php } else { ?>
  	               		<div class="sale"><?php echo $text_sale; ?></div>
  	               		<?php } ?>
  	               	<?php } ?>
  	               	
  	               	<?php if($product_detail['is_latest'] && $theme_options->get( 'display_text_latest' ) != '0'):?>
  	               	    <div class="new-label"><span><?php echo $text_new; ?></span></div>
  	               	<?php endif; ?>
  	               	
  	                    <a href="<?php echo $product['href']; ?>" title="<?php echo $product['name']; ?>" class="product-image">
  	                         <span class="front margin-image">
  	                         	<?php if($product['thumb']) { ?>
  	                         		<?php if($theme_options->get( 'lazy_loading_images' ) != '0') { ?>
  	                         		<img src="image/catalog/blank.gif" data-echo="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" />
  	                         		<?php } else { ?>
  	                         		<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" />
  	                         		<?php } ?>
  	                         	<?php } else { ?>
  	                         	<img src="image/no_image.jpg" alt="<?php echo $product['name']; ?>" />
  	                         	<?php } ?>
  	                         </span>
  	                         
  	                         <?php if($theme_options->get( 'product_image_effect' ) == '1') {
  	                         	$nthumb = str_replace(' ', "%20", ($product['thumb']));
  	                         	$nthumb = str_replace(HTTP_SERVER, "", $nthumb);
  	                         	$image_size = getimagesize($nthumb);
  	                         	$image_swap = $theme_options->productImageSwap($product['product_id'], $image_size[0], $image_size[1]);
  	                         	if($image_swap != '') echo '<span class="product-img-additional back margin-image"><img src="' . $image_swap . '" alt="' . $product['name'] . '" class="swap-image" /></span>';
  	                         } ?> 
  	                    </a>
  	                    
  	                    <?php if($theme_options->get( 'quick_view' ) == 1) { ?>
  	                    <div class="category-over product-show-box">
  	                         <div class="main-quickview quickview">  	                              
  	                              <a class="btn show-quickview" href="index.php?route=product/quickview&product_id=<?php echo $product['product_id']; ?>" title="<?php echo $product['name']; ?>"><span><i aria-hidden="true" class="arrow_expand"></i></span></a>
  	                         </div>
  	                    </div>
  	                    <?php } ?>
  	               </div>
  	          </div>
  	     </div>  	     
  	     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  	          <div class="product-shop clearfix">
  	               <h2 class="product-name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h2>
                  <div>
                    <?php if ($product['quantity']) { ?>
                      <span class="regular-price"><span class="price"><?php echo $product['quantity']; ?></span></span> |
                    <?php } ?>
                  </div>
                  <div>
                    <?php if ($product['weight']) { ?>
                      <span class="regular-price"><span class="price"><?php echo $product['weight']; ?></span></span> |
                    <?php } ?>
                  </div>
  	               <div class="product-value">
  	                    <div class="price-box">
  	                    	<?php if (!$product['special']) { ?>
  	                    	<span class="regular-price"><span class="price"><?php echo $product['price']; ?></span></span>
  	                    	<?php } else { ?>
  	                    	<p class="old-price">
  	                    	     <span class="price"><?php echo $product['price']; ?></span>
  	                    	</p>
  	                    	
  	                    	<p class="special-price">
  	                    	     <span class="price"><?php echo $product['special']; ?></span>
  	                    	</p>
  	                    	<?php } ?>
  	                    </div>
  	                    
  	                    <?php if ($product['rating']) { ?>
  	                    <div class="rating-reviews clearfix">
  	                         <div class="rating"><i class="fa fa-star<?php if($product['rating'] >= 1) { echo ' active'; } ?>"></i><i class="fa fa-star<?php if($product['rating'] >= 2) { echo ' active'; } ?>"></i><i class="fa fa-star<?php if($product['rating'] >= 3) { echo ' active'; } ?>"></i><i class="fa fa-star<?php if($product['rating'] >= 4) { echo ' active'; } ?>"></i><i class="fa fa-star<?php if($product['rating'] >= 5) { echo ' active'; } ?>"></i></div>
  	                    </div>
  	                    <?php } ?>
  	               </div>
                  <!--<div id="product">
                    <div class="product-info listoption options" style="padding-bottom: 0px !important;">
                      <?php if ($product['options']) {  ?>
                      <?php foreach ($product['options'] as $option) { ?>
                      <?php if ($option['type'] == 'radio') { ?>
                      <div class="form-group"
                        <?php echo ($option['required'] ? ' required' : ''); ?>">
                        <div id="input-option"
                          <?php echo $option['product_option_id']; ?>">
                          <?php foreach ($option['product_option_value'] as $option_value) { ?>
                          <div class="radio"
                            <?php if($theme_options->get( 'product_page_radio_style' ) == 1) { echo 'radio-type-button2'; } ?>">
                            <label>
                              <input type="radio" name="option["<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                              <span>
                                <?php echo $option_value['name']; ?>
                                <?php if ($option_value['price']) { ?>(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)<?php } ?>
                              </span>
                            </label>
                          </div>
                          <?php } ?>

                          <?php if($theme_options->get( 'product_page_radio_style' ) == 1) { ?>
                          <script type="text/javascript">
                            $(document).ready(function(){
                            $('#input-option<?php echo $option['product_option_id']; ?>').on('click', 'span', function () {
                            $('#input-option<?php echo $option['product_option_id']; ?> span').removeClass("active");
                            $(this).addClass("active");
                            });
                            });
                          </script>
                          <?php } ?>
                        </div>
                      </div>
                      <?php } ?>
                      <?php } ?>
                      <?php } ?>
                      <input type="hidden" name="product_id" size="2" value=""<?php echo $product['product_id']; ?>" />
                   </div>
                </div>--> 
                <?php if ($product['quantity_per'] !=-1) { ?>
						<div>
							<?php if($product['quantity_per'] >=50) { ?>
								<span class="regular-price"><span class="price" style="font-size: 17px;color:#fff;background-color:<?php echo $product['quantity_color']; ?>">متوفر حالياً</span></span>
								
							<?php } else if($product['quantity_per'] < 50 && $product['quantity_per'] >0) { ?>
								<span class="regular-price"><span class="price" style="font-size: 17px;color:#fff;background-color:<?php echo $product['quantity_color']; ?>">شارف على الانتهاء</span></span> 
								
							<?php } else {?>
								<span class="regular-price"><span class="price" style="font-size: 17px;color:#fff;background-color:<?php echo $product['quantity_color']; ?>">الكمية نفدت</span></span>								
							<?php }?>
						</div>
					<?php } ?>
                <div id="product">
				   <div class="product-info listoption options" style="padding-bottom: 0px !important;">
				   		<?php if ($product['options']) {  ?>
						<?php foreach ($product['options'] as $option) { ?>
						   <?php if ($option['type'] == 'radio') { ?>
							<div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
							  <div id="input-option<?php echo $option['product_option_id']; ?>">
								<?php foreach ($option['product_option_value'] as $option_value) { ?>
									<?php if ($option_value['quantity'] > 0) { ?>
										<div class="radio <?php if($theme_options->get( 'product_page_radio_style' ) == 1) { echo 'radio-type-button2'; } ?>">
										  <label>
											<input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" data-productid="<?php echo $product['product_id']; ?>"/>
											<span><?php echo $option_value['name']; ?>
											 <?php if ($option_value['price']) { ?>(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)<?php } ?>										
											  <?php if ($option_value['discount_price']) { ?>(<?php echo $option_value['discount_price']; ?>)<?php } ?>
											  <?php if ($option_value['discount']) { ?>(<?php echo $option_value['discount']; ?>)<?php } ?>
											  </span>
										  </label>
										</div>
									 <?php } ?>
								<?php } ?>
			            
								<?php if($theme_options->get( 'product_page_radio_style' ) == 1) { ?>
								<script type="text/javascript">
									 $(document).ready(function(){
										  $('#input-option<?php echo $option['product_option_id']; ?>').on('click', 'span', function () {
											   $('#input-option<?php echo $option['product_option_id']; ?> span').removeClass("active");
											   $(this).addClass("active");
											   var productId = $(this).prev().attr('data-productid');
											   $('#product_id').val(productId);
										  });
									 });
								</script>
								<?php } ?>
							  </div>
							</div>
							<?php } ?>
							<?php } ?>
						<?php } ?>						
  	               </div>
				   </div>
  	               <div class="desc std" style="display:none"><?php echo $product['description']; ?></div>
  	               <div class="typo-actions clearfix">
  	                    <div class="addtocart">
  	                           <?php $enquiry = false; if($config->get( 'product_blocks_module' ) != '') { $enquiry = $theme_options->productIsEnquiry($product['product_id']); }
  	                           if(is_array($enquiry)) { ?>
  	                           <a href="javascript:openPopup('<?php echo $enquiry['popup_module']; ?>', '<?php echo $product['product_id']; ?>')" class="button button-enquiry">
  	                                <?php if($enquiry['icon'] != '' && $enquiry['icon_position'] == 'left') { echo '<img src="image/' . $enquiry['icon']. '" align="left" class="icon-enquiry" alt="Icon">'; } ?>
  	                                <span class="text-enquiry"><?php echo $enquiry['block_name']; ?></span>
  	                                <?php if($enquiry['icon'] != '' && $enquiry['icon_position'] == 'right') { echo '<img src="image/' . $enquiry['icon']. '" align="right" class="icon-enquiry" alt="Icon">'; } ?>
  	                           </a>
                          <?php } else { ?>
                          <?php if ($product['options']) {  ?>
                          <a class="button addtocartlist"><?php echo $button_cart; ?></a>
                          <?php } else { ?>
                          <a onclick="cart.add('"<?php echo $product['product_id']; ?>');" class="button"><?php echo $button_cart; ?></a>
                          <?php } ?>
                          <?php } ?>
  	                    </div>

  	                    <div class="addtolist" style="display:none">
  	                         <div class="add-to-links">
  	                              <div class="wishlist">
  	                                   <a onclick="wishlist.add('<?php echo $product['product_id']; ?>');" title="" data-toggle="tooltip" data-placement="top" class="link-wishlist" data-original-title="<?php if($theme_options->get( 'add_to_wishlist_text', $config->get( 'config_language_id' ) ) != '') { echo $theme_options->get( 'add_to_wishlist_text', $config->get( 'config_language_id' ) ); } else { echo 'Add to wishlist'; } ?>"><i aria-hidden="true" class="icon_heart_alt"></i></a>
  	                              </div>
  	                              
  	                              <div class="compare">
  	                                   <a rel="nofollow" onclick="compare.add('<?php echo $product['product_id']; ?>');" title="" data-toggle="tooltip" data-placement="top" class="link-compare" data-original-title="<?php if($theme_options->get( 'add_to_compare_text', $config->get( 'config_language_id' ) ) != '') { echo $theme_options->get( 'add_to_compare_text', $config->get( 'config_language_id' ) ); } else { echo 'Add to compare'; } ?>"><i aria-hidden="true" class="icon_piechart"></i></a>
  	                              </div>
  	                         </div>
  	                    </div>
  	               </div>
  	          </div>
  	     </div>
  	</div>
  	<?php } ?>
  </div>
  
  <!-- Products grid -->
  <?php 
  $class = 2; 
  $row = 4; 
  
  if($theme_options->get( 'product_per_pow2' ) == 6) { $class = 2; }
  if($theme_options->get( 'product_per_pow2' ) == 5) { $class = 25; }
  if($theme_options->get( 'product_per_pow2' ) == 3) { $class = 4; }
  
  if($theme_options->get( 'product_per_pow2' ) > 1) { $row = $theme_options->get( 'product_per_pow2' ); } 
  ?>
  <div class="product-grid clearfix custom-grid"<?php if($theme_options->get('default_list_grid') == '1') { echo ' class="active"'; } ?>>
  
  	<div class="row">
	  	<?php $row_fluid = 0; foreach ($products as $product) { $row_fluid++; ?>
		  	<?php $r=$row_fluid-floor($row_fluid/$row)*$row; if($row_fluid>$row && $r == 1) { echo '</div><div class="row">'; } ?>
		  	<div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
		  	    <?php include('catalog/view/theme/'.$config->get('config_template').'/template/new_elements/product.tpl'); ?>
		  	</div>
	    <?php } ?>
    </div>
  </div>
  
  <div class="row pagination-results">
    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
    <div class="col-sm-6 text-right"><?php echo $results; ?></div>
  </div>
  <?php } ?>
  <?php if (!$categories && !$products) { ?>
  <p style="padding-top: 6px"><?php echo $text_empty; ?></p>
  <div class="buttons">
    <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
  </div>
  <?php } ?>
<script type="text/javascript"><!--
function display(view) {

	if (view == 'list') {
		$('.product-grid').removeClass("active");
		$('.product-list').addClass("active");

		$('.display').html('<button id="grid" rel="tooltip" title="Grid" onclick="display(\'grid\');"><i class="fa fa-th-large"></i></button> <button class="active" id="list" rel="tooltip" title="List" onclick="display(\'list\');"><i class="fa fa-th-list"></i></button>');
		
		localStorage.setItem('display', 'list');
	} else {
	
		$('.product-grid').addClass("active");
		$('.product-list').removeClass("active");
					
		$('.display').html('<button class="active" id="grid" rel="tooltip" title="Grid" onclick="display(\'grid\');"><i class="fa fa-th-large"></i></button> <button id="list" rel="tooltip" title="List" onclick="display(\'list\');"><i class="fa fa-th-list"></i></button>');
		
		localStorage.setItem('display', 'grid');
	}
}

if (localStorage.getItem('display') == 'list') {
	display('list');
} else {
	display('grid');
}
//--></script>
  <script type="text/javascript">
    <!--
$('.addtocartlist').on('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('#product input[type=\'hidden\'], #product input[type=\'radio\']:checked'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-cart').button('loading');
		},
		complete: function() {
			$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			if (json['error']) {
        if(json['error']['stock']){
            $.notify({
					  message: json['error']['stock'],
					  target: '_blank'
				  },{
					  // settings
					  element: 'body',
					  position: null,
					  type: "info",
					  allow_dismiss: true,
					  newest_on_top: false,
					  placement: {
						  from: "top",
						  align: "right"
					  },
					  offset: 20,
					  spacing: 10,
					  z_index: 2031,
					  delay: 5000,
					  timer: 1000,
					  url_target: '_blank',
					  mouse_over: null,
					  animate: {
						  enter: 'animated fadeInDown',
						  exit: 'animated fadeOutUp'
					  },
					  onShow: null,
					  onShown: null,
					  onClose: null,
					  onClosed: null,
					  icon_type: 'class',
					  template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-danger" role="alert">' +
						  '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
						  '<span data-notify="message"><i class="fa fa-check-circle"></i>&nbsp; {2}</span>' +
						  '<div class="progress" data-notify="progressbar">' +
							  '<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
						  '</div>' +
						  '<a href="{3}" target="{4}" data-notify="url"></a>' +
					  '</div>' 
				  });
        }
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));
						
						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}
				
				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}
				
				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}
			
			if (json['success']) {
				$('span').removeClass("active");
				$.notify({
					message: json['success'],
					target: '_blank'
				},{
					// settings
					element: 'body',
					position: null,
					type: "info",
					allow_dismiss: true,
					newest_on_top: false,
					placement: {
						from: "top",
						align: "right"
					},
					offset: 20,
					spacing: 10,
					z_index: 2031,
					delay: 5000,
					timer: 1000,
					url_target: '_blank',
					mouse_over: null,
					animate: {
						enter: 'animated fadeInDown',
						exit: 'animated fadeOutUp'
					},
					onShow: null,
					onShown: null,
					onClose: null,
					onClosed: null,
					icon_type: 'class',
					template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-success" role="alert">' +
						'<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
						'<span data-notify="message"><i class="fa fa-check-circle"></i>&nbsp; {2}</span>' +
						'<div class="progress" data-notify="progressbar">' +
							'<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
						'</div>' +
						'<a href="{3}" target="{4}" data-notify="url"></a>' +
					'</div>' 
				});
				
				$('#cart_block #cart_content').load('index.php?route=common/cart/info #cart_content_ajax');
				$('#cart_block #total_price_ajax').load('index.php?route=common/cart/info #total_price');
				$('#cart_block #total_item_ajax').load('index.php?route=common/cart/info #total_item');
			}
		},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
});
//-->
  </script>
</div>
<?php include('catalog/view/theme/'.$config->get('config_template').'/template/new_elements/wrapper_bottom.tpl'); ?>
<?php echo $footer; ?>