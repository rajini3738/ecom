<?php if ($error_warning) { ?>
<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($additional_payment) { ?>
  <p style="font-weight: bold;color:red"><?php echo $additional_payment; ?></p>
<?php } ?>
<?php if ($text_only_banktransfer) { ?>
  <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $text_only_banktransfer; ?></div>
<?php } ?>
<?php if ($payment_methods) { ?>
<p><?php echo $text_payment_method; ?></p>
<?php $index = 0; foreach ($payment_methods as $payment_method) { $index++;?>
<div class="radio">
  <label>
    <?php if ($payment_method['code'] == $code || !$code) { ?>
    <?php $code = $payment_method['code']; ?>
    <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" checked="checked" />
    <?php } else { ?>
    <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" />
    <?php } ?>
    <?php echo $payment_method['title']; ?>
    <!--<?php if ($payment_method['terms']) { ?>
    (<?php echo $payment_method['terms']; ?>)
    <?php } ?>-->
  </label> <?php if (sizeof($payment_methods)!=$index) { ?> &nbsp;|&nbsp; <?php } ?>
</div>
<?php } ?>
<?php } ?>
<p style="display:none"><strong><?php echo $text_comments; ?></strong></p>
<p style="display:none">
  <textarea name="comment" rows="8" class="form-control"><?php echo $comment; ?></textarea>
</p>
<?php if ($text_agree) { ?>
<div class="buttons buttons_custom">
  <div class="">
    <div> <?php echo $text_agree; ?>
    <?php if ($agree) { ?>
    <input type="checkbox" name="agree" value="1" checked="checked" />
    <?php } else { ?>
    <input type="checkbox" name="agree" value="1" />
    <?php } ?></div>
    <div>
      <?php echo $text_agree1; ?><input type="checkbox" name="agree1" value="1" />
    </div>
    &nbsp;
    <!--<input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" />-->
    <div>
      <input type="button" value="التالي" id="button-payment-method" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" /><label style="float: left;margin-top: 10px;margin-left: 5px;">إعتماد طريقة الدفع</label>
    </div>
  </div>
</div>
<?php } else { ?>
<div class="buttons">
  <div class="pull-right">
    <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" />
  </div>
</div>
<?php } ?>
</script>
