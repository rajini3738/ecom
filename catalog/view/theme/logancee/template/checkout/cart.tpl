<?php echo $header; 
$theme_options = $this->registry->get('theme_options');
$config = $this->registry->get('config'); 
include('catalog/view/theme/' . $config->get('config_template') . '/template/new_elements/wrapper_top.tpl'); ?>
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
  <div class="table-responsive cart-info">
    <table class="table table-bordered">
      <thead>
        <tr>
          <!-- <td class="text-center"><?php echo $column_image; ?></td> -->
          <td><?php echo $column_name; ?></td>
          <td>الحجم</td>
          <td><?php echo $column_quantity; ?></td>
          <!-- <td><?php echo $column_price; ?></td> -->
		      <td><?php echo $column_total; ?></td>
          <!-- <td class="text-right"><?php echo $column_tax; ?></td>
		      <td class="text-right"><?php echo $column_total_tax; ?></td> -->
        </tr>
      </thead>
      <tbody>
        <?php foreach ($products as $product) { ?>
        <tr>
          <!-- <td class="text-center"><?php if ($product['thumb']) { ?>
            <a href="<?php echo $product['href']; ?>">
            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
            <?php } ?>
            <div class="visible-xs"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?><div>
            </td> -->
          <td><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
            <?php if (!$product['stock']) { ?>
            <span class="text-danger">***</span>
            <?php } ?>
            <!-- <?php if ($product['option']) { ?>
            <?php foreach ($product['option'] as $option) { ?>
            <small><?php echo $option['value']; ?></small>
            <?php } ?>
            <?php } ?> -->
            <?php if ($product['reward']) { ?>
            <br />
            <small><?php echo $product['reward']; ?></small>
            <?php } ?>
            <?php if ($product['recurring']) { ?>
            <br />
            <span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><?php echo $product['recurring']; ?></small>
            <?php } ?></td>
            <td>
              <?php if ($product['option']) { ?>
                <?php foreach ($product['option'] as $option) { ?>
                <small><?php echo $option['value']; ?></small>
                <?php } ?>
                <?php } ?>
            </td>
          <td>
            
            <div class="quantity quantity-cus">
				<input type="text" name="quantity[<?php echo $product['cart_id']; ?>]" value="<?php echo $product['quantity']; ?>" size="1"  maxlength="2" data-cart="<?php echo $product['cart_id']; ?>"/>
     			 <!--<input type="text" name="quantity" id="quantity_wanted" size="2" value="<?php echo $product['quantity']; ?>" />-->
     			<a href="#" id="q_up" class="q_up" data-cart="<?php echo $product['cart_id']; ?>" style="cursor:pointer"><i aria-hidden="true" class="icon_plus"></i></a>
     			<a href="#" id="q_down" class="q_down" data-cart="<?php echo $product['cart_id']; ?>" style="cursor:pointer"><i aria-hidden="true" class="icon_minus-06"></i></a>
     		</div>

            <!-- <div class="quantity quantity-cus">
                <input type="text" name="quantity[<?php echo $product['cart_id']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" />
            </div> -->
              <span class="action-btn">
              <input type="image" src="catalog/view/theme/<?php echo $config->get( 'config_template' ); ?>/img/update.png" alt="<?php echo $button_update; ?>" title="<?php echo $button_update; ?>" />
              &nbsp;<a onclick="cart.remove('<?php echo $product['cart_id']; ?>');">
              <img src="catalog/view/theme/<?php echo $config->get( 'config_template' ); ?>/img/remove.png" alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>" /></a>
              </span>
              
              </td>
          <!-- <td><?php echo $product['price']; ?></td>		   -->
          <td><?php echo $product['total']; ?></td>
		      <!-- <td class="text-right hidden-xs"><?php echo $product['tax']; ?></td>
		      <td class="text-right hidden-xs"><?php echo $product['total_tax']; ?></td> -->
        </tr>
        <?php } ?>
        <?php foreach ($vouchers as $vouchers) { ?>
        <tr>
          <td></td>
          <td class="text-center hidden-xs"><?php echo $vouchers['description']; ?></td>
          <td class="text-center hidden-xs"></td>
          <td class="text-center">
          	<input type="text" name="" value="1" size="1" disabled="disabled" />
              &nbsp;<a onclick="voucher.remove('<?php echo $vouchers['key']; ?>');">
              <img src="catalog/view/theme/<?php echo $config->get( 'config_template' ); ?>/img/remove.png" alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>" /></a>
          </td>
          <td class="text-right hidden-xs"><?php echo $vouchers['amount']; ?></td>
          <td class="text-right"><?php echo $vouchers['amount']; ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</form>

<?php if ($coupon || $voucher || $reward || $shipping) { ?>
<!-- <h2><?php echo $text_next; ?></h2> -->
<!-- <p style="padding-bottom: 10px;"><?php echo $text_next_choice; ?></p> -->

<div class="panel-group cartCoupon" id="accordion"><?php echo $coupon; ?><?php echo $reward; ?><?php echo $shipping; ?></div>
<?php } ?>

<div class="cart-total">
    <table>
      <?php foreach ($totals as $total) { ?>
      <tr>
        <td class="text-right"><strong><?php echo $total['title']; ?>:</strong></td>
        <td class="text-right"><?php echo $total['text']; ?></td>
      </tr>
      <?php } ?>
    </table>
</div>

<div class="buttons" style="text-align: left">
  <a style="margin-right:10px;" href="<?php echo $continue; ?>" class="btn btn-default"><?php echo $button_shopping; ?></a>
  <a href="<?php echo $checkout; ?>" class="btn btn-primary"><?php echo $button_checkout; ?></a>
</div>

<?php include('catalog/view/theme/' . $config->get('config_template') . '/template/new_elements/wrapper_bottom.tpl'); ?>
<?php echo $footer; ?>