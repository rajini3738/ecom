<?php if ($competition) { ?>
<div class="panel panel-default">
  <div class="panel-heading"><?php echo $heading_title; ?></div>
  <div class="panel-body">
    <?php foreach ($competition as $competition_details) { ?>
      <div class="box-competition">
	  <?php if ($competition_details['thumb']) { ?>
		<div style="float: left; margin-right: 15px;"><img src="<?php echo $competition_details['thumb']; ?>" alt="<?php echo $heading_title; ?>" /></div>
    <?php } ?>
        <h4><?php echo $competition_details['title']; ?></h4>
        <?php echo $competition_details['description']; ?> &hellip; <a href="<?php echo $competition_details['href']; ?>"><p><?php echo $text_details; ?></p></a>
        <div><b><?php echo $text_end_date; ?></b> <?php echo $competition_details['end_date']; ?></div>
      </div>
    <?php } ?>
	</div>
</div>
<?php } ?>