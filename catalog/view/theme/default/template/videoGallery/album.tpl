<?php echo $header; 
$theme_options = $this->registry->get('theme_options');
$config = $this->registry->get('config'); 
include('catalog/view/theme/' . $config->get('config_template') . '/template/new_elements/wrapper_top.tpl'); ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/videoGallery_album.css" />
<div class="container">
  <ul class="breadcrumb" style="display:none">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li>
      <a href=""
        <?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?>
      </a>
    </li>
    <?php } ?>
  </ul>
  <div class="row">
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      
      <div class="row">
      
      <div class="col-lg-6 col-md-4 col-sm-4 col-xs-6">
      <h2 style="padding-top:0px;">
        <?php echo $heading_title; ?>
      </h2>
      </div>

 <div class="col-lg-6 col-md-8 col-sm-8 col-xs-6 text-right" style="float: left; margin-top: -16px;">
 
 <div class="row">
 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  hidden-xs" style="text-align:left; line-height:54px;"> <label class="control-label" for="input-sort">
          <?php echo $text_sort; ?>
        </label></div>
 
 <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12"><select id="input-sort" class="form-control" onchange="location = this.value;">
          <?php foreach ($sorts as $sorts) { ?>
          <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
          <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?>
          </option>
          <?php } else { ?>
          <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?>
          </option>
          <?php } ?>
          <?php } ?>
        </select></div>
 
 
 </div>
 
       
        
      </div>


      </div>
      
     

      <div class="row" style="margin-top: 60px;">

        <?php if ($albums) { ?>

        <?php foreach ($albums as $album) { ?>
  
        <div class=" col-lg-4 col-md-3 col-sm-6 col-xs-12">
          <div class="transition transition-custom" >
            <div class="album">
              <a href="<?php echo $album['href']; ?>"><img  src="<?php echo $album['thumb']; ?>" title="<?php echo $album['name']; ?>" alt="<?php echo $album['name']; ?>" />
              </a>
            </div>
            <p class="album_date_added">
            <a href="<?php echo $album['href']; ?>" class="album_name"><?php echo $album['name']; ?>
             
            
            </a>
             <!--<?php echo $album['date_added'][0]; ?>-->
            </p>
            <br />
          </div>
        </div>
        <?php } ?>
        <div class="row">
          <div class="col-sm-6 text-left">
            <?php echo $pagination; ?>
          </div>
        </div>
        <?php } ?>

        <?php echo $content_bottom; ?>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>