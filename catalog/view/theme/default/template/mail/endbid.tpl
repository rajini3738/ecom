<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?php echo $endtitle; ?></title>
</head>
<body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">
<div style="width: 680px;">
  <div style="float: right; margin-left: 20px;"><a href="<?php echo $store_url; ?>" title="<?php echo $store_name; ?>"><img src="<?php echo $image; ?>" alt="<?php echo $store_name; ?>" style="margin-bottom: 20px; border: none;" /></a></div>
  <div>
  <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $endtitle; ?></p>
    <p style="margin-top: 0px; margin-bottom: 20px;"><?php echo $text_endmessage; ?></p>
  </div>
  
    <tbody>

      <?php foreach ($products as $product) { ?>

      <tr>

        <td style="font-size: 12px;	border-right: 1px solid #DDDDDD; border-bottom: 1px solid #DDDDDD; text-align: left; padding: 7px;"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></td>

      </tr>

      <?php } ?>

    </tbody>

</div>
</body>
</html>
