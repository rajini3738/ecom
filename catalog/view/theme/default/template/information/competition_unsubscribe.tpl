<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
		<div class="panel panel-default">
		  <div class="panel-body">
			<div class="row">
			<div class="col-sm-12">
			  <form name="unsubscribe" id="unsubscribe" class="form-horizontal">
			  <fieldset>
				<div class="form-group required">
				  <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
				  <div class="col-sm-10">
					<input type="text" name="unsubscribe_email" value="" id="input-email" class="form-control" />
				  </div>
				</div>
				<div class="form-group required">
				  <label class="col-sm-2 control-label" for="input-captcha"><?php echo $entry_captcha; ?></label>
				  <div class="col-sm-10">
					<input type="text" name="captcha" id="input-captcha" class="form-control" />
				  </div>
				</div>
				<div class="form-group">
				  <div class="col-sm-10 pull-right">
					<img src="index.php?route=tool/captcha" alt="" />
				  </div>
				</div>
			  </fieldset>
			  </form>
     <div id="unsubscribe_result"></div>
			</div>
		  </div>
		</div>
	  </div>
	  <div class="buttons">
		<div class="left">
		  <a class="btn btn-success" onclick="remove_email()"><span><?php echo $unsubscribe_button; ?></span></a>
		</div>
	  </div>
      <?php echo $content_bottom; ?>
	</div>
    <?php echo $column_right; ?>
  </div>
</div>
<script type="text/javascript"><!--
function remove_email(){
	$.ajax({
			type: 'post',
			url: 'index.php?route=information/competition/removeEmail',
			dataType: 'html',
            data:$("#unsubscribe").serialize(),
			success: function (html) {
				eval(html);
			}}); 
}
//--></script> 
<?php echo $footer; ?>