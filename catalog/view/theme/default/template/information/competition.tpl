<?php echo $header;
$theme_options = $this->registry->get('theme_options');
$config = $this->registry->get('config');
include('catalog/view/theme/' . $config->get('config_template') . '/template/new_elements/wrapper_top.tpl'); ?>
<div class="container">
  <style>
    .center-column .panel-body {
    position: relative;
    padding-top: 15px;
    }
  </style>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <h1><?php echo $heading_title; ?></h1>
	  <?php if (isset($competition_info)) { ?>
		<div class="panel panel-default">
		  <div class="panel-body">
			<div class="row">
			<div class="col-sm-12">
			  <?php echo $text_end_date; ?> <?php echo $end_date; ?><br/><br />
			  <?php echo $description; ?>		
			  </div>
			</div>
		  </div>
		</div>	  
		<?php if ($winners) { ?>
		  <div class="panel panel-default">
			<div class="panel-body">
			  <div class="row">
				<div class="col-sm-12">
				  <?php echo $text_winner ?><br />
				  <?php foreach ($winners as $place => $winner) { ?>
					<b><?php echo $place+1; ?>:&nbsp<?php echo ucwords($winner['name']); ?><br /></b>
				  <?php } ?>
				</div>
			  </div>
			</div>
		  </div>
		<?php } ?>
      <form action="<?php echo $action; ?>" id="form" method="post" enctype="multipart/form-data" class="form-horizontal">
        <input type="hidden" value="<?php echo $competition_id; ?>" name="competition_id" id="competition_id">
      </form>
      <div>
        <?php if ($error_already_entered) { ?>
        <div class="text-danger">
          <?php echo $error_already_entered; ?>
        </div>
        <?php } ?>
		<?php if ($login && !$logged) { ?>
      <div style="color: red;font-weight: bold;">للمشاركة يجب عليك تسجيل الدخول</div>
      <div><a href="<?php echo $loginurl; ?>" class="btn btn-primary">دخول</a></div>
		<?php }else{ ?>
		  <?php if ($current && !$winners && !$auto) { ?>
			<div>
        <input type="submit" onclick="$('form').submit();" value="تقديم مشاركة" class="btn btn-success" />
			  <?php if ($error_answer) { ?>
				<div class="text-danger"><b><?php echo $error_answer; ?></b></div>
			  <?php } ?>
			</div>
		  <?php } elseif ($auto && $current && !$winners) { ?>
			<div class="col-sm-12">
			  <b><?php echo $auto_message; ?></b>
			</div>
		  <?php }else{ ?>
			  <?php echo $closed; ?>
		  <?php } ?>
		<?php } ?>
		<div>		 
		</div>
	  </div>

	  <?php } elseif (isset($competition_data)) { ?>

		<?php foreach ($competition_data as $competition) { ?>
      <div class="col-sm-12">
		  <div class="panel panel-default">
			<div class="panel-body">
			  <div class="row">			  
				<?php if ($competition['thumb']) { ?>
				  <div style="float: left; margin-right: 15px;"><a href="<?php echo $competition['href']; ?>"><img src="<?php echo $competition['thumb']; ?>" alt="<?php echo $heading_title; ?>" /></a></div>
				<?php } ?>
				
				<h3 style="margin-top: 5px;"><a href="<?php echo $competition['href']; ?>"><?php echo $competition['title']; ?></a></h3>
        <p><?php echo $competition['description']; ?></p>     
				<?php if (($competition['show_closed'] > $closed_test) && !$competition['winners']) { ?>
				  <p><b><?php echo $text_end_date; ?></b>&nbsp;&nbsp;<?php echo $competition['end_date']; ?></p>
				<?php }else{ ?>
				  <p><b><?php echo $text_comp_closed; ?></b></p>
				<?php } ?>
          <p><a href="<?php echo $competition['href']; ?>" class="btn btn-primary"><?php echo $text_details; ?></a> </p>
        </div>
			</div>
		  </div>
      </div>
		<?php } ?>
	  <?php } ?>
      <?php echo $content_bottom; ?>
	</div>
  </div>
</div>
<?php echo $footer; ?>