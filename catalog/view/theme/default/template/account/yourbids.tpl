<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h2><?php echo $heading_title; ?></h2>
      
      <?php if ($products) { ?>
      <div class="row">
        <div class="col-md-4">
          <div class="btn-group hidden-xs">
            <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button>
            <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></button>
          </div>
        </div>
      </div>
      <br />
      <div class="row">
        <?php foreach ($products as $product) { ?>
        <div class="product-layout product-list col-xs-12">
          <div class="product-thumb">
            <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
            <div>
              <div class="caption">
                <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                <p><?php echo $product['description']; ?></p>
                <?php if ($product['rating']) { ?>
                <div class="rating">
                  <?php for ($i = 1; $i <= 5; $i++) { ?>
                  <?php if ($product['rating'] < $i) { ?>
                  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                  <?php } else { ?>
                  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                  <?php } ?>
                  <?php } ?>
                </div>
                <?php } ?>
                <?php if ($product['price'] && $product['buynow_status'] == 0) { ?> 
                <p class="price">
                  <?php if (!$product['special']) { ?>
                  <?php echo $product['price']; ?>
                  <?php } elseif ($product['$buynow_status'] == 0) { ?>
                  <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                  <?php } ?>
                  <?php if ($product['tax'] && $product['buynow_status'] == 0) { ?>
                  <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                  <?php } ?>
                </p>
                <?php } ?>
                <?php if ($product['auc_status'] == 1) { ?>
      <?php if ($product['auc_endstatus'] > 0 || $product['currentdate'] > $product['enddate']) { ?>
      <div class="alert alert-info"><?php echo $text_aucclosed; ?></div>
      <?php } else { ?>
                <span class="price-new"><div id="timedown<?php echo $product['product_id']; ?>"></div></span>
                <script type="text/javascript"><!--    
    var target_date<?php echo $product['product_id']; ?> = new Date('<?php echo $product['date_ende'] ?>').getTime();
    var days, hours, minutes, seconds;
    var timedown<?php echo $product['product_id']; ?> = document.getElementById('timedown<?php echo $product['product_id']; ?>');
    setInterval(function () {
    var current_date = new Date('<?php echo $product['datenow'] ?>').getTime();
    var seconds_left = (target_date<?php echo $product['product_id']; ?> - current_date) / 1000;

    days = parseInt(seconds_left / 86400);
    seconds_left = seconds_left % 86400;
     
    hours = parseInt(seconds_left / 3600);
    seconds_left = seconds_left % 3600;
     
    minutes = parseInt(seconds_left / 60);
    seconds = parseInt(seconds_left % 60);

    timedown<?php echo $product['product_id']; ?>.innerHTML = '<span class="catdays"><?php echo $text_timeleft ?> : ' + days +  ' <b><?php echo $text_days ?></b></span> <span class="hours">' + hours + ' <b><?php echo $text_hours ?></b></span> <span class="minutes">'
    + minutes + ' <b><?php echo $text_minutes ?></b></span> <span class="seconds">' + seconds + ' <b><?php echo $text_seconds ?></b></span>';  
 
    }, 1000);
    //--></script>
                <?php if ($product['current_bid']) { ?>
                <p class="price">

                  <?php echo $text_currentbid; ?> <?php echo $product['current_bid']; ?>

                  <?php if ($product['taxcurrent_bid']) { ?>
                  <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['taxcurrent_bid']; ?></span>
                  <?php } ?>
                </p>
                <?php } ?>
                
                <?php if ($product['yourcurrent']) { ?>
                <p class="price">

                  <?php echo $text_yourcurrent ?> <?php echo $product['yourcurrent']; ?>

                  <?php if ($product['taxcurrent_ypourbid']) { ?>
                  <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['taxcurrent_ypourbid']; ?></span>
                  <?php } ?>
                </p>
                <?php } ?>
                <?php } ?>
                <?php } ?>
                
              </div>
              <div class="button-group">
              <?php if ($product['auc_status'] == 1) { ?>
              <?php if ($product['auc_endstatus'] == 0 && $product['currentdate'] < $product['enddate']) { ?>
              <?php if ($product['buynow_status'] == 0) { ?>
                <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
                <?php } ?>
                <a href="<?php echo $product['href']; ?>"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_bid; ?></span></a>
                <?php } ?>
                
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
      <!-- <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div> -->
      <?php } ?>
      <?php if (!$products) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
