<?php
//  Website: WWW.OpenCartArab.com
//  E-Mail : info@OpenCartArab.com

// Heading
$_['heading_title']                  = 'إنهاء الطلب';

// Text
$_['text_cart']                      = 'سلة الشراء';
$_['text_checkout_option']           = 'الخطوة 1: خيارات إنهاء الطلب';
$_['text_checkout_account']          = 'الخطوة 2: الحساب وتفاصيل الشحن';
$_['text_checkout_payment_address']  = 'الخطوة 2: تفاصيل الفاتورة';
$_['text_checkout_shipping_address'] = 'الخطوة 2: تفاصيل الشحن';
$_['text_checkout_shipping_method']  = 'الخطوة 3: طريقة الشحن';
$_['text_checkout_payment_method']   = 'الخطوة 4: طريقة الدفع';
$_['text_checkout_confirm']          = 'الخطوة 5: تأكيد الطلب';
$_['text_modify']                    = 'تعديل &raquo;';
$_['text_new_customer']              = 'عميل جديد';
$_['text_returning_customer']        = 'تسجيل الدخول';
$_['text_checkout']                  = 'خيارات إنهاء الطلب:';
$_['text_i_am_returning_customer']   = 'انا عميل مسجل بالموقع';
$_['text_register']                  = 'تسجيل';
$_['text_guest']                     = 'إنهاء الطلب للزوار';
$_['text_register_account']          = 'لكي تقوم بشراء اي منتج, يجب عليك ان تنشىء حساب جديد.';
$_['text_forgotten']                 = 'نسيت كلمة المرور';
$_['text_your_details']              = 'معلوماتك الشخصية';
$_['text_your_address']              = 'العناوين الخاصة بك';
$_['text_your_password']             = 'كلمة المرور الخاصة بك';
$_['text_agree']                     = 'لقد قرأت ووافقت على <a target="_blank" href="%s" class="agree"><b>%s</b></a>';
$_['text_agree1']                     = 'لقد قرأت ووافقت على <a target="_blank" href="%s" class="agree1"><b>%s</b></a>';
$_['text_address_new']               = 'أريد استخدام عنوان جديد';
$_['text_address_existing']          = 'أريد استخدام هذا العنوان للفاتورة';
$_['text_address_existing1']          = 'أريد استخدام هذا العنوان للشحن';
$_['text_shipping_method']           = 'الرجاء اختيار طريقة الشحن.';
$_['text_payment_method']            = 'الرجاء اختيار طريقة الدفع المفضلة لهذا الطلب.';
$_['text_comments']                  = 'كتابة ملاحظات مع الطلب.';
$_['text_recurring_item']                 = 'Recurring item';
$_['text_payment_recurring']           = 'Payment Profile';
$_['text_trial_description']         = '%s every %d %s(s) for %d payment(s) then';
$_['text_payment_description']       = '%s every %d %s(s) for %d payment(s)';
$_['text_payment_cancel'] = '%s every %d %s(s) until canceled';
$_['text_day']                       = 'يومي';
$_['text_week']                      = 'اسبوعي';
$_['text_semi_month']                = 'نصف شهري';
$_['text_month']                     = 'شهري';
$_['text_year']                      = 'سنوي';

// Column
$_['column_name']                    = 'الاسم';
$_['column_model']                   = 'النوع';
$_['column_quantity']                = 'الكمية';
$_['column_price']                   = 'السعر';
$_['column_total']                   = 'المجموع';
$_['column_tax']          = 'ضريبة';
$_['column_total_tax']          = 'المجموع الإجمالي';

// Entry
$_['entry_email_address']            = 'عنوان البرید الإلكتروني ';
$_['entry_email']                    = 'البرید الإلكتروني ';
$_['entry_password']                 = 'كلمة المرور ';
$_['entry_confirm']                  = 'تأكيد كلمة المرور ';
$_['entry_firstname']                = 'الاسم الأول ';
$_['entry_lastname']                 = 'اسم العائلة ';
$_['entry_middlename']               = 'الاسم الأوسط';
$_['entry_telephone']                = 'رقم الهاتف أو الجوال ';
$_['entry_fax']                      = 'الفاكس ';
$_['entry_address']                  = 'اختيار العنوان';
$_['entry_company']                  = 'الشركة';
$_['entry_customer_group']           = 'نوع الشركة';
$_['entry_address_1']                = 'العنوان الاول';
$_['entry_address_2']                = 'العنوان الثاني';
$_['entry_postcode']                 = 'صندوق البريد';
$_['entry_city']                     = 'المدينة';
$_['entry_country']                  = 'البلد';
$_['entry_zone']                     = 'المدينة';
$_['entry_newsletter']               = 'أريد الاشتراك في  %s القائمة البريدية.';
$_['entry_shipping'] 	             = 'عنوان الشحن وعنوان الدفع الخاص بي متطابق.';

// Error
$_['error_warning']                  = 'توجد مشكلة أثناء تنفيذ الطلب، اذا تكررت معك نفس المشكاة الرجاء اختيار طريقة دفع مختلفة أو الاتصال بنا <a href="%s"> بالنقر هنا </a>.';
$_['error_login']                    = 'تحذير : لا يوجد تطابق مع البريد الإلكتروني أو كلمة المرور.';
$_['error_attempts']                = 'تحذير: لقد تجاوزت عدد المحاولات المسموحة لتسجيل الدخول. الرجاء اعادة المحاولة بعد ساعة.';
$_['error_approved']                 = 'تحذير : يجب الموافقة على حسابك من الادارة قبل تسجيل الدخول.';
$_['error_exists']                   = 'تحذير : البرید الإلكتروني مسجل مسبقا';
$_['error_firstname']                = 'الاسم الأول يجب أن يكون أكثر من 1 وأقل من 32 حرفا';
$_['error_lastname']                 = 'اسم العائلة يجب أن يكون أكثر من 1 وأقل من 32 حرفا';
$_['error_middlename']                 = 'الاسم الأوسط يجب أن يكون أكثر من 1 وأقل من 32 حرفا';
$_['error_email']                    = 'البريد الإلكتروني غير صحيح الرجاء إعادة كتابته من جديد';
$_['error_telephone']                = 'رقم الهاتف يجب أن يكون أكثر من 3 وأقل من 32 رقم';
$_['error_password']                 = 'كلمة المرور يجب أن تكون أكثر من 3 وأقل من 20 حرفا';
$_['error_confirm']                  = 'لم تتطابق كلمة المرور الرجاء إعادة كتبته مرة أخرى';
$_['error_address_1']                = 'يجب ان يكون بين 3 و 128 رمز ';
$_['error_city']                     = 'يجب ان يكون بين 2 و 128 رمز ';
//$_['error_postcode']                 = 'يجب ان يكون بين 2 و 10 رمز ';
$_['error_postcode']    = 'اذا لم يتوفر صندوق بريد قم بوضع 1234 ';
$_['error_country']                  = 'الرجاء الاختيار ';
$_['error_zone']                     = 'الرجاء الاختيار ';
$_['error_agree']                    = 'تحذير : يجب أن توافق على %s';
$_['error_agree1']                    = 'تحذير : يجب أن توافق على %s';
$_['error_address']                  = 'تحذير : يجب اختيار عنوان';
$_['error_shipping']                 = 'تحذير: طريقة الشحن مطلوبة';
$_['error_no_shipping']              = 'تحذير : طريقة الشحن غير متوفرة حالياً. الرجاء <a href="%s">الاتصال بنا</a> لمساعدتك';
$_['error_payment']                  = 'تحذير: طريقة الدفع مطلوبة';
$_['error_no_payment']               = 'تحذير : طريقة الدفع غير متوفرة حالياً. الرجاء <a href="%s">الاتصال بنا</a> لمساعدتك';
$_['error_custom_field']             = '%s مطلوب' ;
$_['error_telephonecode']  = 'رقم رمز الدولة أن يكون أكثر من 4 وأقل من 5 رقم';
//$_['error_telephonecode1']  = 'يجب أن يتضمن رمز المنطقة مع الأصفار';
$_['error_telephonecode1'] = 'يجب أن تشمل رمز البلد في رقم الجوال، ورقم يبدأ ب 00';
$_['error_telephonecode2'] = 'يجب أن تشمل رمز البلد في رقم الجوال، ورقم يبدأ ب 01';
$_['error_telephone1']     = 'الرجاء عدم أدخل صفر في بداية الرقم';
$_['error_telephone_confirm']        = 'لم تتطابق رقم الجوال الرجاء إعادة كتابته مرة أخرى';
$_['entry_telephone_confirm']        = 'تأكيد رقم الجوال';
$_['text_congrats_message']        = '<p>عزيزي العميل،</p><p>هنيئاً لكم، نظراً لطلبكم من الموقع أكثر من مرة خلال الأسبوع، سيتم توصيل طلبكم مجاناً.</p>';

$_['text_only_banktransfer']        = 'نعتذر منك عزيزي/عزيزتي العميل، قيمة الفتورة تجاوزت الحد الأقصى (٣٥٠٠ ريال) للسداد عند الاستلام. يمكنكم الدفع من خلال التحويل البنكي.';

// Check out confirm message
$_['text_extr_title']        = 'پیغام';
$_['text_extr_amount']        = 'عزيزي العميل لتوفير رسوم التحصيل للسداد عند الاستلام أختر سداد عبر التحويل البنكي او استخدم بطاقة الفيزا';
$_['text_extr_button']        = 'تایید';
$_['text_extr_button1']        = 'انصراف';