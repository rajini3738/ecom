<?php
//  Website: WWW.OpenCartArab.com
//  E-Mail : info@OpenCartArab.com

// Heading
$_['heading_title']        = 'أدخل عنوان الشحن';

// Text
$_['text_success']         = 'تم تطبيق الشحن المتوقع ';
$_['text_shipping']        = 'ادخل بيانات الشحن الخاصة بك.';
$_['text_shipping_method'] = 'الرجاء اختيار طريقة الشحن.';

// Entry
$_['entry_country']        = 'الدولة ';
$_['entry_zone']           = 'المدينة';
$_['entry_postcode']       = 'الرمز البريدي';

// Error
$_['error_postcode']       = 'يجب أن يكون بين 2 و 10 رمز ';
$_['error_country']        = 'الرجاء الاختيار ';
$_['error_zone']           = 'الرجاء الاختيار ';
$_['error_shipping']       = 'مطلوب ';
$_['error_no_shipping']    = 'لا توجد طريقة شحن متاحة. الرجاء <a href="%s">الاتصال بنا</a> لمساعدتك ';