<?php
//  Website: WWW.OpenCartArab.com
//  E-Mail : info@OpenCartArab.com

// Heading
$_['heading_title']        = 'انشاء حساب جديد';

// Text
$_['text_account']         = 'الحساب';
$_['text_register']        = 'تسجيل';
$_['text_account_already'] = 'إذا كان لديك حساب معنا ، الرجاء الدخول إلى صفحة <b><a href="%s"> تسجيل الدخول</a></b>.';
$_['text_your_details']    = 'معلومات التواصل';
$_['text_your_address']    = 'عنوان التوصيل الخاص بك';
$_['text_newsletter']      = 'القائمة البريدية';
$_['text_your_password']   = 'كلمة السر الخاصة بك';
$_['text_agree']           = 'لقد قرأت ووافقت على <a href="%s" class="agree" target="_blank"><b>%s</b></a>';

// Entry
$_['entry_customer_group'] = 'مجموعة العميل';
$_['entry_firstname']      = 'الاسم الثلاثي';
$_['entry_lastname']       = 'اسم العائلة';
$_['entry_middlename']       = 'اسم الاب';
$_['entry_email']          = 'البرید الإلكتروني';
$_['entry_telephone']      = 'الموبايل';
$_['entry_fax']            = 'فاكس';
$_['entry_company']        = 'الشركة';
$_['entry_address_1']      = 'عنوان التوصيل';
$_['entry_address_2']      = 'العنوان الثاني';
$_['entry_postcode']       = 'صندوق البريد';
$_['entry_city']           = 'المدينة';
$_['entry_country']        = 'البلد';
$_['entry_zone']           = 'المدينة';
$_['entry_newsletter']     = 'اشترك';
$_['entry_password']       = 'كلمة السر';
$_['entry_confirm']        = 'تأكيد كلمة السر';
$_['entry_telephone_confirm']        = 'تأكيد رقم الموبايل';

// Error
$_['error_exists']         = 'تحذير : البرید الإلكتروني مسجل مسبقا';
$_['error_firstname']      = 'الاسم الثلاثي يجب أن يكون أكثر من 1 وأقل من 32 حرفا';
$_['error_lastname']       = 'الاسم الأخیر يجب أن يكون أكثر من 1 وأقل من 32 حرفا';
$_['error_middlename']       = 'الاسم الأوسط يجب أن يكون أكثر من 1 وأقل من 32 حرفا';
$_['error_email']          = 'البريد الإلكتروني غير صحيح الرجاء إعادة كتابته من جديد';
$_['error_telephone']      = 'رقم الهاتف يجب أن يكون أكثر من 3 وأقل من 32 رقم';
$_['error_telephonecode']  = 'رقم رمز الدولة أن يكون أكثر من 4 وأقل من 5 رقم';
//$_['error_telephonecode1'] = 'يجب أن يتضمن رمز المنطقة مع الأصفار';
$_['error_telephonecode1'] = 'يجب أن تشمل رمز البلد في رقم الجوال، ورقم يبدأ ب 00';
$_['error_telephonecode2'] = 'يجب أن تشمل رمز البلد في رقم الجوال، ورقم يبدأ ب 01';
$_['error_telephone1']     = 'الرجاء عدم أدخل صفر في بداية الرقم';
$_['error_address_1']      = 'يجب ان يكون بين 3 و 128 رمز ';
$_['error_city']           = 'يجب ان يكون بين 2 و 128 رمز ';
//$_['error_postcode']       = 'يجب ان يكون بين 2 و 10 رمز ';
$_['error_postcode']    = 'اذا لم يتوفر صندوق بريد قم بوضع 1234 ';
$_['error_country']        = 'الرجاء الاختيار ';
$_['error_zone']           = 'الرجاء الاختيار ';
$_['error_custom_field']   = '%s مطلوب ';
$_['error_password']       = 'كلمة المرور يجب أن تكون أكثر من 3 وأقل من 20 حرفا';
$_['error_confirm']        = 'لم تتطابق كلمة المرور الرجاء إعادة كتابته مرة أخرى';
$_['error_telephone_confirm']        = 'لم تتطابق رقم الجوال الرجاء إعادة كتابته مرة أخرى';
$_['error_agree']          = 'تحذير : يجب أن توافق على %s ';