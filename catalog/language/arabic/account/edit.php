<?php
//  Website: WWW.OpenCartArab.com
//  E-Mail : info@OpenCartArab.com

// Heading
$_['heading_title']       = 'معلومات حسابي';

// Text
$_['text_account']         = 'الحساب';
$_['text_register']        = 'تسجيل';
$_['text_account_already'] = '<span stylye="display:none;">إذا كان لديك حساب معنا ، الرجاء الدخول إلى <a href="%s">صفحة تسجيل الدخول</a>.</span>';
$_['text_your_details']    = 'معلوماتك الشخصية';
$_['text_your_address']    = 'العناوين الخاصة بك';
$_['text_newsletter']      = 'القائمة البريدية';
$_['text_your_password']   = 'كلمة المرور الخاصة بك';
$_['text_agree']           = 'لقد قرأت ووافقت على <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_customer_group'] = 'مجموعة العميل';
$_['entry_firstname']      = 'الاسم الأول';
$_['entry_lastname']       = 'الاسم الأخیر';
$_['entry_middlename']       = 'الاسم الأوسط';
$_['entry_filenumber']       = 'رقم الملف';
$_['entry_email']          = 'البرید الإلكتروني';
$_['entry_telephone']      = 'رقم الجوال';
$_['entry_fax']            = 'فاكس';
$_['entry_company']        = 'الشركة';
$_['entry_address_1']      = 'العنوان الاول';
$_['entry_address_2']      = 'العنوان الثاني';
$_['entry_postcode']       = 'صندوق البريد';
$_['entry_city']           = 'المدينة';
$_['entry_country']        = 'البلد';
$_['entry_zone']           = 'المدينة';
$_['entry_newsletter']     = 'اشترك';
$_['entry_password']       = 'كلمة المرور';
$_['entry_confirm']        = 'تأكيد كلمة المرور';
$_['entry_telephone_confirm']        = 'تأكيد رقم الجوال';

// Error
$_['error_exists']         = 'تحذير : البرید الإلكتروني مسجل مسبقا';
$_['error_firstname']      = 'الاسم الأول يجب أن يكون أكثر من 1 وأقل من 32 حرفا';
$_['error_lastname']       = 'الاسم الأخیر يجب أن يكون أكثر من 1 وأقل من 32 حرفا';
$_['error_middlename']       = 'الاسم الأوسط يجب أن يكون أكثر من 1 وأقل من 32 حرفا';
$_['error_email']          = 'البريد الإلكتروني غير صحيح الرجاء إعادة كتابته من جديد';
$_['error_telephone']      = 'رقم الهاتف يجب أن يكون أكثر من 3 وأقل من 32 رقم';
$_['error_telephone1']     = 'الرجاء عدم أدخل صفر في بداية الرقم';
$_['error_telephonecode1'] = 'يجب أن تشمل رمز البلد في رقم الجوال، ورقم يبدأ ب 00';
$_['error_telephonecode2'] = 'يجب أن تشمل رمز البلد في رقم الجوال، ورقم يبدأ ب 01';
$_['error_telephonecode']  = 'رقم رمز الدولة أن يكون أكثر من 4 وأقل من 5 رقم';
//$_['error_telephonecode1'] = 'يجب أن يتضمن رمز المنطقة مع الأصفار';
$_['error_address_1']      = 'يجب ان يكون بين 3 و 128 رمز ';
$_['error_city']           = 'يجب ان يكون بين 2 و 128 رمز ';
$_['error_postcode']       = 'يجب ان يكون بين 2 و 10 رمز ';
$_['error_country']        = 'الرجاء الاختيار ';
$_['error_zone']           = 'الرجاء الاختيار ';
$_['error_custom_field']   = '%s مطلوب ';
$_['error_password']       = 'كلمة المرور يجب أن تكون أكثر من 3 وأقل من 20 حرفا';
$_['error_confirm']        = 'لم تتطابق كلمة المرور الرجاء إعادة كتابته مرة أخرى';
$_['error_telephone_confirm']        = 'لم تتطابق رقم الجوال الرجاء إعادة كتابته مرة أخرى';
$_['error_agree']          = 'تحذير : يجب أن توافق على %s ';
