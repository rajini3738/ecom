<?php
//  Website: WWW.OpenCartArab.com
//  E-Mail : info@OpenCartArab.com

// Heading
$_['heading_title']                = 'تسجيل الدخول إلى حسابك';

// Text
$_['text_account']                 = 'الحساب';
//$_['text_login']                   = 'تسجيل الدخول';
$_['text_new_customer']            = 'تسجيل حساب جديد';
$_['text_register']                = 'تسجيل حساب جديد';
$_['text_register_account']        = 'لكي تقوم بشراء اي منتج, يجب عليك ان تنشىء حساب جديد.';
$_['text_returning_customer']      = 'تسجيل الدخول';
$_['text_i_am_returning_customer'] = 'إذا كنت تملك حساب مسبق في الموقع، فتفضل بتسجيل دخولك...';
$_['text_forgotten']      		   = 'نسيت كلمة المرور؟';
$_['button_continue1']      		   = 'إنشاء حساب';

// Entry
$_['entry_email']                  = 'البريد الإلكتروني';
$_['entry_telephone']              = 'رقم الجوال';
$_['entry_password']               = 'كلمة المرور';

// Error
//$_['error_login']                  = 'تحذير : لا يوجد تطابق مع البريد الإلكتروني أو كلمة المرور.';
$_['error_login']                  = 'تحذير : لا يوجد تطابق مع رقم الجوال أو كلمة المرور.';
$_['error_attempts']               = 'تحذير: لقد تجاوزت عدد المحاولات المسموحة لتسجيل الدخول. الرجاء اعادة المحاولة بعد ساعة.';
$_['error_approved']               = 'تحذير : يجب الموافقة على حسابك من الادارة قبل تسجيل الدخول.';
$_['error_telephonecode1'] = 'يجب أن تشمل رمز البلد في رقم الجوال، ورقم يبدأ ب 00';