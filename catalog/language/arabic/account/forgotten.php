<?php
//  Website: WWW.OpenCartArab.com
//  E-Mail : info@OpenCartArab.com

// Heading
$_['heading_title']   = 'هل نسيت كلمة المرور؟';

// Text
$_['text_account']    = 'الحساب';
$_['text_forgotten']  = 'استعادة كلمة المرور';
$_['text_your_email'] = 'البرید الإلكتروني';
$_['text_email']      = 'أدخل البرید الإلكتروني المرتبط مع حسابك. انقر على "إعتماد" ليتم إرسال كلمة المرور الجديدة إلى بريدك الخاص';
$_['text_success']    = 'تم إرسال كلمة مرور جديدة إلى بريدك الخاص.';

// Entry
$_['entry_email']     = 'البرید الإلكتروني';

// Error
$_['error_email']     = ' تحذير: لم يتم العثور علي البرید الإلكتروني في سجلاتنا ، يرجى المحاولة مرة أخرى';