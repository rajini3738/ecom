<?php
//  Website: WWW.OpenCartArab.com
//  E-Mail : info@OpenCartArab.com

// Heading
$_['heading_title']      = 'شكوى';

// Text
$_['text_account']       = 'حساب';
$_['text_return']        = 'بيانات الشكوى';
$_['text_return_detail'] = 'وصف الشكوى ';
$_['text_description']   = 'الرجاء استكمال النموذج التالي للحصول على رقم إرجاع المنتج.';
$_['text_order']         = 'بيانات الشكوى ';
$_['text_product']       = 'معلومات عن المنتج ونوع الشكوى';
$_['text_message']       = '<p>لقد تم استلام شكوتكم وسيتم التعامل معها في أقرب وقت إن شاء الله</p><p>سيتم إبلاغكم بحال الشكوى عبر الإيميل</p>.';
$_['text_return_id']     = 'رقم الشكوى :';
$_['text_order_id']      = 'رقم الطلب:';
$_['text_date_ordered']  = 'تاريخ الطلب :';
$_['text_status']        = 'الحال :';
$_['text_date_added']    = 'تاريخ الإضافة :';
$_['text_comment']       = 'تفاصيل الشكوى';
$_['text_history']       = 'تاريخ طلبات الإرجاع';
$_['text_empty']         = 'لم يسبق أنق قدمت شكوى من قبل ';

// Column
$_['column_return_id']   = 'رقم الشكوى';
$_['column_order_id']    = 'رقم الطلب';
$_['column_status']      = 'الحالة';
$_['column_date_added']  = 'تاريخ الإضافة';
$_['column_customer']    = 'العميل';
$_['column_product']     = 'اسم المنتج';
$_['column_model']       = 'النوع';
$_['column_quantity']    = 'الكمية';
$_['column_price']       = 'السعر';
$_['column_opened']      = 'مفتوح';
$_['column_comment']     = 'ملاحظات';
$_['column_reason']      = 'السبب';
$_['column_action']      = 'تحرير';

// Entry
$_['entry_order_id']     = 'رقم الطلب';
$_['entry_date_ordered'] = 'تاريخ الطلب ';
$_['entry_firstname']    = 'الاسم الاول';
$_['entry_lastname']     = 'اسم العائلة';
$_['entry_middlename']   = 'الاسم الأوسط';
$_['entry_email']        = 'البرید الإلكتروني';
$_['entry_telephone']      = 'رقم الجوال';
$_['entry_product']      = 'اسم المنتج';
$_['entry_model']        = 'نوع المنتج';
$_['entry_quantity']     = 'الكمية';
$_['entry_reason']       = 'نوع الشكوى';
$_['entry_opened']       = 'تم فتح الشكوى؟';
$_['entry_fault_detail'] = 'ذكر التفاصيل';

// Error
$_['text_error']         = 'الشكوى التي تبحث عنها غير موجودة ';
$_['error_order_id']     = 'رقم الطلب مطلوب ';
$_['error_firstname']    = 'الاسم الاول يجب ان يكون بين 1 و 32 رمزاً';
$_['error_lastname']     = 'اسم العائلة يجب ان يكون بين 1 و 32 رمزا';
$_['error_middlename']     = 'اسم العائلة يجب ان يكون بين 1 و 32 رمزا';
$_['error_email']        = 'البريد الإلكتروني غير صحيح الرجاء إعادة كتابته من جديد';
$_['error_telephone']    = 'الرقم يجب ان يكون بين 3 و 32 رمزاً';
$_['error_product']      = 'اسم المنتج يجب أن يكون أكبر من 3 وأقل من 255 رمزاً';
$_['error_model']        = 'نوع المنتج يجب أن يكون أكبر من 3 وأقل من 64 رمزاً';
$_['error_reason']       = 'يجب اختيار سبب نوع الشكوى ';
$_['error_captcha']      = 'رمز التحقق لا يتطابق مع الصورة ';
$_['error_agree']        = 'تحذير : يجب الموافقة على  %s';