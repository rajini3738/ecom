<?php
//  Website: WWW.OpenCartArab.com
//  E-Mail : info@OpenCartArab.com

// Heading
$_['heading_title']         = 'تـفـاصـيـل الـطـلب';

// Text
$_['text_account']          = 'الحساب';
$_['text_order']            = 'معلومات الطلب';
$_['text_order_detail']     = 'تفاصيل الطلبات';
$_['text_invoice_no']       = 'رقم الفاتورة :';
$_['text_order_id']         = 'رقم الطلب :';
$_['text_date_added']       = 'تاريخ الطلب :';
$_['text_shipping_address'] = 'عنوان الشحن';
$_['text_shipping_method']  = 'طريقة الشحن :';
$_['text_payment_address']  = 'عنوان الدفع';
$_['text_payment_method']   = 'طريقة الدفع :';
$_['text_comment']          = 'الملاحظات على الطلب';
$_['text_history']          = 'سجل الطلب :';
$_['text_success']          = 'لقد قمت بإضافة المنتج إلى سلة الشراء بنجاح';//'لقد قمت بنجاح بإضافة المنتج من الطلب رقم #%s إلى سلة الشراء';
$_['text_empty']            = 'لا توجد طلبات خاصة بك ';
$_['text_error']            = 'لم يتم العثور على طلبات ';

// Column
$_['column_order_id']        = 'رقم الطلب';
$_['column_product']        = 'المنتجات';
$_['column_customer']       = 'العميل';
$_['column_name']           = 'اسم المنتج';
$_['column_model']          = 'النوع';
$_['column_quantity']       = 'الكمية';
$_['column_price']          = 'السعر';
$_['column_total']          = 'المجموع';
$_['column_action']         = 'تحرير';
$_['column_date_added']     = 'تاريخ الطلب';
$_['column_status']         = 'حالة الطلب';
$_['column_comment']        = 'ملاحظات';
$_['column_tax']          = 'ضريبة';
$_['column_total_tax']          = 'المجموع الإجمالي';

// Error
$_['error_reorder']         = '%s هذا المنتج غير متوفر حالياً لإعادة طلبه.';

// Invoice
$_['text_invoice']            = 'الـفـاتـورة';
$_['text_order_detail']       = 'تفاصيل الطلب';
$_['text_invoice_date']       = 'تاريخ الفاتورة:';
$_['text_telephone']          = 'رقم الهاتف أو الجوال:';
$_['text_fax']                = 'فاكس:';
$_['text_email']              = 'البرید الإلكتروني:';
$_['text_website']            = 'الموقع الالكتروني:';
$_['text_to']                 = 'إلى';
$_['text_ship_to']            = 'الشحن إلى (في حالة استخدام عنوان مختلف)';
$_['text_payment_method']     = 'طريقة الدفع:';
$_['text_shipping_method']    = 'طريقة الشحن:';
$_['button_invoice_print']          = 'طباعة الفاتورة';
$_['cart_order_message']      = 'عميلنا العزيز، إن كنت تبحث عن طلبيتك، فربما لم تنتهي منها بعد ومازالت في سلة الشراء.  الرجاء <a style="font-weight: bold;" href="index.php?route=checkout/cart">الضغط هنا</a> للذهاب للسلة .';
