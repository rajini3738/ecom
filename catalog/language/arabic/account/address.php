<?php
//  Website: WWW.OpenCartArab.com
//  E-Mail : info@OpenCartArab.com

// Heading
$_['heading_title']     = 'دفتر العناوين';

// Text
$_['text_account']      = 'الحساب';
$_['text_address_book'] = 'قائمة دفتر العناوين';
$_['text_edit_address'] = 'تحرير العناوين';
$_['text_add']             = 'تم اضافة العنوان بنجاح';
$_['text_edit']            = 'تم تحديث العنوان بنجاح';
$_['text_delete']          = 'تم حذف العنوان';
$_['text_empty']           = 'لاتوجد عناوين في حسابك.';

// Entry
$_['entry_firstname']   = 'الاسم الاول';
$_['entry_lastname']    = 'الاسم الأخیر';
$_['entry_company']     = 'الشركة';
$_['entry_address_1']   = 'العنوان الاول';
$_['entry_address_2']   = 'العنوان الثاني';
$_['entry_postcode']    = 'صندوق البريد';
$_['entry_city']        = 'المدينة';
$_['entry_country']     = 'البلد';
$_['entry_zone']        = 'المدينة';
$_['entry_default']     = 'العنوان الاساسي';
$_['entry_telephone']      = 'رقم الجوال';
$_['entry_telephone_confirm']        = 'تأكيد رقم الجوال';

// Error
$_['error_delete']      = 'تحذير : يجب أن تضع عنوان واحد على الأقل';
$_['error_default']     = 'تحذير : لا يمكنك حذف العنوان الافتراضي';
$_['error_firstname']   = 'الاسم الأول يجب أن يكون أكثر من 3 وأقل من 32 حرفا';
$_['error_lastname']    = 'الاسم الأخیر يجب أن يكون أكثر من 3 وأقل من 32 حرفا';
$_['error_vat']         = 'رقم معرف الضريبة غير صحيح ';
$_['error_address_1']   = 'العنوان يجب أن يكون أكثر من 3 وأقل من 128 حرفا';
//$_['error_postcode']    = 'صندوق البريد يجب أن يكون أكثر من 2 وأقل من 10 رمزاً ';
$_['error_postcode']    = 'اذا لم يتوفر صندوق بريد قم بوضع 1234 ';
$_['error_city']        = 'اسم المدينة يجب أن يكون أكثر من 3 وأقل من 128 حرفا';
$_['error_country']     = 'الرجاء اختيار البلد';
$_['error_zone']        = 'الرجاء اختيار المحافظة / المنطقة';
$_['error_custom_field']   = '%s مطلوب ';
$_['error_telephone']      = 'رقم الهاتف يجب أن يكون أكثر من 3 وأقل من 32 رقم';
$_['error_telephonecode']  = 'رقم رمز الدولة أن يكون أكثر من 4 وأقل من 5 رقم';
$_['error_telephonecode1'] = 'يجب أن تشمل رمز البلد في رقم الجوال، ورقم يبدأ ب 00';
$_['error_telephonecode2'] = 'يجب أن تشمل رمز البلد في رقم الجوال، ورقم يبدأ ب 01';
$_['error_telephone1']     = 'الرجاء عدم أدخل صفر في بداية الرقم';
$_['error_telephone_confirm']        = 'لم تتطابق رقم الجوال الرجاء إعادة كتابته مرة أخرى';
