<?php
//  Website: WWW.OpenCartArab.com
//  E-Mail : info@OpenCartArab.com

// Text
$_['text_search']         = 'للبحث';
$_['text_brand']          = 'الشركة';
$_['text_manufacturer']   = 'الشركة :';
$_['text_model']          = 'النوع :';
$_['text_reward']         = 'نقاط المكافآت :';
$_['text_points']         = 'السعر بنقاط المكافآت :';
$_['text_stock']          = 'حالة التوفر :';
$_['text_instock']        = 'متوفر';
$_['text_tax']                                = 'السعر بدون ضريبة :';
$_['text_discount']                           = 'أو أكثر';
$_['text_option']                             = 'الخيارات المتاحة:';
$_['text_minimum']                            = 'لا يمكن طلب هذا المنتج بكمية أقل من %s';
$_['text_reviews']                            = '(التقييمات: %s)';
$_['text_write']                              = 'أضف تقييمك للمنتج';
$_['text_login']                              = 'الرجاء <a href="%s">الدخول</a> أو <a href="%s">التسجيل</a> لكي تتمكن من تقييم المنتج';
$_['text_no_reviews']                         = 'لا يوجد أي تعليقات لهذا المنتج.';
$_['text_note']                               = '<span class="text-danger">انتبه: </span>لم يتم تفعيل اكواد HTML';
$_['text_success']                            = 'شكرا لكم على تقييمكم. سنقوم بمراجعة التقييم قريباً لاعتماده';
$_['text_related']                            = 'منتجات ذات صلة';
$_['text_tags']                               = 'الكلمات الدليليلة :';
$_['text_error']                              = 'لم يتم العثور على المنتج';
$_['text_payment_recurring']                  = 'Payment Profile';
$_['text_trial_description']                  = '%s every %d %s(s) for %d payment(s) then';
$_['text_payment_description']                = '%s every %d %s(s) for %d payment(s)';
$_['text_payment_cancel'] = '%s every %d %s(s) until canceled';
$_['text_day']                                = 'يومي';
$_['text_week']                               = 'اسبوعي';
$_['text_semi_month']                         = 'نصف شهري';
$_['text_month']                              = 'شهري';
$_['text_year']                               = 'سنوي';
$_['text_amount']                             = 'اختر العدد :';

// Entry
$_['entry_qty']                               = 'الكمية:';
$_['entry_name']          = 'الاسم:';
$_['entry_review']        = 'اضافة تعليق:';
$_['entry_rating']        = 'التقييم:';
$_['entry_good']          = 'ممتاز';
$_['entry_bad']           = 'رديء';

// Tabs
$_['tab_description']     = 'تفاصيل';
$_['tab_attribute']       = 'المواصفات';
$_['tab_review']          = 'التقييمات (%s)';

// Error
$_['error_name']          = 'تحذير : الاسم يجب أن يكون أكثر من 3 وأقل من 25 رمزاً';
$_['error_text']          = 'تحذير : النص يجب أن يكون أكثر من 1 وأقل من 1000 رمز';
$_['error_rating']        = 'تحذير: الرجاء اختيار عدد النجوم لتقييم المنتج';
$_['error_captcha']       = 'تحذير : رمز التحقق لا يتطابق مع الصورة';