<?php
// Heading
$_['heading_title']      = 'Complaint';

// Text
$_['text_account']       = 'Account';
$_['text_return']        = 'Complaint Information';
$_['text_return_detail'] = 'Complaint Details';
$_['text_description']   = 'Please complete the form below to request an RMA number.';
$_['text_order']         = 'Complaint Information';
$_['text_product']       = 'Product Information &amp; Reason for Complaint';
$_['text_message']       = '<p>Thank you for submitting your return request. Your request has been sent to the relevant department for processing.</p><p> You will be notified via e-mail as to the status of your request.</p>';
$_['text_complaint_id']  = 'Complaint ID:';
$_['text_order_id']      = 'Order ID:';
$_['text_status']        = 'Status:';
$_['text_comment']       = 'Complaint Comments';
$_['text_empty']         = 'You have not made any previous complaint!';

// Column
$_['column_return_id']   = 'Complaint ID';
$_['column_order_id']    = 'Order ID';
$_['column_status']      = 'Status';
$_['column_date_added']  = 'Date Added';
$_['column_customer']    = 'Customer';
$_['column_product']     = 'Product Name';
$_['column_model']       = 'Model';
$_['column_opened']      = 'Opened';
$_['column_comment']     = 'Comment';
$_['column_action']      = 'Action';

// Entry
$_['entry_order_id']     = 'Order ID';
$_['entry_firstname']    = 'First Name';
$_['entry_lastname']     = 'Last Name';
$_['entry_email']        = 'E-Mail';
$_['entry_product']      = 'Product Name';
$_['entry_model']        = 'Product Code';
$_['entry_opened']       = 'Complaint is opened';
$_['entry_fault_detail'] = 'Faulty or other details';

// Error
$_['text_error']         = 'The complaint you requested could not be found!';
$_['error_order_id']     = 'Order ID required!';
$_['error_firstname']    = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']     = 'Last Name must be between 1 and 32 characters!';
$_['error_email']        = 'E-Mail Address does not appear to be valid!';
$_['error_product']      = 'Product Name must be greater than 3 and less than 255 characters!';
$_['error_model']        = 'Product Model must be greater than 3 and less than 64 characters!';