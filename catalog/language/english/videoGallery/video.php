<?php
// Heading
$_['heading_title']  = 'فيديو';

// Text 
$_['text_album']        = 'ألبوم';
$_['text_image']        = 'صورة';
$_['text_sort']         = 'الترتيب حسب:';
$_['text_default']      = 'الإفتراضي';
$_['text_name_asc']     = 'ترتيب أبجدي تصاعدي';
$_['text_name_desc']    = 'ترتيب أبجدي تنازلي';
$_['text_date_added_asc']    = 'ترتيب حسب التاريخ من الأقدم للأحدث';
$_['text_date_added_desc']   = 'ترتيب حسب التاريخ من الأحدث للأقدم';
$_['text_viewed_asc']   = 'الأقل مشاهدة';
$_['text_viewed_desc']  = 'الأكثر مشاهدة';
$_['text_videoGallery']        = 'معرض الفيديو';

$_['text_error']        = 'الألبوم غير متوفر';
$_['text_empty']        = 'لا يوجد فيديو للعرض في هذا الألبوم.';
?>
