<?php
// Heading 
$_['heading_title']			= 'Competitions';
$_['heading_title_us']		= 'Unsubscribe (Competition Newsletter)';

// Text
$_['text_error']	   		= 'Sorry, No competitions at the moment';
$_['text_details']	   		= 'See full details';
$_['text_end_date']	   		= 'Closing Date:';
$_['entry_email']	   		= 'Email Address:';
$_['entry_name']	   		= 'Name:';
$_['text_closed']      		= 'Sorry, entry to this competition has now closed. Please choose another competition to enter. > > >';
$_['text_comp_closed'] 		= 'Sorry, this competition has now closed.';
$_['text_entered']	   		= 'Enter Competition Here';
$_['text_question']	   		= 'You\'ll need to answer this question: ';
$_['text_agree']       		= 'I have read and agree to the <a href="%s" class="agree"><b>%s</b></a>';
$_['text_winner'] 			= 'The winner of this competition was:';
$_['text_login_required']	= '<b>You must <a href="%s">login</a> to enter this competition.</b>';
$_['text_unsubscribed']		= 'You have Unsubscribed from the Competition Newsletter';
$_['text_message']			= 'Thank You.  Your Competition Entry has been sent.';
$_['text_form']	   			= 'Entry Form';
$_['text_details']	   		= 'Competition Details';

// Buttons
$_['button_competition'] = 'View all Competitions';

// Heading 
$_['heading_title'] 	 = 'Competitions';

//Entry
$_['entry_email'] 		 = 'Email:';
$_['entry_name'] 		 = 'Name:';
$_['entry_answer'] 		 = 'Answer: ';
$_['entry_captcha'] 	 = 'Enter the code in the box below:';

//Buttons
$_['entry_button'] 		 = 'Enter Competiton';
$_['unsubscribe_button'] = 'Unsubscribe';

// Email Message sent to entrant from competiton page.
$_['mail_subject']   	 = 'Competition Entry';
$_['mail_text_1']   	 = 'Thanks for entering our competition and good luck.';
$_['mail_text_2']   	 = 'We\'ll notify the winner by email shortly after the competition closing date which is %s';
$_['mail_text_3']   	 = 'Kind Regards,';
$_['mail_text_4']   	 = '%s.';

//Error
$_['error_invalid'] 	 = 'Invalid Email Address';
$_['error_answer'] 		 = 'You need to select an answer';
$_['entered']	    	 = 'Competition Entered Successfully, good luck.';
$_['error_existing']     = 'You have already entered this competition.';
$_['error_not_existing'] = 'Email Not Found - You are NOT currently subscribed to the Competition Newsletter.';
$_['error-not_existing'] = 'Email Id not exist';
$_['error_name']     	 = 'Name must be between 3 and 32 characters!';
$_['error_email']    	 = 'E-Mail Address does not appear to be valid!';
$_['error_captcha']  	 = 'Verification code does not match the image!';
$_['error_tickbox']  	 = 'You must agree to the Competition Terms and Conditions of Entry!';
?>
