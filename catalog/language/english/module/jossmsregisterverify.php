<?php
// Heading 
$_['heading_title']    = 'تأكيد التسجيل';

// Text
$_['text_phone']    = 'رقم الهاتف';
$_['text_start']    = 'احصل على رمز التأكيد';
$_['text_verification_code']    = 'رمز التحقق';
$_['text_provide_valid_number'] = 'يرجى إدخال رقم صالح';
$_['text_connection_problem'] = 'خطاء في الإتصال، يرجى إعادة المحاولة,...';
$_['text_provide_valid_mobile_number'] = 'يرجى إدخال رقم جوال صالح';
$_['text_different_number'] = 'الرجاء إدخال رقم جوال صالح مشابه لرقم الهاتف بالأعلى';
$_['text_null_number'] = 'الرجاء تعبئة  الهاتف <strong> في الحقل بالأعلى  <strong/>';
$_['text_invalid_pin'] = 'رمز التحقق غير صالح';
$_['text_verify'] = 'تأكيد';
$_['text_max_retries_exceeded'] = "تنبيه: لقد تعديت عدد المرات المسموح بها لطلب رمز التحقق";
$_['text_please_wait'] = "يرجى الإنتظار";
$_['text_please_wait_next'] = "الرجاء الإنتظار %s ثانية لإرسال رمز تحقق آخر";
$_['text_resend'] = "إعادة الإرسال";
$_['text_send_success'] = "تم إرسال رمز التحقق إلى جوالك";
$_['text_explain1'] = "سوف يتم ارسال رمز تاكيد الاشتراك الى هذا الرقم ";
$_['text_explain_select_type'] = "إختر طريقة التحقق";
$_['text_explain_phone_call'] = "سوف تتلقى إتصالاً للحصول على رقمك السري";
$_['text_explain_sms'] = "سوف يتم إرسال رقمك السري برسالة إلى جوالك ";
$_['text_explain_phone_call2'] = "جاري الإتصال ، سوف تتلقى رقمك السري كرسالة صوتية";
$_['text_explain_sms2'] = "تم إرسال رسالة قصيرة على جوالك، الرجاء الإنتظار حتى تصلك.";
$_['text_explain_started'] = "الرجاء ادخال رمز التحقق المرسل على جوالك";
$_['text_explain_same_number'] = "إنتهت المدة المحددة ";
?>
