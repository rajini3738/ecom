<?php
class ControllerInformationCompetition extends Controller {
	private $error = array(); 

	public function index() {
    $this->load->language('information/competition');
		$this->load->model('competition/competition');
		$this->load->model('tool/image'); 
		$this->load->model('catalog/information');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate_entry()) {
			$newsletter = $this->model_competition_competition->getCompetitionNewsletter($this->request->post['competition_id']);
			$agree = $this->model_catalog_information->getInformation($this->config->get('config_competition_id'));
      $name=$this->customer->getFirstName().' '.$this->customer->getLastName();
			$this->model_competition_competition->submitEntry($this->request->post, $name, $this->customer->getEmail());
				   
			$message = sprintf($this->language->get('mail_text_1')) . "\n\n";
			$message .= sprintf($this->language->get('mail_text_2'), date($this->language->get('date_format_short'), strtotime($this->model_competition_competition->getCompetitionDate($this->request->post['competition_id'])))) . "\n\n";
			$message .= sprintf($this->language->get('mail_text_3')) . "\n\n";
			$message .= sprintf($this->language->get('mail_text_4'), $this->config->get('config_name'));
	
			$subject = sprintf($this->language->get('mail_subject'));

      $mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($this->customer->getEmail());
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			$mail->setSubject($subject);
			$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			$mail->send();

			
			/*if (version_compare(VERSION, '2.0.2', '<')) {
				$mail = new Mail($this->config->get('config_mail'));
			} else {
				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->smtp_hostname = $this->config->get('config_mail_smtp_host');
				$mail->smtp_username = $this->config->get('config_mail_smtp_username');
				$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
				$mail->smtp_port = $this->config->get('config_mail_smtp_port');
				$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
			}
			
			$mail->setReplyTo($this->config->get('config_email'));
			$mail->setTo($this->customer->getEmail());
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender($this->config->get('config_name'));
			$mail->setSubject($subject);
			$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			$mail->send();*/
	
			if (($agree) && ($newsletter ==1)) {
				$this->model_competition_competition->addNewsletter($this->request->post);
			}
			
			$this->response->redirect($this->url->link('information/competition/success'));			
		}
		
		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->error['email'])) {
			$data['error_email'] = $this->error['email'];
		} else {
			$data['error_email'] = '';
		}		

		if (isset($this->error['agree'])) {
			$data['error_agree'] = $this->error['agree'];
		} else {
			$data['error_agree'] = '';
		}		

		if (isset($this->error['captcha'])) {
			$data['error_captcha'] = $this->error['captcha'];
		} else {
			$data['error_captcha'] = '';
		}
		
		if (isset($this->error['already_entered'])) {
			$data['error_already_entered'] = $this->error['already_entered'];
		} else {
			$data['error_already_entered'] = '';
		}	
		
		if (isset($this->error['answer'])) {
			$data['error_answer'] = $this->error['answer'];
		} else {
			$data['error_answer'] = '';
		}
		
		if (isset($this->request->post['entered_answer'])) {
			$data['answer'] = $this->request->post['entered_answer'];
		} else {
			$data['answer'] = '';
		}		

		if (isset($this->request->post['entered_email'])) {
			$data['email'] = $this->request->post['entered_email'];
		} elseif ($this->customer->isLogged()){
      $data['email'] = $this->customer->getEmail();
    }else {
			$data['email'] = '';
		}		

		if (isset($this->request->post['entered_name'])) {
			$data['name'] = $this->request->post['entered_name'];
		} elseif ($this->customer->isLogged()){
      $data['name'] = $this->customer->getFirstName().' '.$this->customer->getLastName();
    } else {
			$data['name'] = '';
		}		
/*		
		if (isset($this->request->post['captcha'])) {
			$data['captcha'] = $this->request->post['captcha'];
		} else {
			$data['captcha'] = '';
		}		
*/

		if (version_compare(VERSION, '2.0.2', '<')) {
			//pre-2020
			$data['use_google_recaptcha'] = false;
		} else {
			//post-2020
			$data['use_google_recaptcha'] = true;
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
			'separator' => false
		);

		if (isset($this->request->get['competition_id'])) {
			$competition_id = $this->request->get['competition_id'];
		} else {
			$competition_id = 0;
		}

		$competition_info = $this->model_competition_competition->getCompetitionDetails($competition_id);
		
		if ($competition_info) {
		//This is the code for the Competition Page - the Competition List page is further down.
	  		$this->document->setTitle($competition_info['title']);

			if ($this->config->get('config_google_captcha_status')) {
				$this->document->addScript('https://www.google.com/recaptcha/api.js');
	
				$data['site_key'] = $this->config->get('config_google_captcha_public');
			} else {
				$data['site_key'] = '';
			}

			$data['breadcrumbs'][] = array(
				'text'      => $this->language->get('heading_title'),
				'href'      => $this->url->link('information/competition')
			);

			$data['breadcrumbs'][] = array(
				'text'      => $competition_info['title'],
				'href'      => $this->url->link('information/competition', 'competition_id=' . $this->request->get['competition_id'])
			);
			
			$today = strtotime(date("d-m-Y"));
			$end_date = strtotime($this->model_competition_competition->getCompetitionDate($competition_id));

			if($end_date > $today) {
				$data['current'] = true;
			}else{
				$data['current'] = false;
			}

     		$data['competition_info'] = $competition_info;
			
     		$data['winners'] = $this->model_competition_competition->getWinners($competition_id);

     		$data['heading_title'] = $competition_info['title'];

			$this->document->setDescription($competition_info['meta_description']);
			$data['description'] = html_entity_decode($competition_info['description']);
			$data['question'] = $competition_info['question'];
			$data['answer'] = $competition_info['answer_is'];
			$data['auto'] = $competition_info['auto'];
			$data['auto_message'] = $competition_info['auto_entry_message'];
	   		$data['closed'] = $this->language->get('text_closed');
     		$data['button_competition'] = $this->language->get('button_competition');
     		$data['entry_email'] = $this->language->get('entry_email');
     		$data['entry_name'] = $this->language->get('entry_name');
			$data['entry_button'] = $this->language->get('entry_button');
     		$data['text_question'] = $this->language->get('text_question');
			$data['entry_answer'] = $this->language->get('entry_answer');	
			$data['competition_id'] = $this->request->get['competition_id'];
			$data['entry_captcha'] = $this->language->get('entry_captcha');
			$data['competition'] = $this->url->link('information/competition');
	   	$data['text_form'] = $this->language->get('text_form');
	   	$data['text_details'] = $this->language->get('text_details');
     	$data['text_winner'] = $this->language->get('text_winner');
      $url = $this->url->link('account/login', 'competition_id=' .$competition_id, 'SSL');
	   	//$data['text_login_required'] = sprintf($this->language->get('text_login_required'), $url);
      $data['loginurl'] = $url;
			$data['text_end_date'] = $this->language->get('text_end_date');
			$data['end_date'] = date($this->language->get('date_format_short'), $end_date);
			
			$data['action'] = $this->url->link('information/competition', 'competition_id=' . $competition_id);

			
			$this->load->model('catalog/information');
			
			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_competition_id'));
			
			if ($information_info) {
				$data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/agree', 'information_id=' . $this->config->get('config_competition_id')), $information_info['title'], $information_info['title']);
				$data['agree'] = true;
			} else {
				$data['text_agree'] = '';
				$data['agree'] = false;
			}
			
			$data['login'] = $competition_info['login'];
			
			if ($this->customer->isLogged()) {
			$data['logged'] = true;
			}else{
			$data['logged'] = false;
			}
			
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/competition.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/competition.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/information/competition.tpl', $data));
			}

	  	} else {
		// This is the code for the Competition List page below
	  		$competition_data = $this->model_competition_competition->getCompetition();

	  		if ($competition_data) {
				foreach ($competition_data as $result) {
					$data['competition_data'][] = array(
						'title'         => $result['title'],
						'description'   => html_entity_decode($result['description']),
						'auto_message'  => html_entity_decode($result['auto_entry_message']),
						'auto'  		=> $result['auto'],
						'end_date'   	=> date($this->language->get('date_format_short'), strtotime($result['end_date'])),
						'show_closed'   => strtotime($result['end_date']),
						'thumb'			=> $this->model_tool_image->resize($result['image'], $result['width'], $result['height']),
						'winners'		=> $this->model_competition_competition->getWinners($result['competition_id']),
						'href'          => $this->url->link('information/competition', 'competition_id=' . $result['competition_id'])
					);
				}
				
				$this->document->setTitle($this->language->get('heading_title'));

				$this->document->breadcrumbs[] = array(
					'text'      => $this->language->get('heading_title'),
					'href'      => $this->url->link('information/competition'),
					'separator' => $this->language->get('text_separator')
				);

				$data['heading_title'] = $this->language->get('heading_title');
				$data['text_details'] = $this->language->get('text_details');
				$data['text_end_date'] = $this->language->get('text_end_date');
				$data['text_comp_closed'] = $this->language->get('text_comp_closed');
				$data['closed_test'] = strtotime(date("d-m-Y"));
				$data['button_continue'] = $this->language->get('button_continue');
				$data['continue'] = $this->url->link('common/home');
				
				$data['column_left'] = $this->load->controller('common/column_left');
				$data['column_right'] = $this->load->controller('common/column_right');
				$data['content_top'] = $this->load->controller('common/content_top');
				$data['content_bottom'] = $this->load->controller('common/content_bottom');
				$data['footer'] = $this->load->controller('common/footer');
				$data['header'] = $this->load->controller('common/header');
	
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/competition.tpl')) {
					$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/competition.tpl', $data));
				} else {
					$this->response->setOutput($this->load->view('default/template/information/competition.tpl', $data));
				}

	    	} else {
		  		$this->document->setTitle($this->language->get('text_error'));

	     		$this->document->breadcrumbs[] = array(
	        		'text'      => $this->language->get('text_error'),
	        		'href'      => $this->url->link('information/competition'),
	        		'separator' => $this->language->get('text_separator')
	     		);

				$data['heading_title'] = $this->language->get('text_error');
				$data['text_error'] = $this->language->get('text_error');

				$data['button_continue'] = $this->language->get('button_continue');
				$data['continue'] = $this->url->link('common/home');

				$data['column_left'] = $this->load->controller('common/column_left');
				$data['column_right'] = $this->load->controller('common/column_right');
				$data['content_top'] = $this->load->controller('common/content_top');
				$data['content_bottom'] = $this->load->controller('common/content_bottom');
				$data['footer'] = $this->load->controller('common/footer');
				$data['header'] = $this->load->controller('common/header');
	
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
					$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
				} else {
					$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
				}
		  	}
		}
	}

	public function validate_entry() {  
		if($this->model_competition_competition->checkmailid($this->customer->getEmail(), $this->request->post['competition_id'])){
			$this->error['already_entered'] = $this->language->get('error_existing');
		}
    
		if (!$this->error) {
			return true;
		} else {
			return false;
		}  	  
	}	
	
	public function unsubscribe() {
		$this->load->language('information/competition');

		$this->document->setTitle($this->language->get('heading_title_us'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title_us'),
			'href'      => $this->url->link('information/competition/unsubscribe')
		);

		$data['heading_title'] = $this->language->get('heading_title_us');

		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_captcha'] = $this->language->get('entry_captcha');
		
		$data['unsubscribe_button'] = $this->language->get('unsubscribe_button');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/competition_unsubscribe.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/competition_unsubscribe.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('template/information/competition_unsubscribe.tpl', $data));
		}
	}
	
	public function removeEmail(){
    	$this->load->language('information/competition');
		$this->load->model('competition/competition');
		$this->load->model('catalog/information');

		if(isset($this->request->post['unsubscribe_email']) and preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['unsubscribe_email'])){
			if($this->model_competition_competition->checkNewsletterExisting($this->request->post)){

				if($this->session->data['captcha'] == $this->request->post['captcha']) {

					$this->model_competition_competition->unsubscribe($this->request->post);
								
					echo('$("#unsubscribe_result").html("'.$this->language->get('text_unsubscribed').'");$("#unsubscribe")[0].reset();');
			   
	 			}else{
	    			echo('$("#unsubscribe_result").html("<span class=\"error\">'.$this->language->get('error_captcha').'</span>")');
	 			}
			}else{
				echo('$("#unsubscribe_result").html("<span class=\"error\">'.$this->language->get('error_not_existing').'</span>");$("#unsubscribe")[0].reset();');	 
			}
		}else{
			echo('$("#unsubscribe_result").html("<span class=\"error\">'.$this->language->get('error_invalid').'</span>")');
		}
	
	}
	
	public function success() {
		$this->load->language('information/competition');

		$this->document->setTitle($this->language->get('heading_title')); 

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('information/competition')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_message'] = $this->language->get('text_message');

		$data['button_continue'] = 'الصفحة الرئيسية';//$this->language->get('button_continue');

		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/success.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/success.tpl', $data));
		}
	}
	
	public function captcha() {
			$this->load->library('captcha');
		
		$captcha = new Captcha();
		
		$this->session->data['captcha'] = $captcha->getCode();
		
		$captcha->showImage();
	}
	
}
?>