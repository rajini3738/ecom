<?php
class ControllerModuleProduct extends Controller {
	public function index($setting) {
		print_r('dfsdfsdf');
		$this->load->language('module/special');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['categories'] = array();

       // $data['t08_kept_design'] = $this->config->get('theme_kept_design');

        $categories = $this->model_catalog_category->getCategories(0);
        foreach ($categories as $category) {
            if ($category) {
                $filter_data = array(
                    'filter_category_id'    => $category['category_id'],
                    'sort'                  => 'pd.name',
                    'order'                 => 'ASC',
                    'start'                 => 0,
                    'limit'                 => $setting['limit']
                );
                $product_data = array();
                $results = $this->model_catalog_product->getProducts($filter_data);

                if ($results) {
                    foreach ($results as $result) {
                        if ($result['image']) {
                            $image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
                        } else {
                            $image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
                        }

                        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                            $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                        } else {
                            $price = false;
                        }

                        if ((float)$result['special']) {
                            $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                        } else {
                            $special = false;
                        }

                        if ($this->config->get('config_tax')) {
                            $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
                        } else {
                            $tax = false;
                        }

                        if ($this->config->get('config_review_status')) {
                            $rating = $result['rating'];
                        } else {
                            $rating = false;
                        }

                        $product_data[] = array(
                            'product_id'  => $result['product_id'],
                            'thumb'       => $image,
                            'name'        => $result['name'],
                            'description' => utf8_substr(trim(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
                            'price'       => $price,
                            'special'     => $special,
                            'tax'         => $tax,
                            'rating'      => $rating,
                            'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
                        );
                    }

                    $data['categories'][] = array(
                     'category_id'      => $category['category_id'],
                     'cname'            => $category['name'],
                     'products1'         => $product_data,
                     'cdescription'     => strip_tags(html_entity_decode($category['description'], ENT_QUOTES, 'UTF-8'))//$category['description']
                    );
                }
            }
        }
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/product.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/product.tpl', $data);
		} else {
			return $this->load->view('default/template/module/product.tpl', $data);
		}
	}
}