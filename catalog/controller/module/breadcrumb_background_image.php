<?php  
/* 
Version: 1.0
Author: Artur Sułkowski
Website: http://artursulkowski.pl
*/

class ControllerModuleBreadcrumbBackgroundImage extends Controller {
	public function index($setting) {
		$data['background_color'] = $setting['background_color'];
		$data['background_image'] = "'image/" .$setting['background_image']."'";
    $category_id=0;
    if (isset($this->request->get['path'])) {
			$category_id = $this->request->get['path'];
		} 
    if($category_id==60){
      $data['background_image'] = "'/image/catalog/subpage banners/image-blog-Musk.jpg'";
    }elseif($category_id==61){
      $data['background_image'] = "'/image/catalog/subpage banners/image-blog-Ward.jpg'";
    }elseif($category_id==62){
      $data['background_image'] = "'/image/catalog/subpage banners/image-wood.jpg'";
    }elseif($category_id==63){
      $data['background_image'] = "'/image/catalog/subpage banners/image-blog-Attar2.jpg'";
    }
		$data['background_image_position'] = $setting['background_image_position'];
		$data['background_image_repeat'] = $setting['background_image_repeat'];
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/breadcrumb_background_image.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/breadcrumb_background_image.tpl', $data);
		} else {
			return $this->load->view('default/template/module/breadcrumb_background_image.tpl', $data);
		}
	}
}
?>