<?php
class ControllerModuleCompetition extends Controller {
	public function index($setting) {

		$this->load->language('module/competition');
		$this->load->model('competition/competition');
		$this->load->model('tool/image'); 

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_details'] = $this->language->get('text_details');
		$data['text_end_date'] = $this->language->get('text_end_date');

		$data['competition'] = array();

		$results = $this->model_competition_competition->getCompetitionTeaser();

		foreach ($results as $result) {
		
		if ($result['image']) {
			$image = $this->model_tool_image->resize($result['image'], $setting['competition_width'], $setting['competition_height']);
		} else {
			$image = false;
		}
		
			$data['competition'][] = array(
				'title'        => $result['title'],
				'description'  => strip_tags(substr(html_entity_decode($result['description']), 0, 100)),
				'end_date'	   => date($this->language->get('date_format_short'), strtotime($result['end_date'])),
				'thumb'		   => $image,
				'href'         => $this->url->link('information/competition', 'competition_id=' . $result['competition_id'])
			);
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/competition.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/competition.tpl', $data);
		} else {
			return $this->load->view('default/template/module/competition.tpl', $data);
		}
	}
}
?>