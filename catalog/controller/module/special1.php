<?php
class ControllerModuleSpecial1 extends Controller {
	public function index($setting) {

		$this->load->language('module/special');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['text_category'] = $this->language->get('text_category');
		$data['text_select'] = $this->language->get('text_select');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$category_id='';
		$data['category_id'] = $category_id;
		if (isset($this->request->get['category'])) {
			$category_id = $this->request->get['category'];
			$data['category_id'] = $category_id;
		}

		$data['products'] = array();

		$filter_data = array(
			'sort'  => 'pd.name',
			'order' => 'ASC',
			'start' => 0,
			'limit' => $setting['limit'],
			'filter_category_id' =>$category_id
		);

		//$results = $this->model_catalog_product->getProductSpecials($filter_data);
		$results = $this->model_catalog_product->getProductsforhome1($filter_data);

		if ($results) {
			foreach ($results as $result) {

				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$mainprice = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$mainprice = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}

				$options = array();

				foreach ($this->model_catalog_product->getProductOptions($result['product_id']) as $option) {
					$product_option_value_data = array();
                    $discount = false;
                    $discount_price = false;
					foreach ($option['product_option_value'] as $option_value) {
						if (!$option_value['subtract'] || ($option_value['quantity'] > 0) || $option_value['weight'] > 0) {
							if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
								$price = $this->currency->format($this->tax->calculate($option_value['price'], $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
							} else {
								$price = false;
							}
                            if($option_value['price']){
                                if ((int)$option_value['discount'] > 0) {
                                    // $product_discounts = $this->model_catalog_product->getProductDiscounts($result['product_id']);

                                    $pricevalue = ($option_value['price'] /100)* $option_value['discount'];
                                    $discount_price = $option_value['price']-$pricevalue;
                                    $discount_price=$this->currency->format($this->tax->calculate($discount_price, $result['tax_class_id'], $this->config->get('config_tax')));
                                    $discount = $option_value['discount'].'%';
                                }
                            }
							$product_option_value_data[] = array(
								'product_option_value_id' => $option_value['product_option_value_id'],
								'option_value_id'         => $option_value['option_value_id'],
								'name'                    => $option_value['name'],
								'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
								'price'                   => $price,
								'price_prefix'            => $option_value['price_prefix'],
								'quantity'				  => $option_value['quantity'],
                                'discount_price'          => $discount_price,
                                'discount'                => $discount,
							);
						}
					}
					$options[] = array(
						'product_option_id'    => $option['product_option_id'],
						'product_option_value' => $product_option_value_data,
						'option_id'            => $option['option_id'],
						'name'                 => $option['name'],
						'type'                 => $option['type'],
						'value'                => $option['value'],
						'required'             => $option['required']
					);
				}

                $discount_price = false;
                if ((float)$result['discount'] > 0) {
                    $product_discounts = $this->model_catalog_product->getProductDiscounts($result['product_id']);

                    foreach ($product_discounts  as $product_discount) {
                        if (($product_discount['date_start'] == '0000-00-00' || strtotime($product_discount['date_start']) < time()) && ($product_discount['date_end'] == '0000-00-00' || strtotime($product_discount['date_end']) > time())) {
                            $pricevalue = ($result['price'] /100)* $product_discount['price'];
                            $discount_price = $result['price']-$pricevalue;
                            $discount_price=$this->currency->format($this->tax->calculate($discount_price, $result['tax_class_id'], $this->config->get('config_tax')));
                            break;
                        }
                    }
                    $discount = $result['discount'].'%';
                } else {
                    $discount = false;
                }

                $quantity_per = -1;
                $color="";
                if ($result['quantity'] >= 0 && $result['orginalquantity'] > 0) {
                    $quantity_per =round(($result['quantity']/$result['orginalquantity'])*100, 0);
                    if($quantity_per >= 50){
                        $color = "#010101";
                    }
                    else if($quantity_per < 50 && $quantity_per > 0){
                        $color = "#E3A419";
                    }
                    else if($quantity_per <= 0){
                        $color = "#9E2828";
                    }
                }

				$data['products'][] = array(
					'product_id'        => $result['product_id'],
					'thumb'             => $image,
					'name'              => $result['name'],
					'description'       => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'             => $mainprice,
					'special'           => $special,
                    'discount_price'    => $discount_price,
                    'discount'          => $discount,
					'tax'               => $tax,
                    'quantity_per'      => $quantity_per,
                    'quantity_color'    => $color,
					'rating'            => $rating,
					'quantity'          => $result['quantity'],
					'weight'            => $this->weight->getTitle($result['weight_class_id']),
					'href'              => $this->url->link('product/product1', 'product_id=' . $result['product_id']),
					'options'           => $options,
				);
			}

			$this->load->model('catalog/category');

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$categories = $this->model_catalog_category->getCategories(0);

			foreach ($categories as $category) {
				$data['categories'][] = array(
					'category_id' => $category['category_id'],
					'name'        => $category['name'],
					'href'  => $this->url->link('common/home', '&category='.$category['category_id'].'&order=ASC#categoryproduct' . $url)
				);
			}

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/special1.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/special1.tpl', $data);
			} else {
				return $this->load->view('default/template/module/special.tpl', $data);
			}
		}
	}
}