<?php
class ControllerCheckoutAjaxedit extends Controller {

	public function resendConfirmEmail() {

		$json = array();

		if ($this->request->server['REQUEST_METHOD'] == 'POST' && isset($this->request->post['order_id'])) {

			$order_id = $this->request->post['order_id'];
			$order_status_id = $this->request->post['order_status_id'];
			$comment = $this->request->post['comment'];

			$this->load->model('checkout/ajaxedit');
			$json['msg'] = $this->model_checkout_ajaxedit->resendConfirmEmail($order_id, $order_status_id, $comment, true);
		} else {
			$json['error'] = 'Error - try again.';
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function addDiscount() {
		$json = array();
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$post = $this->request->post;
			$this->load->model('checkout/ajaxedit');

			$json = $this->model_checkout_ajaxedit->addDiscount($post);

		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


	public function orderProducts() {
		$this->load->language('api/cart');
		$this->load->model('checkout/ajaxedit');

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error']['warning'] = $this->language->get('error_permission');
		} else if (isset($this->request->post['order_id'])) {
			$this->load->model('checkout/order');
			$order_id = $this->request->post['order_id'];

			$order_info = $this->model_checkout_order->getOrder($order_id);

			// Shipping
			$this->session->data['shipping_address'] = array(
				'firstname'      => $order_info['shipping_firstname'],
				'lastname'       => $order_info['shipping_lastname'],
				'company'        => $order_info['shipping_company'],
				'address_1'      => $order_info['shipping_address_1'],
				'address_2'      => $order_info['shipping_address_2'],
				'postcode'       => $order_info['shipping_postcode'],
				'city'           => $order_info['shipping_city'],
				'zone_id'        => $order_info['shipping_zone_id'],
				'zone'           => $order_info['shipping_zone'],
				'zone_code'      => $order_info['shipping_zone_code'],
				'country_id'     => $order_info['shipping_country_id'],
				'country'        => $order_info['shipping_country'],
				'iso_code_2'     => $order_info['shipping_iso_code_2'],
				'iso_code_3'     => $order_info['shipping_iso_code_3'],
				'address_format' => $order_info['shipping_address_format'],
				'custom_field'   => $order_info['shipping_custom_field']
			);

			if (!empty($order_info['shipping_code'])) {

				$shipping = explode('.', $order_info['shipping_code']);

				$this->load->model('shipping/' . $shipping[0]);

				$_SESSION['ajaxedit_order_id'] = $order_id;

				$quote = $this->{'model_shipping_' . $shipping[0]}->getQuote($this->session->data['shipping_address']);

				if (isset($_SESSION['ajaxedit_order_id'])) {
					unset($_SESSION['ajaxedit_order_id']);
				}
                if($quote['error']){
                    $json['error'][$shipping[0]] = $quote['error'];//$this->language->get('error_stock');
                }
                else{
                    $this->session->data['shipping_method'] = $quote['quote'][$shipping[0]];

                    $shipping_info = $this->getOrderShippingInfo($order_id);
                    $this->session->data['shipping_method']['cost'] = $shipping_info['value'];
                    $this->session->data['shipping_method']['title'] = $shipping_info['title'];
                }

			} else {
				$this->session->data['shipping_method'] = array(
					'cost'	=> '',
					'title'	=> ''
				);
			}
			// Payment

			$this->session->data['payment_address'] = array(
				'firstname'      => $order_info['payment_firstname'],
				'lastname'       => $order_info['payment_lastname'],
				'company'        => $order_info['payment_company'],
				'address_1'      => $order_info['payment_address_1'],
				'address_2'      => $order_info['payment_address_2'],
				'postcode'       => $order_info['payment_postcode'],
				'city'           => $order_info['payment_city'],
				'zone_id'        => $order_info['payment_zone_id'],
				'zone'           => $order_info['payment_zone'],
				'zone_code'      => $order_info['payment_zone_code'],
				'country_id'     => $order_info['payment_country_id'],
				'country'        => $order_info['payment_country'],
				'iso_code_2'     => $order_info['payment_iso_code_2'],
				'iso_code_3'     => $order_info['payment_iso_code_3'],
				'address_format' => $order_info['payment_address_format'],
				'custom_field'   => $order_info['payment_custom_field']
			);

			$this->load->model('payment/' . $order_info['payment_code']);

			$_SESSION['ajaxedit_order_id'] = $order_id;

			$payment_method = $this->{'model_payment_' . $order_info['payment_code']}->getMethod($this->session->data['payment_address'], $this->cart->getTotal());

			if (isset($_SESSION['ajaxedit_order_id'])) {
				unset($_SESSION['ajaxedit_order_id']);
			}

			$this->session->data['payment_method'] = $payment_method;

			// Stock
			$_SESSION['ajaxedit_order_id'] = $order_id;
			if (!$this->cart->hasStock() && (!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning'))) {
				$json['error']['stock'] = $this->language->get('error_stock');
			}

			// Products
			$json['products'] = array();

			$_SESSION['ajaxedit_order_id'] = $order_id;
			$products = $this->cart->getProducts();

			foreach ($products as $product) {
				$product_total = 0;

				foreach ($products as $product_2) {
					if ($product_2['product_id'] == $product['product_id']) {
						$product_total += $product_2['quantity'];
					}
				}

				if ($product['minimum'] > $product_total) {
					$json['error']['minimum'][] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
				}

				$option_data = array();

				foreach ($product['option'] as $option) {
					$option_data[] = array(
						'product_option_id'       => $option['product_option_id'],
						'product_option_value_id' => $option['product_option_value_id'],
						'name'                    => $option['name'],
						'value'                   => $option['value'],
						'type'                    => $option['type']
					);
				}

				$json['products'][] = array(
					'key'        => $product['key'],
					'product_id' => $product['product_id'],
					'order_product_id' => $product['order_product_id'],
					'name'       => $product['name'],
					'model'      => $product['model'],
					'option'     => $option_data,
					'image'		 => $this->getProductImage($product['product_id']),
					'quantity'   => $product['quantity'],
					'stock'      => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
					'shipping'   => $product['shipping'],
					'price'      => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'))),
					'total'      => $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']),
					'reward'     => $product['reward'],
					'href'     	 => $this->url->link('catalog/product/edit', 'token=' . $this->request->post['token'] . '&product_id=' . $product['product_id'], 'SSL')
				);
			}

			// Voucher

			$json['vouchers'] = array();

			if (!empty($this->session->data['vouchers'])) {
				foreach ($this->session->data['vouchers'] as $key => $voucher) {
					$json['vouchers'][] = array(
						'code'             => $voucher['code'],
						'description'      => $voucher['description'],
						'from_name'        => $voucher['from_name'],
						'from_email'       => $voucher['from_email'],
						'to_name'          => $voucher['to_name'],
						'to_email'         => $voucher['to_email'],
						'voucher_theme_id' => $voucher['voucher_theme_id'],
						'message'          => $voucher['message'],
						'amount'           => $this->currency->format($voucher['amount'])
					);
				}
			}

			// Totals

			$this->load->model('extension/extension');

			$total_data = array();
			$total = 0;

			$_SESSION['ajaxedit_order_id'] = $order_id;
			$taxes = $this->cart->getTaxes();

			$sort_order = array();

			$results = $this->model_extension_extension->getExtensions('total', $this->cart->hasDiscount());

			$_SESSION['ajaxedit_order_id'] = $order_id;
			$custom_discount = $this->model_checkout_ajaxedit->getCustomDiscount();
			unset($_SESSION['ajaxedit_order_id']);

			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}

			if ($custom_discount) {

				$results[] = array(
					'extension_id' => 99999,
					'type'	=> 'total',
					'code'	=> 'customdiscount'
				);

				$sort_order[] = $custom_discount['sort_order'];
            }

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {
				if ($result['code'] == 'customdiscount') {
					$this->load->model('total/' . $result['code']);

					$_SESSION['ajaxedit_order_id'] = $order_id;

					$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);

					if (isset($_SESSION['ajaxedit_order_id'])) {
						unset($_SESSION['ajaxedit_order_id']);
					}
				} elseif ($this->config->get($result['code'] . '_status')) {
					$this->load->model('total/' . $result['code']);

					$_SESSION['ajaxedit_order_id'] = $order_id;

					$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);

					if (isset($_SESSION['ajaxedit_order_id'])) {
						unset($_SESSION['ajaxedit_order_id']);
					}
				}
			}

			$sort_order = array();

			foreach ($total_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $total_data);

			$json['totals'] = array();

			foreach ($total_data as $total) {
				$json['totals'][] = array(
					'code'	=> $total['code'],
					'title' => $total['title'],
					'text'  => $this->currency->format($total['value']),
					'value' => $total['value'],
					'sort_order'	=> $total['sort_order']
				);
			}

			// Write the Order Totals to the database
			$this->model_checkout_ajaxedit->editOrderTotals($json['totals'],$order_id);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	protected function calculateCustomDiscount($custom_discount, &$total_data, &$total, &$taxes) {

        $total_data[] = array(
            'code'       => 'customdiscount',
            'title'      => $custom_discount['title'],
            'value'      => $custom_discount['value'],
            'sort_order' => $custom_discount['sort_order']
        );

        $discount = $custom_discount['value'];
        $discount_partial = -$discount/($this->cart->countProducts());

        foreach ($this->cart->getProducts() as $product) {

            if ($product['tax_class_id']) {
                $tax_rates = $this->tax->getRates($product['total'] - ($product['total'] - ($discount_partial * $product['quantity'])), $product['tax_class_id']);

                foreach ($tax_rates as $tax_rate) {
                    if ($tax_rate['type'] == 'P') {
                        $taxes[$tax_rate['tax_rate_id']] -= $tax_rate['amount'];
                    }
                }

            }
        }

        if (isset($_SESSION['ajaxedit_order_id'])) {
            unset($_SESSION['ajaxedit_order_id']);
        }

        $total += $discount;
        return true;
	}

	protected function getProductImage($product_id) {
		$query = $this->db->query("SELECT image FROM `". DB_PREFIX. "product` WHERE `product_id` = '" . (int)$product_id . "'");
		$this->load->model('tool/image');

		if ($query->num_rows) {
			$image = $this->model_tool_image->resize($query->row['image'], 40, 40);
		} else {
			$image = $this->model_tool_image->resize('no_image.png', 40, 40);
		}
		return $image;
	}

	protected function getOrderShippingInfo($order_id) {
		$query = $this->db->query("SELECT * FROM `". DB_PREFIX. "order_total` WHERE `code` = 'shipping' AND `order_id` = '" . (int)$order_id . "' LIMIT 1");

		return !empty($query->num_rows) ? $query->row : false ;
	}
}