<?php
class ControllerCheckoutPaymentMethod extends Controller {
	public function index() {
		$this->load->language('checkout/checkout');
		if (isset($this->session->data['payment_address'])) {
            $data['additional_payment']=false;
            //if($this->session->data['shipping_method']['code']=="aramex.aramex"){
            //    $data['additional_payment'] = "لتوفير الرسوم الإضافية الخاصة بالسداد عند الاستلام، اجعل خيارك التحويل البنكي أو استخدم بطاقة الفيزا";
            //}
			// Totals
            $address = $this->session->data['shipping_address']['iso_code_2'];
            $gulfCounry=false;
            if($address=="SA" || $address=="BH" || $address=="KW" || $address=="OM" || $address=="QA" || $address=="AE")
                $gulfCounry=true;

			$total_data = array();
			$total = 0;
			//$taxes = $this->cart->getTaxes();
			$taxes = $this->cart->getTaxes1();
			$data['live'] =$this->config->get('sadad_live');
			$this->load->model('extension/extension');

			$sort_order = array();

			$results = $this->model_extension_extension->getExtensions('total', $this->cart->hasDiscount());

			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('total/' . $result['code']);

					$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
				}
			}

			// Payment Methods
			$products = $this->cart->getProducts();
			$is_zero_price_product=false;
			/*if(sizeof($products)==1){
            foreach ($products as $product_2) {
            if ($product_2['product_id'] == '142') {
            //if ($product_2['product_id'] == '51') {
            $is_zero_price_product=true;
            }
            }
			}*/
            $data['text_only_banktransfer']=false;
			$method_data = array();

			$this->load->model('extension/extension');

			$results = $this->model_extension_extension->getExtensions('payment');

			$recurring = $this->cart->hasRecurringProducts();
			if(!$is_zero_price_product){
				foreach ($results as $result) {
                    if($total > 3500 || !$gulfCounry){
                        if($result['code']=="payfort_fort" || $result['code']=="bank_transfer" || $result['code']=="paytabs"){
                            if ($this->config->get($result['code'] . '_status')) {
                                $this->load->model('payment/' . $result['code']);

                                $method = $this->{'model_payment_' . $result['code']}->getMethod($this->session->data['payment_address'], $total);

                                if ($method) {
                                    if ($recurring) {
                                        if (method_exists($this->{'model_payment_' . $result['code']}, 'recurringPayments') && $this->{'model_payment_' . $result['code']}->recurringPayments()) {
                                            $method_data[$result['code']] = $method;
                                        }
                                    } else {
                                        $method_data[$result['code']] = $method;
                                    }
                                }
                            }
                        }
                        //    $data['text_only_banktransfer'] = $this->language->get('text_only_banktransfer');
                        //$this->load->model('payment/bank_transfer');

                        //$method = $this->{'model_payment_bank_transfer'}->getMethod($this->session->data['payment_address'], $total);
                        //if ($method) {
                        //    $method_data['bank_transfer'] = $method;
                        //}
                    }
                    else{
                        if ($this->config->get($result['code'] . '_status')) {
                            $this->load->model('payment/' . $result['code']);

                            $method = $this->{'model_payment_' . $result['code']}->getMethod($this->session->data['payment_address'], $total);

                            if ($method) {
                                if ($recurring) {
                                    if (method_exists($this->{'model_payment_' . $result['code']}, 'recurringPayments') && $this->{'model_payment_' . $result['code']}->recurringPayments()) {
                                        $method_data[$result['code']] = $method;
                                    }
                                } else {
                                    $method_data[$result['code']] = $method;
                                }
                            }
                        }
                    }
				}
			}
			else{
				$this->load->model('payment/cod');

				$method = $this->{'model_payment_cod'}->getMethod($this->session->data['payment_address'], $total);
				if ($method) {
					$method_data['cod'] = $method;
				}
			}

			$sort_order = array();

			foreach ($method_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $method_data);

			$this->session->data['payment_methods'] = $method_data;


		}

		$data['text_payment_method'] = $this->language->get('text_payment_method');
		$data['text_comments'] = $this->language->get('text_comments');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['shipping_adressss'] = $this->session->data['shipping_address'];
		if (empty($this->session->data['payment_methods'])) {
			$data['error_warning'] = sprintf($this->language->get('error_no_payment'), $this->url->link('information/contact'));
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['payment_methods'])) {
			$data['payment_methods'] = $this->session->data['payment_methods'];
		} else {
			$data['payment_methods'] = array();
		}

		if (isset($this->session->data['payment_method']['code'])) {
			$data['code'] = $this->session->data['payment_method']['code'];
		} else {
			$data['code'] = '';
		}

		if (isset($this->session->data['comment'])) {
			$data['comment'] = $this->session->data['comment'];
		} else {
			$data['comment'] = '';
		}

		$data['scripts'] = $this->document->getScripts();

		if ($this->config->get('config_checkout_id')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_checkout_id'));

			if ($information_info) {
				$data['text_agree'] = sprintf($this->language->get('text_agree'), $this->url->link('information/information/agree', 'information_id=' . $this->config->get('config_checkout_id'), 'SSL'), $information_info['title'], $information_info['title']);
				$data['text_agree1'] = sprintf($this->language->get('text_agree1'), $this->url->link('information/information/agree', 'information_id=' . 9, 'SSL'), 'شروط الاسترجاع والاستبدال', 'شروط الاسترجاع والاستبدال');

				$data['text_agree'] = sprintf($this->language->get('text_agree'),'https://www.eleganceoud.com/index.php?route=information/information/agree&information_id=' . $this->config->get('config_checkout_id'), $information_info['title'], $information_info['title']);
				$data['text_agree1'] = sprintf($this->language->get('text_agree1'), 'https://www.eleganceoud.com/index.php?route=information/information/agree&information_id=8', 'شروط الاسترجاع والاستبدال', 'شروط الاسترجاع والاستبدال');
			} else {
				$data['text_agree'] = '';
				$data['text_agree1'] = '';
			}
		} else {
			$data['text_agree'] = '';
			$data['text_agree1'] = '';
		}

		if (isset($this->session->data['agree'])) {
			$data['agree'] = $this->session->data['agree'];
		} else {
			$data['agree'] = '';
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/payment_method.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/checkout/payment_method.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/checkout/payment_method.tpl', $data));
		}
	}

	public function save() {
		$this->load->language('checkout/checkout');

		$json = array();

		// Validate if payment address has been set.
		if (!isset($this->session->data['payment_address'])) {
			$json['redirect'] = $this->url->link('checkout/checkout', '', 'SSL');
		}

		// Validate cart has products and has stock.
		//if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
		if (((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout')))&& $this->cart->hasSubtract()) {
			$json['redirect'] = $this->url->link('checkout/cart');
		}

		// Validate minimum quantity requirements.
		$products = $this->cart->getProducts();

		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total) {
				$json['redirect'] = $this->url->link('checkout/cart');

				break;
			}
		}

		if (!isset($this->request->post['payment_method'])) {
			$json['error']['warning'] = $this->language->get('error_payment');
		} elseif (!isset($this->session->data['payment_methods'][$this->request->post['payment_method']])) {
			$json['error']['warning'] = $this->language->get('error_payment');
		}

		if ($this->config->get('config_checkout_id')) {
			$this->load->model('catalog/information');

			$information_info = $this->model_catalog_information->getInformation($this->config->get('config_checkout_id'));

			if ($information_info && !isset($this->request->post['agree'])) {
				$json['error']['warning'] = sprintf($this->language->get('error_agree'), $information_info['title']);
			}
			if ($information_info && !isset($this->request->post['agree1'])) {
				$json['error']['warning1'] = sprintf($this->language->get('error_agree1'), 'شروط الاسترجاع والاستبدال');
			}
		}

		if (!$json) {
			$this->session->data['payment_method'] = $this->session->data['payment_methods'][$this->request->post['payment_method']];

			$this->session->data['comment'] = strip_tags($this->request->post['comment']);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
