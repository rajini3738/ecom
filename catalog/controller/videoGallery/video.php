<?php 
class ControllerVideoGalleryVideo extends Controller {  
	public function index() {
		$this->load->language('videoGallery/video');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('videoGallery/video')
		);
		
		if(isset($this->request->get['album_id']) || !isset($this->request->get['album_id'])){
			
			$this->load->model('catalog/videoGallery');
			
			$data['text_sort'] = $this->language->get('text_sort');	
      $data['button_back'] = $this->language->get('button_back');
      $data['back'] = $this->url->link('videoGallery/album');
				        
			if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
			} else { 
				$page = 1;
			}	
					
			if (isset($this->request->get['sort'])) {
				$sort = $this->request->get['sort'];
			} else {
				$sort = 'sort_order';
			}
		
			if (isset($this->request->get['order'])) {
				$order = $this->request->get['order'];
			} else {
				$order = 'ASC';
			}
				
			$url = '';
					
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	
		
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			$results = $this->model_catalog_videoGallery->getVideosbyAlbumID(21,$sort, $order, ($page - 1) * 12, 12);
			$video_total = $this->model_catalog_videoGallery->getTotalVideo(21);
			$this->model_catalog_videoGallery->updateViewed(21);
			
			if($results){
				
				$album_info = $this->model_catalog_videoGallery->getAlbum(21);
				
				$data['heading_title'] = $this->language->get('text_album') . ' : ' . $album_info['name'];
				
				$data['breadcrumbs'][] = array(
					'text'      => $this->language->get('text_videoGallery'),
					'href'      => $this->url->link('videoGallery/album'),			
				);

				$data['breadcrumbs'][] = array(
					'text'      => $this->language->get('text_album') . ' : ' .  $album_info['name'],
					'href'      => $this->url->link('videoGallery/video&album_id=21'),			
				);
						
				$data['videos'] = array();
				
				$this->load->model('tool/image');
			
		        foreach ($results as $result) {
					if ($result['image']) {
						$image = $result['image'];
					} else {
						$image = 'no_image.jpg';
					}	
					
					$data['videos'][] = array(
		            	'name'         => $result['name'],
						'description'  => $result['description'],
						'code'         => $result['code'],
						'source'       => $result['source'],
						'thumb'        => $image,
					    'popup'        => $this->model_tool_image->resize($image, $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height')),
						'date_added'   => explode(" ",$result['date_added']),
		          	);
		        }
	
		        $url = '';
				
				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}			
				
				$data['sorts'] = array();
						
				$data['sorts'][] = array(
					'text'  => $this->language->get('text_default'),
					'value' => 'sort_order-ASC',
					'href'  => $this->url->link('videoGallery/video&album_id='. 21 .'&sort=sort_order&order=ASC')
				);
						
				$data['sorts'][] = array(
					'text'  => $this->language->get('text_name_asc'),
					'value' => 'name-ASC',
					'href'  => $this->url->link('videoGallery/video&album_id='. 21 .'&sort=name&order=ASC')
				);
		 
				$data['sorts'][] = array(
					'text'  => $this->language->get('text_name_desc'),
					'value' => 'name-DESC',
					'href'  => $this->url->link('videoGallery/video&album_id='. 21 .'&sort=name&order=DESC')
				);
		
				$data['sorts'][] = array(
					'text'  => $this->language->get('text_date_added_asc'),
					'value' => 'date_added-ASC',
					'href'  => $this->url->link('videoGallery/video&album_id='. 21 .'&sort=date_added&order=ASC')
				); 
		
				$data['sorts'][] = array(
					'text'  => $this->language->get('text_date_added_desc'),
					'value' => 'date_added-DESC',
					'href'  => $this->url->link('videoGallery/video&album_id='. 21 .'&sort=date_added&order=DESC')
				); 
												
				$url = '';
				
				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}	
		
				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}
				
				$pagination = new Pagination();
				$pagination->total = $video_total;
				$pagination->page = $page;
				$pagination->limit = 12;
				$pagination->text = $this->language->get('text_pagination');
				$pagination->url = $this->url->link('videoGallery/video&album_id='. 21 . $url . '&page={page}');
					
				$data['pagination'] = $pagination->render();
					
				$data['sort'] = $sort;
				$data['order'] = $order;
				
				$this->document->setTitle('Album : '.$album_info['name']);
	   					

				$data['text_error'] = $this->language->get('text_error');

				$data['button_continue'] = $this->language->get('button_continue');

				$data['continue'] = $this->url->link('common/home');
					
        		$data['column_left'] = $this->load->controller('common/column_left');
				$data['column_right'] = $this->load->controller('common/column_right');
				$data['content_top'] = $this->load->controller('common/content_top');
				$data['content_bottom'] = $this->load->controller('common/content_bottom');
				$data['footer'] = $this->load->controller('common/footer');
				$data['header'] = $this->load->controller('common/header');
					
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . 'default/template/videoGallery/video.tpl')) {
					$this->response->setOutput($this->load->view($this->config->get('config_template') . 'default/template/videoGallery/video.tpl', $data));
				} else {
					$this->response->setOutput($this->load->view('default/template/videoGallery/video.tpl', $data));
				}			
				
			} else {
				
				$this->document->setTitle($this->language->get('text_empty'));
   			    
   			    $data['heading_title'] = $this->language->get('Not Found');

				$data['text_error'] = $this->language->get('text_error');

				$data['button_continue'] = $this->language->get('button_continue');

				$data['continue'] = $this->url->link('common/home');
				
				
				$data['column_left'] = $this->load->controller('common/column_left');
				$data['column_right'] = $this->load->controller('common/column_right');
				$data['content_top'] = $this->load->controller('common/content_top');
				$data['content_bottom'] = $this->load->controller('common/content_bottom');
				$data['footer'] = $this->load->controller('common/footer');
				$data['header'] = $this->load->controller('common/header');
			
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . 'default/template/error/not_found.tpl')) {
					$this->response->setOutput($this->load->view($this->config->get('config_template') . 'default/template/error/not_found.tpl', $data));
				} else {
					$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
				}	
				
			}
			
		} else {
			
			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('Not Found');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');
			
			
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

		
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . 'default/template/error/not_found.tpl')) {
					$this->response->setOutput($this->load->view($this->config->get('config_template') . 'default/template/error/not_found.tpl', $data));
				} else {
					$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
				}	
		}					
    	
	}  	
}
?>
