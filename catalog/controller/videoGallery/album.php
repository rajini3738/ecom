<?php 
class ControllerVideoGalleryAlbum extends Controller {
	private $error = array();
	
	public function index() {

		$this->load->language('videoGallery/album');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('videoGallery/album')
		);
		
		 
		$data['text_sort'] = $this->language->get('text_sort');
		
		
		$this->load->model('catalog/videoGallery'); 
		$this->load->model('tool/image');
		
		
			
			if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
			} else { 
				$page = 1;
			}	
				
			if (isset($this->request->get['sort'])) {
				$sort = $this->request->get['sort'];
			} else {
				$sort = 'sort_order';
			}
	
			if (isset($this->request->get['order'])) {
				$order = $this->request->get['order'];
			} else {
				$order = 'ASC';
			}
			
			$url = '';
				
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	
	
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			$data['albums'] = array();
        		
			$results = $this->model_catalog_videoGallery->getAlbums($sort, $order, ($page - 1) * 8, 8);
			$album_total = $this->model_catalog_videoGallery->getTotalAlbum();
		
		
			
	        foreach ($results as $result) {
				if ($result['image']) {
					$image = $result['image'];
				} else {
					$image = 'no_image.jpg';
				}	
				
				$data['albums'][] = array(
	            	'name'       => $result['name'],
					'thumb'      => $this->model_tool_image->cropsize($image, 320, 180),
				    'date_added' => explode(" ",$result['date_added']),
	          		'href'    	 => $this->url->link('videoGallery/video', 'album_id=' . $result['album_id']),
				);
	        }
	        
	        $url = '';
			
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}			
			
			$data['sorts'] = array();
					
			$data['sorts'][] = array(
				'text'  => $this->language->get('text_default'),
				'value' => 'sort_order-ASC',
				'href'    	 => $this->url->link('videoGallery/album&sort=sort_order&order=ASC'),
			);
					
			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_asc'),
				'value' => 'name-ASC',
				'href'  => $this->url->link('videoGallery/album&sort=name&order=ASC'),
			);
	 
			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_desc'),
				'value' => 'name-DESC',
				'href'  => $this->url->link('videoGallery/album&sort=name&order=DESC')
			);
	
			$data['sorts'][] = array(
				'text'  => $this->language->get('text_date_added_asc'),
				'value' => 'date_added-ASC',
				'href'  => $this->url->link('videoGallery/album&sort=date_added&order=ASC')
			); 
	
			$data['sorts'][] = array(
				'text'  => $this->language->get('text_date_added_desc'),
				'value' => 'date_added-DESC',
				'href'  => $this->url->link('videoGallery/album&sort=date_added&order=DESC')
			); 
					
			if ($this->config->get('config_review')) {
				$data['sorts'][] = array(
					'text'  => $this->language->get('text_viewed_desc'),
					'value' => 'viewed-DESC',
					'href'  => $this->url->link('videoGallery/album&sort=viewed&order=DESC')
				); 
						
				$data['sorts'][] = array(
					'text'  => $this->language->get('text_viewed_asc'),
					'value' => 'viewed-ASC',
					'href'  => $this->url->link('videoGallery/album?sort=viewed&order=ASC')
				);
			}
					
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	
	
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
				
			$pagination = new Pagination();
		    $pagination->total = $album_total;
		    $pagination->page = $page;
		    $pagination->limit = 5;
		    $pagination->url = $this->url->link('videoGallery/album?page={page}');

		    $data['pagination'] = $pagination->render();
				
			$data['sort'] = $sort;
			$data['order'] = $order;
		
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_message'] = $this->language->get('text_success');

		$data['button_continue'] = $this->language->get('button_continue');

		$data['continue'] = $this->url->link('common/home');
					
        $data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

			
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . 'default/template/videoGallery/album.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . 'default/template/videoGallery/album.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/videoGallery/album.tpl', $data));
		}			
    	
	}
}
?>
