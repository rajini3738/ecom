<?php
class ControllerAccountWinningbids extends Controller {
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/winningbids', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->language('account/yourbids');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_winninbids'),
			'href' => $this->url->link('account/winningbids', '', true)
		);

		$this->load->model('account/yourbids');
		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_empty'] = $this->language->get('text_empty');

		$data['column_order_id'] = $this->language->get('column_order_id');
		$data['column_name'] = $this->language->get('column_name');
		$data['column_size'] = $this->language->get('column_size');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['text_refine'] = $this->language->get('text_refine');
		$data['text_empty'] = $this->language->get('text_empty');
		$data['text_quantity'] = $this->language->get('text_quantity');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_currentbid'] = $this->language->get('text_currentbid');
		$data['text_yourcurrent'] = $this->language->get('text_yourcurrent');
		$data['text_yourbid'] = $this->language->get('text_yourbid');
		$data['text_model'] = $this->language->get('text_model');
		$data['text_price'] = $this->language->get('text_price');
		$data['text_tax'] = $this->language->get('text_tax');
		$data['text_points'] = $this->language->get('text_points');
		$data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
		$data['text_sort'] = $this->language->get('text_sort');
		$data['text_limit'] = $this->language->get('text_limit');
		$data['text_winninbids'] = $this->language->get('text_winninbids');

		$data['button_buy'] = $this->language->get('button_buy');  
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_list'] = $this->language->get('button_list');
		$data['button_grid'] = $this->language->get('button_grid');
		$data['button_bid'] = $this->language->get('button_bid');

		$data['products'] = array();
    
		$products = $this->model_account_yourbids->getWinningbids();
		if (!empty($products)) {
			foreach ($products as $product) {
				$product_id = $product['product_id'];
				$product_info = $this->model_catalog_product->getProduct($product_id);

				if ($product_info) {
					if ($product_info['image']) {
					$image = $this->model_tool_image->resize($product_info['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				}

				if ($product_info['image']) {
					$image = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'));
				} else {
					$image = '';
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}
          
				  $high_infa = $this->model_catalog_product->getHighbid($product_info['product_id']);
				  if ($high_infa > $product_info['start_bid']) {
				  $current_bid = $this->currency->format($this->tax->calculate($high_infa['bid'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				  } else {
				  $current_bid = $this->currency->format($this->tax->calculate($product_info['start_bid'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				  }

					if ((float)$product_info['special']) {
						$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$special = false;
					}
          
          $current_ypourbid = $this->model_account_yourbids->getYourbid($product_info['product_id']);
          if ((float)$current_ypourbid) {
						$yourcurrent = $this->currency->format($this->tax->calculate($current_ypourbid, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$yourcurrent = false;
					}

					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
					} else {
						$tax = false;
					}
          
          if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
					} else {
						$tax = false;
					}
          
          if ($this->config->get('config_tax')) {
						$taxcurrent_bid = $this->currency->format((float)$high_infa['bid'], $this->session->data['currency']);
					} else {
						$taxcurrent_bid = $this->currency->format((float)$product_info['start_bid'], $this->session->data['currency']);;
					}
          
          if ($this->config->get('config_tax')) {
						$taxcurrent_ypourbid = $this->currency->format((float)$current_ypourbid, $this->session->data['currency']);
					} else {
						$taxcurrent_ypourbid = false;
					}

					if ($this->config->get('config_review_status')) {
						$rating = $product_info['rating'];
					} else {
						$rating = false;
					}

					$data['products'][] = array(
						'product_id'  => $product_info['product_id'],
						'thumb'       => $image,
						'name'        => $product_info['name'],
						'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
						'price'       => $price,
            'current_bid' => $current_bid,
            'yourcurrent' => $yourcurrent,
						'special'     => $special,
						'tax'         => $tax,
            'taxcurrent_ypourbid'         => $taxcurrent_ypourbid,
            'taxcurrent_bid'         => $taxcurrent_bid,
						'rating'      => $rating,
						'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
					);
				}
			}
		}
    /*
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['downloads'] = array();

		$download_total = $this->model_account_download->getTotalDownloads();

		$results = $this->model_account_download->getDownloads(($page - 1) * $this->config->get($this->config->get('config_theme') . '_product_limit'), $this->config->get($this->config->get('config_theme') . '_product_limit'));

		

		$pagination = new Pagination();
		$pagination->total = $yourbids_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get($this->config->get('config_theme') . '_product_limit');
		$pagination->url = $this->url->link('account/yourbids', 'page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($yourbids_total) ? (($page - 1) * $this->config->get($this->config->get('config_theme') . '_product_limit')) + 1 : 0, ((($page - 1) * $this->config->get($this->config->get('config_theme') . '_product_limit')) > ($yourbids_total - $this->config->get($this->config->get('config_theme') . '_product_limit'))) ? $yourbids_total : ((($page - 1) * $this->config->get($this->config->get('config_theme') . '_product_limit')) + $this->config->get($this->config->get($this->config->get('config_theme') . '_theme') . '_product_limit')), $yourbids_total, ceil($yourbids_total / $this->config->get($this->config->get('config_theme') . '_product_limit')));
     */
		$data['continue'] = $this->url->link('account/account', '', true);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/winningbids.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/winningbids.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/order_list.tpl', $data));
		}
		//$this->response->setOutput($this->load->view('account/winningbids.tpl', $data));
	}


}
