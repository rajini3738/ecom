<?php
class ControllerAccountComplaint extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('account/complaint');

		$this->load->model('account/complaint');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$complaint_id = $this->model_account_complaint->addComplaint($this->request->post);
	            $data = $this->request->post;

                //if($data['code']){
                //    if ($this->request->server['HTTPS']) {
                //        $server = HTTPS_CATALOG;
                //    } else {
                //        $server = HTTP_CATALOG;
                //    }
                //    $file = $server . 'system/storage/upload/' . $data['code'];
                //    $data['filename'] = $file;
                //}

	            $message  = '<html xmlns="http://www.w3.org/1999/xhtml">' . "\n";
	            $message .= '  <head>' . "\n";
	            $message .= '    <title>Complaint</title>' . "\n";
	            $message .= '    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . "\n";
	            $message .= '  </head>' . "\n";
	            $message .= '  <body>  <table><tr>';
	            $message .= '  <td>First Name : </td><td>'.$data['firstname']. "</td></tr>\n";
	            $message .= '  <tr><td>Middle Name :  </td><td>'.$data['middlename']. "</td></tr>\n";
	            $message .= '  <tr><td>Last Name :  </td><td>'.$data['lastname']. "</td></tr>\n";
	            $message .= '  <tr><td>Email :  </td><td>'.$data['email']. "</td></tr>\n";
	            $message .= '  <tr><td>Telephone :  </td><td>'.$data['telephone']. "</td></tr>\n";
	            if($data['reason_id']==1)
	                $message .= '  <tr><td>Reason :  </td><td>'.html_entity_decode('تتعلق بقسم المالية', ENT_QUOTES, 'UTF-8'). "</td></tr>\n";
	            else  if($data['reason_id']==2)
	                $message .= '  <tr><td>Reason :  </td><td>'.html_entity_decode('تتعلق بالإرجاع', ENT_QUOTES, 'UTF-8'). "</td></tr>\n";
	            else  if($data['reason_id']==3)
	                $message .= '  <tr><td>Reason :  </td><td>'.html_entity_decode('تتعلق بالشحن', ENT_QUOTES, 'UTF-8'). "</td></tr>\n";
	            else  if($data['reason_id']==4)
	                $message .= '  <tr><td>Reason :  </td><td>'.html_entity_decode('الطلبية فيها خطأ', ENT_QUOTES, 'UTF-8'). "</td></tr>\n";
	            else  if($data['reason_id']==5)
	                $message .= '  <tr><td>Reason :  </td><td>'.html_entity_decode('سبب آخر', ENT_QUOTES, 'UTF-8'). "</td></tr>\n";
                //if($data['code']){
                //    $message .= '  <tr><td>Uploaded File :  </td><td>'.$data['filename']. "</td></tr>\n";
                //}
	            $message .= '  </table></body>' . "\n";
	            $message .= '</html>' . "\n";

	            $mail = new Mail();
	            $mail->protocol = $this->config->get('config_mail_protocol');
	            $mail->parameter = $this->config->get('config_mail_parameter');
	            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
	            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
	            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
	            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
	            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

	            $mail->setTo('Sokkatalteebksa3@gmail.com');
	            $mail->setFrom($this->config->get('config_email'));
	            $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
	            $mail->setSubject('Complaint');
	            $mail->setHtml($message);
	            $mail->send();

		    $this->response->redirect($this->url->link('account/complaint/success', '', 'SSL'));
		}

		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('account/complaint', '', 'SSL')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_description'] = $this->language->get('text_description');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_product'] = $this->language->get('text_product');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');

        	$data['text_payment_proof'] = $this->language->get('text_payment_proof');
		$data['button_upload'] = $this->language->get('button_upload');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['entry_order_id'] = $this->language->get('entry_order_id');
		$data['entry_date_ordered'] = $this->language->get('entry_date_ordered');
		$data['entry_firstname'] = $this->language->get('entry_firstname');
		$data['entry_lastname'] = $this->language->get('entry_lastname');
		$data['entry_middlename'] = $this->language->get('entry_middlename');
		$data['entry_email'] = $this->language->get('entry_email');
       		$data['entry_telephone'] = $this->language->get('entry_telephone');
		$data['entry_product'] = $this->language->get('entry_product');
		$data['entry_model'] = $this->language->get('entry_model');
		$data['entry_opened'] = $this->language->get('entry_opened');
		$data['entry_fault_detail'] = $this->language->get('entry_fault_detail');
        	$data['entry_reason'] = $this->language->get('entry_reason');

		$data['button_submit'] = $this->language->get('button_submit');
		$data['button_back'] = $this->language->get('button_back');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['order_id'])) {
			$data['error_order_id'] = $this->error['order_id'];
		} else {
			$data['error_order_id'] = '';
		}

		if (isset($this->error['firstname'])) {
			$data['error_firstname'] = $this->error['firstname'];
		} else {
			$data['error_firstname'] = '';
		}

		if (isset($this->error['lastname'])) {
			$data['error_lastname'] = $this->error['lastname'];
		} else {
			$data['error_lastname'] = '';
		}

		if (isset($this->error['middlename'])) {
			$data['error_middlename'] = $this->error['middlename'];
		} else {
			$data['error_middlename'] = '';
		}

		if (isset($this->error['email'])) {
			$data['error_email'] = $this->error['email'];
		} else {
			$data['error_email'] = '';
		}

		if (isset($this->error['telephone'])) {
			$data['error_telephone'] = $this->error['telephone'];
		} else {
			$data['error_telephone'] = '';
		}

        if (isset($this->error['reason'])) {
			$data['error_reason'] = $this->error['reason'];
		} else {
			$data['error_reason'] = '';
		}

		$data['action'] = $this->url->link('account/complaint', '', 'SSL');

		$this->load->model('account/order');

        if (isset($this->request->post['order_id'])) {
			$data['order_id'] = $this->request->post['order_id'];
		} else {
			$data['order_id'] = '';
		}

		if (isset($this->request->post['firstname'])) {
			$data['firstname'] = $this->request->post['firstname'];
		} else {
			$data['firstname'] = $this->customer->getFirstName();
		}

		if (isset($this->request->post['lastname'])) {
			$data['lastname'] = $this->request->post['lastname'];
		} else {
			$data['lastname'] = $this->customer->getLastName();
		}

		if (isset($this->request->post['middlename'])) {
			$data['middlename'] = $this->request->post['middlename'];
		} else {
			$data['middlename'] = $this->customer->getMiddleName();
		}

		if (isset($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		} else {
			$data['email'] = $this->customer->getEmail();
		}

        if (isset($this->request->post['telephone'])) {
			$data['telephone'] = $this->request->post['telephone'];
		} else {
			$data['telephone'] = $this->customer->getTelephone();
		}

        if (isset($this->request->post['reason_id'])) {
			$data['reason_id'] = $this->request->post['reason_id'];
		} else {
			$data['reason_id'] = '';
		}

		if (isset($this->request->post['opened'])) {
			$data['opened'] = $this->request->post['opened'];
		} else {
			$data['opened'] = false;
		}

        if (isset($this->request->post['code'])) {
			$data['code'] = $this->request->post['code'];
		} else {
			$data['code'] = '';
		}

		if (isset($this->request->post['comment'])) {
			$data['comment'] = $this->request->post['comment'];
		} else {
			$data['comment'] = '';
		}

		$data['back'] = $this->url->link('account/account', '', 'SSL');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/complaint_form.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/complaint_form.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/account/complaint_form.tpl', $data));
		}
	}

	public function success() {
		$this->load->language('account/complaint');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('account/complaint', '', 'SSL')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_message'] = $this->language->get('text_message');

		$data['button_continue'] = $this->language->get('button_continue');

		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/success.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/success.tpl', $data));
		}
	}

	protected function validate() {

		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen(trim($this->request->post['middlename'])) < 1) || (utf8_strlen(trim($this->request->post['middlename'])) > 32)) {
			$this->error['middlename'] = $this->language->get('error_middlename');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
			$this->error['email'] = $this->language->get('error_email');
		}

        if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
			$this->error['telephone'] = $this->language->get('error_telephone');
		}

        if (empty($this->request->post['reason_id'])) {
			$this->error['reason'] = $this->language->get('error_reason');
		}

		return !$this->error;
	}
}
