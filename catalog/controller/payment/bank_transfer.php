<?php
class ControllerPaymentBankTransfer extends Controller {
	public function index() {
		$this->load->language('payment/bank_transfer');

		$data['text_instruction'] = $this->language->get('text_instruction');
		$data['text_description'] = $this->language->get('text_description');
		$data['text_payment'] = $this->language->get('text_payment');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['text_payment_proof'] = $this->language->get('text_payment_proof');
		$data['button_upload'] = $this->language->get('button_upload');
		$data['text_loading'] = $this->language->get('text_loading');
		//$data['token'] = $this->session->data['token'];
		$data['entry_progress'] = $this->language->get('entry_progress');

		$data['button_confirm'] = $this->language->get('button_confirm');

		$data['bank'] = nl2br($this->config->get('bank_transfer_bank' . $this->config->get('config_language_id')));

		$data['continue'] = $this->url->link('checkout/success');

        $data['user_f_name'] = $this->customer->getFirstName(). " ". $this->customer->getLastName();

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/bank_transfer.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/payment/bank_transfer.tpl', $data);
		} else {
			return $this->load->view('default/template/payment/bank_transfer.tpl', $data);
		}
	}

	public function confirm() {
		if ($this->session->data['payment_method']['code'] == 'bank_transfer') {
			$this->load->language('payment/bank_transfer');

			$this->load->model('checkout/order');

			$comment  = $this->language->get('text_instruction') . "\n\n";
			$comment .= $this->config->get('bank_transfer_bank' . $this->config->get('config_language_id')) . "\n\n";
			$comment .= "<p style='color:red'>".$this->language->get('text_payment')."</p>";

			// upload reciept

			$this->load->model('account/order');

			$code = $this->request->post['code'];
			$orderid = $this->session->data['order_id'];

			$account_name = $this->request->post['account_name'];
			$account_number = $this->request->post['account_number'];
			$sukkat_bank = $this->request->post['sukkat_bank'];
			$bank_transfer_made = $this->request->post['bank_transfer_made'];

			$this->model_account_order->updateOrder($code, $orderid,$account_name,$account_number,$sukkat_bank,$bank_transfer_made);	

			// end

			$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('bank_transfer_order_status_id'), $comment, true,true);
		}
	}
}