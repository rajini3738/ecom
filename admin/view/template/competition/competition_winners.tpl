<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
      <a href="<?php echo $cancel; ?>" class="btn btn-primary"><?php echo $button_cancel; ?></a>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form').submit() : false;"><i class="fa fa-trash-o"></i></button>
		</div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
			<thead>
              <tr>
				<td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
				<td class="text-left"><?php echo $column_name; ?></td>
				<td class="text-left"><?php echo $column_email; ?></td>
				<td class="text-left"><?php echo $column_notified; ?></td>
				<td class="text-right"><?php echo $column_action; ?></td>
			  </tr>
			</thead>
			<tbody>
			  <?php if ($emails) { ?>
				<?php foreach ($emails as $email) { ?>
				  <tr>
					<td class="text-center">
					  <input type="checkbox" name="selected[]" value="<?php echo $email['id']; ?>"/>
					  <input type="hidden" name="competition_id" id="competition_id" value="<?php echo $competition_id; ?>" >
					</td>
					<td class="text-left"><?php echo $email['name']; ?><input type="hidden" name="email_name" id="email_name" value="<?php echo $email['name']; ?>" /></td>
					
					<td class="text-left"><?php echo $email['email']; ?><input type="hidden" name="email_id" id="email_id" value="<?php echo $email['email']; ?>" /></td>
					
					<td class="text-left"><?php echo $email['notified']; ?></td>
					<td class="text-right">
					  <?php foreach ($email['action'] as $action) { ?>
						[ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
					  <?php } ?>
					</td>
				  </tr>
				<?php } ?>
			  <?php } else { ?>
				<tr>
				  <td class="text-center" colspan="5"><?php echo $text_no_results; ?></td>
				</tr>
			  <?php } ?>
			</tbody>
			</table>
		  </div>
		</form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>