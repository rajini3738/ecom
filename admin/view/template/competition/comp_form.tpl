<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form" data-toggle="tooltip" title="<?php echo $button_save_competition; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
            <li><a href="#tab-data" data-toggle="tab"><?php echo $tab_data; ?></a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-general">
              <ul class="nav nav-tabs" id="language">
                <?php foreach ($languages as $language) { ?>
                <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                <?php } ?>
              </ul>
              <div class="tab-content">
                <?php foreach ($languages as $language) { ?>
				  <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-title<?php echo $language['language_id']; ?>"><?php echo $entry_title; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="competition_data[<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($competition_data[$language['language_id']]) ? $competition_data[$language['language_id']]['title'] : ''; ?>" placeholder="<?php echo $entry_title; ?>" id="input-title<?php echo $language['language_id']; ?>" class="form-control" />
                      <?php if (isset($error_title[$language['language_id']])) { ?>
                      <div class="text-danger"><?php echo $error_title[$language['language_id']]; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-meta_description<?php echo $language['language_id']; ?>"><?php echo $entry_meta_description; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="competition_data[<?php echo $language['language_id']; ?>][meta_description]" value="<?php echo isset($competition_data[$language['language_id']]) ? $competition_data[$language['language_id']]['meta_description'] : ''; ?>" placeholder="<?php echo $entry_meta_description; ?>" id="input-meta_description<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-auto_entry_message<?php echo $language['language_id']; ?>"><?php echo $entry_auto_message; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="competition_data[<?php echo $language['language_id']; ?>][auto_entry_message]" value="<?php echo isset($competition_data[$language['language_id']]) ? $competition_data[$language['language_id']]['auto_entry_message'] : ''; ?>" placeholder="e.g. Spend $xxx to be entered into this competition." id="input-auto_entry_message<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-entry_question<?php echo $language['language_id']; ?>"><?php echo $entry_question; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="competition_data[<?php echo $language['language_id']; ?>][question]" value="<?php echo isset($competition_data[$language['language_id']]) ? $competition_data[$language['language_id']]['question'] : ''; ?>" placeholder="<?php echo $entry_question; ?>" id="input-entry_question<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-answer1<?php echo $language['language_id']; ?>"><?php echo $entry_answer1; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="competition_data[<?php echo $language['language_id']; ?>][answer1]" value="<?php echo isset($competition_data[$language['language_id']]) ? $competition_data[$language['language_id']]['answer1'] : ''; ?>" placeholder="<?php echo $entry_answer1; ?>" id="input-answer1<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-answer2<?php echo $language['language_id']; ?>"><?php echo $entry_answer2; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="competition_data[<?php echo $language['language_id']; ?>][answer2]" value="<?php echo isset($competition_data[$language['language_id']]) ? $competition_data[$language['language_id']]['answer2'] : ''; ?>" placeholder="<?php echo $entry_answer2; ?>" id="input-answer2<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-answer3<?php echo $language['language_id']; ?>"><?php echo $entry_answer3; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="competition_data[<?php echo $language['language_id']; ?>][answer3]" value="<?php echo isset($competition_data[$language['language_id']]) ? $competition_data[$language['language_id']]['answer3'] : ''; ?>" placeholder="<?php echo $entry_answer3; ?>" id="input-answer3<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-answer4<?php echo $language['language_id']; ?>"><?php echo $entry_answer4; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="competition_data[<?php echo $language['language_id']; ?>][answer4]" value="<?php echo isset($competition_data[$language['language_id']]) ? $competition_data[$language['language_id']]['answer4'] : ''; ?>" placeholder="<?php echo $entry_answer4; ?>" id="input-answer4<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                    <div class="col-sm-10">
                      <textarea name="competition_data[<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>"><?php echo isset($competition_data[$language['language_id']]) ? $competition_data[$language['language_id']]['description'] : ''; ?></textarea>
                      <?php if (isset($error_description[$language['language_id']])) { ?>
                      <div class="text-danger"><?php echo $error_description[$language['language_id']]; ?></div>
                      <?php } ?>
                    </div>
                  </div>
				  </div>
				<?php } ?>
			  </div>
			</div>
            <div class="tab-pane" id="tab-data">
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-answer_is"><?php echo $entry_answer_is; ?></label>
                <div class="col-sm-10">
                  <select name="answer_is" id="input-answer_is" class="form-control">
                    <option value="">-none-</option>
					<?php foreach ($answerlists as $answerlist) { ?>
					  <?php if ($answerlist == $answer_is) { ?>
					    <option value="<?php echo $answer_is ?>" selected="selected"><?php echo $answerlist; ?></option>
					  <?php } else { ?>
					    <option value="<?php echo $answerlist; ?>"><?php echo $answerlist; ?></option>
					  <?php } ?>
				    <?php } ?>
                  </select>
                </div>
              </div>
			  <div class="form-group">
				<label class="col-sm-2 control-label" for="input-keyword"><?php echo $entry_keyword; ?></label>
				<div class="col-sm-10">
				  <input type="text" name="keyword" value="<?php echo $keyword; ?>" placeholder="<?php echo $entry_keyword; ?>" id="input-keyword" class="form-control" />
				</div>
			  </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-winners"><?php echo $entry_winners; ?></label>
				<div class="col-sm-10">
				  <input type="text" name="winners" value="<?php echo $winners; ?>" placeholder="<?php echo $entry_winners; ?>" id="input-winners" class="form-control" />
				</div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-auto"><?php echo $entry_auto; ?></label>
                <div class="col-sm-10">
                  <select name="auto" id="input-auto" class="form-control">
					<?php if ($auto) { ?>
					  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					  <option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					  <option value="1"><?php echo $text_enabled; ?></option>
					  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-auto_value"><?php echo $entry_auto_value; ?></label>
				<div class="col-sm-10">
				  <input type="text" name="auto_entry_value" value="<?php echo $auto_entry_value; ?>" id="input-auto_value" class="form-control" />
				</div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-login"><?php echo $entry_login; ?></label>
                <div class="col-sm-10">
                  <select name="login" id="input-login" class="form-control">
					<?php if ($login) { ?>
					  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					  <option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					  <option value="0"><?php echo $text_disabled; ?></option>
					<?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-newsletter"><?php echo $entry_newsletter; ?></label>
                <div class="col-sm-10">
                  <select name="newsletter" id="input-newsletter" class="form-control">
					<?php if ($newsletter) { ?>
					  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					  <option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					  <option value="1"><?php echo $text_enabled; ?></option>
					  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-end_date"><?php echo $entry_end_date; ?></label>
                <div class="col-sm-3">
                  <div class="input-group date">
                    <input type="text" name="end_date" value="<?php echo $end_date; ?>" placeholder="<?php echo $entry_end_date; ?>" data-date-format="YYYY-MM-DD" id="input-end_date" class="form-control" />
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                    </span></div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-image"><?php echo $entry_image; ?></label>
                <div class="col-sm-10">
                  <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                  <input type="hidden" name="image" value="<?php echo $image; ?>" id="input-image" />
                </div>
              </div>            
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-image_size"><?php echo $entry_image_size; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="width" value="<?php echo $width; ?>" id="input-image_size"/> x <input type="text" name="height" value="<?php echo $height; ?>" />
                </div>
              </div>            
              <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $entry_store; ?></label>
                <div class="col-sm-10">
                  <div class="well well-sm" style="height: 150px; overflow: auto;">
                    <div class="checkbox">
                      <label>
						<?php if (in_array(0, $competition_store)) { ?>
						<input type="checkbox" name="competition_store[]" value="0" checked="checked" />
						<?php echo $text_default; ?>
						<?php } else { ?>
						<input type="checkbox" name="competition_store[]" value="0" />
						<?php echo $text_default; ?>
						<?php } ?>
                      </label>
                    </div>
                    <?php foreach ($stores as $store) { ?>
                    <div class="checkbox">
                      <label>
						<?php if (in_array($store['store_id'], $competition_store)) { ?>
						<input type="checkbox" name="competition_store[]" value="<?php echo $store['store_id']; ?>" checked="checked" />
						<?php echo $store['name']; ?>
						<?php } else { ?>
						<input type="checkbox" name="competition_store[]" value="<?php echo $store['store_id']; ?>" />
						<?php echo $store['name']; ?>
						<?php } ?>
                      </label>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                <div class="col-sm-10">
                  <select name="status" id="input-status" class="form-control">
					<?php if ($status) { ?>
					  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					  <option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					  <option value="1"><?php echo $text_enabled; ?></option>
					  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
                  </select>
                </div>
              </div>
              </div>
            </div>
      </div>
    </div>
  </div>
<script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
$('#input-description<?php echo $language['language_id']; ?>').summernote({height: 300});
<?php } ?>
//--></script> 
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

//--></script> 
  <script type="text/javascript"><!--
$('#language a:first').tab('show');
$('#option a:first').tab('show');
//--></script></div>
<?php echo $footer; ?> 