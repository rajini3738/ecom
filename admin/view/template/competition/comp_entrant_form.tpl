<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form" data-toggle="tooltip" title="<?php echo $button_save_entrant; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_entrant_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">
		  <div class="form-group required">
			<label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
			<div class="col-sm-10">
			  <input type="text" name="email_name" value="<?php echo isset($email_name)?$email_name:""; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
			</div>
		  </div>
		  <div class="form-group required">
			<label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
			<div class="col-sm-10">
			<input type="text" name="email_id" id="input-email" value="<?php echo isset($email_id)?$email_id:''; ?>" placeholder="<?php echo $entry_email; ?>"  class="form-control" />
			<input type="hidden" name="competition_id" id="competition_id" value="<?php echo $compid; ?>" />
			<?php if ($error_email_exist) { ?>
			  <div class="text-danger"><?php echo $error_email_exist; ?></div>
			<?php } ?>
			<?php if ($error_email_name) { ?>
			  <div class="text-danger"><?php echo $error_email_name; ?></div>
			<?php } ?>
			</div>
		  </div>
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="input-answer"><?php echo $entry_answer_is; ?></label>
			<div class="col-sm-10">
			<select name="answer_is" id="input-answer" class="form-control">
			  <option value="">-none-</option>  
			  <?php foreach ($answerlists as $answerlist) { ?>
				<?php if ($answerlist == $answer_is) { ?>
				  <option value="<?php echo $answer_is ?>" selected="selected"><?php echo $answerlist; ?></option>
				<?php } else { ?>
				  <option value="<?php echo $answerlist; ?>"><?php echo $answerlist; ?></option>
				<?php } ?>
			  <?php } ?>
			</select>
			</div>
		  </div>
		</form>
	  </div>
	</div>
  </div>
</div>
<?php echo $footer; ?>