<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <style>
    #loading {
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    position: fixed;
    display: block;
    opacity: 0.7;
    background-color: #FFFFFF;
    z-index: 99;
    text-align: center;
    }

    #loading-image {
    position: absolute;
    top: 200px;
    z-index: 100;
    }
  </style>
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right" style="display: flex;">
		<?php if ($emails) { ?>
		  <?php if (empty($check_winner)) { ?>
			<form>
			  <select name="choose">
				<option value="" disabled selected>Select Action</option>
				<?php foreach ($dropdown as $dd) { ?>
				  <option value="<?php echo $dd['link']; ?>"><?php echo $dd['name']; ?></option>
				<?php } ?>
				</select>
				<a onclick="page_redirect();" class="btn btn-primary"><span><?php echo $button_go; ?></span></a>
			</form> 
			&nbsp;<a onclick="$('#mainform').submit();" class="btn btn-danger" data-toggle="tooltip" title="<?php echo $button_delete; ?>"><i class="fa fa-trash-o"></i></a>
		  <?php } else { ?>
			&nbsp;<a href="<?php echo $view_winner; ?>" class="btn btn-primary"><span><?php echo $button_winner; ?></span></a>
		  <?php } ?>
		<?php } ?>
		&nbsp;<a href="<?php echo $insert; ?>" class="btn btn-success" data-toggle="tooltip" title="<?php echo $button_insert; ?>"><i class="fa fa-plus"></i></a>
	  </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="mainform">
		  <input type="hidden" name="competition_id" id="competition_id" value="<?php echo $competition_id; ?>" />
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
			<thead>
          <tr>
            <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
            <td class="text-left"><?php echo $column_name; ?></td>
            <td class="text-left"><?php echo $column_email; ?></td>
            <td class="text-left"><?php echo $column_answer; ?></td>
            <td class="text-right"><?php echo $column_action; ?></td>
          </tr>
        </thead>
        <tbody>
          <?php if ($emails) { ?>
          <?php foreach ($emails as $email) { ?>
          <tr>
            <td style="text-align: center;">
              <input type="checkbox" name="selected[]" value="<?php echo $email['id']; ?>"/>
             </td>
            <td class="text-left"><?php echo $email['name']; ?></td>
            <td class="text-left"><?php echo $email['email']; ?></td>
            <td class="text-left"><?php echo $email['answer']; ?></td>
            <td class="text-right"><?php foreach ($email['action'] as $action) { ?>
              [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
              <?php } ?></td>
          </tr>
          <?php } ?>
          <?php } else { ?>
          <tr>
            <td class="text-center" colspan="5"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
	  </div>
    <div id="loading" style="display:none">
      <img id="loading-image" src="view/image/ajax.gif" alt="Loading..." />
    </div>
    </form>
</div></div>
   <div class="pagination"><?php echo $pagination; ?></div>
</div>
<script type="text/javascript">
    <!--
function page_redirect() {
var url = $('select[name=\'choose\']').val();
var retVal = confirm("Are You Sure?");
  if( retVal == true ){
    $('#loading').show();
	  location.href = url;
  }
}
//--></script></div>
<?php echo $footer; ?> 