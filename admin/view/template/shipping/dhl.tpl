<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-dhl" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-dhl" class="form-horizontal">
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="wf_dhl_shipping_"><?php echo 'Site ID' ?></label> 
            <div class="col-sm-10">   
              <input class="form-control" type="text" name="dhl_site_id" id="wf_dhl_shipping_site_id" value="<?php echo $dhl_site_id; ?>" placeholder="<?php echo 'Site ID' ?>">
              <?php if ($error_site_id) { ?>
              <div class="text-danger"><?php echo $error_site_id; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="wf_dhl_shipping_site_pwd"><?php echo 'Site Password' ?></label> 
            <div class="col-sm-10">   
              <input class="form-control" type="Password" name="dhl_site_pwd" id="wf_dhl_shipping_site_pwd" value="<?php echo $dhl_site_pwd; ?>" placeholder="<?php echo 'Site Password' ?>"> 
              <?php if ($error_site_pwd) { ?>
              <div class="text-danger"><?php echo $error_site_pwd; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-country"><?php echo $entry_country; ?></label>
            <div class="col-sm-10">
              <select name="dhl_country" id="input-country" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($countries as $country) { ?>
                <?php if ($country['country_id'] == $dhl_country) { ?>
                <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
              <?php if ($error_country) { ?>
              <div class="text-danger"><?php echo $error_country; ?></div>
              <?php } ?>
            </div>
          </div>      
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="dhl_city"><?php echo 'City' ?></label> 
            <div class="col-sm-10">   
              <input class="form-control" type="text" name="dhl_city" id="dhl_city" value="<?php echo $dhl_city; ?>" placeholder="<?php echo 'City' ?>">
              <?php if ($error_city) { ?>
              <div class="text-danger"><?php echo $error_city; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="dhl_postcode"><?php echo 'Postcode' ?></label> 
            <div class="col-sm-10">   
              <input class="form-control" type="text" name="dhl_postcode" id="dhl_postcode" value="<?php echo $dhl_postcode; ?>" placeholder="<?php echo 'Postcode' ?>">
              <?php if ($error_postcode) { ?>
              <div class="text-danger"><?php echo $error_postcode; ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-2">
                <span class="response">
                  <?php if(isset($error_message)) {
                    echo $error_message; 
                  } ?>
                </span>
                <input type="button" value=" Validate Credentials" class="btn btn-default" name="validate_credentials" >
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-geo-zone"><?php echo $entry_geo_zone; ?></label>
            <div class="col-sm-10">
              <select name="dhl_geo_zone_id" id="input-geo-zone" class="form-control">
                <option value="0"><?php echo $text_all_zones; ?></option>
                <?php foreach ($geo_zones as $geo_zone) { ?>
                <?php if ($geo_zone['geo_zone_id'] == $dhl_geo_zone_id) { ?>
                <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="dhl_status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">                          
              <select id="dhl_status" name="dhl_status" class="form-control">
                  <?php if ($dhl_status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="sort_order"><?php echo $entry_sort_order; ?></label>
            <div class="col-sm-10">   
              <input id="sort_order" type="text" name="dhl_sort_order" value="<?php echo $dhl_sort_order; ?>" class="form-control">
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
$('input[name="validate_credentials"]').click(function(){
  $('.response').html(' ');
  $.ajax({
    url: 'index.php?route=shipping/dhl/validation&token=<?= $token; ?>',
    type: 'post',
    data: {site_id: $('input[name="dhl_site_id"]').val(), site_pwd: $('input[name="dhl_site_pwd"]').val(), country_id: $('select[name="dhl_country"]').val(), city: $('input[name="dhl_city"]').val(), postcode: $('input[name="dhl_postcode"]').val()},
    dataType: 'json',
    success: function(json){
      if(json['error_message']){
        $('.response').html(json['error_message']);
      } else {
        $('.dashicons-yes').removeClass('hidden');
      }
    },
    error: function(xhr, ajaxOptions, thrownError) {

      console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);

    }
  });
});
</script>
<?php echo $footer; ?> 