<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-manufacturer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">

         <div class="form-group">
            <label class="col-sm-2 control-label" for="input-image"><?php echo $entry_source; ?></label>
            <div>
              <input id="sourceid" type="hidden" value="<?php echo $source;?>"/>
              <?php if ($source == 'youtube') { ?>
                  <input id="yout" type="radio" name="source" value="youtube" checked="checked"/> YouTube<br/>
                  <input id="vime" type="radio" name="source" value="vimeo" /> Vimeo<br/>
                  <?php } elseif ($source == 'vimeo') { ?>
                  <input id="yout" type="radio" name="source" value="youtube"/> YouTube<br/>
                  <input id="vime" type="radio" name="source" value="vimeo" checked="checked"/> Vimeo<br/>
                  <?php } else { ?>
                  <input id="yout" type="radio" name="source" value="youtube"/> YouTube<br/>
                  <input id="vime" type="radio" name="source" value="vimeo"/> Vimeo<br/> 
                  <?php }?>
            </div>
          </div>

          <div class="form-group" style="display: none;" id="youttr">
            <label class="col-sm-2 control-label" for="input-sort-order">YouTube URL:</label>
            <div class="col-sm-10">
            <b>Enter YouTube Video ID or URL in the text box below</b><br/>
                <input type="text" id="youtubeDataFetcherInput" style="width: 380px;" value="<?php if($source=='youtube') echo $code; ?>" maxlength="200">
                 <input type="button" value="Fetch Video Information" onClick="youtubeFetchData( );">
            </div>
          </div>

          <div class="form-group" style="display: none;" id="vimetr">
            <label class="col-sm-2 control-label" for="input-sort-order">Vimeo URL:</label>
            <div class="col-sm-10">
            <b>Enter Vimeo URL in the text box below - </b><span style="color: #666;">http://vimeo.com/xxxxxxxx</span><br/>
                <input type="text" id="vimeoDataFetcherInput" style="width: 380px;" value="<?php if($source=='vimeo') echo $code; ?>" maxlength="200">
            <input type="button" value="Fetch Video Information" onClick="getVimeoId();">
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
            <div class="col-sm-10">
              <input type="text" id="input-name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" name="name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>

          <div class="form-group" style="display:none;">
            <label class="col-sm-2 control-label" for="input-description"><?php echo $entry_description; ?></label>
            <div class="col-sm-10">
              <textarea type="text" name="description" value="<?php echo $description; ?>" placeholder="<?php echo $entry_description; ?>" id="input-description" class="form-control" /><?php echo $description; ?></textarea>
              <?php if ($error_description) { ?>
              <div class="text-danger"><?php echo $error_description; ?></div>
              <?php } ?>
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-image"><?php echo $entry_image; ?></label>
            <div class="col-sm-10"> 

             <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" width="100" height="100"/></a>

               <input type="hidden" name="image" value="<?php echo $image; ?>" id="input-image"/>
            
            </div>
          </div>


       <div class="form-group">
          <label class="col-sm-2 control-label" for="input-album"><?php echo $entry_album; ?></span></label>
          <div class="col-sm-10">
            <div class="scrollbox">
              <?php foreach ($albums as $album) { ?>
              <div>
                <?php if (in_array($album['album_id'], $video_album)) { ?>
                <input type="checkbox" name="video_album[]" value="<?php echo $album['album_id']; ?>" checked="checked" />
                <?php echo $album['name']; ?>
                <?php } else { ?>
                <input type="checkbox" name="video_album[]" value="<?php echo $album['album_id']; ?>" />
                <?php echo $album['name']; ?>
                <?php } ?>
              </div>
              <?php } ?>
            </div>
        </div>

        </div>

          <div style="display: none;">
             <input  name="code" value="<?php echo $code; ?>" id="code" />
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
            <div class="col-sm-10">
              <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

$('#input-description').summernote({
  height: 300
});
</script>

<script type="text/javascript">
  if($('#sourceid').val() == 'youtube'){
    $('#youttr').show();
  } else if ($('#sourceid').val() == 'vimeo'){
    $('#vimetr').show();
  } else {  
    $('#vimetr').hide();
    $('#youttr').hide();
  }
$('#yout').click(function() {
  $('#youttr').show(); 
  $('#vimetr').hide();
});

$('#vime').click(function() {
  $('#vimetr').show();
  $('#youttr').hide()
});
</script>

 <script type="text/javascript">
    
  function youtubeFetchData( )
    {

      var videoid = '';
      var tempvar = $.trim($('#youtubeDataFetcherInput').val());

      if ( /^https?\:\/\/.+/i.test( tempvar ) )
      {
        tempvar = /[\?\&]v=([^\?\&]+)/.exec( tempvar );
        if ( ! tempvar )
        {
          alert( 'YouTube video URL has a problem!' );
          return;
        }
        videoid = tempvar[ 1 ];
      }
      else
      {
        if ( /^[A-Za-z0-9_\-]{8,32}$/.test( tempvar ) == false )
        {
          alert( 'YouTube video ID has a problem!' );
          return;
        }
        videoid = tempvar;
      }


      $.getJSON("https://www.googleapis.com/youtube/v3/videos", {
          key: "AIzaSyAWE9QGBrLxNlS20aOCVfmsPP6VBiJSL_8",
          part: "snippet,statistics",
          id: videoid
        }, function(data) {
          if (data.items.length === 0) {
            alert( 'YouTube video ID has a problem!' );
            return;
          }

          $('#input-name').val(data.items[0].snippet.title);
          $('#input-description').code(data.items[0].snippet.description);

          var url = $('#youtubeDataFetcherInput').val();

          $('#code').val(url);

          var i = '';
    
          i = '<img src="' + data.items[0].snippet.thumbnails.medium.url +'" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" width="100" height="100"/>';
      
          $('#thumb-image').html(i);

          $('#input-image').val(data.items[0].snippet.thumbnails.medium.url);

     });
        
    }
 

  </script>
 
<script type="text/javascript">  

  

function getVimeoId() {

 var tempvar = $.trim($('#vimeoDataFetcherInput').val());

 if(tempvar.length>0){

    var re = new RegExp('/[0-9]+', "g");
    var match = re.exec(tempvar);
    if (match == null) {
       alert( 'Vimeo video ID has a problem!' );
          return;
    } else {
       videoid = match[0].substring(1);
   

     $.getJSON('http://www.vimeo.com/api/v2/video/' +videoid+ '.json?callback=?', {format: "json"}, function(data) {
      
    $('#input-name').val(data[0].title);
    $('#code').val(data[0].url);
      
      // Get the HTML and remove <br /> variants
    var htmlCleaned = (data[0].description).replace(/<br \/>/g, "\n");

    // Set the cleaned HTML
    $('#input-description').code(htmlCleaned);

    var i = '';

    i = '<img src="' + data[0].thumbnail_large +'" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" width="100" height="100"/>';
      
      $('#thumb-image').html(i);

      $('#input-image').val(data[0].thumbnail_large);


});

    }
  }
  return false;
}

 
</script> 

<?php echo $footer; ?>


