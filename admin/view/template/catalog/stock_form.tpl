<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-product" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
       <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="product_name" value="<?php echo $product_name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>
		     <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-quantity"><?php echo $entry_quantity; ?></label>
            <div class="col-sm-10">
              <input type="text" name="quantity" value="<?php echo $quantity; ?>" placeholder="<?php echo $entry_quantity; ?>" id="input-quantity" class="form-control" />
              <?php if ($error_quantity) { ?>
              <div class="text-danger"><?php echo $error_quantity; ?></div>
              <?php } ?>
            </div>
          </div>
		     <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-orginalquantity"><?php echo $entry_orginalquantity; ?></label>
            <div class="col-sm-10">
              <input type="text" name="orginalquantity" value="<?php echo $orginalquantity; ?>" placeholder="<?php echo $entry_orginalquantity; ?>" id="input-orginalquantity" class="form-control" />
              <?php if ($error_orginalquantity) { ?>
              <div class="text-danger"><?php echo $error_orginalquantity; ?></div>
              <?php } ?>
            </div>
          </div>
		     <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-adminquantity"><?php echo $entry_adminquantity; ?></label>
            <div class="col-sm-10">
              <input type="text" name="adminquantity" value="<?php echo $adminquantity; ?>" placeholder="<?php echo $entry_adminquantity; ?>" id="input-adminquantity" class="form-control" />
              <?php if ($error_adminquantity) { ?>
              <div class="text-danger"><?php echo $error_adminquantity; ?></div>
              <?php } ?>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?> 