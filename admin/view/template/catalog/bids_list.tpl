<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-category">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td class="text-left"><?php echo $column_customer; ?></td>
                  <td class="text-left"><?php echo $column_bid; ?></td>
                  <td class="text-right"><?php echo $column_date_added; ?></td>
                  <td class="text-right"><?php echo $column_action; ?></td>
                </tr>
              </thead>
                            <tbody>
                <?php if ($bids) { ?>
                <?php foreach ($bids as $bid) { ?>
                <tr>
                  <td class="text-left"><?php echo $bid['customer']; ?></td>
                  <td class="text-right"><?php echo $bid['bid']; ?></td>
                  <td class="text-right"><?php echo $bid['date_added']; ?></td>
                  <td class="text-right">
                   <?php if ($bid['bid_status'] == 0 && $bid['customer_id']) { ?> 
                    <?php if ($bid['bid_status'] == 0) { ?>
                  <a id="approve<?php echo $bid['bid_id']; ?>" class="btn btn-success"><?php echo $text_selectbid; ?></a>
                  <?php } else { ?>
                  <span id="seleckted" class="btn btn-danger"><?php echo $text_selectedbid; ?></span>
                  <?php } ?>
                  <?php } else { ?>
                  <span id="seleckted" class="btn btn-danger"><?php echo $text_selectedbid; ?></span>
                  <?php } ?>
                    
                  </td>  
                </tr>
                <script type="text/javascript"><!--
$(document).delegate('#approve<?php echo $bid['bid_id']; ?>', 'click', function() {
	$.ajax({
		url: 'index.php?route=catalog/bids/approve&token=<?php echo $token; ?>&customer_id=<?php echo $bid['customer_id']; ?>&product_id=<?php echo $product_id; ?>&bid=<?php echo $bid['pbid']; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#approve<?php echo $bid['bid_id']; ?>').button('loading');
		},
		complete: function() {
			$('#approve<?php echo $bid['bid_id']; ?>').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
                $('#content').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
				$('#approve<?php echo $bid['bid_id']; ?>').replaceWith('<span id="seleckted" class="btn btn-danger"><?php echo $text_selectedbid; ?></span>');
				location.reload()
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#disapprove', 'click', function() {
	$.ajax({
		url: 'index.php?route=catalog/bids/disapprove&token=<?php echo $token; ?>&customer_id=<?php echo $bid['customer_id']; ?>&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#disapprove<?php echo $bid['bid_id']; ?>').button('loading');
		},
		complete: function() {
			$('#disapprove<?php echo $bid['bid_id']; ?>').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
                $('#content').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
				$('#seleckted').replaceWith('<a id="approve<?php echo $bid['bid_id']; ?>" class="btn btn-success"><?php echo $text_selectbid; ?></a>');
				location.reload()
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

//--></script>
                <?php } ?>
                <div class="buttons">
          <div class="pull-right">
            <a id="disapprove" class="btn btn-danger"><?php echo $text_disapproveallbid; ?></a>
          </div>
        </div>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="6"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>

            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>