<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <a href="<?php echo $invoice; ?>" target="_blank" data-toggle="tooltip" title="<?php echo $button_invoice_print; ?>" class="btn btn-info"><i class="fa fa-print"></i>
        </a>
        <a href="<?php echo $shipping; ?>" target="_blank" data-toggle="tooltip" title="<?php echo $button_shipping_print; ?>" class="btn btn-info"><i class="fa fa-truck"></i>
        </a>
        <a href="<?php echo $edit; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i>
        </a>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i>
        </a>
      </div>
      <h1>
        <?php echo $heading_title; ?>
      </h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li>
          <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?>
          </a>
        </li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">
          <i class="fa fa-list"></i>
          <?php echo $heading_title; ?>
        </h3>
      </div>
      <div class="panel-body">
        <ul class="nav nav-tabs">
          <li class="active">
            <a href="#tab-order" data-toggle="tab">
              <?php echo $tab_order; ?>
            </a>
          </li>
          <li>
            <a href="#tab-payment" data-toggle="tab">
              <?php echo $tab_payment; ?>
            </a>
          </li>
          <?php if ($shipping_method) { ?>
          <li>
            <a href="#tab-shipping" data-toggle="tab">
              <?php echo $tab_shipping; ?>
            </a>
          </li>
          <?php } ?>
          <li>
            <a href="#tab-product" data-toggle="tab">
              <?php echo $tab_product; ?>
            </a>
          </li>
          <li>
            <a href="#tab-history" data-toggle="tab">
              <?php echo $tab_history; ?>
            </a>
          </li>
          <?php if ($payment_action) { ?>
          <li>
            <a href="#tab-action" data-toggle="tab">
              <?php echo $tab_action; ?>
            </a>
          </li>
          <?php } ?>
          <?php if ($frauds) { ?>
          <?php foreach ($frauds as $fraud) { ?>
          <li>
            <a href="#tab-"
              <?php echo $fraud['code']; ?>" data-toggle="tab"><?php echo $fraud['title']; ?>
            </a>
          </li>
          <?php } ?>
          <?php } ?>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="tab-order">
            <table class="table table-bordered">
              <tr style="display:none">
                <td>
                  <?php echo $text_order_id; ?>
                </td>
                <td>
                  #<?php echo $order_id; ?>
                </td>
              </tr>
              <tr>
                <td>
                  <?php echo $text_order_id; ?>
                </td>
                <td>
                  <?php if ($invoice_no) { ?>
                  <?php echo $invoice_no; ?>
                  <?php } else { ?>
                  <button id="button-invoice" class="btn btn-success btn-xs">
                    <i class="fa fa-cog"></i>
                    <?php echo $button_generate; ?>
                  </button>
                  <?php } ?>
                </td>
              </tr>
              <tr>
                <td>
                  <?php echo $text_store_name; ?>
                </td>
                <td>
                  <?php echo $store_name; ?>
                </td>
              </tr>
              <tr>
                <td>
                  <?php echo $text_store_url; ?>
                </td>
                <td>
                  <a href="<?php echo $store_url; ?>" target="_blank"><?php echo $store_url; ?>
                  </a>
                </td>
              </tr>
              <?php if ($customer) { ?>
              <tr>
                <td>
                  <?php echo $text_customer; ?>
                </td>
                <td>
                  <a href="<?php echo $customer; ?>" target="_blank"><?php echo $firstname; ?> <?php echo $lastname; ?>
                  </a>
                </td>
              </tr>
              <?php } else { ?>
              <tr>
                <td>
                  <?php echo $text_customer; ?>
                </td>
                <td>
                  <?php echo $firstname; ?>
                  <?php echo $lastname; ?>
                </td>
              </tr>
              <?php } ?>
              <?php if ($customer_group) { ?>
              <tr>
                <td>
                  <?php echo $text_customer_group; ?>
                </td>
                <td>
                  <?php echo $customer_group; ?>
                </td>
              </tr>
              <?php } ?>
              <tr>
                <td>
                  <?php echo $text_email; ?>
                </td>
                <td>
                  <a href="mailto:"
                    <?php echo $email; ?>"><?php echo $email; ?>
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <?php echo $text_telephone; ?>
                </td>
                <td>
                  <?php echo $telephone; ?>
                </td>
              </tr>
              <?php if ($fax) { ?>
              <tr>
                <td>
                  <?php echo $text_fax; ?>
                </td>
                <td>
                  <?php echo $fax; ?>
                </td>
              </tr>
              <?php } ?>
              <?php foreach ($account_custom_fields as $custom_field) { ?>
              <tr>
                <td>
                  <?php echo $custom_field['name']; ?>:
                </td>
                <td>
                  <?php echo $custom_field['value']; ?>
                </td>
              </tr>
              <?php } ?>
              <tr>
                <td>
                  <?php echo $text_total; ?>
                </td>
                <td>
                  <?php echo $total; ?>
                </td>
              </tr>
              <?php if ($customer && $reward) { ?>
              <tr>
                <td>
                  <?php echo $text_reward; ?>
                </td>
                <td>
                  <?php echo $reward; ?>
                  <?php if (!$reward_total) { ?>
                  <button id="button-reward-add" class="btn btn-success btn-xs">
                    <i class="fa fa-plus-circle"></i>
                    <?php echo $button_reward_add; ?>
                  </button>
                  <?php } else { ?>
                  <button id="button-reward-remove" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i> <?php echo $button_reward_remove; ?>
                  </button>
                  <?php } ?>
                </td>
              </tr>
              <?php } ?>
              <?php if ($order_status) { ?>
              <tr>
                <td>
                  <?php echo $text_order_status; ?>
                </td>
                <td id="order-status">
                  <?php echo $order_status; ?>
                </td>
              </tr>
              <?php } ?>
              <!--<?php if ($comment) { ?>
              <tr>
                <td><?php echo $text_comment; ?></td>
                <td><?php echo $comment; ?></td>
              </tr>
              <?php } ?>-->
              <?php if ($affiliate) { ?>
              <tr>
                <td>
                  <?php echo $text_affiliate; ?>
                </td>
                <td>
                  <a href="<?php echo $affiliate; ?>"><?php echo $affiliate_firstname; ?> <?php echo $affiliate_lastname; ?>
                  </a>
                </td>
              </tr>
              <tr>
                <td>
                  <?php echo $text_commission; ?>
                </td>
                <td>
                  <?php echo $commission; ?>
                  <?php if (!$commission_total) { ?>
                  <button id="button-commission-add" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> <?php echo $button_commission_add; ?>
                  </button>
                  <?php } else { ?>
                  <button id="button-commission-remove" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i> <?php echo $button_commission_remove; ?>
                  </button>
                  <?php } ?>
                </td>
              </tr>
              <?php } ?>
              <?php if ($ip) { ?>
              <tr>
                <td>
                  <?php echo $text_ip; ?>
                </td>
                <td>
                  <?php echo $ip; ?>
                </td>
              </tr>
              <?php } ?>
              <?php if ($forwarded_ip) { ?>
              <tr>
                <td>
                  <?php echo $text_forwarded_ip; ?>
                </td>
                <td>
                  <?php echo $forwarded_ip; ?>
                </td>
              </tr>
              <?php } ?>
              <?php if ($user_agent) { ?>
              <tr>
                <td>
                  <?php echo $text_user_agent; ?>
                </td>
                <td>
                  <?php echo $user_agent; ?>
                </td>
              </tr>
              <?php } ?>
              <?php if ($accept_language) { ?>
              <tr>
                <td>
                  <?php echo $text_accept_language; ?>
                </td>
                <td>
                  <?php echo $accept_language; ?>
                </td>
              </tr>
              <?php } ?>
              <tr>
                <td>
                  <?php echo $text_date_added; ?>
                </td>
                <td>
                  <?php echo $date_added; ?>
                </td>
              </tr>
              <tr>
                <td>
                  <?php echo $text_date_modified; ?>
                </td>
                <td>
                  <?php echo $date_modified; ?>
                </td>
              </tr>
            </table>
          </div>
          <div class="tab-pane" id="tab-payment">
            <table class="table table-bordered">
              <tr>
                <td>
                  <?php echo $text_firstname; ?>
                </td>
                <td>
                  <?php echo $payment_firstname; ?>
                </td>
              </tr>
              <tr>
                <td>
                  <?php echo $text_lastname; ?>
                </td>
                <td>
                  <?php echo $payment_lastname; ?>
                </td>
              </tr>
              <?php if ($payment_company) { ?>
              <tr>
                <td>
                  <?php echo $text_company; ?>
                </td>
                <td>
                  <?php echo $payment_company; ?>
                </td>
              </tr>
              <?php } ?>
              <tr>
                <td>
                  <?php echo $text_address_1; ?>
                </td>
                <td>
                  <?php echo $payment_address_1; ?>
                </td>
              </tr>
              <?php if ($payment_address_2) { ?>
              <tr style="display:none">
                <td>
                  <?php echo $text_address_2; ?>
                </td>
                <td>
                  <?php echo $payment_address_2; ?>
                </td>
              </tr>
              <?php } ?>
              <tr style="display:none">
                <td>
                  <?php echo $text_city; ?>
                </td>
                <td>
                  <?php echo $payment_city; ?>
                </td>
              </tr>
              <?php if ($payment_postcode) { ?>
              <tr>
                <td>
                  <?php echo $text_postcode; ?>
                </td>
                <td>
                  <?php echo $payment_postcode; ?>
                </td>
              </tr>
              <?php } ?>
              <tr>
                <td>
                  <?php echo $text_zone; ?>
                </td>
                <td>
                  <?php echo $payment_zone; ?>
                </td>
              </tr>
              <?php if ($payment_zone_code) { ?>
              <tr>
                <td>
                  <?php echo $text_zone_code; ?>
                </td>
                <td>
                  <?php echo $payment_zone_code; ?>
                </td>
              </tr>
              <?php } ?>
              <tr>
                <td>
                  <?php echo $text_country; ?>
                </td>
                <td>
                  <?php echo $payment_country; ?>
                </td>
              </tr>
              <?php foreach ($payment_custom_fields as $custom_field) { ?>
              <tr data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">
                <td>
                  <?php echo $custom_field['name']; ?>:
                </td>
                <td>
                  <?php echo $custom_field['value']; ?>
                </td>
              </tr>
              <?php } ?>
              <tr>
                <td>
                  <?php echo $text_payment_method; ?>
                </td>
                <td>
                  <?php echo $payment_method; ?><?php if (strcmp($filename,'#')!=0 ) { ?> | <a href="<?php echo $filename; ?>" target="_blank">view receipt
                  </a><?php } ?>
                </td>
              </tr>
              <?php if ($member_account_name) { ?>
              <tr>
                <td>Member Bank Account Name</td>
                <td>
                  <?php echo $member_account_name; ?>
                </td>
              </tr>
              <?php } ?>
              <?php if ($member_account_number) { ?>
              <tr>
                <td>Member Bank Account Number </td>
                <td>
                  <?php echo $member_account_number; ?>
                </td>
              </tr>
              <?php } ?>
              <?php if ($sukkat_bank_name) { ?>
              <tr>
                <td>Sukkat Bank Name </td>
                <td>
                  <?php if ($sukkat_bank_name =="NCB") { ?> الأهلي <?php } ?>
                  <?php if ($sukkat_bank_name =="ARB") { ?> مصرف الراجحي <?php } ?>
                </td>
              </tr>
              <?php } ?>
            </table>
          </div>
          <?php if ($shipping_method) { ?>
          <div class="tab-pane" id="tab-shipping">
            <table class="table table-bordered">
              <tr>
                <td>
                  <?php echo $text_firstname; ?>
                </td>
                <td>
                  <?php echo $shipping_firstname; ?>
                </td>
              </tr>
              <tr>
                <td>
                  <?php echo $text_lastname; ?>
                </td>
                <td>
                  <?php echo $shipping_lastname; ?>
                </td>
              </tr>
              <?php if ($shipping_company) { ?>
              <tr>
                <td>
                  <?php echo $text_company; ?>
                </td>
                <td>
                  <?php echo $shipping_company; ?>
                </td>
              </tr>
              <?php } ?>
              <tr>
                <td>
                  <?php echo $text_address_1; ?>
                </td>
                <td>
                  <?php echo $shipping_address_1; ?>
                </td>
              </tr>
              <?php if ($shipping_address_2) { ?>
              <tr style="display:none">
                <td>
                  <?php echo $text_address_2; ?>
                </td>
                <td>
                  <?php echo $shipping_address_2; ?>
                </td>
              </tr>
              <?php } ?>
              <tr style="display:none">
                <td>
                  <?php echo $text_city; ?>
                </td>
                <td>
                  <?php echo $shipping_city; ?>
                </td>
              </tr>
              <?php if ($shipping_postcode) { ?>
              <tr>
                <td>
                  <?php echo $text_postcode; ?>
                </td>
                <td>
                  <?php echo $shipping_postcode; ?>
                </td>
              </tr>
              <?php } ?>
              <tr>
                <td>
                  <?php echo $text_zone; ?>
                </td>
                <td>
                  <?php echo $shipping_zone; ?>
                </td>
              </tr>
              <?php if ($shipping_zone_code) { ?>
              <tr>
                <td>
                  <?php echo $text_zone_code; ?>
                </td>
                <td>
                  <?php echo $shipping_zone_code; ?>
                </td>
              </tr>
              <?php } ?>
              <tr>
                <td>
                  <?php echo $text_country; ?>
                </td>
                <td>
                  <?php echo $shipping_country; ?>
                </td>
              </tr>
              <?php foreach ($shipping_custom_fields as $custom_field) { ?>
              <tr data-sort="<?php echo $custom_field['sort_order'] + 1; ?>">
                <td>
                  <?php echo $custom_field['name']; ?>:
                </td>
                <td>
                  <?php echo $custom_field['value']; ?>
                </td>
              </tr>
              <?php } ?>
              <?php if ($shipping_method) { ?>
              <tr>
                <td>
                  <?php echo $text_shipping_method; ?>
                </td>
                <td>
                  <?php echo $shipping_method; ?>
                </td>
              </tr>
              <?php } ?>
            </table>
          </div>
          <?php } ?>
          <div class="tab-pane" id="tab-product">
            <?php $i = 0; ?>
            <table class="table table-bordered">
              <thead>
                <tr>
                   <td class="text-left"><?php echo $column_product; ?></td>
                  <td class="text-right"><?php echo $column_quantity; ?></td>
                  <td class="text-right"><?php echo $column_price; ?></td>
                  <td class="text-right"><b>Intial Total</b></td>
				  <td class="text-right"><b>Discount</b></td>
				  <td class="text-right"><b><?php echo $column_total; ?></b></td>
				  <td class="text-right"><b><?php echo $column_tax; ?></b></td>
				  <td class="text-right"><b><?php echo $column_total_tax; ?></b></td>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($products as $product) { ?>
                <tr id="row-product-<?php echo $product['order_product_id'] ?>">
                  <td class="text-left">
                    <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
					<?php foreach ($product['option'] as $option) { ?>
                    <br />
                    <?php if ($option['type'] != 'file') { ?>
                    &nbsp;<small>
                      <?php echo $option['value']; ?>
                    </small>
                    <?php } else { ?>
                    &nbsp;<small>
                      <a href="<?php echo $option['href']; ?>"><?php echo $option['value']; ?>
                      </a>
                    </small>
                    <?php } ?>
                    <?php } ?>
                    <input type="hidden" name="product["<?php echo $i; ?>][product_id]" value="<?php echo $product['product_id']; ?>"/>
                    <?php if ($i != '0') { ?>
                      <span class="pull-right"><input type="hidden" name="pricevalue<?php echo $product['order_product_id'] ?>" id="pricevalue<?php echo $product['order_product_id'] ?>" value="<?php echo $product['quantity']; ?>" /><button class="btn btn-sm btn-danger" id="remove-product-<?php echo $product['order_product_id'] ?>" title="<?php echo $text_remove_product; ?>" onclick="confirmDeleteProduct('remove-product',<?php echo $product['order_product_id'] ?>, 'right');" ><i class="fa fa-minus-circle"></i></button></span>
                    <?php } ?>
                  </td>
                   <td class="text-right"><?php echo $product['quantity']; ?></td>
                  <td class="text-right"><?php echo $product['unit_price']; ?></td>
                  <td class="text-right"><?php echo $product['intial_total']; ?></td>
				  <td class="text-right"><?php echo $product['discount']; ?></td>
				  <td class="text-right"><?php echo $product['total']; ?></td>
				  <td class="text-right"><?php echo $product['tax']; ?></td>
				  <td class="text-right"><?php echo $product['total_tax']; ?></td>
                </tr>
                <?php $i++; ?>
                <?php } ?>
				<?php foreach ($total_last_datas as $total_data) { ?>
				<tr>
				  <td colspan='3'>Total</td>
				  <td class="text-right"><?php echo $total_data['total_intial']; ?></td>
				  <td class="text-right"></td>
				  <td class="text-right"><?php echo $total_data['total_total']; ?></td>
				  <td class="text-right"><?php echo $total_data['total_tax']; ?></td>
				  <td class="text-right"><?php echo $total_data['total_grand_total']; ?></td>
				</tr>
				<?php } ?>
                <?php foreach ($vouchers as $voucher) { ?>
                <tr>
                  <td class="text-left">
                    <a href="<?php echo $voucher['href']; ?>"><?php echo $voucher['description']; ?>
                    </a>
                  </td>
                  <td class="text-left"></td>
                  <td class="text-right">1</td>
                  <td class="text-right">
                    <?php echo $voucher['amount']; ?>
                  </td>
                  <td class="text-right">
                    <?php echo $voucher['amount']; ?>
                  </td>
                </tr>
                <?php } ?>
                <?php foreach ($totals as $total) { ?>
                <tr id="<?php echo $total['code']; ?>_tr">
                  <td colspan="7" class="text-right" style="direction:rtl">
                    <?php echo $total['title']; ?>
                  </td>
                  <td class="text-right" id="<?php echo $total['code']; ?>">
                    <?php echo $total['text']; ?>
                  </td>
                </tr>
                <?php } ?>
                <?php if ($comment) { ?>
                <tr style="color:red">
                  <td>Comment:</td>
                  <td colspan="7">
                    <?php echo $comment; ?>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#add-product-modal"><i class="fa fa-plus"></i> <?php echo $text_add_product; ?></button>
          </div>
          <div class="tab-pane" id="tab-history">
            <div id="history"></div>
            <br />
            <fieldset>
              <legend>
                <?php echo $text_history; ?>
              </legend>
              <form class="form-horizontal">
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-order-status">
                    <?php echo $entry_order_status; ?>
                  </label>
                  <div class="col-sm-10">
                    <select name="order_status_id" id="input-order-status" class="form-control">
                      <?php foreach ($order_statuses as $order_statuses) { ?>
                      <?php if ($order_statuses['order_status_id'] == $order_status_id) { ?>
                      <option value="<?php echo $order_statuses['order_status_id']; ?>" selected="selected"><?php echo $order_statuses['name']; ?>
                      </option>
                      <?php } else { ?>
                      <option value="<?php echo $order_statuses['order_status_id']; ?>"><?php echo $order_statuses['name']; ?>
                      </option>
                      <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group" style="display:none">
                  <label class="col-sm-2 control-label" for="input-notify">
                    <?php echo $entry_notify; ?>
                  </label>
                  <div class="col-sm-10">
                    <input type="checkbox" name="notify" value="1" id="input-notify" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-comment">
                    <?php echo $entry_comment; ?>
                  </label>
                  <div class="col-sm-10">
                    <textarea name="comment" rows="8" id="input-comment" class="form-control"></textarea>
                  </div>
                </div>
              </form>
              <div class="text-right">
                <button id="button-comment" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i> <?php echo $button_comment_add; ?>
                </button>
                <button id="button-history" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i> <?php echo $button_history_add; ?>
                </button>
              </div>
            </fieldset>
          </div>
          <?php if ($payment_action) { ?>
          <div class="tab-pane" id="tab-action">
            <?php echo $payment_action; ?>
          </div>
          <?php } ?>
          <?php if ($frauds) { ?>
          <?php foreach ($frauds as $fraud) { ?>
          <div class="tab-pane" id="tab-"
            <?php echo $fraud['code']; ?>">
            <?php echo $fraud['content']; ?>
          </div>
          <?php } ?>
          <?php } ?>
        </div>
      </div>
    </div>
    <!-- Add Products Modal -->
    <div class="modal fade" id="add-product-modal" tabindex="-1" role="dialog" aria-labelledby="AddProductModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo $button_cancel; ?>"><span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="AddProductModalLabel">
              <?php echo $text_add_product; ?>
            </h4>
          </div>
          <div class="modal-body">
            <form id="add-product-form">
              <fieldset form="add-product-form">
                <legend>
                  <?php echo $text_add_product; ?>
                </legend>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-product">
                    <?php echo $entry_product; ?>
                  </label>
                  <div class="col-sm-10">
                    <input type="text" name="product" value="" id="input-product" class="form-control" />
                    <input type="hidden" name="product_id"  value="" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-quantity">
                    <?php echo $entry_quantity; ?>
                  </label>
                  <div class="col-sm-10">
                    <input type="text" name="quantity" value="1" id="input-quantity" class="form-control" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-price">
                    <?php echo $entry_price; ?>
                  </label>
                  <div class="col-sm-10">
                    <input type="text" name="product_price" id="input-price" class="form-control" />
                  </div>
                </div>
              </fieldset>
              <div id="option"></div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">
              <?php echo $button_cancel; ?>
            </button>
            <button type="button" class="btn btn-primary" id="add-discount-btn" onclick="addProduct()">
              <?php echo $text_add_product; ?>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    <!--
    var product_row = 0;	
$(document).delegate('#button-invoice', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/createinvoiceno&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		dataType: 'json',
		beforeSend: function() {
			$('#button-invoice').button('loading');			
		},
		complete: function() {
			$('#button-invoice').button('reset');
		},
		success: function(json) {
			$('.alert').remove();
						
			if (json['error']) {
				$('#tab-order').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}
			
			if (json['invoice_no']) {
				$('#button-invoice').replaceWith(json['invoice_no']);
			}
		},			
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-reward-add', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/addreward&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-reward-add').button('loading');
		},
		complete: function() {
			$('#button-reward-add').button('reset');
		},									
		success: function(json) {
			$('.alert').remove();
						
			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}
			
			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
				
				$('#button-reward-add').replaceWith('<button id="button-reward-remove" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i> <?php echo $button_reward_remove; ?></button>');
			}
		},			
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-reward-remove', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/removereward&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-reward-remove').button('loading');
		},
		complete: function() {
			$('#button-reward-remove').button('reset');
		},				
		success: function(json) {
			$('.alert').remove();
						
			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}
			
			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
				
				$('#button-reward-remove').replaceWith('<button id="button-reward-add" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> <?php echo $button_reward_add; ?></button>');
			}
		},			
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-commission-add', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/addcommission&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-commission-add').button('loading');
		},
		complete: function() {
			$('#button-commission-add').button('reset');
		},			
		success: function(json) {
			$('.alert').remove();
						
			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}
			
			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
                
				$('#button-commission-add').replaceWith('<button id="button-commission-remove" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i> <?php echo $button_commission_remove; ?></button>');
			}
		},			
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-commission-remove', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/removecommission&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-commission-remove').button('loading');
		
		},
		complete: function() {
			$('#button-commission-remove').button('reset');
		},		
		success: function(json) {
			$('.alert').remove();
						
			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}
			
			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
				
				$('#button-commission-remove').replaceWith('<button id="button-commission-add" class="btn btn-success btn-xs"><i class="fa fa-minus-circle"></i> <?php echo $button_commission_add; ?></button>');
			}
		},			
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('#history').delegate('.pagination a', 'click', function(e) {
	e.preventDefault();
	
	$('#history').load(this.href);
});			

$('#history').load('index.php?route=sale/order/history&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>');

$('#button-history').on('click', function() {
  if(typeof verifyStatusChange == 'function'){
    if(verifyStatusChange() == false){
      return false;
    }else{
      addOrderInfo();
    }
  }else{
    addOrderInfo();
  }

	$.ajax({
		url: 'index.php?route=sale/order/api&token=<?php echo $token; ?>&api=api/order/history&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'order_status_id=' + encodeURIComponent($('select[name=\'order_status_id\']').val()) + '&notify=' + ($('input[name=\'notify\']').prop('checked') ? 1 : 0) + '&append=' + ($('input[name=\'append\']').prop('checked') ? 1 : 0) + '&comment=' + encodeURIComponent($('textarea[name=\'comment\']').val()),
		beforeSend: function() {
			$('#button-history').button('loading');			
		},
		complete: function() {
			$('#button-history').button('reset');	
		},
		success: function(json) {
			$('.alert').remove();
			
			if (json['error']) {
				$('#history').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			} 
		
			if (json['success']) {
				$('#history').load('index.php?route=sale/order/history&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>');
				
				$('#history').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				
				$('textarea[name=\'comment\']').val('');
				
				$('#order-status').html($('select[name=\'order_status_id\'] option:selected').text());			
			}			
		},			
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

function changeStatus(){
  var status_id = $('select[name="order_status_id"]').val();

  $('#openbay-info').remove();

  $.ajax({
    url: 'index.php?route=extension/openbay/getorderinfo&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>&status_id='+status_id,
    dataType: 'html',
    success: function(html) {
      $('#history').after(html);
    }
  });
}

function addOrderInfo(){
  var status_id = $('select[name="order_status_id"]').val();

  $.ajax({
    url: 'index.php?route=extension/openbay/addorderinfo&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>&status_id='+status_id,
    type: 'post',
    dataType: 'html',
    data: $(".openbay-data").serialize()
  });
}

$(document).ready(function() {
  changeStatus();
});

$('select[name="order_status_id"]').change(function(){ changeStatus(); });
//-->
  </script>

  <script type="text/javascript">
    <!--
// Sort the custom fields
$('#tab-payment tr[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#tab-payment tr').length) {
		$('#tab-payment tr').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('#tab-payment tr').length) {
		$('#tab-payment tr:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#tab-payment tr').length) {
		$('#tab-payment tr:first').before(this);
	}
});

$('#tab-shipping tr[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#tab-shipping tr').length) {
		$('#tab-shipping tr').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('#tab-shipping tr').length) {
		$('#tab-shipping tr:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#tab-shipping tr').length) {
		$('#tab-shipping tr:first').before(this);
	}
});
$('#button-comment').on('click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/api&token=<?php echo $token; ?>&api=api/order/comment&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'order_status_id=18&notify=' + ($('input[name=\'notify\']').prop('checked') ? 1 : 0) + '&append=' + ($('input[name=\'append\']').prop('checked') ? 1 : 0) + '&comment=' + encodeURIComponent($('textarea[name=\'comment\']').val()),
		beforeSend: function() {
			$('#button-comment').button('loading');			
		},
		complete: function() {
			$('#button-comment').button('reset');	
		},
		success: function(json) {
			$('.alert').remove();
			
			if (json['error']) {
				$('#history').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			} 
		
			if (json['success']) {
				$('#history').load('index.php?route=sale/order/history&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>');
				
				$('#history').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				
				$('textarea[name=\'comment\']').val('');
				
				$('#order-status').html($('select[name=\'order_status_id\'] option:selected').text());			
			}			
		},			
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

  $('#add-product-form input[name=\'product\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',			
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id'],
						model: item['model'],
						option: item['option'],
						price: item['price']						
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('#add-product-form input[name=\'product\']').val(item['label']);
		$('#add-product-form input[name=\'product_id\']').val(item['value']);
		$('#add-product-form input[name=\'product_price\']').val(item['price']);

		if (item['option'] != '') {
			html  = '<fieldset form="add-product-form">';
			html += '  <legend><?php echo $entry_option; ?></legend>';
	  
			for (i = 0; i < item['option'].length; i++) {
				option = item['option'][i];
		
				if (option['type'] == 'select') {
					html += '<div class="form-group' + (option['required'] ? ' required' : '') + '">';
					html += '  <label class="col-sm-2 control-label" for="input-option' + option['product_option_id'] + '">' + option['name'] + '</label>';
					html += '  <div class="col-sm-10">';
					html += '    <select name="option[' + option['product_option_id'] + ']" id="input-option' + option['product_option_id'] + '" class="form-control">';
					html += '      <option value=""><?php echo $text_select; ?></option>';
		
					for (j = 0; j < option['product_option_value'].length; j++) {
						option_value = option['product_option_value'][j];
				
						html += '<option value="' + option_value['product_option_value_id'] + '">' + option_value['name'];
				
						if (option_value['price']) {
							html += ' (' + option_value['price_prefix'] + option_value['price'] + ')';
						}
				
						html += '</option>';
					}
				
					html += '    </select>';
					html += '  </div>';
					html += '</div>';
				}
		
				if (option['type'] == 'radio') {
					html += '<div class="form-group' + (option['required'] ? ' required' : '') + '">';
					html += '  <label class="col-sm-2 control-label" for="input-option' + option['product_option_id'] + '">' + option['name'] + '</label>';
					html += '  <div class="col-sm-10">';
					html += '    <select name="option[' + option['product_option_id'] + ']" id="input-option' + option['product_option_id'] + '" class="form-control">';
					html += '      <option value=""><?php echo $text_select; ?></option>';
		
					for (j = 0; j < option['product_option_value'].length; j++) {
						option_value = option['product_option_value'][j];
				
						html += '<option value="' + option_value['product_option_value_id'] + '">' + option_value['name'];
				
						if (option_value['price']) {
							html += ' (' + option_value['price_prefix'] + option_value['price'] + ')';
						}
				
						html += '</option>';
					}
				
					html += '    </select>';
					html += '  </div>';
					html += '</div>';
				}
			
				if (option['type'] == 'checkbox') {
					html += '<div class="form-group' + (option['required'] ? ' required' : '') + '">';
					html += '  <label class="col-sm-2 control-label">' + option['name'] + '</label>';
					html += '  <div class="col-sm-10">';
					html += '    <div id="input-option' + option['product_option_id'] + '">';
			
					for (j = 0; j < option['product_option_value'].length; j++) {
						option_value = option['product_option_value'][j];
				
						html += '<div class="checkbox">';
				
						html += '  <label><input type="checkbox" name="option[' + option['product_option_id'] + '][]" value="' + option_value['product_option_value_id'] + '" /> ' + option_value['name'];
				
						if (option_value['price']) {
							html += ' (' + option_value['price_prefix'] + option_value['price'] + ')';
						}
				
						html += '  </label>';
						html += '</div>';
					}
								
					html += '    </div>';
					html += '  </div>';
					html += '</div>';
				}
	
				if (option['type'] == 'image') {
					html += '<div class="form-group' + (option['required'] ? ' required' : '') + '">';
					html += '  <label class="col-sm-2 control-label" for="input-option' + option['product_option_id'] + '">' + option['name'] + '</label>';
					html += '  <div class="col-sm-10">';
					html += '    <select name="option[' + option['product_option_id'] + ']" id="input-option' + option['product_option_id'] + '" class="form-control">';
					html += '      <option value=""><?php echo $text_select; ?></option>';
		
					for (j = 0; j < option['product_option_value'].length; j++) {
						option_value = option['product_option_value'][j];
				
						html += '<option value="' + option_value['product_option_value_id'] + '">' + option_value['name'];
				
						if (option_value['price']) {
							html += ' (' + option_value['price_prefix'] + option_value['price'] + ')';
						}
				
						html += '</option>';
					}
				
					html += '    </select>';					
					html += '  </div>';
					html += '</div>';
				}
				
				if (option['type'] == 'text') {
					html += '<div class="form-group' + (option['required'] ? ' required' : '') + '">';
					html += '  <label class="col-sm-2 control-label" for="input-option' + option['product_option_id'] + '">' + option['name'] + '</label>';
					html += '  <div class="col-sm-10"><input type="text" name="option[' + option['product_option_id'] + ']" value="' + option['value'] + '" id="input-option' + option['product_option_id'] + '" class="form-control" /></div>';
					html += '</div>';					
				}
		
				if (option['type'] == 'textarea') {
					html += '<div class="form-group' + (option['required'] ? ' required' : '') + '">';
					html += '  <label class="col-sm-2 control-label" for="input-option' + option['product_option_id'] + '">' + option['name'] + '</label>';
					html += '  <div class="col-sm-10"><textarea name="option[' + option['product_option_id'] + ']" rows="5" id="input-option' + option['product_option_id'] + '" class="form-control">' + option['value'] + '</textarea></div>';
					html += '</div>';
				}
		
				if (option['type'] == 'file') {
					html += '<div class="form-group' + (option['required'] ? ' required' : '') + '">';
					html += '  <label class="col-sm-2 control-label">' + option['name'] + '</label>';
					html += '  <div class="col-sm-10">';
					html += '    <button type="button" id="button-upload' + option['product_option_id'] + '" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>';
					html += '    <input type="hidden" name="option[' + option['product_option_id'] + ']" value="' + option['value'] + '" id="input-option' + option['product_option_id'] + '" />';
					html += '  </div>';
					html += '</div>';
				}
		
				if (option['type'] == 'date') {
					html += '<div class="form-group' + (option['required'] ? ' required' : '') + '">';
					html += '  <label class="col-sm-2 control-label" for="input-option' + option['product_option_id'] + '">' + option['name'] + '</label>';
					html += '  <div class="col-sm-3"><div class="input-group date"><input type="text" name="option[' + option['product_option_id'] + ']" value="' + option['value'] + '" placeholder="' + option['name'] + '" data-date-format="YYYY-MM-DD" id="input-option' + option['product_option_id'] + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
					html += '</div>';
				}
		
				if (option['type'] == 'datetime') {
					html += '<div class="form-group' + (option['required'] ? ' required' : '') + '">';
					html += '  <label class="col-sm-2 control-label" for="input-option' + option['product_option_id'] + '">' + option['name'] + '</label>';
					html += '  <div class="col-sm-3"><div class="input-group datetime"><input type="text" name="option[' + option['product_option_id'] + ']" value="' + option['value'] + '" placeholder="' + option['name'] + '" data-date-format="YYYY-MM-DD HH:mm" id="input-option' + option['product_option_id'] + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
					html += '</div>';					
				}
		
				if (option['type'] == 'time') {
					html += '<div class="form-group' + (option['required'] ? ' required' : '') + '">';
					html += '  <label class="col-sm-2 control-label" for="input-option' + option['product_option_id'] + '">' + option['name'] + '</label>';
					html += '  <div class="col-sm-3"><div class="input-group time"><input type="text" name="option[' + option['product_option_id'] + ']" value="' + option['value'] + '" placeholder="' + option['name'] + '" data-date-format="HH:mm" id="input-option' + option['product_option_id'] + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
					html += '</div>';					
				}
			}
	
			html += '</fieldset>';
	
			$('#option').html(html);
	
			$('.date').datetimepicker({
				pickTime: false
			});
	
			$('.datetime').datetimepicker({
				pickDate: true,
				pickTime: true
			});
	
			$('.time').datetimepicker({
				pickDate: false
			});
		} else {
			$('#option').html('');
		}		
	}	
});

function addProduct() {
	param = 'add-product';
	param_id = <?php echo $order_id; ?>;
	
	$.ajax({
	  type: 'post',
	  url: 'index.php?route=sale/ajaxedit/addProduct&token=<?php echo $token; ?>',
	  dataType: 'json',
	  data: { 
		product_data : $('#add-product-form').serialize(),
		order_id : param_id
	  },
	  beforeSend: function() {
			$('#add-product-modal').modal('hide')
	  },
	  success: function(json) {
		 addProductLine(json['product']);
		$('#sub_total').text(json['sub_total']);
		$('#total').text(json['total']);
		 //replaceTotals(param, param_id, json['order_total']);
		 alertJson('alert alert-success', json);			 
	  },
	  error: function(json) { 
		 alertJson('alert alert-danger', json);
	  }
	});
}

function addProductLine(json) {
	
	html  = '<tr id="row-product-' + json['order_product_id'] + '">';
	//html += '<td></td>';
	html += '  <td class="text-left"><span id="product_name' + json['order_product_id'] + '" onclick="inputValue(\'product_name\', ' + json['order_product_id'] + ' , \'left\');"> ' + json['name'] + ' </span>';
	html += ' <span class="pull-right"><input type="hidden" name="pricevalue' + json['order_product_id'] + '" id="pricevalue' + json['order_product_id'] + '" value="' + json['quantity'] + '" /> <button class="btn btn-sm btn-danger" id="remove-product-' + json['order_product_id'] + '" title="<?php echo $text_remove_product; ?>" onclick="confirmDeleteProduct(\'remove-product\', ' + json['order_product_id'] + ' , \'right\');" ><i class="fa fa-minus"></i></button></span>';
	
	html += '</td>';
	html += '<td class="text-left" id="model' + json['order_product_id'] + '" onclick="inputValue(\'model\', ' + json['order_product_id'] + ' , \'left\');">' + json['model'] + '</td>'
	html += '<td class="text-right" id="qty' + json['order_product_id'] + '" onclick="inputValue(\'qty\', ' + json['order_product_id'] + ' , \'right\');">' + json['quantity'] + '</td>';
  html += '  <td class="text-right">'
  for (index = 0, len = json['option'].length; index < len; ++index) {
		//html +=	'<br />';
		if (json['option'][index]['type'] != 'file') {
			html += '&nbsp;<small>' + json['option'][index]['value'] + '</small>';
		} else {
			html += '&nbsp;<small><a href="' + json['option'][index]['href'] + '">' + json['option'][index]['value'] + '</a></small>';
		}
	}
  html += '</td>';
	html += '<td class="text-right" id="price' + json['order_product_id'] + '" onclick="inputValue(\'price\', ' + json['order_product_id'] + ' , \'right\');">' + json['price'] + '</td>'; 
	html += '<td class="text-right" id="product_total' + json['order_product_id'] + '" onclick="inputValue(\'product_total\', ' + json['order_product_id'] + ' , \'right\');">' + json['product_total'] + '</td>';     
	html += '</tr>'; 
	
	// Set hidden fields for cart
	
	product_row++;
					
	html += '<tr class="hidden" id="hidden-cart-' + json['order_product_id'] + '">';
	html += '  	<td><input type="hidden" id="row-count-' +  json['order_product_id'] + '" value="' + product_row + '"><input type="hidden" name="product[' + product_row + '][product_id]" value="' + json['product_id'] + '" /></td>';
	html += '	<td>';
	
	if (json['option']) {
		for (j = 0; j < json['option'].length; j++) {
			option = json['option'][j];
	
	
			if (option['type'] == 'select' || option['type'] == 'radio' || option['type'] == 'image') {
				html += '<input type="hidden" name="product[' + product_row + '][option][' + option['product_option_id'] + ']" value="' + option['product_option_value_id'] + '" />';
			}
	
			if (option['type'] == 'checkbox') {
				html += '<input type="hidden" name="product[' + product_row + '][option][' + option['product_option_id'] + '][]" value="' + option['product_option_value_id'] + '" />';
			}
	
			if (option['type'] == 'text' || option['type'] == 'textarea' || option['type'] == 'file' || option['type'] == 'date' || option['type'] == 'datetime' || option['type'] == 'time') {
				html += '<input type="hidden" name="product[' + product_row + '][option][' + option['product_option_id'] + ']" value="' + option['value'] + '" />';
			}
		}
	}
	html += '	</td>';
	html += '  	<td><input type="hidden" name="product[' + product_row + '][quantity]" value="' + json['quantity'] + '" /></td>';
		
	$('#sub_total_tr').before(html);
	//cartAPI();
}

function removeProductLine(order_product_id) {
	row_count = $('#row-count-' + order_product_id).val();
	$('#hidden-cart-' + order_product_id).remove();
	$('#row-product-' + order_product_id).remove();
	
	for (c = row_count++; c <= product_row + 1; c++) {
		// Product input
		row = c - 1 ;
		str = 'product[' + row + ']';
		
		$('input[name=\'product[' + c + '][product_id]\']').attr('name', str + '[product_id]');
		// Quantity input
		$('input[name=\'product[' + c + '][quantity]\']').attr('name', str + '[quantity]');
		
		// Option input
		
		option_name = $('input[name^=\'product[' + c + '][option]\']').attr('name');
		
		if (option_name) {
			n = option_name.indexOf(c);
			first = option_name.slice(0,n);
			last = option_name.slice((n+1),option_name.length);
			new_option_name = first.concat(row);
			new_option_name = new_option_name.concat(last);
			$('input[name^=\'product[' + c + '][option]\']').attr('name', new_option_name);
		}
	}
	//cartAPI();
}

function closeInput(param, param_id, align) {
	
	var new_value = $('#' + param + param_id +' input').val();
	
	if ( param !== 'product_name')	{	
	 html = '<td class="text-' + align + '" id="' + param + (param !== 'shipping' ? param_id : '') + '" onclick="inputValue(\'' + param + '\',\'' + param_id + '\', \'' + align + '\');">' +  new_value + '</td>';
	} else {
		html = '<span id="' + param +  param_id + '" onclick="inputValue(\'' + param + '\',\'' + param_id + '\', \'' + align + '\');">' +  new_value + '</td>';
	} 
	
	$('#' + param + param_id).fadeOut('slow');				
	
	if (param !== 'shipping-method-ot') {
		$('#' + param + param_id).replaceWith(html);
	}

	if (param == 'shipping-method-ot') {
		html = '<td colspan="5" class="text-' + align + '" id="' + param + (param !== 'shipping' ? param_id : '') + '" onclick="inputValue(\'' + param + '\',\'' + param_id + '\', \'' + align + '\');">' +  new_value + '</td>';
		$('#' + param + param_id).replaceWith(html);
		html = '<td class="text-left" id="shipping_method' + param_id + '" onclick="inputValue(\'shipping_method\',\'' + param_id + '\', \'left\');">' +  new_value + '</td>';
		$('#shipping_method' + param_id).replaceWith(html);
	}
	if (param == 'shipping_method') {
		html = '<td colspan="5" class="text-right" id="shipping-method-ot' + param_id + '" onclick="inputValue(\'shipping-method-ot\',\'' + param_id + '\', \'right\');">' + new_value + ':</td>';
		$('#shipping-method-ot' + param_id).replaceWith(html);
	}	
}

function alertJson(action, json) {
	$('.alert').remove();
	
	if (json['error']) {
		$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
	}
	
	if (json['msg']) {
		$('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['msg'] + '</div>');
	}
}

function replaceTotals(param, param_id, json) {
	if ( param == 'qty') {
		$('input[name=\'product[' + $('#row-count-' + param_id).val() + '][quantity]\']').val($('#qty' + param_id).html());
		//cartAPI();
	}
	if ( param == 'price') {
		$('#price' + param_id).text(json['price']);
		$('#product_total' + param_id).text(json['product_total']);
		//cartAPI();
	}
	if ( param == 'product_total') {
		$('#price' + param_id).text(json['price']);
		$('#product_total' + param_id).text(json['product_total']);
		//cartAPI();
	}
	if ( param == 'shipping') {
		$('#shipping').text(json['shipping']);
		//cartAPI();
	}
	if ( param == 'remove-product') {
		removeProductLine(param_id);
	}
}

function confirmDeleteProduct(param, param_id, align) {
	
	var productName = $('#product_name' + param_id).text();
	
	html = '<div class="modal fade" id="confirm-delete-' + param_id + '" tabindex="-1" role="dialog" aria-labelledby="confirmDeleteLabel">';
	html += '<div class="modal-dialog" role="document">';
	html += '<div class="modal-content">';
	html += '<div class="modal-header bg-danger">';
	html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
	html += '<h4 class="modal-title" id="confirmDeleteLabel">Confirm Delete</h4>';
	html += '</div>';
	html += '<div class="modal-body">';
	html += 'Are you sure you want to delete the product <b>' + productName +  '</b>? <span class="text-danger"><b>This action can not be undone!</b></span>';
	html += '</div>';
	html += '<div class="modal-footer">';
	html += '<button type="button" class="btn btn-default pull-left" data-dismiss="modal">No thanks!</button>';
	html += '<button type="button" class="btn btn-danger" onclick="saveValue(\'' + param + '\', ' + param_id + ', \'' + align + '\');">Permanently Delete Product</button>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '</div>';

	$('body').append(html);

	$('#confirm-delete-' + param_id).modal();
}

function saveValue(param, param_id, align) {
			
	var new_value = $('#pricevalue' + param_id).val();
	
	$.ajax({
	  type: 'post',	
	  url: 'index.php?route=sale/ajaxedit/saveNewValue&token=<?php echo $token; ?>',
	  dataType: 'json',
	  data: { param: param, new_value: new_value , param_id: param_id},
	  beforeSend: function() {
		if (param == 'remove-product') {
			$('#confirm-delete-' + param_id).modal('hide')
			$('#confirm-delete-' + param_id).on('hidden.bs.modal', function () {
				$('#confirm-delete-' + param_id).remove();
			})
		}	
	  },
	  success: function(json) {
		 closeInput(param, param_id , align);
		 replaceTotals(param, param_id, json);
     $('#sub_total').text(json['sub_total']);
     $('#total').text(json['total']);
		 alertJson('alert alert-success', json);			 
	  },
	  error: function(json) { 
		 alertJson('alert alert-danger', json);
		 closeInput(param, param_id, align);
	  },
	  complete: function(json) {
		
	  }
	});
}

function inputValue(param, param_id, align) {
	
	if ( param !== 'product_name')	{
		html  = '<td class="' + param + '_edit" id="' + param + param_id + '">';
		html += '<div class="input-group">';
		html += '<span class="input-group-btn">';
		html += '<a onclick="saveValue(\'' + param + '\' , ' + param_id + ' , \'' + align + '\')" role="button" class="btn  btn-success" data-toggle="tooltip" title="<?php echo $button_save; ?>" data-container="body"><i class="fa fa-check"></i></a></span>';
		html += '<input type="text" value="' + $('#' + param + (param !== 'shipping' ? param_id : '')).html() + '" class="form-control">';
		html += '<span class="input-group-btn">';
		html += '<a onclick="closeInput(\'' + param + '\', \'' + param_id + '\' , \'' + align + '\')" role="button" class="btn btn-danger" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" data-container="body"><i class="fa fa-times"></i></a></span>';
		html += '</div></td>';
	} else {
		html  = '<span class="' + param + '_edit" id="' + param + param_id + '">';
		html += '<div class="input-group">';
		html += '<span class="input-group-btn">';
		html += '<a onclick="saveValue(\'' + param + '\' , ' + param_id + ' , \'' + align + '\')" role="button" class="btn  btn-success" data-toggle="tooltip" title="<?php echo $button_save; ?>" data-container="body"><i class="fa fa-check"></i></a></span>';
		html += '<input type="text" value="' + $('#' + param + (param !== 'shipping' ? param_id : '')).html() + '" class="form-control">';
		html += '<span class="input-group-btn">';
		html += '<a onclick="closeInput(\'' + param + '\', \'' + param_id + '\' , \'' + align + '\')" role="button" class="btn btn-danger" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" data-container="body"><i class="fa fa-times"></i></a></span>';
		html += '</div></span>';
	}	
	$('#' + param + (param !== 'shipping' ? param_id : '') ).replaceWith(html);
}

//-->
  </script>
</div>
<?php echo $footer; ?>
