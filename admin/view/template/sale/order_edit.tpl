<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
      	<a href="<?php echo $invoice; ?>" target="_blank" data-toggle="tooltip" title="<?php echo $button_invoice_print; ?>" class="btn btn-info"><i class="fa fa-print"></i></a>
		<?php if ($permission) { ?>
			<a class="btn btn-warning" id="email_resend" data-toggle="tooltip" title="<?php echo $button_resend_confirmation; ?>" onclick="resendConfirmEmail();"><i class="fa fa-envelope-o"></i></a> 
		<?php } ?>
		<a href="<?php echo $shipping; ?>" target="_blank" data-toggle="tooltip" title="<?php echo $button_shipping_print; ?>" class="btn btn-info"><i class="fa fa-truck"></i></a> <a href="<?php echo $edit; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a> <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
      </div>
      <div class="panel-body">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab-order" data-toggle="tab"><?php echo $tab_order; ?></a></li>
          <li><a href="#tab-payment" data-toggle="tab"><?php echo $tab_payment; ?></a></li>
          <?php if ($shipping_method) { ?>
          <li><a href="#tab-shipping" data-toggle="tab"><?php echo $tab_shipping; ?></a></li>
          <?php } ?>
          <li><a href="#tab-product" data-toggle="tab" id="order-products"><?php echo $tab_product; ?></a></li>
          <li><a href="#tab-history" data-toggle="tab"><?php echo $tab_history; ?></a></li>
          <?php if ($payment_action) { ?>
          <li><a href="#tab-action" data-toggle="tab"><?php echo $tab_action; ?></a></li>
          <?php } ?>
          <?php if ($frauds) { ?>
          <?php foreach ($frauds as $fraud) { ?>
          <li><a href="#tab-<?php echo $fraud['code']; ?>" data-toggle="tab"><?php echo $fraud['title']; ?></a></li>
          <?php } ?>
          <?php } ?>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="tab-order">
            <table class="table table-bordered">
              <tr>
                <td><?php echo $text_order_id; ?></td>
                <td>#<?php echo $order_id; ?></td>
              </tr>
              <tr>
                <td><?php echo $text_invoice_no; ?></td>
                <td><?php if ($invoice_no) { ?>
					<div class="invoice_prefix" style="display:inline;">
						<div class="invoice_prefix_edit" id="invoice_prefix<?php echo $order_id; ?>" onclick="inputValue('invoice_prefix', <?php echo $order_id; ?>, 'right');" style="display:inline;"><?php echo $invoice_prefix; ?></div>
					</div>
					<div class="invoice-number" style="display:inline;">
						<div class="invoice_no_edit" id="invoice_no<?php echo $order_id; ?>" onclick="inputValue('invoice_no', <?php echo $order_id; ?>, 'right');" style="display:inline;"><?php echo $invoice_no; ?></div>
					</div>
                  <?php } else { ?>
                  <button id="button-invoice" class="btn btn-success btn-xs"><i class="fa fa-cog"></i> <?php echo $button_generate; ?></button>
                  <?php } ?></td>
              </tr>
              <tr>
                <td><?php echo $text_store_name; ?></td>
                <td><?php echo $store_name; ?></td>
              </tr>
              <tr>
                <td><?php echo $text_store_url; ?></td>
                <td><a href="<?php echo $store_url; ?>" target="_blank"><?php echo $store_url; ?></a></td>
              </tr>
              <?php if ($customer) { ?>
              <tr>
                <td><?php echo $text_customer; ?></td>
                <td><a href="<?php echo $customer; ?>" target="_blank"><?php echo $firstname; ?> <?php echo $lastname; ?></a></td>
              </tr>
              <?php } else { ?>
              <tr>
                <td><?php echo $text_customer; ?></td>                
				<td><span id="firstname<?php echo $order_id; ?>" onclick="inputValue('firstname', <?php echo $order_id; ?>, 'left')"><?php echo $firstname; ?> </span><span> </span><span id="lastname<?php echo $order_id; ?>" onclick="inputValue('lastname', <?php echo $order_id; ?>, 'left')"><?php echo $lastname; ?></span></td>
              </tr>
              <?php } ?>
              <?php if ($customer_group) { ?>
              <tr>
                <td><?php echo $text_customer_group; ?></td>
                <td><?php echo $customer_group; ?></td>
              </tr>
              <?php } ?>
              <tr>
                <td><?php echo $text_email; ?></td>
                <td><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></td>
              </tr>
              <tr>
                <td><?php echo $text_telephone; ?></td>
				<td id="telephone<?php echo $order_id; ?>" onclick="inputValue('telephone', <?php echo $order_id; ?>, 'left')"><?php echo $telephone; ?></td>
              </tr>
              <?php if ($fax) { ?>
              <tr>
                <td><?php echo $text_fax; ?></td>
				<td id="fax<?php echo $order_id; ?>" onclick="inputValue('fax', <?php echo $order_id; ?>, 'left')"><?php echo $fax; ?></td>
              </tr>
              <?php } ?>
              <?php foreach ($account_custom_fields as $custom_field) { ?>
              <tr>
                <td><?php echo $custom_field['name']; ?>:</td>
                <td><?php echo $custom_field['value']; ?></td>
              </tr>
              <?php } ?>
              <tr>
                <td><?php echo $text_total; ?></td>
				<td id="order-total"><?php echo $total; ?></td>
              </tr>
              <?php if ($customer && $reward) { ?>
              <tr>
                <td><?php echo $text_reward; ?></td>
                <td><?php echo $reward; ?>
                  <?php if (!$reward_total) { ?>
                  <button id="button-reward-add" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> <?php echo $button_reward_add; ?></button>
                  <?php } else { ?>
                  <button id="button-reward-remove" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i> <?php echo $button_reward_remove; ?></button>
                  <?php } ?></td>
              </tr>
              <?php } ?>
              <?php if ($order_status) { ?>
              <tr>
                <td><?php echo $text_order_status; ?></td>
                <td id="order-status"><?php echo $order_status; ?></td>
              </tr>
              <?php } ?>
              <?php if ($comment) { ?>
              <tr>
                <td><?php echo $text_comment; ?></td>
                <td><?php echo $comment; ?></td>
              </tr>
              <?php } ?>
              <?php if ($affiliate) { ?>
              <tr>
                <td><?php echo $text_affiliate; ?></td>
                <td><a href="<?php echo $affiliate; ?>"><?php echo $affiliate_firstname; ?> <?php echo $affiliate_lastname; ?></a></td>
              </tr>
              <tr>
                <td><?php echo $text_commission; ?></td>
                <td><?php echo $commission; ?>
                  <?php if (!$commission_total) { ?>
                  <button id="button-commission-add" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> <?php echo $button_commission_add; ?></button>
                  <?php } else { ?>
                  <button id="button-commission-remove" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i> <?php echo $button_commission_remove; ?></button>
                  <?php } ?></td>
              </tr>
              <?php } ?>
              <?php if ($ip) { ?>
              <tr>
                <td><?php echo $text_ip; ?></td>
                <td><?php echo $ip; ?></td>
              </tr>
              <?php } ?>
              <?php if ($forwarded_ip) { ?>
              <tr>
                <td><?php echo $text_forwarded_ip; ?></td>
                <td><?php echo $forwarded_ip; ?></td>
              </tr>
              <?php } ?>
              <?php if ($user_agent) { ?>
              <tr>
                <td><?php echo $text_user_agent; ?></td>
                <td><?php echo $user_agent; ?></td>
              </tr>
              <?php } ?>
              <?php if ($accept_language) { ?>
              <tr>
                <td><?php echo $text_accept_language; ?></td>
                <td><?php echo $accept_language; ?></td>
              </tr>
              <?php } ?>
              <tr>
                <td><?php echo $text_date_added; ?></td>
                <td><?php echo $date_added; ?></td>
              </tr>
              <tr>
                <td><?php echo $text_date_modified; ?></td>
                <td><?php echo $date_modified; ?></td>
              </tr>
            </table>		
          </div>
          <div class="tab-pane" id="tab-payment">
            <table class="table table-bordered">
              <tr>
                <td><?php echo $text_firstname; ?></td>
				<td id="payment_firstname<?php echo $order_id; ?>" onclick="inputValue('payment_firstname', <?php echo $order_id; ?>, 'left')"><?php echo $payment_firstname; ?></td>
              </tr>
              <tr>
                <td><?php echo $text_lastname; ?></td>
				<td id="payment_lastname<?php echo $order_id; ?>" onclick="inputValue('payment_lastname', <?php echo $order_id; ?>, 'left')"><?php echo $payment_lastname; ?></td>
              </tr>
              <?php if ($payment_company) { ?>
              <tr>
                <td><?php echo $text_company; ?></td>
				<td id="payment_company<?php echo $order_id; ?>" onclick="inputValue('payment_company', <?php echo $order_id; ?>, 'left')"><?php echo $payment_company; ?></td>
              </tr>
              <?php } ?>
              <tr>
                <td><?php echo $text_address_1; ?></td>
				<td id="payment_address_1<?php echo $order_id; ?>" onclick="inputValue('payment_address_1', <?php echo $order_id; ?>, 'left')"><?php echo $payment_address_1; ?></td>
              </tr>
              <?php if ($payment_address_2) { ?>
              <tr>
                <td><?php echo $text_address_2; ?></td>
				<td id="payment_address_2<?php echo $order_id; ?>" onclick="inputValue('payment_address_2', <?php echo $order_id; ?>, 'left')"><?php echo $payment_address_2; ?></td>
              </tr>
              <?php } ?>
              <tr>
                <td><?php echo $text_city; ?></td>
				<td id="payment_city<?php echo $order_id; ?>" onclick="inputValue('payment_city', <?php echo $order_id; ?>, 'left')"><?php echo $payment_city; ?></td>
              </tr>
              <?php if ($payment_postcode) { ?>
              <tr>
                <td><?php echo $text_postcode; ?></td>
				<td id="payment_postcode<?php echo $order_id; ?>" onclick="inputValue('payment_postcode', <?php echo $order_id; ?>, 'left')"><?php echo $payment_postcode; ?></td>
              </tr>
              <?php } ?>
              <tr>
                <td><?php echo $text_zone; ?></td>
				<td id="payment_zone<?php echo $order_id; ?>" onclick="inputValue('payment_zone', <?php echo $order_id; ?>, 'left')"><?php echo $payment_zone; ?></td>
              </tr>
              <?php if ($payment_zone_code) { ?>
              <tr>
                <td><?php echo $text_zone_code; ?></td>
				<td id="payment_zone_code<?php echo $order_id; ?>" onclick="inputValue('payment_zone_code', <?php echo $order_id; ?>, 'left')"><?php echo $payment_zone_code; ?></td>
              </tr>
              <?php } ?>
              <tr>
                <td><?php echo $text_country; ?></td>
				<td id="payment_country<?php echo $order_id; ?>" onclick="inputValue('payment_country', <?php echo $order_id; ?>, 'left')"><?php echo $payment_country; ?></td>
              </tr>
              <?php foreach ($payment_custom_fields as $custom_field) { ?>
              <tr>
                <td><?php echo $custom_field['name']; ?>:</td>
                <td><?php echo $custom_field['value']; ?></td>
              </tr>
              <?php } ?>
              <tr>
                <td><?php echo $text_payment_method; ?></td>
				<td id="payment_method<?php echo $order_id; ?>" onclick="inputValue('payment_method', <?php echo $order_id; ?>, 'left')"><?php echo $payment_method; ?></td>
              </tr>
            </table>		
          </div>
          <?php if ($shipping_method) { ?>
          <div class="tab-pane" id="tab-shipping">
            <table class="table table-bordered">
              <tr>
                <td><?php echo $text_firstname; ?></td>
				<td id="shipping_firstname<?php echo $order_id; ?>" onclick="inputValue('shipping_firstname', <?php echo $order_id; ?>, 'left')"><?php echo $shipping_firstname; ?></td>
              </tr>
              <tr>
                <td><?php echo $text_lastname; ?></td>
				<td id="shipping_lastname<?php echo $order_id; ?>" onclick="inputValue('shipping_lastname', <?php echo $order_id; ?>, 'left')"><?php echo $shipping_lastname; ?></td>
              </tr>
              <?php if ($shipping_company) { ?>
              <tr>
                <td><?php echo $text_company; ?></td>
				<td id="shipping_company<?php echo $order_id; ?>" onclick="inputValue('shipping_company', <?php echo $order_id; ?>, 'left')"><?php echo $shipping_company; ?></td>
              </tr>
              <?php } ?>
              <tr>
                <td><?php echo $text_address_1; ?></td>
				<td id="shipping_address_1<?php echo $order_id; ?>" onclick="inputValue('shipping_address_1', <?php echo $order_id; ?>, 'left')"><?php echo $shipping_address_1; ?></td>
              </tr>
              <?php if ($shipping_address_2) { ?>
              <tr>
                <td><?php echo $text_address_2; ?></td>
				<td id="shipping_address_2<?php echo $order_id; ?>" onclick="inputValue('shipping_address_2', <?php echo $order_id; ?>, 'left')"><?php echo $shipping_address_2; ?></td>
              </tr>
              <?php } ?>
              <tr>
                <td><?php echo $text_city; ?></td>
				<td id="shipping_city<?php echo $order_id; ?>" onclick="inputValue('shipping_city', <?php echo $order_id; ?>, 'left')"><?php echo $shipping_city; ?></td>
              </tr>
              <?php if ($shipping_postcode) { ?>
              <tr>
                <td><?php echo $text_postcode; ?></td>
				<td id="shipping_postcode<?php echo $order_id; ?>" onclick="inputValue('shipping_postcode', <?php echo $order_id; ?>, 'left')"><?php echo $shipping_postcode; ?></td>
              </tr>
              <?php } ?>
              <tr>
                <td><?php echo $text_zone; ?></td>
				<td id="shipping_zone<?php echo $order_id; ?>" onclick="inputValue('shipping_zone', <?php echo $order_id; ?>, 'left')"><?php echo $shipping_zone; ?></td>
              </tr>
              <?php if ($shipping_zone_code) { ?>
              <tr>
                <td><?php echo $text_zone_code; ?></td>
				<td id="shipping_zone_code<?php echo $order_id; ?>" onclick="inputValue('shipping_zone_code', <?php echo $order_id; ?>, 'left')"><?php echo $shipping_zone_code; ?></td>
              </tr>
              <?php } ?>
              <tr>
                <td><?php echo $text_country; ?></td>
				<td id="shipping_country<?php echo $order_id; ?>" onclick="inputValue('shipping_country', <?php echo $order_id; ?>, 'left')"><?php echo $shipping_country; ?></td>
              </tr>
              <?php foreach ($shipping_custom_fields as $custom_field) { ?>
              <tr>
                <td><?php echo $custom_field['name']; ?>:</td>
                <td><?php echo $custom_field['value']; ?></td>
              </tr>
              <?php } ?>
              <?php if ($shipping_method) { ?>
              <tr>
                <td><?php echo $text_shipping_method; ?></td>
				<td id="shipping_method<?php echo $order_id; ?>" onclick="inputValue('shipping_method', <?php echo $order_id; ?>, 'left')"><?php echo $shipping_method; ?></td>
              </tr>
              <?php } ?>
            </table>		
          </div>
          <?php } ?>
          <div class="tab-pane" id="tab-product">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <td class="text-left"><?php echo $column_image; ?></td>
                  <td class="text-left"><?php echo $column_product; ?></td>
                  <td class="text-left"><?php echo $column_model; ?></td>
                  <td class="text-right"><?php echo $column_quantity; ?></td>
                  <td class="text-right"><?php echo $column_price; ?></td>
                  <td class="text-right"><?php echo $column_total; ?></td>
                </tr>
              </thead>
              
		<tbody id="cart">
			<?php $i = 0; ?>
			<?php foreach ($products as $product) { ?>                  
			<tr id="row-product-<?php echo $product['order_product_id']; ?>">
				<td><img src="<?php echo $product['image']; ?>"/></td>
				<td class="text-left"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
				<input type="hidden" name="product[<?php echo $i; ?>][product_id]" value="<?php echo $product['product_id']; ?>">
                    <?php foreach ($product['option'] as $option) { ?>
						<br />
						<?php if ($option['type'] != 'file') { ?>
						&nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>

						<?php if ($option['type'] == 'select' || $option['type'] == 'radio' || $option['type'] == 'image') { ?>
						<input type="hidden" name="product[<?php echo $i; ?>][option][<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['product_option_value_id']; ?>" />
						<?php } ?>
						<?php if ($option['type'] == 'checkbox') { ?>
						<input type="hidden" name="product[<?php echo $i; ?>][option][<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option['product_option_value_id']; ?>" />
						<?php } ?>
						<?php if ($option['type'] == 'text' || $option['type'] == 'textarea' || $option['type'] == 'date' || $option['type'] == 'datetime' || $option['type'] == 'time') { ?>
						<input type="hidden" name="product[<?php echo $i; ?>][option][<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" />
						<?php } ?>
		
						<?php } else { ?>
						&nbsp;<small> - <?php echo $option['name']; ?>: <a href="<?php echo $option['href']; ?>"><?php echo $option['value']; ?></a></small>
						<input type="hidden" name="product[<?php echo $i; ?>][option][<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" />
						<?php } ?>
                    <?php } ?>
                </td>                  
				<td class="text-left" id="model<?php echo $product['order_product_id']; ?>" onclick="inputValue('model', <?php echo $product['order_product_id']; ?> , 'left');"><?php echo $product['model']; ?></td>                  
				<td class="text-right"><?php echo $product['quantity']; ?>
					<input type="hidden" name="product[<?php echo $i; ?>][quantity]" value="<?php echo $product['quantity']; ?>">
				</td>                  
				<td class="text-right" id="price<?php echo $product['order_product_id']; ?>" onclick="inputValue('price', <?php echo $product['order_product_id']; ?> , 'right');"><?php echo $product['price']; ?></td>                  
				<td class="text-right" id="product_total<?php echo $product['order_product_id']; ?>" onclick="inputValue('product_total', <?php echo $product['order_product_id']; ?> , 'right');"><?php echo $product['total']; ?></td>
        </tr>
		<?php $i++; ?>
		
                <?php } ?>
                <?php foreach ($vouchers as $voucher) { ?>
                <tr>
                  <td class="text-left"><a href="<?php echo $voucher['href']; ?>"><?php echo $voucher['description']; ?></a></td>
                  <td class="text-left"></td>
                  <td class="text-right">1</td>
                  <td class="text-right"><?php echo $voucher['amount']; ?></td>
                  <td class="text-right"><?php echo $voucher['amount']; ?></td>
                </tr>
                <?php } ?>
                
				<?php foreach ($totals as $total) { ?>
				<tr id="<?php echo $total['code']; ?>_tr">
					<?php if ($total['code'] == 'shipping') { ?>
						<td colspan="5" class="text-right" id="<?php echo $total['code']; ?>-method-ot<?php echo $order_id; ?>" onclick="inputValue('<?php echo $total['code']; ?>-method-ot', <?php echo $order_id; ?> , 'right')"><?php echo $total['title']; ?>:</td>
					<?php } else { ?>
						<td colspan="5" class="text-right">
						<?php if ($total['code'] == 'discount') { ?>
							<button type="button" onclick="confirmDelete('<?php echo $total['code']; ?>', <?php echo $order_id; ?>);" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
						<?php } ?>
					<?php echo $total['title']; ?>:</td>
					<?php } ?>
				  <td class="text-right" id="<?php echo $total['code']; ?>" onclick="inputValue('<?php echo $total['code']; ?>', <?php echo $order_id; ?> , 'right')"><?php echo $total['text']; ?></td>
				</tr>
                <?php } ?>
              </tbody>
            </table>
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#add-product-modal"><i class="fa fa-plus"></i> <?php echo $text_add_product; ?></button>
			<button type="button" class="btn btn-info" data-toggle="modal" data-target="#add-discount-modal" id="discount-button"><i class="fa fa-plus"></i> <?php echo $text_add_discount; ?></button>
			<button type="button" class="btn btn-warning pull-right" id="refresh-cart-button"><i class="fa fa-refresh"></i> Refresh Cart</button>		
          </div>
          <div class="tab-pane" id="tab-history">
            <div id="history"></div>
            <br />
            <fieldset>
              <legend><?php echo $text_history; ?></legend>
              <form class="form-horizontal">
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-order-status"><?php echo $entry_order_status; ?></label>
                  <div class="col-sm-10">
                    <select name="order_status_id" id="input-order-status" class="form-control">
                      <?php foreach ($order_statuses as $order_statuses) { ?>
                      <?php if ($order_statuses['order_status_id'] == $order_status_id) { ?>
                      <option value="<?php echo $order_statuses['order_status_id']; ?>" selected="selected"><?php echo $order_statuses['name']; ?></option>
                      <?php } else { ?>
                      <option value="<?php echo $order_statuses['order_status_id']; ?>"><?php echo $order_statuses['name']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-notify"><?php echo $entry_notify; ?></label>
                  <div class="col-sm-10">
                    <input type="checkbox" name="notify" value="1" id="input-notify" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-comment"><?php echo $entry_comment; ?></label>
                  <div class="col-sm-10">
                    <textarea name="comment" rows="8" id="input-comment" class="form-control"></textarea>
                  </div>
                </div>
              </form>
              <div class="text-right">
                <button id="button-history" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i> <?php echo $button_history_add; ?></button>
              </div>
            </fieldset>
          </div>
          <?php if ($payment_action) { ?>
          <div class="tab-pane" id="tab-action"><?php echo $payment_action; ?></div>
          <?php } ?>
          <?php if ($frauds) { ?>
          <?php foreach ($frauds as $fraud) { ?>
          <div class="tab-pane" id="tab-<?php echo $fraud['code']; ?>">
          <?php echo $fraud['content']; ?>
          </div>
          <?php } ?>
          <?php } ?>
        </div>
        <p>Order Info Edit (With AJAX) v2.0.0.beta <a href="http://jorimvanhove.com">Jorim van Hove</a> &copy; 2015</p>
      </div>
    </div>
  </div>
  
<!-- Add Products Modal -->
<div class="modal fade" id="add-product-modal" tabindex="-1" role="dialog" aria-labelledby="AddProductModalLabel">
  <div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="<?php echo $button_cancel; ?>"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="AddProductModalLabel"><?php echo $text_add_product; ?></h4>
	  </div>
	  <div class="modal-body">
		  <form id="add-product-form">
			  <fieldset form="add-product-form">
				<legend><?php echo $text_add_product; ?></legend>
				<div class="form-group">
				  <label class="col-sm-2 control-label" for="input-product"><?php echo $entry_product; ?></label>
				  <div class="col-sm-10">
					<input type="text" name="product" value="" id="input-product" class="form-control" />
					<input type="hidden" name="product_id"  value="" />
				  </div>
				</div>
				<div class="form-group">
				  <label class="col-sm-2 control-label" for="input-quantity"><?php echo $entry_quantity; ?></label>
				  <div class="col-sm-10">
					<input type="text" name="quantity" value="1" id="input-quantity" class="form-control" />
				  </div>
				</div>
				<div class="form-group">
				  <label class="col-sm-2 control-label" for="input-price"><?php echo $entry_price; ?></label>
				  <div class="col-sm-10">
					<input type="text" name="product_price" id="input-price" class="form-control" />
				  </div>
				</div>
			  </fieldset>
				<div id="option"></div>
		</form>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $button_cancel; ?></button>
		<button type="button" class="btn btn-primary" id="add-discount-btn" onclick="addProduct()"><?php echo $text_add_product; ?></button>
	  </div>
	</div>
  </div>
</div>

<!-- Add Discount Modal -->
<div class="modal fade" id="add-discount-modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="<?php echo $button_cancel; ?>"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="ModalLabel"><?php echo $text_add_discount; ?></h4>
	  </div>
	  <div class="modal-body">
		<form id="discount-form">
		  <div class="form-group">
			<label for="discount-type-select">Discount Type</label>
				<select class="form-control" name="discount-type" id="discount-type-select">
					<option value="P">Percentage (%)</option>
					<option value="F">Fixed amount</option>
				</select>
		  </div>
		  <div class="form-group">
			<label for="discount-value">Discount Value</label>
				<input type="text" name="discount-value" id="discount-value" value="" class="form-control">
			  
		 </div>
		 <div class="form-group">
			<label for="discount-title">Discount Title</label>
				<input type="text" name="discount-title" id="discount-title" value="" class="form-control">
		 </div>
		 <div class="form-group">
			<label for="discount-sort-order">Sort Order</label>
				<input type="text" name="sort_order" id="discount-sort-order" value="" class="form-control">
		 </div>
		</form>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $button_cancel; ?></button>
		<button type="button" class="btn btn-primary" id="add-discount-btn" onclick="addDiscount();"><?php echo $text_add_discount; ?></button>
	  </div>
	</div>
  </div>
</div>
		
<script type="text/javascript"><!--
$(document).delegate('#button-invoice', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/createinvoiceno&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		dataType: 'json',
		beforeSend: function() {
			$('#button-invoice').button('loading');			
		},
		complete: function() {
			$('#button-invoice').button('reset');
		},
		success: function(json) {
			$('.alert').remove();
						
			if (json['error']) {
				$('#tab-order').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}
			
			if (json['invoice_no']) {
				$('#button-invoice').replaceWith(json['invoice_no']);
			}
		},			
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-reward-add', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/addreward&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-reward-add').button('loading');
		},
		complete: function() {
			$('#button-reward-add').button('reset');
		},									
		success: function(json) {
			$('.alert').remove();
						
			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}
			
			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
				
				$('#button-reward-add').replaceWith('<button id="button-reward-remove" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i> <?php echo $button_reward_remove; ?></button>');
			}
		},			
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-reward-remove', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/removereward&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-reward-remove').button('loading');
		},
		complete: function() {
			$('#button-reward-remove').button('reset');
		},				
		success: function(json) {
			$('.alert').remove();
						
			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}
			
			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
				
				$('#button-reward-remove').replaceWith('<button id="button-reward-add" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> <?php echo $button_reward_add; ?></button>');
			}
		},			
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-commission-add', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/addcommission&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-commission-add').button('loading');
		},
		complete: function() {
			$('#button-commission-add').button('reset');
		},			
		success: function(json) {
			$('.alert').remove();
						
			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}
			
			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
                
				$('#button-commission-add').replaceWith('<button id="button-commission-remove" class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i> <?php echo $button_commission_remove; ?></button>');
			}
		},			
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$(document).delegate('#button-commission-remove', 'click', function() {
	$.ajax({
		url: 'index.php?route=sale/order/removecommission&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		beforeSend: function() {
			$('#button-commission-remove').button('loading');
		
		},
		complete: function() {
			$('#button-commission-remove').button('reset');
		},		
		success: function(json) {
			$('.alert').remove();
						
			if (json['error']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}
			
			if (json['success']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
				
				$('#button-commission-remove').replaceWith('<button id="button-commission-add" class="btn btn-success btn-xs"><i class="fa fa-minus-circle"></i> <?php echo $button_commission_add; ?></button>');
			}
		},			
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('#history').delegate('.pagination a', 'click', function(e) {
	e.preventDefault();
	
	$('#history').load(this.href);
});			

$('#history').load('index.php?route=sale/order/history&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>');

$('#button-history').on('click', function() {
  if(typeof verifyStatusChange == 'function'){
    if(verifyStatusChange() == false){
      return false;
    }else{
      addOrderInfo();
    }
  }else{
    addOrderInfo();
  }

	$.ajax({
		url: 'index.php?route=sale/order/api&token=<?php echo $token; ?>&api=api/order/history&order_id=<?php echo $order_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'order_status_id=' + encodeURIComponent($('select[name=\'order_status_id\']').val()) + '&notify=' + ($('input[name=\'notify\']').prop('checked') ? 1 : 0) + '&append=' + ($('input[name=\'append\']').prop('checked') ? 1 : 0) + '&comment=' + encodeURIComponent($('textarea[name=\'comment\']').val()),
		beforeSend: function() {
			$('#button-history').button('loading');			
		},
		complete: function() {
			$('#button-history').button('reset');	
		},
		success: function(json) {
			$('.alert').remove();
			
			if (json['error']) {
				$('#history').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			} 
		
			if (json['success']) {
				$('#history').load('index.php?route=sale/order/history&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>');
				
				$('#history').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				
				$('textarea[name=\'comment\']').val('');
				
				$('#order-status').html($('select[name=\'order_status_id\'] option:selected').text());			
			}			
		},			
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

function changeStatus(){
  var status_id = $('select[name="order_status_id"]').val();

  $('#openbay-info').remove();

  $.ajax({
    url: 'index.php?route=extension/openbay/getorderinfo&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>&status_id='+status_id,
    dataType: 'html',
    success: function(html) {
      $('#history').after(html);
    }
  });
}

function addOrderInfo(){
  var status_id = $('select[name="order_status_id"]').val();

  $.ajax({
    url: 'index.php?route=extension/openbay/addorderinfo&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>&status_id='+status_id,
    type: 'post',
    dataType: 'html',
    data: $(".openbay-data").serialize()
  });
}

$(document).ready(function() {
  changeStatus();
});

$('select[name="order_status_id"]').change(function(){ changeStatus(); });
//--></script></div>
		
<script type="text/javascript"><!--

var product_row = 0;	

$(function(){
	hideDiscountButton();
});

function hideDiscountButton() {

	if (document.getElementById('customdiscount')) {
		$('#discount-button').hide();
	}
}

// Add all products to the cart using the api
$('#order-products').on('click', function(){
 		cartAPI();
});

function cartAPI() {
	$.ajax({
		url: 'index.php?route=sale/order/api&token=<?php echo $token; ?>&api=api/cart/add&store_id=<?php echo $store_id; ?>',
		type: 'post',
		data: $('#cart input[name^=\'product\'][type=\'text\'], #cart input[name^=\'product\'][type=\'hidden\'], #cart input[name^=\'product\'][type=\'radio\']:checked, #cart input[name^=\'product\'][type=\'checkbox\']:checked, #cart select[name^=\'product\'], #cart textarea[name^=\'product\']'),
		dataType: 'json',
		beforeSend: function() {
			//$('#button-product-add').button('loading');
		},
		complete: function() {
			//$('#button-product-add').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');
		
			if (json['error'] && json['error']['warning']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});		
			
	$.ajax({
		url: 'index.php?route=sale/order/api&token=<?php echo $token; ?>&api=api/voucher/add&store_id=<?php echo $store_id; ?>',
		type: 'post',
		data: $('#cart input[name^=\'voucher\'][type=\'text\'], #cart input[name^=\'voucher\'][type=\'hidden\'], #cart input[name^=\'voucher\'][type=\'radio\']:checked, #cart input[name^=\'voucher\'][type=\'checkbox\']:checked, #cart select[name^=\'voucher\'], #cart textarea[name^=\'voucher\']'),
		dataType: 'json',
		beforeSend: function() {
			//$('#button-voucher-add').button('loading');
		},
		complete: function() {
			//$('#button-voucher-add').button('reset');
			// Refresh products, vouchers and totals
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');
		
			if (json['error'] && json['error']['warning']) {
				$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + '\r\n' + xhr.statusText + '\r\n' + xhr.responseText);
		}
	});
	
	$('#refresh-cart-button').trigger('click');
}

function refreshCart() {
	$.ajax({
		url: 'index.php?route=sale/order/api&token=<?php echo $token; ?>&api=checkout/ajaxedit/orderProducts&store_id=<?php echo $store_id; ?>',
		type: 'post',
		data: {
		order_id : '<?php echo $order_id; ?>',
		token: '<?php echo $token; ?>'
		},
		dataType: 'json',
		beforeSend: function() {
   			$('#cart').html('<td colspan="6" class="text-center"><i class="fa fa-spinner fa-spin fa-4x"></i></td>');
		},
		success: function(json) {
			$('.alert-danger, .text-danger').remove();
	
			// Check for errors
			if (json['error']) {
				if (json['error']['warning']) {
					$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				}
							
				if (json['error']['stock']) {
					$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['stock'] + '</div>');
				}
						
				if (json['error']['minimum']) {
					for (i in json['error']['minimum']) {
						$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['minimum'][i] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
					}
				}
			}				
	
			var shipping = false;
	
			html = '';
				
			if (json['products']) {
				for (i = 0; i < json['products'].length; i++) {
					product = json['products'][i];
					product_row = i;
					
					html += '<tr id="row-product-' + product['order_product_id'] + '">';
					html += '  <td><img src="' + product['image'] + '" class="img-thumbnail" alt="' + product['name'] + '"/></td>';
					html += '  <td class="text-left"><span id="product_name' + product['order_product_id'] + '"';
					html += ' onclick="inputValue(\'product_name\', ' + product['order_product_id'] + ' , \'left\');">';
					html += product['name'] + '</span>';
					html +=  (!product['stock'] ? '<span class="text-danger">***</span>' : '');
					html += '	<span class="pull-right"><a href="' + product['href']+ '" class="btn btn-sm btn-info text-right"><i class="fa fa-eye"></i></a> ';
					html += '		<button class="btn btn-sm btn-danger" id="remove-product-' + product['order_product_id'] + '" title="<?php echo $text_remove_product; ?>" onclick="confirmDeleteProduct(\'remove-product\',' + product['order_product_id'] + ', \'right\');" ><i class="fa fa-minus-circle"></i></button>';
					
					html += '</span><br/>';
					
					if (product['option']) {
						for (j = 0; j < product['option'].length; j++) {
							option = product['option'][j];
					
							html += '  - <small>' + option['name'] + ': ' + option['value'] + '</small><br />';
						}
					}
			
					html += '</td>';							
					html += '  <td class="text-left" id="model' + product['order_product_id'] + '" onclick="inputValue(\'model\', ' + product['order_product_id'] + ' , \'left\');">' + product['model'] + '</td>';
					
					html += '  <td class="text-right" id="qty' + product['order_product_id'] + '" onclick="inputValue(\'qty\', ' + product['order_product_id'] + ' , \'right\');">' + product['quantity'] + '</td>';
												
					html += '  <td class="text-right" id="price' + product['order_product_id'] + '" onclick="inputValue(\'price\', ' + product['order_product_id'] + ' , \'right\');">' + product['price'] + '</td>';
					
					html += '  <td class="text-right" id="product_total' + product['order_product_id'] + '" onclick="inputValue(\'product_total\', ' + product['order_product_id'] + ' , \'right\');">' + product['total'] + '</td>';
					
					html += '</tr>';
					
					// Set hidden fields for cart
					
					html += '<tr class="hidden" id="hidden-cart-' +  product['order_product_id'] + '">';
					html += '  	<td><input type="hidden" id="row-count-' +  product['order_product_id'] + '" value="' + i + '"><input type="hidden" name="product[' + i + '][product_id]" value="' + product['product_id'] + '" /></td>';
					html += '	<td>';
					
					if (product['option']) {
						for (j = 0; j < product['option'].length; j++) {
							option = product['option'][j];
					
					
							if (option['type'] == 'select' || option['type'] == 'radio' || option['type'] == 'image') {
								html += '<input type="hidden" name="product[' + i + '][option][' + option['product_option_id'] + ']" value="' + option['product_option_value_id'] + '" />';
							}
					
							if (option['type'] == 'checkbox') {
								html += '<input type="hidden" name="product[' + i + '][option][' + option['product_option_id'] + '][]" value="' + option['product_option_value_id'] + '" />';
							}
					
							if (option['type'] == 'text' || option['type'] == 'textarea' || option['type'] == 'file' || option['type'] == 'date' || option['type'] == 'datetime' || option['type'] == 'time') {
								html += '<input type="hidden" name="product[' + i + '][option][' + option['product_option_id'] + ']" value="' + option['value'] + '" />';
							}
						}
					}
					html += '	</td>';
					html += '  	<td><input type="hidden" name="product[' + i + '][quantity]" value="' + product['quantity'] + '" /></td>';
					
					if (product['shipping'] != 0) {
						shipping = true;
					}
				}
			}
			
			if (json['vouchers']) {
				for (i in json['vouchers']) {
					voucher = json['vouchers'][i];
			
					html += '<tr>';
					html += '  <td class="text-left">' + voucher['description'];
					html += '    <input type="hidden" name="voucher[' + i + '][code]" value="' + voucher['code'] + '" />';
					html += '    <input type="hidden" name="voucher[' + i + '][description]" value="' + voucher['description'] + '" />';
					html += '    <input type="hidden" name="voucher[' + i + '][from_name]" value="' + voucher['from_name'] + '" />';
					html += '    <input type="hidden" name="voucher[' + i + '][from_email]" value="' + voucher['from_email'] + '" />';
					html += '    <input type="hidden" name="voucher[' + i + '][to_name]" value="' + voucher['to_name'] + '" />';
					html += '    <input type="hidden" name="voucher[' + i + '][to_email]" value="' + voucher['to_email'] + '" />';
					html += '    <input type="hidden" name="voucher[' + i + '][voucher_theme_id]" value="' + voucher['voucher_theme_id'] + '" />';
					html += '    <input type="hidden" name="voucher[' + i + '][message]" value="' + voucher['message'] + '" />';
					html += '    <input type="hidden" name="voucher[' + i + '][amount]" value="' + voucher['amount'] + '" />';
					html += '  </td>';
					html += '  <td class="text-left"></td>';
					html += '  <td class="text-right">1</td>';
					html += '  <td class="text-right">' + voucher['amount'] + '</td>';
					html += '  <td class="text-right">' + voucher['amount'] + '</td>';
					html += '  <td class="text-center" style="width: 3px;"><button type="button" value="' + voucher['code'] + '" data-toggle="tooltip" title="<?php echo $button_remove; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
					html += '</tr>';	
				}
			}
	
			if (json['products'] == '' && json['vouchers'] == '') {				
				html += '<tr>';
				html += '  <td colspan="6" class="text-center"><?php echo $text_no_results; ?></td>';
				html += '</tr>';	
			}
			
			if (json['totals']) {
				for (i in json['totals']) {
					total = json['totals'][i];
			
					html += '<tr id="'+ total['code'] +'_tr">';
					
					if (total['code'] == 'shipping') { 
						html += '	<td colspan="5" class="text-right" id="' + total['code'] + '-method-ot<?php echo $order_id; ?>" onclick="inputValue(\'' + total['code'] + '-method-ot\', <?php echo $order_id; ?> , \'right\')">' + total['title'] + ':</td>';
					} else {
						html += '<td colspan="5" class="text-right">';
						if (total['code'] == 'customdiscount') { 
							html +=   '<button type="button" onclick="confirmDelete(\''+ total['code'] + '\', <?php echo $order_id; ?>);" data-toggle="tooltip" title="Remove" class="btn btn-danger btn-sm"><i class="fa fa-minus-circle"></i></button>&nbsp;&nbsp;';
						} 
						html += total['title'] + ':</td>';
				 	}
					
					html += '  <td class="text-right" id="' + total['code'] + '" onclick="inputValue(\'' + total['code'] + '\', <?php echo $order_id; ?> , \'right\')">' + total['text'] + '</td>';
					html += '</tr>';
					
					if (total['code'] == 'total') {
						$('#order-total').html(total['text']);
					}
				}
			}
	
			if (!json['totals'] && !json['products'] && !json['vouchers']) {				
				html += '<tr>';
				html += '  <td colspan="6" class="text-center"><?php echo $text_no_results; ?></td>';
				html += '</tr>';	
			}

			$('#cart').html(html);
			//console.log(product_row);
		},	
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
}

$('#refresh-cart-button').on('click', function() {
		refreshCart();
});


function confirmDelete(total,id) {

	html = '<div class="modal fade" id="confirm-delete-' + total + '" tabindex="-1" role="dialog" aria-labelledby="confirmDeleteLabel">';
	html += '<div class="modal-dialog" role="document">';
	html += '<div class="modal-content">';
	html += '<div class="modal-header bg-danger">';
	html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
	html += '<h4 class="modal-title" id="confirmDeleteLabel">Confirm Delete</h4>';
	html += '</div>';
	html += '<div class="modal-body">';
	html += 'Are you sure you want to delete this Order Total? This action can not be undone!';
	html += '</div>';
	html += '<div class="modal-footer">';
	html += '<button type="button" class="btn btn-default pull-left" data-dismiss="modal">No thanks!</button>';
	html += '<button type="button" class="btn btn-danger" onclick="removeDiscount(\'' + total + '\', ' + id + ');">Permanently Delete Discount</button>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '</div>';

	$('body').append(html);

	$('#confirm-delete-' + total).modal();
}

function confirmDeleteProduct(param, param_id, align) {
	
	var productName = $('#product_name' + param_id).text();
	
	html = '<div class="modal fade" id="confirm-delete-' + param_id + '" tabindex="-1" role="dialog" aria-labelledby="confirmDeleteLabel">';
	html += '<div class="modal-dialog" role="document">';
	html += '<div class="modal-content">';
	html += '<div class="modal-header bg-danger">';
	html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
	html += '<h4 class="modal-title" id="confirmDeleteLabel">Confirm Delete</h4>';
	html += '</div>';
	html += '<div class="modal-body">';
	html += 'Are you sure you want to delete the product <b>' + productName +  '</b>? <span class="text-danger"><b>This action can not be undone!</b></span>';
	html += '</div>';
	html += '<div class="modal-footer">';
	html += '<button type="button" class="btn btn-default pull-left" data-dismiss="modal">No thanks!</button>';
	html += '<button type="button" class="btn btn-danger" onclick="saveValue(\'' + param + '\', ' + param_id + ', \'' + align + '\');">Permanently Delete Product</button>';
	html += '</div>';
	html += '</div>';
	html += '</div>';
	html += '</div>';

	$('body').append(html);

	$('#confirm-delete-' + param_id).modal();
}

function removeDiscount(total,order_id) {		
	$.ajax({
		url: '<?php echo $store_url; ?>index.php?route=checkout/ajaxedit/addDiscount&token=<?php echo $token; ?>',
		type: 'post',
		data: {
			order_id: order_id, 
			type: '', 
			value: '',
			title: '',
			sort_order: '',
			remove: '1'
		},
		dataType: 'json',
		beforeSend: function() {
			$('#confirm-delete-' + total).modal('hide')
			$('#confirm-delete-' + total).on('hidden.bs.modal', function () {
				$('#confirm-delete-' + total).remove();
			})
		
		},
		success: function(json) {
			$('#' + total + '_tr').remove();
			$('#discount-button').show();
			cartAPI();
			
		}
	});

}

function addDiscount() {
	var order_id = <?php echo $order_id; ?>;
	$.ajax({
	  type: 'post',	
	  url: '<?php echo $store_url; ?>index.php?route=checkout/ajaxedit/addDiscount&token=<?php echo $token; ?>',
	  dataType: 'json',
	  data: { 
		order_id: order_id, 
		type: $("#discount-type-select").val(), 
		value:$("#discount-value").val(),
		title:$("#discount-title").val(),
		sort_order:$("#discount-sort-order").val(),
		remove: '0'
	  },
	  beforeSend: function() {
		$('#add-discount-modal').modal('hide');
		$('#discount-button').hide();
	  },
	  success: function(json) {
		 cartAPI(); 
	  },
	  error: function(json) { 
		 alertJson('warning', json);
	  },
	  complete: function(json) {
	  }
	});
}

function resendConfirmEmail() {
	var order_id = <?php echo $order_id; ?>;
	
	$.ajax({
	  type: 'post',	
	  url: '<?php echo $store_url; ?>index.php?route=checkout/ajaxedit/resendConfirmEmail&token=<?php echo $token; ?>',
	  dataType: 'json',
	  data: { order_id: order_id, order_status_id: $("#input-order-status").val(),comment:$("#input-comment").val()},
	  beforeSend: function() {
	  },
	  success: function(json) {
		 alertJson('success', json);			 
	  },
	  error: function(json) { 
		 alertJson('warning', json);
	  },
	  complete: function(json) {
	  }
	});
}

function inputValue(param, param_id, align) {
	
	if ( param !== 'product_name')	{
		html  = '<td class="' + param + '_edit" id="' + param + param_id + '">';
		html += '<div class="input-group">';
		html += '<span class="input-group-btn">';
		html += '<a onclick="saveValue(\'' + param + '\' , ' + param_id + ' , \'' + align + '\')" role="button" class="btn  btn-success" data-toggle="tooltip" title="<?php echo $button_save; ?>" data-container="body"><i class="fa fa-check"></i></a></span>';
		html += '<input type="text" value="' + $('#' + param + (param !== 'shipping' ? param_id : '')).html() + '" class="form-control">';
		html += '<span class="input-group-btn">';
		html += '<a onclick="closeInput(\'' + param + '\', \'' + param_id + '\' , \'' + align + '\')" role="button" class="btn btn-danger" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" data-container="body"><i class="fa fa-times"></i></a></span>';
		html += '</div></td>';
	} else {
		html  = '<span class="' + param + '_edit" id="' + param + param_id + '">';
		html += '<div class="input-group">';
		html += '<span class="input-group-btn">';
		html += '<a onclick="saveValue(\'' + param + '\' , ' + param_id + ' , \'' + align + '\')" role="button" class="btn  btn-success" data-toggle="tooltip" title="<?php echo $button_save; ?>" data-container="body"><i class="fa fa-check"></i></a></span>';
		html += '<input type="text" value="' + $('#' + param + (param !== 'shipping' ? param_id : '')).html() + '" class="form-control">';
		html += '<span class="input-group-btn">';
		html += '<a onclick="closeInput(\'' + param + '\', \'' + param_id + '\' , \'' + align + '\')" role="button" class="btn btn-danger" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" data-container="body"><i class="fa fa-times"></i></a></span>';
		html += '</div></span>';
	}	
	$('#' + param + (param !== 'shipping' ? param_id : '') ).replaceWith(html);
}

function saveValue(param, param_id, align) {
			
	var new_value = $('#' + param + param_id + ' input').val();
	
	$.ajax({
	  type: 'post',	
	  url: 'index.php?route=sale/ajaxedit/saveNewValue&token=<?php echo $token; ?>',
	  dataType: 'json',
	  data: { param: param, new_value: new_value , param_id: param_id},
	  beforeSend: function() {
		if (param == 'remove-product') {
			$('#confirm-delete-' + param_id).modal('hide')
			$('#confirm-delete-' + param_id).on('hidden.bs.modal', function () {
				$('#confirm-delete-' + param_id).remove();
			})
		}	
	  },
	  success: function(json) {
		 closeInput(param, param_id , align);
		 replaceTotals(param, param_id, json);
		 alertJson('alert alert-success', json);			 
	  },
	  error: function(json) { 
		 alertJson('alert alert-danger', json);
		 closeInput(param, param_id, align);
	  },
	  complete: function(json) {
		
	  }
	});
}

function addProduct() {
	param = 'add-product';
	param_id = <?php echo $order_id; ?>;
	
	$.ajax({
	  type: 'post',
	  url: 'index.php?route=sale/ajaxedit/addProduct&token=<?php echo $token; ?>',
	  dataType: 'json',
	  data: { 
		product_data : $('#add-product-form').serialize(),
		order_id : param_id
	  },
	  beforeSend: function() {
			$('#add-product-modal').modal('hide')
	  },
	  success: function(json) {
		 addProductLine(json['product']);
		 //replaceTotals(param, param_id, json['order_total']);
		 alertJson('alert alert-success', json);			 
	  },
	  error: function(json) { 
		 alertJson('alert alert-danger', json);
	  }
	});
}

function addProductLine(json) {
	
	html  = '<tr id="row-product-' + json['order_product_id'] + '">';
	html += '<td></td>';
	html += '  <td class="text-left"><span id="product_name' + json['order_product_id'] + '" onclick="inputValue(\'product_name\', ' + json['order_product_id'] + ' , \'left\');"> ' + json['name'] + ' </span>';
	html += ' <span class="pull-right"></a> <button class="btn btn-sm btn-danger" id="remove-product-' + json['order_product_id'] + '" title="<?php echo $text_remove_product; ?>" onclick="confirmDeleteProduct(\'remove-product\', ' + json['order_product_id'] + ' , \'right\');" ><i class="fa fa-minus"></i></button></span>';
	for (index = 0, len = json['option'].length; index < len; ++index) {
		html +=	'<br />';
		if (json['option'][index]['type'] != 'file') {
			html += '&nbsp;<small> - ' + json['option'][index]['name'] + ': ' + json['option'][index]['value'] + '</small>';
		} else {
			html += '&nbsp;<small> - ' + json['option'][index]['name'] + ': <a href="' + json['option'][index]['href'] + '">' + json['option'][index]['value'] + '</a></small>';
		}
	}
	html += '</td>';
	html += '<td class="text-left" id="model' + json['order_product_id'] + '" onclick="inputValue(\'model\', ' + json['order_product_id'] + ' , \'left\');">' + json['model'] + '</td>'
	html += '<td class="text-right" id="qty' + json['order_product_id'] + '" onclick="inputValue(\'qty\', ' + json['order_product_id'] + ' , \'right\');">' + json['quantity'] + '</td>';
	html += '<td class="text-right" id="price' + json['order_product_id'] + '" onclick="inputValue(\'price\', ' + json['order_product_id'] + ' , \'right\');">' + json['price'] + '</td>'; 
	html += '<td class="text-right" id="product_total' + json['order_product_id'] + '" onclick="inputValue(\'product_total\', ' + json['order_product_id'] + ' , \'right\');">' + json['product_total'] + '</td>';     
	html += '</tr>'; 
	
	// Set hidden fields for cart
	
	product_row++;
					
	html += '<tr class="hidden" id="hidden-cart-' + json['order_product_id'] + '">';
	html += '  	<td><input type="hidden" id="row-count-' +  json['order_product_id'] + '" value="' + product_row + '"><input type="hidden" name="product[' + product_row + '][product_id]" value="' + json['product_id'] + '" /></td>';
	html += '	<td>';
	
	if (json['option']) {
		for (j = 0; j < json['option'].length; j++) {
			option = json['option'][j];
	
	
			if (option['type'] == 'select' || option['type'] == 'radio' || option['type'] == 'image') {
				html += '<input type="hidden" name="product[' + product_row + '][option][' + option['product_option_id'] + ']" value="' + option['product_option_value_id'] + '" />';
			}
	
			if (option['type'] == 'checkbox') {
				html += '<input type="hidden" name="product[' + product_row + '][option][' + option['product_option_id'] + '][]" value="' + option['product_option_value_id'] + '" />';
			}
	
			if (option['type'] == 'text' || option['type'] == 'textarea' || option['type'] == 'file' || option['type'] == 'date' || option['type'] == 'datetime' || option['type'] == 'time') {
				html += '<input type="hidden" name="product[' + product_row + '][option][' + option['product_option_id'] + ']" value="' + option['value'] + '" />';
			}
		}
	}
	html += '	</td>';
	html += '  	<td><input type="hidden" name="product[' + product_row + '][quantity]" value="' + json['quantity'] + '" /></td>';
		
	$('#sub_total_tr').before(html);
	cartAPI();
}

function removeProductLine(order_product_id) {
	row_count = $('#row-count-' + order_product_id).val();
	$('#hidden-cart-' + order_product_id).remove();
	$('#row-product-' + order_product_id).remove();
	
	for (c = row_count++; c <= product_row + 1; c++) {
		// Product input
		row = c - 1 ;
		str = 'product[' + row + ']';
		
		$('input[name=\'product[' + c + '][product_id]\']').attr('name', str + '[product_id]');
		// Quantity input
		$('input[name=\'product[' + c + '][quantity]\']').attr('name', str + '[quantity]');
		
		// Option input
		
		option_name = $('input[name^=\'product[' + c + '][option]\']').attr('name');
		
		if (option_name) {
			n = option_name.indexOf(c);
			first = option_name.slice(0,n);
			last = option_name.slice((n+1),option_name.length);
			new_option_name = first.concat(row);
			new_option_name = new_option_name.concat(last);
			$('input[name^=\'product[' + c + '][option]\']').attr('name', new_option_name);
		}
	}
	cartAPI();
}

function closeInput(param, param_id, align) {
	
	var new_value = $('#' + param + param_id +' input').val();
	
	if ( param !== 'product_name')	{	
	 html = '<td class="text-' + align + '" id="' + param + (param !== 'shipping' ? param_id : '') + '" onclick="inputValue(\'' + param + '\',\'' + param_id + '\', \'' + align + '\');">' +  new_value + '</td>';
	} else {
		html = '<span id="' + param +  param_id + '" onclick="inputValue(\'' + param + '\',\'' + param_id + '\', \'' + align + '\');">' +  new_value + '</td>';
	} 
	
	$('#' + param + param_id).fadeOut('slow');				
	
	if (param !== 'shipping-method-ot') {
		$('#' + param + param_id).replaceWith(html);
	}

	if (param == 'shipping-method-ot') {
		html = '<td colspan="5" class="text-' + align + '" id="' + param + (param !== 'shipping' ? param_id : '') + '" onclick="inputValue(\'' + param + '\',\'' + param_id + '\', \'' + align + '\');">' +  new_value + '</td>';
		$('#' + param + param_id).replaceWith(html);
		html = '<td class="text-left" id="shipping_method' + param_id + '" onclick="inputValue(\'shipping_method\',\'' + param_id + '\', \'left\');">' +  new_value + '</td>';
		$('#shipping_method' + param_id).replaceWith(html);
	}
	if (param == 'shipping_method') {
		html = '<td colspan="5" class="text-right" id="shipping-method-ot' + param_id + '" onclick="inputValue(\'shipping-method-ot\',\'' + param_id + '\', \'right\');">' + new_value + ':</td>';
		$('#shipping-method-ot' + param_id).replaceWith(html);
	}	
}

function alertJson(action, json) {
	$('.alert').remove();
	
	if (json['error']) {
		$('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
	}
	
	if (json['msg']) {
		$('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['msg'] + '</div>');
	}
}

function replaceTotals(param, param_id, json) {
	if ( param == 'qty') {
		$('input[name=\'product[' + $('#row-count-' + param_id).val() + '][quantity]\']').val($('#qty' + param_id).html());
		cartAPI();
	}
	if ( param == 'price') {
		$('#price' + param_id).text(json['price']);
		$('#product_total' + param_id).text(json['product_total']);
		cartAPI();
	}
	if ( param == 'product_total') {
		$('#price' + param_id).text(json['price']);
		$('#product_total' + param_id).text(json['product_total']);
		cartAPI();
	}
	if ( param == 'shipping') {
		$('#shipping').text(json['shipping']);
		cartAPI();
	}
	if ( param == 'remove-product') {
		removeProductLine(param_id);
	}
}

$('#add-product-form input[name=\'product\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',			
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id'],
						model: item['model'],
						option: item['option'],
						price: item['price']						
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('#add-product-form input[name=\'product\']').val(item['label']);
		$('#add-product-form input[name=\'product_id\']').val(item['value']);
		$('#add-product-form input[name=\'product_price\']').val(item['price']);

		if (item['option'] != '') {
			html  = '<fieldset form="add-product-form">';
			html += '  <legend><?php echo $entry_option; ?></legend>';
	  
			for (i = 0; i < item['option'].length; i++) {
				option = item['option'][i];
		
				if (option['type'] == 'select') {
					html += '<div class="form-group' + (option['required'] ? ' required' : '') + '">';
					html += '  <label class="col-sm-2 control-label" for="input-option' + option['product_option_id'] + '">' + option['name'] + '</label>';
					html += '  <div class="col-sm-10">';
					html += '    <select name="option[' + option['product_option_id'] + ']" id="input-option' + option['product_option_id'] + '" class="form-control">';
					html += '      <option value=""><?php echo $text_select; ?></option>';
		
					for (j = 0; j < option['product_option_value'].length; j++) {
						option_value = option['product_option_value'][j];
				
						html += '<option value="' + option_value['product_option_value_id'] + '">' + option_value['name'];
				
						if (option_value['price']) {
							html += ' (' + option_value['price_prefix'] + option_value['price'] + ')';
						}
				
						html += '</option>';
					}
				
					html += '    </select>';
					html += '  </div>';
					html += '</div>';
				}
		
				if (option['type'] == 'radio') {
					html += '<div class="form-group' + (option['required'] ? ' required' : '') + '">';
					html += '  <label class="col-sm-2 control-label" for="input-option' + option['product_option_id'] + '">' + option['name'] + '</label>';
					html += '  <div class="col-sm-10">';
					html += '    <select name="option[' + option['product_option_id'] + ']" id="input-option' + option['product_option_id'] + '" class="form-control">';
					html += '      <option value=""><?php echo $text_select; ?></option>';
		
					for (j = 0; j < option['product_option_value'].length; j++) {
						option_value = option['product_option_value'][j];
				
						html += '<option value="' + option_value['product_option_value_id'] + '">' + option_value['name'];
				
						if (option_value['price']) {
							html += ' (' + option_value['price_prefix'] + option_value['price'] + ')';
						}
				
						html += '</option>';
					}
				
					html += '    </select>';
					html += '  </div>';
					html += '</div>';
				}
			
				if (option['type'] == 'checkbox') {
					html += '<div class="form-group' + (option['required'] ? ' required' : '') + '">';
					html += '  <label class="col-sm-2 control-label">' + option['name'] + '</label>';
					html += '  <div class="col-sm-10">';
					html += '    <div id="input-option' + option['product_option_id'] + '">';
			
					for (j = 0; j < option['product_option_value'].length; j++) {
						option_value = option['product_option_value'][j];
				
						html += '<div class="checkbox">';
				
						html += '  <label><input type="checkbox" name="option[' + option['product_option_id'] + '][]" value="' + option_value['product_option_value_id'] + '" /> ' + option_value['name'];
				
						if (option_value['price']) {
							html += ' (' + option_value['price_prefix'] + option_value['price'] + ')';
						}
				
						html += '  </label>';
						html += '</div>';
					}
								
					html += '    </div>';
					html += '  </div>';
					html += '</div>';
				}
	
				if (option['type'] == 'image') {
					html += '<div class="form-group' + (option['required'] ? ' required' : '') + '">';
					html += '  <label class="col-sm-2 control-label" for="input-option' + option['product_option_id'] + '">' + option['name'] + '</label>';
					html += '  <div class="col-sm-10">';
					html += '    <select name="option[' + option['product_option_id'] + ']" id="input-option' + option['product_option_id'] + '" class="form-control">';
					html += '      <option value=""><?php echo $text_select; ?></option>';
		
					for (j = 0; j < option['product_option_value'].length; j++) {
						option_value = option['product_option_value'][j];
				
						html += '<option value="' + option_value['product_option_value_id'] + '">' + option_value['name'];
				
						if (option_value['price']) {
							html += ' (' + option_value['price_prefix'] + option_value['price'] + ')';
						}
				
						html += '</option>';
					}
				
					html += '    </select>';					
					html += '  </div>';
					html += '</div>';
				}
				
				if (option['type'] == 'text') {
					html += '<div class="form-group' + (option['required'] ? ' required' : '') + '">';
					html += '  <label class="col-sm-2 control-label" for="input-option' + option['product_option_id'] + '">' + option['name'] + '</label>';
					html += '  <div class="col-sm-10"><input type="text" name="option[' + option['product_option_id'] + ']" value="' + option['value'] + '" id="input-option' + option['product_option_id'] + '" class="form-control" /></div>';
					html += '</div>';					
				}
		
				if (option['type'] == 'textarea') {
					html += '<div class="form-group' + (option['required'] ? ' required' : '') + '">';
					html += '  <label class="col-sm-2 control-label" for="input-option' + option['product_option_id'] + '">' + option['name'] + '</label>';
					html += '  <div class="col-sm-10"><textarea name="option[' + option['product_option_id'] + ']" rows="5" id="input-option' + option['product_option_id'] + '" class="form-control">' + option['value'] + '</textarea></div>';
					html += '</div>';
				}
		
				if (option['type'] == 'file') {
					html += '<div class="form-group' + (option['required'] ? ' required' : '') + '">';
					html += '  <label class="col-sm-2 control-label">' + option['name'] + '</label>';
					html += '  <div class="col-sm-10">';
					html += '    <button type="button" id="button-upload' + option['product_option_id'] + '" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>';
					html += '    <input type="hidden" name="option[' + option['product_option_id'] + ']" value="' + option['value'] + '" id="input-option' + option['product_option_id'] + '" />';
					html += '  </div>';
					html += '</div>';
				}
		
				if (option['type'] == 'date') {
					html += '<div class="form-group' + (option['required'] ? ' required' : '') + '">';
					html += '  <label class="col-sm-2 control-label" for="input-option' + option['product_option_id'] + '">' + option['name'] + '</label>';
					html += '  <div class="col-sm-3"><div class="input-group date"><input type="text" name="option[' + option['product_option_id'] + ']" value="' + option['value'] + '" placeholder="' + option['name'] + '" data-date-format="YYYY-MM-DD" id="input-option' + option['product_option_id'] + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
					html += '</div>';
				}
		
				if (option['type'] == 'datetime') {
					html += '<div class="form-group' + (option['required'] ? ' required' : '') + '">';
					html += '  <label class="col-sm-2 control-label" for="input-option' + option['product_option_id'] + '">' + option['name'] + '</label>';
					html += '  <div class="col-sm-3"><div class="input-group datetime"><input type="text" name="option[' + option['product_option_id'] + ']" value="' + option['value'] + '" placeholder="' + option['name'] + '" data-date-format="YYYY-MM-DD HH:mm" id="input-option' + option['product_option_id'] + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
					html += '</div>';					
				}
		
				if (option['type'] == 'time') {
					html += '<div class="form-group' + (option['required'] ? ' required' : '') + '">';
					html += '  <label class="col-sm-2 control-label" for="input-option' + option['product_option_id'] + '">' + option['name'] + '</label>';
					html += '  <div class="col-sm-3"><div class="input-group time"><input type="text" name="option[' + option['product_option_id'] + ']" value="' + option['value'] + '" placeholder="' + option['name'] + '" data-date-format="HH:mm" id="input-option' + option['product_option_id'] + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
					html += '</div>';					
				}
			}
	
			html += '</fieldset>';
	
			$('#option').html(html);
	
			$('.date').datetimepicker({
				pickTime: false
			});
	
			$('.datetime').datetimepicker({
				pickDate: true,
				pickTime: true
			});
	
			$('.time').datetimepicker({
				pickDate: false
			});
		} else {
			$('#option').html('');
		}		
	}	
});

//--></script>
<?php echo $footer; ?> 