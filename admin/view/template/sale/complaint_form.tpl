<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-return" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-return" class="form-horizontal">
          <fieldset>
            <legend>
              <?php echo $text_order; ?>
            </legend>
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-order-id">
                <?php echo $entry_order_id; ?>
              </label>
              <div class="col-sm-10">
                <input type="text" name="order_id" value="<?php echo $order_id; ?>" placeholder="<?php echo $entry_order_id; ?>" id="input-order-id" class="form-control" />
                <?php if ($error_order_id) { ?>
                <div class="text-danger">
                  <?php echo $error_order_id; ?>
                </div>
                <?php } ?>
              </div>
            </div>      
            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-firstname">
                <?php echo $entry_firstname; ?>
              </label>
              <div class="col-sm-10">
                <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" />
                <?php if ($error_firstname) { ?>
                <div class="text-danger">
                  <?php echo $error_firstname; ?>
                </div>
                <?php } ?>
              </div>
            </div>
            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-lastname">
                <?php echo $entry_middlename; ?>
              </label>
              <div class="col-sm-10">
                <input type="text" name="middlename" value="<?php echo $middlename; ?>" placeholder="<?php echo $entry_middlename; ?>" id="input-middlename" class="form-control" />                
              </div>
            </div>
            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-lastname">
                <?php echo $entry_lastname; ?>
              </label>
              <div class="col-sm-10">
                <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />
                <?php if ($error_lastname) { ?>
                <div class="text-danger">
                  <?php echo $error_lastname; ?>
                </div>
                <?php } ?>
              </div>
            </div>
            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-email">
                <?php echo $entry_email; ?>
              </label>
              <div class="col-sm-10">
                <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
                <?php if ($error_email) { ?>
                <div class="text-danger">
                  <?php echo $error_email; ?>
                </div>
                <?php  } ?>
              </div>
            </div>
            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-telephone">
                <?php echo $entry_telephone; ?>
              </label>
              <div class="col-sm-10">
                <input type="text" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $entry_telephone; ?>" id="input-telephone" class="form-control" />
                <?php if ($error_telephone) { ?>
                <div class="text-danger">
                  <?php echo $error_telephone; ?>
                </div>
                <?php  } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-opened">
                <?php echo $entry_opened; ?>
              </label>
              <div class="col-sm-10">
                <select name="opened" id="input-opened" class="form-control">
                  <?php if ($opened) { ?>
                  <option value="1" selected="selected">
                    <?php echo $text_opened; ?>
                  </option>
                  <option value="0">
                    <?php echo $text_unopened; ?>
                  </option>
                  <?php } else { ?>
                  <option value="1">
                    <?php echo $text_opened; ?>
                  </option>
                  <option value="0" selected="selected">
                    <?php echo $text_unopened; ?>
                  </option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-return-reason">
                <?php echo $entry_reason; ?>
              </label>
              <div class="col-sm-10">
                <select name="reason_id" id="input-reason" class="form-control">
                  <option value=""> --- الرجاء الاختيار --- </option>
                  <option value="1">تتعلق بقسم المالية</option>
                  <option value="2">تتعلق بالإرجاع</option>
                  <option value="3">تتعلق بالشحن</option>
                  <option value="4">الطلبية فيها خطأ</option>
                  <option value="5">سبب آخر</option>
                </select>
                <?php if ($error_reason) { ?>
                <div class="text-danger">
                  <?php echo $error_reason; ?>
                </div>
                <?php  } ?>
              </div>
            </div>
          </fieldset>
          <fieldset>
            <legend style="display:none">
              <?php echo $text_product; ?>
            </legend>
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-comment">
                <?php echo $entry_comment; ?>
              </label>
              <div class="col-sm-10">
                <textarea name="comment" rows="5" placeholder="
                  <?php echo $entry_comment; ?>" id="input-comment" class="form-control"><?php echo $comment; ?></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-admin-comment">
                <?php echo $entry_admin_comment; ?>
              </label>
              <div class="col-sm-10">
                <textarea name="admin_comment" rows="5" placeholder="<?php echo $entry_admin_comment; ?>" id="input-comment" class="form-control"><?php echo $admin_comment; ?></textarea>
              </div>
            </div>
          </fieldset>
          <input type="hidden" id="hndreason" value="<?php echo $reason_id; ?>" />
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript">
  <!--
$('.date').datetimepicker({
	pickTime: false
});
$('#input-reason').val($('#hndreason').val());
//--></script></div>
<?php echo $footer; ?>