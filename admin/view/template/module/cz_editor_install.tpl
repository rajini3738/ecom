<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
    <div class="install-block" style="text-align: center;height: 100%;background-color: #E8E8E8;">
        <a href="<?php echo $href_install_module; ?>" class="btn btn-success" style="margin-top: 20%;margin-bottom: 20%;"><i class="fa fa-plus-circle"></i> <?php echo $text_install_module; ?></a>
    </div>
</div>
<?php echo $footer; ?>