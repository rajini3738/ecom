<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/cron.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons">
        <a href="<?php echo $insert; ?>" class="button"><?php echo $button_insert; ?></a>
        <a onclick="$('form').submit();" class="button"><?php echo $button_delete; ?></a>
        <a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a>
      </div>
    </div>
    <div class="content">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>

              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>

              <td class="left"><?php if ($sort == 'cron_file') { ?>
                <a href="<?php echo $sort_file; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_file; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_file; ?>"><?php echo $column_file; ?></a>
                <?php } ?></td>

              <td class="center"><?php if ($sort == 'cron_datetime') { ?>
                <a href="<?php echo $sort_datetime; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_datetime; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_datetime; ?>"><?php echo $column_datetime; ?></a>
                <?php } ?></td>

              <td class="right"><?php if ($sort == 'cron_every') { ?>
                <a href="<?php echo $sort_every; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_every; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_every; ?>"><?php echo $column_every; ?></a>
                <?php } ?></td>

              <td class="center"><?php if ($sort == 'cron_status') { ?>
                <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                <?php } ?></td>

              <td class="center"><?php echo $column_action; ?></td>

            </tr>
          </thead>
          <tbody>
            <?php if ($cron_jobs) { ?>
            <?php foreach ($cron_jobs as $cron_job) { ?>
            <tr>

              <td style="text-align: center;"><?php if ($cron_job['selected']) { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $cron_job['cron_id']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $cron_job['cron_id']; ?>" />
                <?php } ?></td>

              <td class="left"><?php echo $cron_job['cron_file']; ?></td>

              <td class="center"><?php echo $cron_job['cron_datetime']; ?></td>

              <td class="right"><?php echo $cron_job['cron_every']; ?></td>

              <td class="center">
                <?php echo ( $cron_job['cron_status'] ? $text_enabled : $text_disabled ); ?>                
              </td>

              <td class="center"><?php foreach ($cron_job['action'] as $action) { ?>
                [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                <?php } ?></td>

            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="5"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<?php echo $footer; ?>