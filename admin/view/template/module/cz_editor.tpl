<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <a href="<?php echo $country; ?>" class="btn btn-info"><i class="fa fa-globe"></i> <?php echo $button_country; ?></a>
        <a href="<?php echo $zone; ?>" class="btn btn-info"><i class="fa fa-map-marker"></i> <?php echo $button_zone; ?></a>
        <a href="<?php echo $clear; ?>" data-toggle="tooltip" title="<?php echo $button_clear_cache; ?>" class="btn btn-warning"><i class="fa fa-trash-o"></i></a>
        <button type="submit" form="form-cz-editor" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a onclick="$('#apply').attr('value', '1'); $('#form-cz-editor').submit();" data-toggle="tooltip" title="<?php echo $button_apply; ?>" class="btn btn-success"><i class="fa fa-refresh"></i></a>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><i class="fa fa-globe" style="color:#00BFFF;"></i> <?php echo $heading_title; ?> <div style="position: absolute;margin-top:3px;margin-left:272px;font-size:12px;"><a href="http://vanstudio.co.ua" target="_blank">by vanstudio</a></div></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-cz-editor" class="form-horizontal">
          <input type="hidden" name="apply" id="apply" value="0">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><i class="fa fa-bars tab-icon"></i> <?php echo $tab_general; ?></a></li>
            <li><a href="#tab-developer" data-toggle="tab"><i class="fa fa-life-ring tab-icon"></i> <?php echo $tab_developer; ?></a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-general">
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-country-multilang"><?php echo $entry_country_multilang; ?></label>
                <div class="col-sm-10">
                  <select name="cz_editor_country_multilang" id="input-country-multilang" class="form-control">
                    <option value="1" <?php if (isset($cz_editor_country_multilang) && $cz_editor_country_multilang == 1) { ?>selected="selected"<?php } ?>><?php echo $text_enabled; ?></option>
                    <option value="0" <?php if (!isset($cz_editor_country_multilang) || $cz_editor_country_multilang == 0) { ?>selected="selected"<?php } ?>><?php echo $text_disabled; ?></option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-sort-country"><?php echo $entry_sort_country; ?></label>
                <div class="col-sm-10">
                  <select name="cz_editor_sort_country" id="input-sort-country" class="form-control">
                    <option value="0" <?php if (!isset($cz_editor_sort_country) || $cz_editor_sort_country == 0) { ?>selected="selected"<?php } ?>><?php echo $text_name_asc; ?></option>
                    <option value="1" <?php if (isset($cz_editor_sort_country) && $cz_editor_sort_country == 1) { ?>selected="selected"<?php } ?>><?php echo $text_name_desc; ?></option>
                    <option value="2" <?php if (isset($cz_editor_sort_country) && $cz_editor_sort_country == 2) { ?>selected="selected"<?php } ?>><?php echo $text_sort_order_asc; ?></option>
                    <option value="3" <?php if (isset($cz_editor_sort_country) && $cz_editor_sort_country == 3) { ?>selected="selected"<?php } ?>><?php echo $text_sort_order_desc; ?></option>
                    <option value="4" <?php if (isset($cz_editor_sort_country) && $cz_editor_sort_country == 4) { ?>selected="selected"<?php } ?>><?php echo $text_sort_order_zero; ?></option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-multilang"><?php echo $entry_zone_multilang; ?></label>
                <div class="col-sm-10">
                  <select name="cz_editor_zone_multilang" id="input-zone-multilang" class="form-control">
                    <option value="1" <?php if (isset($cz_editor_zone_multilang) && $cz_editor_zone_multilang == 1) { ?>selected="selected"<?php } ?>><?php echo $text_enabled; ?></option>
                    <option value="0" <?php if (!isset($cz_editor_zone_multilang) || $cz_editor_zone_multilang == 0) { ?>selected="selected"<?php } ?>><?php echo $text_disabled; ?></option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-sort-zone"><?php echo $entry_sort_zone; ?></label>
                <div class="col-sm-10">
                  <select name="cz_editor_sort_zone" id="input-sort-zone" class="form-control">
                    <option value="0" <?php if (!isset($cz_editor_sort_zone) || $cz_editor_sort_zone == 0) { ?>selected="selected"<?php } ?>><?php echo $text_name_asc; ?></option>
                    <option value="1" <?php if (isset($cz_editor_sort_zone) && $cz_editor_sort_zone == 1) { ?>selected="selected"<?php } ?>><?php echo $text_name_desc; ?></option>
                    <option value="2" <?php if (isset($cz_editor_sort_zone) && $cz_editor_sort_zone == 2) { ?>selected="selected"<?php } ?>><?php echo $text_sort_order_asc; ?></option>
                    <option value="3" <?php if (isset($cz_editor_sort_zone) && $cz_editor_sort_zone == 3) { ?>selected="selected"<?php } ?>><?php echo $text_sort_order_desc; ?></option>
                    <option value="4" <?php if (isset($cz_editor_sort_zone) && $cz_editor_sort_zone == 4) { ?>selected="selected"<?php } ?>><?php echo $text_sort_order_zero; ?></option>
                  </select>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="tab-developer">
              <div class="form-group">
                  <label class="control-label col-sm-2">
                    <span data-toggle="tooltip" data-original-title="<?php echo $help_activation; ?>"><?php echo $entry_activation; ?></span> :
                  </label>
                <label class="control-label col-sm-1" for="input-order_id"><span data-toggle="tooltip" data-original-title="<?php echo $help_order_id; ?>"><?php echo $entry_order_id; ?></span></label>
                <div class="col-sm-2">
                  <input type="text" name="cz_editor_order_id" value="<?php echo isset($cz_editor_order_id)?$cz_editor_order_id:''; ?>" placeholder="<?php echo $entry_order_id; ?>" id="input-order_id" class="form-control" />
                </div>
                <label class="control-label col-sm-1" for="input-url"><span data-toggle="tooltip" data-original-title="<?php echo $help_url; ?>"><?php echo $entry_url; ?></span></label>
                <div class="col-sm-3">
                  <input type="text" name="cz_editor_url" value="<?php echo isset($cz_editor_url)?$cz_editor_url:''; ?>" placeholder="http://your-site-name.com" id="input-url" class="form-control" />
                  <div class="text-info"><?php echo $text_url_note; ?></div>
                </div>
              </div>
              <div class="container-fluid">
                <div class="alert alert-info">
                  <div id="message_wrapper"></div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--

  function getNotifications() {
    $('#message_wrapper').empty().html('<i class="fa fa-spinner"> <?php echo $text_load_message; ?>');
    setTimeout(
      function(){
        $.ajax({
          type: 'GET',
          url: 'index.php?route=module/cz_editor/getNotifications&token=<?php echo $token; ?>',
          dataType: 'json',
          success: function(json) {
            if (json['error']) {
              $('#message_wrapper').empty().html(json['error']+' <span style="cursor:pointer;float:right;" onclick="getNotifications();"><i class="fa fa-refresh"></i> <?php echo $text_retry; ?></span>');
            } else if (json['message']) {
              $('#message_wrapper').html(json['message']);
            } else {
              $('#message_wrapper').html('<?php echo $text_no_message; ?>');
            }
          },
          failure: function(){
            $('#message_wrapper').html('<?php echo $error_message; ?> <span style="cursor:pointer;float:right;" onclick="getNotifications();"><i class="fa fa-refresh"></i> <?php echo $text_retry; ?></span>');
          },
          error: function() {
            $('#message_wrapper').html('<?php echo $error_message; ?> <span style="cursor:pointer;float:right;" onclick="getNotifications();"><i class="fa fa-refresh"></i> <?php echo $text_retry; ?></span>');
          }
        });
      },
      500
    );
  }

  $(document).ready(function() {
    getNotifications();
  });
  //--></script>
<style>
  li.active .tab-icon{
    color: #1E91CF;
  }
</style>
<?php echo $footer; ?>