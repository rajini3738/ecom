<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <a href="<?php echo $setting; ?>" class="btn btn-info"><i class="fa fa-cog"></i> <?php echo $button_setting; ?></a>
        <a href="<?php echo $country; ?>" class="btn btn-info"><i class="fa fa-globe"></i> <?php echo $button_country; ?></a>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_disable; ?>" class="btn btn-danger" onclick="$('#form-zone').attr('action', '<?php echo $disable; ?>'); $('#form-zone').submit();"><i class="fa fa-minus"></i></button>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_enable; ?>" class="btn btn-success" onclick="$('#form-zone').attr('action', '<?php echo $enable; ?>'); $('#form-zone').submit();"><i class="fa fa-check"></i></button>
        <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-zone').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><i class="fa fa-globe" style="color:#00BFFF;"></i> <?php echo $heading_title; ?> <div style="position: absolute;margin-top:3px;margin-left:272px;font-size:12px;"><a href="http://vanstudio.co.ua" target="_blank">by vanstudio</a></div></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_zone_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-country-id"><?php echo $entry_country_id; ?></label>
                <input type="text" name="filter_country_id" value="<?php echo $filter_country_id; ?>" placeholder="<?php echo $entry_country_id; ?>" id="input-country-id" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-country"><?php echo $entry_country_name; ?></label>
                <input type="text" name="filter_country" value="<?php echo $filter_country; ?>" placeholder="<?php echo $entry_country_name; ?>" id="input-country" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-zone-id"><?php echo $entry_zone_id; ?></label>
                <input type="text" name="filter_zone_id" value="<?php echo $filter_zone_id; ?>" placeholder="<?php echo $entry_zone_id; ?>" id="input-zone-id" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-name"><?php echo $entry_zone_name; ?></label>
                <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $entry_zone_name; ?>" id="input-name" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-code"><?php echo $entry_code; ?></label>
                <input type="text" name="filter_code" value="<?php echo $filter_code; ?>" placeholder="<?php echo $entry_code; ?>" id="input-code" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-status"><?php echo $entry_status; ?></label>
                <select name="filter_status" id="input-status" class="form-control">
                  <option value="*"></option>
                  <option value="1" <?php if ($filter_status) { ?>selected="selected"<?php } ?>><?php echo $text_enabled; ?></option>
                  <option value="0" <?php if (!$filter_status && !is_null($filter_status)) { ?>selected="selected"<?php } ?>><?php echo $text_disabled; ?></option>
                </select>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="pull-right">
                <button type="button" id="button-clear-filter" class="btn btn-default"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></button>
                <button type="button" id="button-filter" class="btn btn-primary"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
              </div>
            </div>
          </div>
        </div>
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-zone">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
              <tr>
                <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                <td class="text-left"><?php if ($sort == 'c.country_id') { ?>
                  <a href="<?php echo $sort_country_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_country_id; ?></a>
                  <?php } else { ?>
                  <a href="<?php echo $sort_country_id; ?>"><?php echo $column_country_id; ?></a>
                  <?php } ?></td>
                <td class="text-left"><?php if ($sort == 'c.name') { ?>
                  <a href="<?php echo $sort_country; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_country_name; ?></a>
                  <?php } else { ?>
                  <a href="<?php echo $sort_country; ?>"><?php echo $column_country_name; ?></a>
                  <?php } ?></td>
                <td class="text-left"><?php if ($sort == 'z.zone_id') { ?>
                  <a href="<?php echo $sort_zone_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_zone_id; ?></a>
                  <?php } else { ?>
                  <a href="<?php echo $sort_zone_id; ?>"><?php echo $column_zone_id; ?></a>
                  <?php } ?></td>
                <td class="text-left"><?php if ($sort == 'z.name') { ?>
                  <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_zone_name; ?></a>
                  <?php } else { ?>
                  <a href="<?php echo $sort_name; ?>"><?php echo $column_zone_name; ?></a>
                  <?php } ?></td>
                <?php if($cz_editor_zone_multilang){ ?>
                <td class="text-left"><?php echo $column_name_multi; ?></td>
                <?php } ?>
                <td class="text-left"><?php if ($sort == 'z.code') { ?>
                  <a href="<?php echo $sort_code; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_code; ?></a>
                  <?php } else { ?>
                  <a href="<?php echo $sort_code; ?>"><?php echo $column_code; ?></a>
                  <?php } ?></td>
                <td class="text-right"><?php if ($sort == 'sort_order') { ?>
                  <a href="<?php echo $sort_sort_order; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_sort_order; ?></a>
                  <?php } else { ?>
                  <a href="<?php echo $sort_sort_order; ?>"><?php echo $column_sort_order; ?></a>
                  <?php } ?></td>
                <td class="text-right"><?php if ($sort == 'status') { ?>
                  <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                  <?php } else { ?>
                  <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                  <?php } ?></td>

                <td class="text-right"><?php echo $column_action; ?></td>
              </tr>
              </thead>
              <tbody>
              <?php if ($zones) { ?>
              <?php foreach ($zones as $zone) { ?>
              <tr>
                <td class="text-center"><?php if (in_array($zone['zone_id'], $selected)) { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $zone['zone_id']; ?>" checked="checked" />
                  <?php } else { ?>
                  <input type="checkbox" name="selected[]" value="<?php echo $zone['zone_id']; ?>" />
                  <?php } ?></td>
                <td class="text-left"><?php echo $zone['country_id']; ?></td>
                <td class="text-left"><?php echo $zone['country']; ?> <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].'/image/flags/' . strtolower($zone['iso_code_2']) . '.png')) { ?> <img src="<?php echo '/image/flags/' . strtolower($zone['iso_code_2']) . '.png'; ?>" title="<?php echo $zone['country']; ?>" /><?php } ?></td>
                <td class="text-left"><?php echo $zone['zone_id']; ?></td>
                <td class="text-left"><?php echo $zone['name']; ?></td>
                <?php if($cz_editor_zone_multilang){ ?>
                <td class="text-left">
                  <?php foreach($languages as $language) { ?>
                  <?php if(isset($zone['name_multi'][$language['language_id']])) { ?><img src="<?php echo file_exists('language/'.$language['code'].'/'.$language['code'].'.png')?'language/'.$language['code'].'/'.$language['code'].'.png':'view/image/flags/'.$language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $zone['name_multi'][$language['language_id']]['name']; ?></br><?php } ?>
                  <?php } ?>
                </td>
                <?php } ?>
                <td class="text-left"><?php echo $zone['code']; ?></td>
                <td class="text-right"><?php echo $zone['sort_order']; ?></td>
                <td class="text-right"><?php if($zone['status']){ ?><i class="fa fa-check" aria-hidden="true" style="color:#32CD32;"></i><?php } else { ?><i class="fa fa-minus" aria-hidden="true" style="color:#FF0000;"></i><?php } ?></td>
                <td class="text-right"><a href="<?php echo $zone['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="text-center" colspan="10"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript"><!--
  $('#button-filter').on('click', function() {
    var url = 'index.php?route=module/cz_editor/zone&token=<?php echo $token; ?>';

    var filter_country_id = $('input[name=\'filter_country_id\']').val();

    if (filter_country_id) {
      url += '&filter_country_id=' + encodeURIComponent(filter_country_id);
    }

    var filter_country = $('input[name=\'filter_country\']').val();

    if (filter_country) {
      url += '&filter_country=' + encodeURIComponent(filter_country);
    }

    var filter_zone_id = $('input[name=\'filter_zone_id\']').val();

    if (filter_zone_id) {
      url += '&filter_zone_id=' + encodeURIComponent(filter_zone_id);
    }

    var filter_name = $('input[name=\'filter_name\']').val();

    if (filter_name) {
      url += '&filter_name=' + encodeURIComponent(filter_name);
    }

    var filter_code = $('input[name=\'filter_code\']').val();

    if (filter_code) {
      url += '&filter_code=' + encodeURIComponent(filter_code);
    }

    var filter_status = $('select[name=\'filter_status\']').val();

    if (filter_status != '*') {
      url += '&filter_status=' + encodeURIComponent(filter_status);
    }

    location = url;
  });

  $('#button-clear-filter').on('click', function() {
    var url = 'index.php?route=module/cz_editor/zone&token=<?php echo $token; ?>';

    location = url;
  });
  //--></script>

<?php echo $footer; ?>