<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <a href="<?php echo $setting; ?>" class="btn btn-info"><i class="fa fa-cog"></i> <?php echo $button_setting; ?></a>
        <a href="<?php echo $zone; ?>" class="btn btn-info"><i class="fa fa-map-marker"></i> <?php echo $button_zone; ?></a>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_disable; ?>" class="btn btn-danger" onclick="$('#form-country').attr('action', '<?php echo $disable; ?>'); $('#form-country').submit();"><i class="fa fa-minus"></i></button>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_enable; ?>" class="btn btn-success" onclick="$('#form-country').attr('action', '<?php echo $enable; ?>'); $('#form-country').submit();"><i class="fa fa-check"></i></button>
        <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-country').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><i class="fa fa-globe" style="color:#00BFFF;"></i> <?php echo $heading_title; ?> <div style="position: absolute;margin-top:3px;margin-left:272px;font-size:12px;"><a href="http://vanstudio.co.ua" target="_blank">by vanstudio</a></div></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_country_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-country_id"><?php echo $entry_country_id; ?></label>
                <input type="text" name="filter_country_id" value="<?php echo $filter_country_id; ?>" placeholder="<?php echo $entry_country_id; ?>" id="input-model" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-name"><?php echo $entry_country_name; ?></label>
                <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $entry_country_name; ?>" id="input-name" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-iso-code-2"><?php echo $entry_iso_code_2; ?></label>
                <input type="text" name="filter_iso_code_2" value="<?php echo $filter_iso_code_2; ?>" placeholder="<?php echo $entry_iso_code_2; ?>" id="input-iso-code-2" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-iso-code-3"><?php echo $entry_iso_code_3; ?></label>
                <input type="text" name="filter_iso_code_3" value="<?php echo $filter_iso_code_3; ?>" placeholder="<?php echo $entry_iso_code_3; ?>" id="input-iso-code-3" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-postcode-required"><?php echo $entry_postcode_required; ?></label>
                <select name="filter_postcode_required" id="input-postcode-required" class="form-control">
                  <option value="*"></option>
                  <option value="1" <?php if ($filter_postcode_required) { ?>selected="selected"<?php } ?>><?php echo $text_enabled; ?></option>
                  <option value="0" <?php if (!$filter_postcode_required && !is_null($filter_postcode_required)) { ?>selected="selected"<?php } ?>><?php echo $text_disabled; ?></option>
                </select>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-status"><?php echo $entry_status; ?></label>
                <select name="filter_status" id="input-status" class="form-control">
                  <option value="*"></option>
                  <option value="1" <?php if ($filter_status) { ?>selected="selected"<?php } ?>><?php echo $text_enabled; ?></option>
                  <option value="0" <?php if (!$filter_status && !is_null($filter_status)) { ?>selected="selected"<?php } ?>><?php echo $text_disabled; ?></option>
                </select>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="pull-right">
                <button type="button" id="button-clear-filter" class="btn btn-default"><i class="fa fa-eraser"></i> <?php echo $button_clear; ?></button>
                <button type="button" id="button-filter" class="btn btn-primary"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
              </div>
            </div>
          </div>
        </div>
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-country">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                    <td class="text-left"><?php if ($sort == 'country_id') { ?>
                    <a href="<?php echo $sort_country_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_country_id; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_country_id; ?>"><?php echo $column_country_id; ?></a>
                    <?php } ?></td>
                    <td class="text-left"><?php if ($sort == 'name') { ?>
                      <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_country_name; ?></a>
                      <?php } else { ?>
                      <a href="<?php echo $sort_name; ?>"><?php echo $column_country_name; ?></a>
                      <?php } ?></td>
                    <?php if($cz_editor_country_multilang){ ?>
                      <td class="text-left"><?php echo $column_name_multi; ?></td>
                    <?php } ?>
                    <td class="text-left"><?php if ($sort == 'iso_code_2') { ?>
                      <a href="<?php echo $sort_iso_code_2; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_iso_code_2; ?></a>
                      <?php } else { ?>
                      <a href="<?php echo $sort_iso_code_2; ?>"><?php echo $column_iso_code_2; ?></a>
                      <?php } ?></td>
                    <td class="text-left"><?php if ($sort == 'iso_code_3') { ?>
                    <a href="<?php echo $sort_iso_code_3; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_iso_code_3; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_iso_code_3; ?>"><?php echo $column_iso_code_3; ?></a>
                    <?php } ?></td>
                    <td class="text-right">
                        <a href="<?php echo $sort_sort_order; ?>" <?php if ($sort == 'sort_order') { ?>class="<?php echo strtolower($order); ?>"<?php } ?>><?php echo $column_sort_order; ?></a>
                    </td>
                    <td class="text-right">
                        <a href="<?php echo $sort_postcode_required; ?>" <?php if ($sort == 'postcode_required') { ?>class="<?php echo strtolower($order); ?>"<?php } ?>><?php echo $column_postcode; ?></a>
                    </td>
                    <td class="text-right">
                        <a href="<?php echo $sort_status; ?>" <?php if ($sort == 'status') { ?>class="<?php echo strtolower($order); ?>"<?php } ?>><?php echo $column_status; ?></a>
                    </td>
                  <td class="text-right"><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($countries) { ?>
                <?php foreach ($countries as $country) { ?>
                <tr>
                  <td class="text-center"><?php if (in_array($country['country_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $country['country_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $country['country_id']; ?>" />
                    <?php } ?>
                  </td>
                  <td class="text-left"><?php echo $country['country_id']; ?></td>
                  <td class="text-left"><?php echo $country['name']; ?> <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].'/image/flags/' . strtolower($country['iso_code_2']) . '.png')) { ?><img src="<?php echo '/image/flags/' . strtolower($country['iso_code_2']) . '.png'; ?>" title="<?php echo $country['name']; ?>" /><?php } ?></td>
                  <?php if($cz_editor_country_multilang){ ?>
                  <td class="text-left">
                    <?php foreach($languages as $language) { ?>
                    <?php if(isset($country['name_multi'][$language['language_id']])) { ?><img src="<?php echo file_exists('language/'.$language['code'].'/'.$language['code'].'.png')?'language/'.$language['code'].'/'.$language['code'].'.png':'view/image/flags/'.$language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $country['name_multi'][$language['language_id']]['name']; ?></br><?php } ?>
                    <?php } ?>
                  </td>
                  <?php } ?>
                  <td class="text-left"><?php echo $country['iso_code_2']; ?></td>
                  <td class="text-left"><?php echo $country['iso_code_3']; ?></td>
                  <td class="text-right"><?php echo $country['sort_order']; ?></td>
                  <td class="text-right"><?php if($country['postcode_required']){ ?><i class="fa fa-check" aria-hidden="true" style="color:#32CD32;"></i><?php } else { ?><i class="fa fa-minus" aria-hidden="true" style="color:#FF0000;"></i><?php } ?></td>
                  <td class="text-right"><?php if($country['status']){ ?><i class="fa fa-check" aria-hidden="true" style="color:#32CD32;"></i><?php } else { ?><i class="fa fa-minus" aria-hidden="true" style="color:#FF0000;"></i><?php } ?></td>
                  <td class="text-right"><a href="<?php echo $country['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="10"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript"><!--
  $('#button-filter').on('click', function() {
    var url = 'index.php?route=module/cz_editor/country&token=<?php echo $token; ?>';

    var filter_country_id = $('input[name=\'filter_country_id\']').val();

    if (filter_country_id) {
      url += '&filter_country_id=' + encodeURIComponent(filter_country_id);
    }

    var filter_name = $('input[name=\'filter_name\']').val();

    if (filter_name) {
      url += '&filter_name=' + encodeURIComponent(filter_name);
    }

    var filter_iso_code_2 = $('input[name=\'filter_iso_code_2\']').val();

    if (filter_iso_code_2) {
      url += '&filter_iso_code_2=' + encodeURIComponent(filter_iso_code_2);
    }

    var filter_iso_code_3 = $('input[name=\'filter_iso_code_3\']').val();

    if (filter_iso_code_3) {
      url += '&filter_iso_code_3=' + encodeURIComponent(filter_iso_code_3);
    }

    var filter_postcode_required = $('select[name=\'filter_postcode_required\']').val();

    if (filter_postcode_required != '*') {
      url += '&filter_postcode_required=' + encodeURIComponent(filter_postcode_required);
    }

    var filter_status = $('select[name=\'filter_status\']').val();

    if (filter_status != '*') {
      url += '&filter_status=' + encodeURIComponent(filter_status);
    }

    location = url;
  });

  $('#button-clear-filter').on('click', function() {
    var url = 'index.php?route=module/cz_editor/country&token=<?php echo $token; ?>';

    location = url;
  });
  //--></script>
            
<?php echo $footer; ?>