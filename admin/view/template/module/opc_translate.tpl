<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-module_opc_translate" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-module_opc_translate" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="module_opc_translate_status" id="input-status" class="form-control">
                <?php if ($module_opc_translate_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-admin"><?php echo $entry_admin; ?></label>
            <div class="col-sm-10">
              <select name="module_opc_translate_admin" id="input-admin" class="form-control">
                <?php if ($module_opc_translate_admin) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-admin_position"><?php echo $entry_admin_position; ?></label>
            <div class="col-sm-10">
              <select name="module_opc_translate_admin_position" id="input-admin_position" class="form-control">
                <option value="1" <?php if ($module_opc_translate_admin_position == 1) { ?> selected="selected" <?php } ?> ><?php echo $text_left; ?></option>
                <option value="2" <?php if ($module_opc_translate_admin_position == 2) { ?> selected="selected" <?php } ?> ><?php echo $text_center; ?></option>
                <option value="3" <?php if ($module_opc_translate_admin_position == 3) { ?> selected="selected" <?php } ?> ><?php echo $text_right; ?></option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-catalog"><?php echo $entry_catalog; ?></label>
            <div class="col-sm-10">
              <select name="module_opc_translate_catalog" id="input-catalog" class="form-control">
                <?php if ($module_opc_translate_catalog) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-catalog_position"><?php echo $entry_catalog_position; ?></label>
            <div class="col-sm-10">
              <select name="module_opc_translate_catalog_position" id="input-catalog_position" class="form-control">
                <option value="1" <?php if ($module_opc_translate_catalog_position == 1) { ?> selected="selected" <?php } ?> ><?php echo $text_left; ?></option>
                <option value="2" <?php if ($module_opc_translate_catalog_position == 2) { ?> selected="selected" <?php } ?> ><?php echo $text_center; ?></option>
                <option value="3" <?php if ($module_opc_translate_catalog_position == 3) { ?> selected="selected" <?php } ?> ><?php echo $text_right; ?></option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-custom_code_status"><?php echo $entry_custom_code_status; ?></label>
            <div class="col-sm-10">
              <select name="module_opc_translate_custom_code_status" id="input-custom_code_status" class="form-control">
                <?php if ($module_opc_translate_custom_code_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="alert alert-info"><i class="fa fa-exclamation-circle"></i> <?php echo $text_info; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-custom_code"><?php echo $entry_custom_code; ?></label>
            <div class="col-sm-10">
              <textarea name="module_opc_translate_custom_code" placeholder="<?php echo $entry_custom_code; ?>" id="input-custom_code" class="form-control" rows="10"><?php echo $module_opc_translate_custom_code; ?></textarea>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
