<?php
class ModelCatalogBids extends Model {
	public function getBids($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_bid WHERE product_id = '" . (int)$product_id . "' ORDER BY bid DESC");

		return $query->rows;
	}
  public function getTotalBids($product_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_bid WHERE product_id = '" . (int)$product_id . "'");

		return $query->row['total'];
	}
  public function selecktBid($customer_id, $product_id, $bid) {
    $current_customer =  $this->db->query("SELECT customer_id FROM " . DB_PREFIX . "customer_bid WHERE product_id = '" . (int)$product_id . "' AND bid_status = '1'");
    $currents = $current_customer->rows;
    foreach ($currents as $current) {
		$current_id = $current['customer_id'];
		if ($current_id !== $customer_id) {
			$query = $this->db->query("UPDATE " . DB_PREFIX . "customer_bid SET bid_status = '0' WHERE product_id = '" . (int)$product_id . "' AND customer_id = '" . (int)$current_id . "'");
		}
	}
	$query = $this->db->query("UPDATE " . DB_PREFIX . "customer_bid SET bid_status = '1' WHERE product_id = '" . (int)$product_id . "' AND customer_id = '" . (int)$customer_id . "'");
	$query = $this->db->query("UPDATE " . DB_PREFIX . "product SET auc_customer_id = '" . (int)$customer_id . "', price = '" . (float)$bid . "' WHERE product_id = '" . (int)$product_id . "'");
    //Alert Mail   �� �����
    // Load the language for any mails that might be required to be sent out
	$language = new Language($this->config->get('config_language'));
	$language->load($this->config->get('config_language'));
	$language->load('mail/winbid');
        
	$subject = sprintf($language->get('text_winnbid'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
	
	// HTML Mail
    $produt_name = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");
    $data['text_greeting'] = $language->get('text_greeting');
    $data['text_greeting_a'] = $language->get('text_greeting_a');
    $data['produt_name'] = $produt_name->row['name'];
    $data['produt_url'] = 'https://www.eleganceoud.com/index.php?route=account/winningbids';
    $data['title'] = $language->get('text_winnbid');
	$data['image'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');
	$data['store_name'] = $this->config->get('config_name');
	$data['store_url'] = $this->config->get('config_url');
	// Text
	$text  = $language->get('text_new_received') . "\n\n";
    
    $customer_info = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");
	$html = $this->load->view('mail/winbid.tpl', $data);
    //print_r($html);
    $mail = new Mail();
	$mail->protocol = $this->config->get('config_mail_protocol');
	$mail->parameter = $this->config->get('config_mail_parameter');
	$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
	$mail->smtp_username = $this->config->get('config_mail_smtp_username');
	$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
	$mail->smtp_port = $this->config->get('config_mail_smtp_port');
	$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
	$mail->setTo($customer_info->row['email']);
	$mail->setFrom($this->config->get('config_email'));
	$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
	$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
	$mail->setHtml($html);
	$mail->setText($text);
	$mail->send();
  }
  
  public function unselecktBid($product_id, $bn_price) {
	$query = $this->db->query("UPDATE " . DB_PREFIX . "customer_bid SET bid_status = '0' WHERE product_id = '" . (int)$product_id . "'");
	$query = $this->db->query("UPDATE " . DB_PREFIX . "product SET auc_customer_id = '0', price = '" . (float)$bn_price . "' WHERE product_id = '" . (int)$product_id . "'");
  }
  
}