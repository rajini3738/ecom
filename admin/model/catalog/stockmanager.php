<?php
class ModelCatalogStockManager extends Model {
	public function editStock($stock_id, $data) {
		$this->event->trigger('pre.admin.stock_manager.edit', $data);

		$this->db->query("UPDATE " . DB_PREFIX . "stock_manager SET quantity = '" . (int)$data['quantity'] . "', orginalquantity = '" . (int)$data['orginalquantity'] . "', adminquantity = '" . (int)$data['adminquantity'] . "', date_modified = NOW() WHERE stock_id = '" . (int)$stock_id . "'");
		
		$this->event->trigger('post.admin.stock_manager.edit', $product_id);
	}

	public function getStock($stock_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "stock_manager WHERE stock_id = '" . (int)$stock_id ."'");

		return $query->row;
	}

	public function getStocks($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "stock_manager";

		$implode = array();
		
		if (!empty($data['filter_name'])) {
			$implode[] = "product_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		
		if (!empty($data['filter_quantity'])) {
			$implode[] = "quantity = '" . (int)$data['filter_quantity'] . "'";
		}	
		
		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}

		$sql .= " GROUP BY stock_id";

		$sort_data = array(
			'product_name',
			'quantity'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY product_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalStocks($data = array()) {
		$sql = "SELECT COUNT(DISTINCT stock_id) AS total FROM " . DB_PREFIX . "stock_manager";

		$implode = array();
		
		if (!empty($data['filter_name'])) {
			$implode[] = "product_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		
		if (!empty($data['filter_quantity'])) {
			$implode[] = "quantity = '" . (int)$data['filter_quantity'] . "'";
		}	
		
		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
}
