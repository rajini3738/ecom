<?php
class ModelModuleCzEditor extends Model {
    public function addZone($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "zone SET sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', name = '" . $this->db->escape($data['name']) . "', code = '" . $this->db->escape($data['code']) . "', country_id = '" . (int)$data['country_id'] . "'");

        $zone_id = $this->db->getLastId();

        foreach ($data['zone_description'] as $language_id => $value) {
            if($value['name']){
                $this->db->query("INSERT INTO " . DB_PREFIX . "zone_description SET zone_id = '" . (int)$zone_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
            }
        }

        $this->cache->delete('zone');

        return $zone_id;
    }

    public function editZone($zone_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "zone SET sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', name = '" . $this->db->escape($data['name']) . "', code = '" . $this->db->escape($data['code']) . "', country_id = '" . (int)$data['country_id'] . "' WHERE zone_id = '" . (int)$zone_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "zone_description WHERE zone_id = '" . (int)$zone_id . "'");

        foreach ($data['zone_description'] as $language_id => $value) {
            if($value['name']){
                $this->db->query("INSERT INTO " . DB_PREFIX . "zone_description SET zone_id = '" . (int)$zone_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
            }
        }

        $this->cache->delete('zone');
    }

    public function statusZone($zone_id, $status) {
        $this->db->query("UPDATE " . DB_PREFIX . "zone SET status = '" . (int)$status . "' WHERE zone_id = '" . (int)$zone_id . "'");

        $this->cache->delete('zone');
    }

    public function deleteZone($zone_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "zone WHERE zone_id = '" . (int)$zone_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "zone_description WHERE zone_id = '" . (int)$zone_id . "'");

        $this->cache->delete('zone');
    }

    public function getZone($zone_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "zone WHERE zone_id = '" . (int)$zone_id . "'");

        return $query->row;
    }

    public function getZones($data = array()) {

        $sql = "SELECT *, z.name, z.sort_order AS sort_order, z.status AS status, c.name AS country FROM " . DB_PREFIX . "zone z LEFT JOIN " . DB_PREFIX . "country c ON (z.country_id = c.country_id) WHERE z.zone_id > 0";

        $sort_data = array(
            'c.country_id',
            'c.name',
            'z.zone_id',
            'z.name',
            'z.code',
            'z.sort_order',
            'z.status'
        );

        if (!empty($data['filter_country_id']) && !is_null($data['filter_country_id'])) {
            $sql .= " AND c.country_id = '" . (int)$data['filter_country_id'] . "'";
        }

        if (!empty($data['filter_country'])) {
            $sql .= " AND c.name LIKE '%" . $this->db->escape($data['filter_country']) . "%'";
        }

        if (!empty($data['filter_zone_id']) && !is_null($data['filter_zone_id'])) {
            $sql .= " AND z.zone_id = '" . (int)$data['filter_zone_id'] . "'";
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND z.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['filter_code'])) {
            $sql .= " AND z.code LIKE '" . $this->db->escape($data['filter_code']) . "%'";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $sql .= " AND z.status = '" . (int)$data['filter_status'] . "'";
        }

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY c.name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getZonesByCountryId($country_id) {
        $zone_data = $this->cache->get('zone.admin' . (int)$country_id);

        if (!$zone_data) {
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone WHERE country_id = '" . (int)$country_id . "' AND status = '1' ORDER BY name");

            $zone_data = $query->rows;

            $this->cache->set('zone.admin' . (int)$country_id, $zone_data);
        }

        return $zone_data;
    }

    public function getZoneDescriptions($zone_id) {
        $country_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_description WHERE zone_id = '" . (int)$zone_id . "'");

        foreach ($query->rows as $result) {
            $country_data[$result['language_id']] = array('name' => $result['name']);
        }

        return $country_data;
    }

    public function getTotalZones($data = array()) {

        $sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "zone z LEFT JOIN " . DB_PREFIX . "country c ON (z.country_id = c.country_id) WHERE z.zone_id > 0 ";

        if (!empty($data['filter_country_id']) && !is_null($data['filter_country_id'])) {
            $sql .= " AND c.country_id = '" . (int)$data['filter_country_id'] . "'";
        }

        if (!empty($data['filter_country'])) {
            $sql .= " AND c.name LIKE '%" . $this->db->escape($data['filter_country']) . "%'";
        }

        if (!empty($data['filter_zone_id']) && !is_null($data['filter_zone_id'])) {
            $sql .= " AND z.zone_id = '" . (int)$data['filter_zone_id'] . "'";
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND z.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['filter_code'])) {
            $sql .= " AND z.code LIKE '" . $this->db->escape($data['filter_code']) . "%'";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $sql .= " AND z.status = '" . (int)$data['filter_status'] . "'";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getTotalZonesByCountryId($country_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "zone WHERE country_id = '" . (int)$country_id . "'");

        return $query->row['total'];
    }

    public function addCountry($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "country SET name = '" . $this->db->escape($data['name']) . "', iso_code_2 = '" . $this->db->escape($data['iso_code_2']) . "', iso_code_3 = '" . $this->db->escape($data['iso_code_3']) . "', address_format = '" . $this->db->escape($data['address_format']) . "', postcode_required = '" . (int)$data['postcode_required'] . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "'");

        $country_id = $this->db->getLastId();

        foreach ($data['country_description'] as $language_id => $value) {
            if($value['name']) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "country_description SET country_id = '" . (int)$country_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
            }
        }

        $this->cache->delete('country');

        return $country_id;
    }

    public function editCountry($country_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "country SET name = '" . $this->db->escape($data['name']) . "', iso_code_2 = '" . $this->db->escape($data['iso_code_2']) . "', iso_code_3 = '" . $this->db->escape($data['iso_code_3']) . "', address_format = '" . $this->db->escape($data['address_format']) . "', postcode_required = '" . (int)$data['postcode_required'] . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "' WHERE country_id = '" . (int)$country_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "country_description WHERE country_id = '" . (int)$country_id . "'");

        foreach ($data['country_description'] as $language_id => $value) {
            if($value['name']) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "country_description SET country_id = '" . (int)$country_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
            }
        }

        $this->cache->delete('country');
    }


    public function statusCountry($country_id, $status) {
        $this->db->query("UPDATE " . DB_PREFIX . "country SET status = '" . (int)$status . "' WHERE country_id = '" . (int)$country_id . "'");

        $this->cache->delete('country');
    }

    public function deleteCountry($country_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$country_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "country_description WHERE country_id = '" . (int)$country_id . "'");

        $this->cache->delete('country');
    }

    public function getCountry($country_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$country_id . "'");

        return $query->row;
    }

    public function getCountries($data = array()) {
        if ($data) {

            $sql = "SELECT * FROM " . DB_PREFIX . "country WHERE country_id > 0";

            if (!empty($data['filter_country_id']) && !is_null($data['filter_country_id'])) {
                $sql .= " AND country_id = '" . (int)$data['filter_country_id'] . "'";
            }

            if (!empty($data['filter_name'])) {
                $sql .= " AND name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
            }

            if (!empty($data['filter_iso_code_2'])) {
                $sql .= " AND iso_code_2 LIKE '" . $this->db->escape($data['filter_iso_code_2']) . "%'";
            }

            if (isset($data['filter_iso_code_3'])) {
                $sql .= " AND iso_code_3 LIKE '" . $this->db->escape($data['filter_iso_code_3']) . "%'";
            }

            if (isset($data['filter_postcode_required']) && !is_null($data['filter_postcode_required'])) {
                $sql .= " AND postcode_required = '" . (int)$data['filter_postcode_required'] . "'";
            }

            if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
                $sql .= " AND status = '" . (int)$data['filter_status'] . "'";
            }

            $sort_data = array(
                'country_id',
                'sort_order',
                'postcode_required',
                'status',
                'name',
                'iso_code_2',
                'iso_code_3'
            );

            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                $sql .= " ORDER BY " . $data['sort'];
            } else {
                $sql .= " ORDER BY name";
            }

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
            }

            $query = $this->db->query($sql);

            return $query->rows;
        } else {
            $country_data = $this->cache->get('country.admin');

            if (!$country_data) {
                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "country ORDER BY name ASC");

                $country_data = $query->rows;

                $this->cache->set('country.admin', $country_data);
            }

            return $country_data;
        }
    }

    public function getCountryDescriptions($country_id) {
        $country_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "country_description WHERE country_id = '" . (int)$country_id . "'");

        foreach ($query->rows as $result) {
            $country_data[$result['language_id']] = array('name' => $result['name']);
        }

        return $country_data;
    }

    public function getTotalCountries($data = array()) {

        $sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "country WHERE country_id > 0 ";

        if (!empty($data['filter_country_id']) && !is_null($data['filter_country_id'])) {
            $sql .= " AND country_id = '" . (int)$data['filter_country_id'] . "'";
        }

        if (!empty($data['filter_name'])) {
            $sql .= " AND name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        if (!empty($data['filter_iso_code_2'])) {
            $sql .= " AND iso_code_2 LIKE '" . $this->db->escape($data['filter_iso_code_2']) . "%'";
        }

        if (isset($data['filter_iso_code_3'])) {
            $sql .= " AND iso_code_3 LIKE '" . $this->db->escape($data['filter_iso_code_3']) . "%'";
        }

        if (isset($data['filter_postcode_required']) && !is_null($data['filter_postcode_required'])) {
            $sql .= " AND postcode_required = '" . (int)$data['filter_postcode_required'] . "'";
        }

        if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
            $sql .= " AND status = '" . (int)$data['filter_status'] . "'";
        }
        $query = $this->db->query($sql);


        return $query->row['total'];
    }

    public function clearCache() {
        $this->cache->delete('country');
        $this->cache->delete('zone');
    }

    public function install() {
        $this->load->model('localisation/language');

        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "country_description` (
          `country_id` int(11) NOT NULL,
          `language_id` int(11) NOT NULL,
          `name` varchar(255) NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

        $result =$this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "country` LIKE 'sort_order'");

        if(!$result->num_rows){
            $this->db->query("ALTER TABLE  `" . DB_PREFIX . "country` ADD  `sort_order` INT( 3 ) NOT NULL AFTER  `postcode_required` ;");

            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "country");

            $languages = $this->model_localisation_language->getLanguages();

            foreach ($query->rows as $item){
                foreach ($languages as $language_code => $value) {
                    if($item['name']){
                        $this->db->query("INSERT INTO " . DB_PREFIX . "country_description SET country_id = '" . (int)$item['country_id'] . "', language_id = '" . (int)$value['language_id'] . "', name = '" . $this->db->escape($item['name']) . "'");
                    }
                }
            }
        }

        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "zone_description` (
          `zone_id` int(11) NOT NULL,
          `language_id` int(11) NOT NULL,
          `name` varchar(255) NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

        $result =$this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "zone` LIKE 'sort_order'");

        if(!$result->num_rows){
            $this->db->query("ALTER TABLE  `" . DB_PREFIX . "zone` ADD  `sort_order` INT( 3 ) NOT NULL AFTER  `code` ;");

            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone");

            $languages = $this->model_localisation_language->getLanguages();

            foreach ($query->rows as $item){
                foreach ($languages as $language_code => $value) {
                    if($item['name']){
                        $this->db->query("INSERT INTO " . DB_PREFIX . "zone_description SET zone_id = '" . (int)$item['zone_id'] . "', language_id = '" . (int)$value['language_id'] . "', name = '" . $this->db->escape($item['name']) . "'");
                    }
                }
            }
        }

        $this->db->query("UPDATE `" . DB_PREFIX . "modification` SET status=1 WHERE `name` LIKE'%Country and Zone Editor%'");
        $modifications = $this->load->controller('extension/modification/refresh');
    }

    public function uninstall() {
        
        $this->cache->delete('country');
        $this->cache->delete('zone');

        $this->db->query("UPDATE `" . DB_PREFIX . "modification` SET status=0 WHERE `name` LIKE'%Country and Zone Editor%'");
        $modifications = $this->load->controller('extension/modification/refresh');
    }

    public function getTableExist($table_name) {

        $query = $this->db->query("SHOW TABLES LIKE '" . DB_PREFIX . $table_name . "'");

        return $query->num_rows;
    }

    protected function curl_get_contents($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    public function getNotifications($version) {

        $site = $this->config->get('config_secure')?HTTPS_CATALOG:HTTP_CATALOG;

        $result = $this->curl_get_contents("http://vanstudio.co.ua/index.php?route=module/activation/info&extension_id=27280&site=" . $site . "&order_id=".$this->config->get('cz_editor_order_id')."&url=".$this->config->get('cz_editor_url')."&version=$version&language_code=" . $this->config->get('config_admin_language'));

        if (stripos($result,'<html') !== false) {
            return '';
        }
        return $result;
    }
}