<?php
class ModelModuleCron extends Model {

	public function addCronJob($data) {		
		$this->db->query("INSERT INTO " . DB_PREFIX . "cron 
                      SET 
                        cron_file     = '" . $data['cron_file'] . "', 
                        cron_every    = '" . $data['cron_every'] . "', 
                        cron_status   = '" . $data['cron_status'] . "',
                        cron_datetime = NOW()
                      ");		
		
    $this->cache->delete('cron_jobs');
	} // addCronJob
	
	public function editCronJob($cron_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "cron 
                      SET 
                        cron_file     = '" . $data['cron_file'] . "', 
                        cron_every    = '" . $data['cron_every'] . "', 
                        cron_status   = '" . $data['cron_status'] . "',
                        cron_datetime = NOW()
                      WHERE cron_id = '" . (int)$cron_id . "'
                    ");
		
		$this->cache->delete('cron_jobs');	
	} // editCronJob 
	
	public function deleteCronJob($cron_id) {
		$this->db->query("DELETE 
                      FROM " . DB_PREFIX . "cron 
                      WHERE cron_id = '" . (int)$cron_id . "'
                    ");	
		
		$this->cache->delete('cron_jobs');
	} // deleteCronJob 
	
	public function getCronJobs($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "cron ";
		
			$sort_data = array(
				'cron_file',
				'cron_datetime',
				'cron_every',
        'cron_status'
			);	
			
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY cron_file";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}				

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}	
			
			$query = $this->db->query($sql);
	
			return $query->rows;			
		} else {
			$cron_data = $this->cache->get('cron_jobs');

			if (!$cron_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cron ORDER BY cron_file ");	
				$cron_data = $query->rows;			
				$this->cache->set('cron_jobs', $cron_data);
			}
			
			return $cron_data;
		}
	} // getCronJobs
		
	public function getCronJob($cron_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cron WHERE cron_id = '" . (int)$cron_id . "' ");		
		return $query->row;
	} // getCronJob 
			
	public function getTotalCronJob() {
    $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "cron");		
		return $query->row['total'];
	} // getTotalCronJob 		

	public function checkCronJob() {

		$create_cron = "
      CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "cron` (
        `cron_id` int(11) NOT NULL AUTO_INCREMENT,
        `cron_file` varchar(255) NOT NULL,
        `cron_datetime` datetime NOT NULL,
        `cron_every` int(11) NOT NULL,
        `cron_status` tinyint(1) NOT NULL DEFAULT '1',
        PRIMARY KEY (`cron_id`)
      ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;      
    ";
		$this->db->query($create_cron);

		$create_cron_history = "
      CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "cron_history` (
        `cron_history_id` int(11) NOT NULL AUTO_INCREMENT,
        `cron_id` int(11) NOT NULL,
        `cron_history_datetime` datetime NOT NULL,
        PRIMARY KEY (`cron_history_id`)
      ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;      
    ";
		$this->db->query($create_cron_history);

	} // checkCronJob

}
?>