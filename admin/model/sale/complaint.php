<?php
class ModelSaleComplaint extends Model {

	public function editComplaint($complaint_id, $data) {
		$this->db->query("UPDATE `" . DB_PREFIX . "complaint` SET order_id = '" . (int)$data['order_id'] . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', reason_id = '" . (int)$data['reason_id'] . "', opened = '" . (int)$data['opened'] . "', comment = '" . $this->db->escape($data['comment']) . "', admin_comment = '" . $this->db->escape($data['admin_comment']) . "', date_modified = NOW() WHERE complaint_id = '" . (int)$complaint_id . "'");
        if((int)$data['opened']==0){
            $query2 = $this->db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "complaint`  WHERE complaint_id = '" . (int)$complaint_id . "'");
            $upload_info = $this->db->query("SELECT * FROM " . DB_PREFIX . "upload WHERE code = '" . $query2->row['uploadphoto'] . "'");
            if ($upload_info->row){
                $this->db->query("DELETE FROM " . DB_PREFIX . "upload WHERE upload_id = '" . (int)$upload_info->row['upload_id']. "'");
                return $upload_info;
            }
            return "";
        }
        return "";
	}

	public function deleteComplaint($complaint_id) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "complaint` WHERE complaint_id = '" . (int)$complaint_id . "'");
	}

	public function getComplaint($complaint_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "complaint`  WHERE complaint_id = '" . (int)$complaint_id . "'");

		return $query->row;
	}

	public function getComplaints($data = array()) {
		$sql = "SELECT *, CONCAT(firstname, ' ', lastname) AS customer FROM `" . DB_PREFIX . "complaint` ";

		$implode = array();

		if (!empty($data['filter_return_id'])) {
			$implode[] = "complaint_id = '" . (int)$data['filter_return_id'] . "'";
		}

		if (!empty($data['filter_order_id'])) {
			$implode[] = "order_id = '" . (int)$data['filter_order_id'] . "'";
		}

		if (!empty($data['filter_customer'])) {
			$implode[] = "CONCAT(firstname, ' ', lastname) LIKE '" . $this->db->escape($data['filter_customer']) . "%'";
		}

		if (!empty($data['filter_product'])) {
			$implode[] = "product = '" . $this->db->escape($data['filter_product']) . "'";
		}

		if (!empty($data['filter_model'])) {
			$implode[] = "model = '" . $this->db->escape($data['filter_model']) . "'";
		}

		if (!empty($data['filter_date_added'])) {
			$implode[] = "DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		if (!empty($data['filter_date_modified'])) {
			$implode[] = "DATE(date_modified) = DATE('" . $this->db->escape($data['filter_date_modified']) . "')";
		}

		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}

		$sort_data = array(
			'complaint_id',
			'order_id',
			'customer',
			'product',
			'model',
			'status',
			'date_added',
			'date_modified'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY complaint_id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalComplaints($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "complaint` ";

		$implode = array();

		if (!empty($data['filter_return_id'])) {
			$implode[] = "complaint_id = '" . (int)$data['filter_complaint_id'] . "'";
		}

		if (!empty($data['filter_customer'])) {
			$implode[] = "CONCAT(firstname, ' ', lastname) LIKE '" . $this->db->escape($data['filter_customer']) . "%'";
		}

		if (!empty($data['filter_order_id'])) {
			$implode[] = "order_id = '" . $this->db->escape($data['filter_order_id']) . "'";
		}

		if (!empty($data['filter_product'])) {
			$implode[] = "product = '" . $this->db->escape($data['filter_product']) . "'";
		}

		if (!empty($data['filter_model'])) {
			$implode[] = "model = '" . $this->db->escape($data['filter_model']) . "'";
		}

		if (!empty($data['filter_date_added'])) {
			$implode[] = "DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		if (!empty($data['filter_date_modified'])) {
			$implode[] = "DATE(date_modified) = DATE('" . $this->db->escape($data['filter_date_modified']) . "')";
		}

		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

    public function getUploadedPhoto($code) {

		$query = $this->db->query("SELECT filename FROM " . DB_PREFIX . "upload WHERE code = '" . $code . "'");

        if($query->row)
            return $query->row['filename'];
        else
            return "";
	}
}