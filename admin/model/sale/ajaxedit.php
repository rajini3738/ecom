<?php
class ModelSaleAjaxedit extends Model {

    public function addProduct($product_data, $order_id) {
        $product_id = $product_data['product_id'];
        $quantity = $product_data['quantity'];
        $price = $product_data['product_price'];

        $options = !empty($product_data['option']) ? $product_data['option'] : false ;

        $this->load->model('sale/order');

        $order_totals = $this->getOrderTotalCodes($order_id);
        $old_order_totals = $this->getOldTotals($order_totals, $order_id);

        if (in_array('tax', $order_totals)) {
            $tax_rate = $this->getTaxRate($product_id, $order_id);
        }

        // Get new Product information
        $this->load->model('catalog/product');
        $result = $this->model_catalog_product->getProduct($product_id);

        $price = $this->cleanValue($price, $order_id);

        $product = array(
            'product_id' 	=> $product_id,
            'price' 		=> $price,
            'name'			=> $result['name'],
            'model'			=> $result['model'],
            'quantity' 	    => $quantity,
            'reward' 	    => $this->getProductReward($product_id,$this->getOrderCustomerGroupId($order_id)) * $quantity,
            'tax'		    => !empty($tax_rate) ? $price / 100 * $tax_rate['rate'] : 0 ,
            'total'		    => $price * $quantity,
            'option' 	    => array()
        );
        $product['weight']=0;
        if (!empty($options)) {
            $option_data = $this->getOptionData($product_id, $quantity, $options);
            if(!empty($option_data)) {
                $product['option'] = $option_data['option_data'];
                if (!empty($option_data['price'])) {
                    $product['price'] = $option_data['price'];
                    $product['total'] = $product['price'] * $quantity;
                    if (!empty($tax_rate)) {
                        $product['tax'] = $product['price'] / 100 * $tax_rate['rate'];
                    }
                }
                if (!empty($option_data['points'])) {
                    $product['reward'] += $option_data['points'];
                }
                if (!empty($option_data['weight'])) {
                    $product['weight'] = $option_data['weight'] == 0 ? 1 : (int)$option_data['weight'];
                }
            }
        }

        $product['order_product_id'] = $this->addProductToOrder($product,$order_id);

        $new_order_totals['price'] = $product['price'] + $product['tax'];
        $new_order_totals['product_total'] = $new_order_totals['price'] * $product['quantity'];

        $currency = $this->getCurrency($order_id);

        $new_order_totals['currency_code'] = $currency['currency_code'];
        $new_order_totals['currency_value'] = $currency['currency_value'];
        $new_order_totals['sub_total'] =$new_order_totals['product_total']+$old_order_totals['sub_total'];
        $new_order_totals['total'] =$new_order_totals['product_total']+$old_order_totals['total'];//+$old_order_totals['shipping'];

        $this->setOrderTotals($order_totals, $new_order_totals, $order_id);

        $result = $this->rewriteTotals($new_order_totals);

        $response['product'] = $product;
        $response['product']['price'] = $result['price'];
        $response['product']['product_total'] = $result['product_total'];
        $response['product']['product_total'] = $result['product_total'];
        $response['sub_total'] = $result['sub_total'];
        $response['total'] = $result['total'];

        return $response;
    }

    protected function getOptionData($product_id, $quantity, $options) {

        $option_data = array();

        $option_price = 0;
        $option_points = 0;
        $option_weight = 0;

        foreach ($options as $product_option_id => $value) {
            $option_query = $this->db->query("SELECT po.product_option_id, po.option_id, od.name, o.type FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_option_id = '" . (int)$product_option_id . "' AND po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'");

            if ($option_query->num_rows) {
                if ($option_query->row['type'] == 'select' || $option_query->row['type'] == 'radio' || $option_query->row['type'] == 'image') {
                    $option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$value . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

                    if ($option_value_query->num_rows) {
                        if ($option_value_query->row['price_prefix'] == '+') {
                            $option_price += $option_value_query->row['price'];
                        } elseif ($option_value_query->row['price_prefix'] == '-') {
                            $option_price -= $option_value_query->row['price'];
                        }

                        if ($option_value_query->row['points_prefix'] == '+') {
                            $option_points += $option_value_query->row['points'];
                        } elseif ($option_value_query->row['points_prefix'] == '-') {
                            $option_points -= $option_value_query->row['points'];
                        }

                        if ($option_value_query->row['weight_prefix'] == '+') {
                            $option_weight += $option_value_query->row['weight'];
                        } elseif ($option_value_query->row['weight_prefix'] == '-') {
                            $option_weight -= $option_value_query->row['weight'];
                        }

                        if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $quantity))) {
                            $stock = false;
                        }

                        $option_data[] = array(
                            'product_option_id'       => $product_option_id,
                            'product_option_value_id' => $value,
                            'option_id'               => $option_query->row['option_id'],
                            'option_value_id'         => $option_value_query->row['option_value_id'],
                            'name'                    => $option_query->row['name'],
                            'value'                   => $option_value_query->row['name'],
                            'type'                    => $option_query->row['type'],
                            'quantity'                => $option_value_query->row['quantity'],
                            'subtract'                => $option_value_query->row['subtract'],
                            'price'                   => $option_value_query->row['price'],
                            'price_prefix'            => $option_value_query->row['price_prefix'],
                            'points'                  => $option_value_query->row['points'],
                            'points_prefix'           => $option_value_query->row['points_prefix'],
                            'weight'                  => $option_value_query->row['weight'],
                            'weight_prefix'           => $option_value_query->row['weight_prefix']
                        );
                    }
                } elseif ($option_query->row['type'] == 'checkbox' && is_array($value)) {
                    foreach ($value as $product_option_value_id) {
                        $option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$product_option_value_id . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

                        if ($option_value_query->num_rows) {
                            if ($option_value_query->row['price_prefix'] == '+') {
                                $option_price += $option_value_query->row['price'];
                            } elseif ($option_value_query->row['price_prefix'] == '-') {
                                $option_price -= $option_value_query->row['price'];
                            }

                            if ($option_value_query->row['points_prefix'] == '+') {
                                $option_points += $option_value_query->row['points'];
                            } elseif ($option_value_query->row['points_prefix'] == '-') {
                                $option_points -= $option_value_query->row['points'];
                            }

                            if ($option_value_query->row['weight_prefix'] == '+') {
                                $option_weight += $option_value_query->row['weight'];
                            } elseif ($option_value_query->row['weight_prefix'] == '-') {
                                $option_weight -= $option_value_query->row['weight'];
                            }

                            if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $quantity))) {
                                $stock = false;
                            }

                            $option_data[] = array(
                                'product_option_id'       => $product_option_id,
                                'product_option_value_id' => $product_option_value_id,
                                'option_id'               => $option_query->row['option_id'],
                                'option_value_id'         => $option_value_query->row['option_value_id'],
                                'name'                    => $option_query->row['name'],
                                'value'                   => $option_value_query->row['name'],
                                'type'                    => $option_query->row['type'],
                                'quantity'                => $option_value_query->row['quantity'],
                                'subtract'                => $option_value_query->row['subtract'],
                                'price'                   => $option_value_query->row['price'],
                                'price_prefix'            => $option_value_query->row['price_prefix'],
                                'points'                  => $option_value_query->row['points'],
                                'points_prefix'           => $option_value_query->row['points_prefix'],
                                'weight'                  => $option_value_query->row['weight'],
                                'weight_prefix'           => $option_value_query->row['weight_prefix']
                            );
                        }
                    }
                } elseif ($option_query->row['type'] == 'text' || $option_query->row['type'] == 'textarea' || $option_query->row['type'] == 'file' || $option_query->row['type'] == 'date' || $option_query->row['type'] == 'datetime' || $option_query->row['type'] == 'time') {
                    $option_data[] = array(
                        'product_option_id'       => $product_option_id,
                        'product_option_value_id' => '',
                        'option_id'               => $option_query->row['option_id'],
                        'option_value_id'         => '',
                        'name'                    => $option_query->row['name'],
                        'value'                   => $value,
                        'type'                    => $option_query->row['type'],
                        'quantity'                => '',
                        'subtract'                => '',
                        'price'                   => '',
                        'price_prefix'            => '',
                        'points'                  => '',
                        'points_prefix'           => '',
                        'weight'                  => '',
                        'weight_prefix'           => ''
                    );
                }
            }
        }

        return array('option_data' => $option_data, 'price' => $option_price, 'points' => $option_points, 'weight' => $option_weight);

    }

    protected function addProductToOrder($product,$order_id) {

        // Add Product to DB

        $this->db->query("INSERT INTO " . DB_PREFIX . "order_product SET order_id = '" . (int)$order_id . "', product_id = '" . (int)$product['product_id'] . "', weight = '" . (int)$product['weight'] . "', name = '" . $this->db->escape($product['name']) . "', model = '" . $this->db->escape($product['model']) . "', quantity = '" . (int)$product['quantity'] . "', price = '" . (float)$product['price'] . "', total = '" . (float)$product['total'] . "', tax = '" . (float)$product['tax'] . "', reward = '" . (int)$product['reward'] . "'");

        $order_product_id = $this->db->getLastId();

        foreach ($product['option'] as $option) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "order_option SET order_id = '" . (int)$order_id . "', order_product_id = '" . (int)$order_product_id . "', product_option_id = '" . (int)$option['product_option_id'] . "', product_option_value_id = '" . (int)$option['product_option_value_id'] . "', name = '" . $this->db->escape($option['name']) . "', `value` = '" . $this->db->escape($option['value']) . "', `type` = '" . $this->db->escape($option['type']) . "'");
        }

        $this->updateProductQty($product['product_id'], $product['option'], $product['quantity'], $product['weight']);

        return $order_product_id;

    }

    protected function updateProductQty($product_id, $options, $quantity, $weight) {

        $selectedProduct = $this->db->query("SELECT * FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product_id . "'");

        if($selectedProduct->row['quantity'] > 0 && $selectedProduct->row['subtract'] != 0 ){

            $total_quantity = ((int)$quantity) * ((int)($weight == 0 ? 1 : $weight));

            if (empty($options)) {
                $query = $this->db->query("SELECT quantity FROM `" . DB_PREFIX . "product` WHERE `product_id` = '" . (int)$product_id . "'");
                $qty = $query->row['quantity'];
                $sql = "UPDATE `" . DB_PREFIX . "product` SET `quantity` = '" . ($qty - (int)$quantity) . "' WHERE `product_id` = '" . (int)$product_id . "'";
                $this->db->query($sql);
            } else {

                $this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = (quantity - " . $total_quantity . ") WHERE product_id = '" . (int)$product_id . "' AND subtract = '1'");

                foreach ($options as $option) {
                    $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_option_value` WHERE `product_option_value_id` = '" . (int)$option['product_option_value_id'] . "' AND `product_option_id` = '" . (int)$option['product_option_id'] . "' AND `product_id` = '" . (int)$product_id . "'");

                    if(!empty($query->num_rows)) {
                        $option_data = $query->row;
                        // if ($option_data['subtract']) {
                        if($weight <= 0){
                            $sql = "UPDATE `" . DB_PREFIX . "product_option_value` SET `quantity` = '" . ($option_data['quantity'] - (int)$total_quantity) . "' WHERE `product_option_value_id` = '" . (int)$option['product_option_value_id'] . "' AND `product_option_id` = '" . (int)$option['product_option_id'] . "' AND `product_id` = '" . (int)$product_id . "'";
                            $this->db->query($sql);
                        }
                    }
                }
            }
        }
    }

    protected function getProductReward($product_id, $customer_group_id) {
        $query = $this->db->query("SELECT points FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$customer_group_id. "'");
        return !empty($query->num_rows) ? $query->row['points'] : false ;
    }

    protected function getOrderCustomerGroupId($order_id) {
        $query = $this->db->query("SELECT customer_group_id FROM " . DB_PREFIX . "order WHERE order_id = '" . (int)$order_id . "'");
        return $query->row['customer_group_id'];
    }

    public function saveValue($table, $d) {

        $this->language->load('sale/order');

        if (is_int($d['new_value'])) {
            $this->db->query("UPDATE `" . DB_PREFIX . $table . "` SET `" . $d['param'] . "` = '" . (int)$d['new_value'] . "' " . ($table == 'order' ? (', date_modified = NOW()') : '') . "  WHERE `" . $table . "_id` = '" . (int)$d['param_id'] . "'");
            $json['msg'] = $this->language->get('text_success');
        } elseif (is_string($d['new_value'])) {
            $this->db->query("UPDATE `" . DB_PREFIX . $table . "` SET `" . $d['param'] . "` = '" . $this->db->escape($d['new_value']) . "' " . ($table == 'order' ? (', date_modified = NOW()') : '') . " WHERE `" . $table . "_id` = '" . (int)$d['param_id'] . "'");
            $json['msg'] = $this->language->get('text_success');
        } else {
            $json['error'] = $this->language->get('text_error_ajax');
        }

        return $json;
    }

    public function saveQty($d) {

        $old_data = $this->getOldValues($d['param_id']);

        $new_total = $old_data['price'] * $d['new_value'];

        $currency = $this->getCurrency($old_data['order_id']);

        $new_order_totals['currency_code'] = $currency['currency_code'];
        $new_order_totals['currency_value'] = $currency['currency_value'];

        $product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_product_id = '" . (int)$d['param_id'] . "'");

        foreach($product_query->rows as $product) {

            $selectedProduct = $this->db->query("SELECT * FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product['product_id'] . "'");

            if($selectedProduct->row['quantity'] > 0 && $selectedProduct->row['subtract'] != 0 ){

                $total_quantity = ((int)$product['quantity']) * ((int)$product['weight'] == 0 ? 1 : (int)$product['weight']);

                $this->db->query("UPDATE `" . DB_PREFIX . "product` SET quantity = (quantity + " . $total_quantity . ") WHERE product_id = '" . (int)$product['product_id'] . "' AND subtract = '1'");

                $option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$old_data['order_id'] . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");

                foreach ($option_query->rows as $option) {
                    if($product['weight'] <= 0){
                        $this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity + " . $total_quantity . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'");
                    }
                }
            }
        }

        $order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$old_data['order_id'] . "' ORDER BY sort_order ASC");

        foreach ($order_total_query->rows as $order_total) {
            if($order_total['code']=="sub_total"){
                $sub_total=$order_total['value']-$new_total;
                $new_order_totals['sub_total'] = $sub_total;
                $this->db->query("UPDATE `" . DB_PREFIX . "order_total` SET value='".(float)$sub_total."' WHERE code='sub_total' AND order_id = '" . (int)$old_data['order_id'] . "'");
            }
            if($order_total['code']=="total"){
                $total=$order_total['value']-$new_total;
                $new_order_totals['total'] = $total;
                $this->db->query("UPDATE `" . DB_PREFIX . "order_total` SET value='".(float)$total."' WHERE code='total' AND  order_id = '" . (int)$old_data['order_id'] . "'");
            }
        }

        $this->db->query("UPDATE `" . DB_PREFIX . "order` SET total='".(float)$new_order_totals['total']."' WHERE order_id = '" . (int)$old_data['order_id'] . "'");

        $this->db->query("DELETE FROM `" . DB_PREFIX . "order_product` WHERE `order_product_id` = '" . (int)$d['param_id'] . "'");

        $this->db->query("DELETE FROM `" . DB_PREFIX . "order_option` WHERE `order_product_id` = '" . (int)$d['param_id'] . "'");

        return $this->rewriteTotals($new_order_totals);

    }

    public function savePrice($d) {

        $old_data = $this->getOldValues($d['param_id']);
        $order_totals = $this->getOrderTotalCodes($old_data['order_id']);

        $d['new_value'] = $this->cleanValue($d['new_value'], $old_data['order_id']);

        $new_order_totals['price'] = $d['new_value'];

        $tax_rate = $this->getTaxRate($old_data['product_id'], $old_data['order_id']);

        $old_total = in_array('tax', $order_totals) ? $old_data['total'] + ($old_data['tax'] * $old_data['quantity']) : $old_data['total'] ;

        $new_tax = in_array('tax', $order_totals) ? ($d['new_value']/(100 + $tax_rate['rate'])) * $tax_rate['rate'] : 0 ;
        $new_price_excl = $d['new_value'] - $new_tax;

        $new_product_total = $old_data['quantity'] * $new_price_excl;
        $new_order_totals['product_total'] = $new_product_total + ($new_tax * $old_data['quantity']);

        $this->setNewValues('order_product', array('price' => $new_price_excl, 'total' => $new_product_total, 'tax' => $new_tax), $d['param_id']);

        $currency = $this->getCurrency($old_data['order_id']);
        $new_order_totals['currency_code'] = $currency['currency_code'];
        $new_order_totals['currency_value'] = $currency['currency_value'];

        return $this->rewriteTotals($new_order_totals);
    }

    public function savePTotal($d) {

        $old_data = $this->getOldValues($d['param_id']);
        $order_totals = $this->getOrderTotalCodes($old_data['order_id']);

        $tax_rate = $this->getTaxRate($old_data['product_id'], $old_data['order_id']);

        $d['new_value'] = $this->cleanValue($d['new_value'], $old_data['order_id']);

        $old_total = in_array('tax', $order_totals) ? $old_data['total'] + ($old_data['total'] * $tax_rate['rate'] / 100) : $old_data['total'] ;
        $new_tax = in_array('tax', $order_totals) ? (($d['new_value']/(100 + $tax_rate['rate'])) * $tax_rate['rate']) / $old_data['quantity'] : 0 ;
        $new_price_excl = ($d['new_value'] / $old_data['quantity'] ) - $new_tax;
        $new_order_totals['price'] = $new_price_excl + $new_tax;
        $new_order_totals['product_total'] = $d['new_value'];


        $new_sub_total = $new_order_totals['product_total'] - ($new_tax * $old_data['quantity']);

        $this->setNewValues('order_product', array('price' => $new_price_excl, 'total' => $new_sub_total, 'tax' => $new_tax), $d['param_id']);

        $currency = $this->getCurrency($old_data['order_id']);

        $new_order_totals['currency_code'] = $currency['currency_code'];
        $new_order_totals['currency_value'] = $currency['currency_value'];

        return $this->rewriteTotals($new_order_totals);
    }

    public function saveShipping($d) {

        $d['new_value'] = $this->cleanValue($d['new_value'], $d['param_id']);

        $tax_rate = $this->getShippingTaxRate($d['param_id']);

        $order_totals = $this->getOrderTotalCodes($d['param_id']);

        $new_order_totals['shipping'] = $d['new_value'];

        $currency = $this->getCurrency($d['param_id']);

        $new_order_totals['currency_code'] = $currency['currency_code'];
        $new_order_totals['currency_value'] = $currency['currency_value'];

        $this->setOrderTotals($order_totals, $new_order_totals, $d['param_id']);

        return $this->rewriteTotals($new_order_totals);
    }

    public function saveShippingMethod($d) {

        $this->db->query("UPDATE `" . DB_PREFIX . "order_total` SET `title` = '" . $d['new_value'] . "' WHERE `order_id` = '" . (int)$d['param_id'] . "' AND `code` = 'shipping'; ");
        $this->db->query("UPDATE `" . DB_PREFIX . "order` SET `shipping_method` = '" . $d['new_value'] . "' WHERE `order_id` = '" . (int)$d['param_id'] . "';");
    }

    protected function getOldValues($id) {

        // Old values
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_product` WHERE order_product_id = '" . (int)$id . "'");

        return $query->row;
    }

    protected function getTaxRate($product_id, $order_id) {

        // Get tax rate for product
        $sql  = "SELECT trate.rate as rate, trate.geo_zone_id as geo_zone_id, tr.based as based  FROM `" . DB_PREFIX . "product` p ";
        $sql .=	"INNER JOIN `" . DB_PREFIX . "tax_class` tc ON p.tax_class_id = tc.tax_class_id ";
        $sql .=	"INNER JOIN `" . DB_PREFIX . "tax_rule` tr ON tc.tax_class_id = tr.tax_class_id ";
        $sql .=	"INNER JOIN `" . DB_PREFIX . "tax_rate_to_customer_group` trcg ON tr.tax_rate_id = trcg.tax_rate_id ";
        $sql .=	"INNER JOIN `" . DB_PREFIX . "tax_rate` trate ON trcg.tax_rate_id = trate.tax_rate_id ";
        $sql .=	"WHERE `product_id` = '" . (int)$product_id . "' ";
        $sql .=	"AND `customer_group_id` = (SELECT customer_group_id FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$order_id . "')";

        $query = $this->db->query($sql);

        return $query->row;

    }

    protected function getShippingTaxRate ($order_id) {

        $query = $this->db->query("SELECT shipping_code as code, customer_group_id FROM `" . DB_PREFIX . "order` WHERE `order_id` = '" . $order_id . "'");
        $order = $query->row;
        $shipping_code = strstr($order['code'], '.', true);

        $sql = "SELECT trate.rate FROM `" . DB_PREFIX . "tax_rule` tr ";
        $sql .= "INNER JOIN `" . DB_PREFIX . "tax_rate_to_customer_group` trcg ON tr.tax_rate_id = trcg.tax_rate_id ";
        $sql .=	"INNER JOIN `" . DB_PREFIX . "tax_rate` trate ON trcg.tax_rate_id = trate.tax_rate_id ";
        $sql .= "WHERE tax_class_id = (SELECT value FROM `" . DB_PREFIX . "setting` WHERE `code` = '" . $shipping_code . "' AND `key` LIKE '%_class_id')";
        $sql .=	"AND `customer_group_id` = '" . $order['customer_group_id'] . "'";

        $query = $this->db->query($sql);

        return (empty($query->row) ? array('rate' => 0 ) : $query->row);

    }

    protected function getOldTotals($order_totals, $order_id) {

        // Get old Order Total values

        $old_order_totals = array();

        foreach ($order_totals as $total) {
            $query = $this->db->query("SELECT value FROM `" . DB_PREFIX . "order_total` WHERE `order_id` = '" . (int)$order_id . "' AND `code` = '" . $total . "'");
            $old_order_totals[$total] = $query->row['value'];
        }

        return $old_order_totals;
    }

    protected function setNewValues($table, $keys, $id) {

        foreach ($keys as $key => $value ) {
            $this->db->query("UPDATE `" . DB_PREFIX . $table . "` SET `" . $key . "` = '" . $value . "' WHERE `" . $table . "_id` = '" . (int)$id . "'");
        }

    }

    protected function setOrderTotals($order_totals, $new_order_totals, $order_id) {

        foreach ($order_totals as $total) {
            if (!empty($new_order_totals[$total])) {
                $sql = "UPDATE `" . DB_PREFIX . "order_total` SET `value` = '" . $new_order_totals[$total] . "' WHERE `" . DB_PREFIX . "order_total`.`order_id` = '" . (int)$order_id . "' AND `" . DB_PREFIX . "order_total`.`code` = '" . $total . "'";
                $this->db->query($sql);
            }
        }

        if (isset($new_order_totals['total'])) {
            $this->db->query("UPDATE `" . DB_PREFIX . "order` SET `total` = '" . $new_order_totals['total'] . "', date_modified = NOW() WHERE `order_id` = '" . (int)$order_id . "'");
        }
    }

    protected function getCurrency($order_id){
        $query = $this->db->query("SELECT currency_code, currency_value FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$order_id . "'");

        return $query->row;
    }

    protected function getOrderTotalCodes($order_id) {
        $query = $this->db->query("SELECT code FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$order_id . "'");

        $order_totals = array();
        foreach ($query->rows as $code) {
            foreach ($code as $value) {
                $order_totals[] = $value;
            }
        }
        return $order_totals;
    }

    protected function setProductQty($data, $qty){
        $product_id = $data['product_id'];
        $query = $this->db->query("SELECT quantity FROM `" . DB_PREFIX . "product` WHERE `product_id` = '" . $product_id . "'");
        $current = $query->row;
        $qty = $current['quantity'] + $data['quantity'] - $qty;
        $this->db->query("UPDATE `" . DB_PREFIX . "product` SET `quantity` = '" . $qty . "' WHERE `product_id` = '" . $product_id . "'");

    }

    protected function removeOrderProduct($order_product_id) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_product` WHERE `order_product_id` = '" . (int)$order_product_id . "'");
        $qty = $query->row['quantity'];
        $product_id = $query->row['product_id'];
        $this->db->query("DELETE FROM `" . DB_PREFIX . "order_product` WHERE `order_product_id` = '" . (int)$order_product_id . "'");

        // if product has options delete options and update option qty
        $option = $this->removeOrderProductOption($order_product_id);
        // else update product qty
        if (!$option) {
            $this->setProductQty(array('product_id' => $product_id, 'quantity' => 0), $qty);
        }
    }

    protected function removeOrderProductOption($order_product_id) {
        $query = $this->db->query("SELECT oo.*, op.quantity FROM `" . DB_PREFIX . "order_option` oo LEFT JOIN `" . DB_PREFIX . "order_product` op ON oo.order_product_id = op.order_product_id WHERE oo.order_product_id = '" . (int)$order_product_id . "'");
        $option = $query->row;
        $this->db->query("DELETE FROM `" . DB_PREFIX . "order_option` WHERE `order_product_id` = '" . (int)$order_product_id . "'");
        //$this->setProductOptionQty($option);
    }

    protected function setProductOptionQty($option, $quantity = 0){
        if (!empty($option['quantity'])) {
            $qty = $option['quantity'];
        } else {
            $qty = $quantity;
        }
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_option_value` WHERE `product_option_id` = '" . $option['product_option_id'] . "' AND `product_option_value_id` = '" . $option['product_option_value_id'] . "'");
        $current = $query->row;
        if ($current['subtract']) {
            $qty = $current['quantity'] + $quantity - $qty;
            $this->db->query("UPDATE `" . DB_PREFIX . "product_option_value` SET `quantity` = '" . $qty . "' WHERE `product_option_id` = '" . $option['product_option_id'] . "' AND `product_option_value_id` = '" . $option['product_option_value_id'] . "'");
        }
        return $current['subtract'];
    }

    protected function cleanValue($price, $order_id) {

        $query = $this->db->query("SELECT o.currency_id, c.symbol_left, c.symbol_right FROM `" . DB_PREFIX . "order` o INNER JOIN `" . DB_PREFIX . "currency` c ON o.currency_id = c.currency_id WHERE order_id = '" . (int)$order_id . "'");
        $currency_data = $query->row;

        $currency_symbol = empty($currency_data['symbol_left']) ? $currency_data['symbol_right'] : $currency_data['symbol_left'] ;
        $regex = '/\\' . $currency_symbol . '[\$\€\£]/';

        $price = preg_replace($regex, '' , $price);

        return $price;
    }

    protected function rewriteTotals($totals) {

        $this->language->load('sale/order');
        $json = array();

        if (isset($totals['currency_code']) && isset($totals['currency_value'])) {
            if (isset($totals['price'])) {
                $json['price'] = $this->currency->format($totals['price'], $totals['currency_code'], $totals['currency_value']);
            }
            if (isset($totals['product_total'])) {
                $json['product_total'] = $this->currency->format($totals['product_total'], $totals['currency_code'], $totals['currency_value']);
            }
            if (isset($totals['sub_total'])) {
                $json['sub_total'] = $this->currency->format($totals['sub_total'], $totals['currency_code'], $totals['currency_value']);
            }
            if (isset($totals['shipping'])) {
                $json['shipping'] = $this->currency->format($totals['shipping'], $totals['currency_code'], $totals['currency_value']);
            }
            if (isset($totals['tax'])) {
                $json['tax'] = $this->currency->format($totals['tax'], $totals['currency_code'], $totals['currency_value']);
            }
            if (isset($totals['total'])) {
                $json['total'] = $this->currency->format($totals['total'], $totals['currency_code'], $totals['currency_value']);
            }
            if (isset($totals['discount'])) {
                $json['discount'] = $this->currency->format($totals['discount'], $totals['currency_code'], $totals['currency_value']);
            }
            $json['msg'] = $this->language->get('text_success');
        } else {
            $json['error'] = $this->language->get('error_currency_format');
        }

        return $json;

    }

    public function saveQty1($d) {

        $old_data = $this->getOldValues($d['param_id']);

        $new_total = $old_data['price'] * $d['new_value'];

        $currency = $this->getCurrency($old_data['order_id']);

        $new_order_totals['currency_code'] = $currency['currency_code'];
        $new_order_totals['currency_value'] = $currency['currency_value'];

        $product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$old_data['order_id'] . "'");

        foreach($product_query->rows as $product) {

            $selectedProduct = $this->db->query("SELECT * FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product['product_id'] . "'");

            if($selectedProduct->row['quantity'] > 0 && $selectedProduct->row['subtract'] != 0 ){

                $total_quantity = ((int)$product['quantity']) * ((int)$product['weight'] == 0 ? 1 : (int)$product['weight']);

                $this->db->query("UPDATE `" . DB_PREFIX . "product` SET quantity = (quantity + " . $total_quantity . ") WHERE product_id = '" . (int)$product['product_id'] . "' AND subtract = '1'");

                $option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$old_data['order_id'] . "' AND order_product_id = '" . (int)$product['order_product_id'] . "'");

                foreach ($option_query->rows as $option) {
                    if($product['weight'] <= 0){
                        $this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = (quantity + " . $total_quantity . ") WHERE product_option_value_id = '" . (int)$option['product_option_value_id'] . "' AND subtract = '1'");
                    }
                }
            }
        }

        $order_total_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_total` WHERE order_id = '" . (int)$old_data['order_id'] . "' ORDER BY sort_order ASC");

        foreach ($order_total_query->rows as $order_total) {
            if($order_total['code']=="sub_total"){
                $sub_total=$order_total['value']-$new_total;
                $new_order_totals['sub_total'] = $sub_total;
                $this->db->query("UPDATE `" . DB_PREFIX . "order_total` SET value='".(float)$sub_total."' WHERE order_id = '" . (int)$old_data['order_id'] . "'");
            }
            if($order_total['code']=="total"){
                $total=$order_total['value']-$new_total;
                $new_order_totals['total'] = $total;
                $this->db->query("UPDATE `" . DB_PREFIX . "order_total` SET value='".(float)$total."' WHERE order_id = '" . (int)$old_data['order_id'] . "'");
            }
        }

        $this->db->query("UPDATE `" . DB_PREFIX . "order` SET value='".(float)$new_order_totals['total']."' WHERE order_id = '" . (int)$old_data['order_id'] . "'");

        $this->db->query("DELETE FROM `" . DB_PREFIX . "order_product` WHERE `order_product_id` = '" . (int)$d['param_id'] . "'");

        $this->db->query("DELETE FROM `" . DB_PREFIX . "order_option` WHERE `order_product_id` = '" . (int)$d['param_id'] . "'");

        return $this->rewriteTotals($new_order_totals);

    }
}