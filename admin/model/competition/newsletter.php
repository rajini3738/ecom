<?php
class ModelCompetitionNewsletter extends Model {
	public function addNewsletter($data) {
      	$this->db->query("INSERT INTO " . DB_PREFIX . "c_newsletter SET name = '" . $this->db->escape($data['name']) . "', email = '" . $this->db->escape($data['email']) . "'");       	
	}
	
	public function editNewsletter($newsletter_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "c_newsletter SET name = '" . $this->db->escape($data['name']) . "', email = '" . $this->db->escape($data['email']) . "' WHERE newsletter_id = '" . (int)$newsletter_id . "'");
	
	}
	
	public function deleteNewsletter($newsletter_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "c_newsletter WHERE newsletter_id = '" . (int)$newsletter_id . "'");
	}
	
	public function getNewsletter($newsletter_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "c_newsletter WHERE newsletter_id = '" . (int)$newsletter_id . "'");
	
		return $query->row;
	}
	
	public function getNewsletterByEmail($email) {
		$query = $this->db->query("(SELECT DISTINCT newsletter_id, email FROM " . DB_PREFIX . "c_newsletter WHERE email = '" . $this->db->escape($email) . "') UNION (SELECT DISTINCT customer_id AS newsletter_id, email FROM " . DB_PREFIX . "customer WHERE email = '" . $this->db->escape($email) . "')");
		
		return $query->row;
	}
			
	public function getNewsletters($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "c_newsletter";

		$implode = array();
		
		if (!empty($data['filter_name'])) {
			$implode[] = "LCASE(name) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
		}

		if (!empty($data['filter_email'])) {
			$implode[] = "LCASE(email) LIKE '" . $this->db->escape(utf8_strtolower($data['filter_email'])) . "%'";
		}
		
		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}
		
		$sort_data = array(
			'name',
			'email',
		);	
			
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY name";	
		}
			
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}		

		$query = $this->db->query($sql);
		
		return $query->rows;	
	}
	
	public function getNewslettersByNewsletter() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "c_newsletter WHERE newsletter = '1' ORDER BY name, email");
	
		return $query->rows;
	}
		
	public function getTotalNewsletters($data = array()) {
      	$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "c_newsletter";
		
		$implode = array();
		
		if (!empty($data['filter_name'])) {
			$implode[] = "CONCAT(name) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		
		if (!empty($data['filter_email'])) {
			$implode[] = "email = '" . $this->db->escape($data['filter_email']) . "'";
		}	
		
		if ($implode) {
			$sql .= " WHERE " . implode(" AND ", $implode);
		}
				
		$query = $this->db->query($sql);
				
		return $query->row['total'];
	}
}
?>