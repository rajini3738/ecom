<?php
// Version 1.6
class ModelCompetitionCompetition extends Model {
	public function addCompetition($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "competition SET status = '" . (int)$data['status'] . "', width = '" . (int)$data['width'] . "', height = '" . (int)$data['height'] . "', newsletter = '" . (int)$data['newsletter'] . "',  winners = '" . (int)$data['winners'] . "', auto = '" . (int)$data['auto'] . "', auto_entry_value = '" . (float)$data['auto_entry_value'] . "', answer_is = '" . $this->db->escape($data['answer_is']) . "', date_added = now(), end_date ='" . $data['end_date'] . "'");
		$competition_id = $this->db->getLastId();
		
		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "competition SET image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "' WHERE competition_id = '" . (int)$competition_id . "'");
		}
		
		foreach ($data['competition_data'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "competition_description SET competition_id = '" . (int)$competition_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', auto_entry_message = '" . $this->db->escape($value['auto_entry_message']) . "', question = '" . $this->db->escape($value['question']) . "', answer1 = '" . $this->db->escape($value['answer1']) . "', answer2 = '" . $this->db->escape($value['answer2']) . "', answer3 = '" . $this->db->escape($value['answer3']) . "', answer4 = '" . $this->db->escape($value['answer4']) . "', description = '" . $this->db->escape($value['description']) . "'");
		}
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'competition_id=" . (int)$competition_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
		if (isset($data['competition_store'])) {
			foreach ($data['competition_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "competition_to_store SET competition_id = '" . (int)$competition_id . "', store_id = '" . (int)$store_id . "'");
			}
		}
		$this->cache->delete('competition');
	}

	public function editCompetition($competition_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "competition SET login = '" . (int)$data['login'] . "', status = '" . (int)$data['status'] . "', width = '" . (int)$data['width'] . "', height = '" . (int)$data['height'] . "', newsletter = '" . (int)$data['newsletter'] . "', winners = '" . (int)$data['winners'] . "', auto = '" . (int)$data['auto'] . "', auto_entry_value = '" . (float)$data['auto_entry_value'] . "', answer_is = '" . $this->db->escape($data['answer_is']) . "', end_date = '" . $data['end_date'] . "' WHERE competition_id = '" . (int)$competition_id . "'");
		
		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "competition SET image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "' WHERE competition_id = '" . (int)$competition_id . "'");
		}
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "competition_description WHERE competition_id = '" . (int)$competition_id . "'");

		foreach ($data['competition_data'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "competition_description SET competition_id = '" . (int)$competition_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($value['title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', auto_entry_message = '" . $this->db->escape($value['auto_entry_message']) . "', question = '" . $this->db->escape($value['question']) . "', answer1 = '" . $this->db->escape($value['answer1']) . "', answer2 = '" . $this->db->escape($value['answer2']) . "', answer3 = '" . $this->db->escape($value['answer3']) . "', answer4 = '" . $this->db->escape($value['answer4']) . "', description = '" . $this->db->escape($value['description']) . "'");
		}
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'competition_id=" . (int)$competition_id. "'");
		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'competition_id=" . (int)$competition_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
		$this->db->query("DELETE FROM " . DB_PREFIX . "competition_to_store WHERE competition_id = '" . (int)$competition_id . "'");
		if (isset($data['competition_store'])) {		
			foreach ($data['competition_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "competition_to_store SET competition_id = '" . (int)$competition_id . "', store_id = '" . (int)$store_id . "'");
			}
		}
		$this->cache->delete('competition');
	}

	public function deleteCompetition($competition_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "competition WHERE competition_id = '" . (int)$competition_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "competition_description WHERE competition_id = '" . (int)$competition_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "competition_entrants WHERE competition_id = '" . (int)$competition_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "competition_winners WHERE competition_id = '" . (int)$competition_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'competition_id=" . (int)$competition_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "competition_to_store WHERE competition_id = '" . (int)$competition_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE `query` = 'competition_id='" . (int)$competition_id . "'");
		$this->cache->delete('competition');
	}	

	public function getCompetitionDetails($competition_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'competition_id=" . (int)$competition_id . "') AS keyword FROM " . DB_PREFIX . "competition WHERE competition_id = '" . (int)$competition_id . "'");
		return $query->row;
	}

	public function getCompetitionData($competition_id) {
		$competition_data = array();
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "competition_description WHERE competition_id = '" . (int)$competition_id . "'");
		foreach ($query->rows as $result) {
			$competition_data[$result['language_id']] = array(
				'title'              => $result['title'],
				'question'           => $result['question'],
				'answer1'             => $result['answer1'],
				'answer2'             => $result['answer2'],
				'answer3'             => $result['answer3'],
				'answer4'             => $result['answer4'],
				'meta_description'   => $result['meta_description'],
				'auto_entry_message' => $result['auto_entry_message'],
				'description'        => $result['description']
			);
		}
		return $competition_data;
	}

	public function getCompetitionStores($competition_id) {
		$competitionpage_store_data = array();
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "competition_to_store WHERE competition_id = '" . (int)$competition_id . "'");
		foreach ($query->rows as $result) {
			$competitionpage_store_data[] = $result['store_id'];
		}
		return $competitionpage_store_data;
	}

	public function getCompetition() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "competition c LEFT JOIN " . DB_PREFIX . "competition_description cd ON (c.competition_id = cd.competition_id) WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY c.date_added");
		return $query->rows;
	}

	public function getTotalCompetitions() {
     	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "competition");
		return $query->row['total'];
	}	

	public function installCompetition() {
		$create_competition = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "competition` (`competition_id` int(11) NOT NULL auto_increment, `newsletter` int(1) NOT NULL DEFAULT '0', `winners` INT( 3 ) NOT NULL DEFAULT '1', `auto` TINYINT( 1 ) NOT NULL, `auto_entry_value`  DECIMAL(15, 4) NOT NULL, `status` int(1) NOT NULL, `answer_is` varchar(4) NOT NULL, `image` varchar(255) DEFAULT NULL, `width` SMALLINT( 3 ) NULL, `height` SMALLINT( 3 ) NULL, `login` int(1) NOT NULL DEFAULT '0', `date_added` datetime default NULL, `end_date` date default NULL,PRIMARY KEY  (`competition_id`)) ENGINE=MyISAM COLLATE=utf8_general_ci;";
		$this->db->query($create_competition);
		$create_competition_descriptions = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "competition_description` (`competition_id` int(11) NOT NULL default '0', `language_id` int(11) NOT NULL default '0', `title` varchar(64) NOT NULL default '', `meta_description` varchar(255) NOT NULL, `auto_entry_message` varchar(255) NOT NULL, `question` varchar(255) NOT NULL, `answer1` VARCHAR( 255 ) NOT NULL, `answer2` VARCHAR( 255 ) NOT NULL, `answer3` VARCHAR( 255 ) NOT NULL, `answer4` VARCHAR( 255 ) NOT NULL, `description` text NOT NULL, PRIMARY KEY  (`competition_id`,`language_id`)) ENGINE=MyISAM COLLATE=utf8_general_ci;";
		$this->db->query($create_competition_descriptions);
		$create_competition_to_store = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "competition_to_store` (`competition_id` int(11) NOT NULL, `store_id` int(11) NOT NULL, PRIMARY KEY  (`competition_id`, `store_id`)) ENGINE=MyISAM COLLATE=utf8_general_ci;";
		$this->db->query($create_competition_to_store);
		$create_entrants = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "competition_entrants` (`id` int(10) unsigned NOT NULL AUTO_INCREMENT, `email_id` varchar(225) NOT NULL, `name` varchar(225) NOT NULL, `answer` varchar(4) NOT NULL, `competition_id` int(11) NOT NULL default '0', PRIMARY KEY (`id`)) ENGINE=MyISAM COLLATE=utf8_general_ci;";
		$this->db->query($create_entrants);
		$create_winners = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "competition_winners` (`id` int(10) unsigned NOT NULL AUTO_INCREMENT, `email_id` varchar(225) NOT NULL, `name` varchar(225) NOT NULL, `notified` TINYINT( 1 ) NOT NULL DEFAULT '0', `competition_id` int(11) NOT NULL default '0', PRIMARY KEY (`id`)) ENGINE=MyISAM COLLATE=utf8_general_ci;";
		$this->db->query($create_winners);
		$create_newsletter = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "c_newsletter` (`newsletter_id` int(10) NOT NULL AUTO_INCREMENT, `name` varchar(100) NOT NULL, `email` varchar(120) NOT NULL, PRIMARY KEY (`newsletter_id`)) ENGINE=MyISAM COLLATE=utf8_general_ci;";
		$this->db->query($create_newsletter);
	}
	
	public function uninstallCompetition() {
	$this->db->query("DROP TABLE IF EXISTS " . DB_PREFIX . "competition_entrants, " . DB_PREFIX . "competition_description, " . DB_PREFIX . "competition, " . DB_PREFIX . "competition_to_store," . DB_PREFIX . "competition_winners");
	$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE `query` LIKE 'competition_id=%'");
	}
	
	public function addEmail($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "competition_entrants SET email_id='" . $this->db->escape($data['email_id']) . "',name='" . $this->db->escape($data['email_name']) . "', competition_id='" . (int)$data['competition_id'] . "', answer='" . $this->db->escape($data['answer_is']) . "'");
	}
	
	public function editEmail($id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "competition_entrants SET name='" . $this->db->escape($data['email_name']) . "', answer='" . $this->db->escape($data['answer_is']) . "' WHERE id = '" . (int)$id . "'");
	}
	
	public function deleteEmail($id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "competition_entrants WHERE id = '" . (int)$id . "'");
	}
	
	public function deleteWinner($id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "competition_winners WHERE id = '" . (int)$id . "'");
	}
	
	public function getEmail($id) {
		$query = $this->db->query("SELECT  * FROM " . DB_PREFIX . "competition_entrants WHERE id = '" . (int)$id . "'");
		return $query->row;
	}
	
	public function getEmails($data) {
	
		$sql = "SELECT * FROM " . DB_PREFIX . "competition_entrants WHERE competition_id = '" . (int)$data['competition_id'] . "'"; 
		
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}				

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}
				
		$query = $this->db->query($sql);
		return $query->rows;
	}
	
	public function getTotalEmails($data) {
		$sql = "SELECT * FROM " . DB_PREFIX . "competition_entrants WHERE competition_id = '" . (int)$data['competition_id'] . "'"; 
		$query = $this->db->query($sql);
		return $query->num_rows;
	}
	
	public function getTotalEntrants($comp_id) {
		$sql = "SELECT * FROM " . DB_PREFIX . "competition_entrants WHERE competition_id = '" . (int)$comp_id . "'"; 
		$query = $this->db->query($sql);
		return $query->num_rows;
	}
	
	public function checkmail($data) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "competition_entrants WHERE email_id='" . $data['email_id'] . "' AND competition_id = '" . (int)$data['competition_id'] . "'"); 
		return $query->num_rows;
	}
	
	public function getWinnersAll($competition_id) {
	
		$limit = $this->db->query("SELECT winners FROM " . DB_PREFIX . "competition WHERE competition_id = '" . (int)$competition_id . "'");
		$sql = "SELECT * FROM " . DB_PREFIX . "competition_entrants WHERE competition_id = '" . (int)$competition_id . "' ORDER BY RAND() LIMIT " . $limit->row['winners'] . ""; 
		$query = $this->db->query($sql);
		return $query->rows;
	}
	
	public function getWinnersCorrectOnly($competition_id) {
	
		$limit = $this->db->query("SELECT winners FROM " . DB_PREFIX . "competition WHERE competition_id = '" . (int)$competition_id . "'");
		$answer = $this->db->query("SELECT answer_is FROM " . DB_PREFIX . "competition WHERE competition_id = '" . (int)$competition_id . "'");
		$sql = "SELECT * FROM " . DB_PREFIX . "competition_entrants WHERE competition_id = '" . (int)$competition_id . "' AND answer = '" . $this->db->escape($answer->row['answer_is']) . "' ORDER BY RAND() LIMIT " . $limit->row['winners'] . ""; 
		$query = $this->db->query($sql);
		return $query->rows;
	}
	
	public function deleteIncorrect($competition_id) {
	
		$answer = $this->db->query("SELECT answer_is FROM " . DB_PREFIX . "competition WHERE competition_id = '" . (int)$competition_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "competition_entrants WHERE answer <> '" . $this->db->escape($answer->row['answer_is']) . "'");
	}
	
	public function checkWinner($competition_id) {
		$sql = "SELECT * FROM " . DB_PREFIX . "competition_winners WHERE competition_id = '" . (int)$competition_id . "'";
		$query = $this->db->query($sql);
		return $query->rows;
	}
	
	public function getName($competition_id) {
		$sql = "SELECT title FROM " . DB_PREFIX . "competition_description WHERE competition_id = '" . (int)$competition_id . "'"; 
		$query = $this->db->query($sql);
		return $query->row['title'];
	}
	
	public function saveWinners($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "competition_winners SET email_id= '" . $this->db->escape($data['email']) . "', name= '" . $this->db->escape($data['name']) . "', competition_id='" . (int)$data['competition_id'] . "'");
	}

	public function notify($competition_id, $email_id) {
		$this->db->query("UPDATE " . DB_PREFIX . "competition_winners SET notified = 1 WHERE email_id = '" . $this->db->escape($email_id) . "' AND competition_id = '" . (int)$competition_id . "'");
	}

	public function doUpgrade() {
		$test = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "competition` WHERE Field = 'image'");
		if (!$test->num_rows) {
			$upgrade1 = "ALTER TABLE `" . DB_PREFIX . "competition` ADD `image` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `competition_id`, ADD `width` SMALLINT(3) NULL AFTER `image`, ADD `height` SMALLINT(3) NULL AFTER `width` ";
			$this->db->query($upgrade1);
		}
		
		$test = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "competition` WHERE Field = 'newsletter'");
		if (!$test->num_rows) {
			$upgrade2 = "ALTER TABLE `" . DB_PREFIX . "competition` ADD `newsletter` TINYINT( 1 ) NOT NULL DEFAULT '0' AFTER `competition_id`";
			$this->db->query($upgrade2);
		}
		
		$test = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "competition` WHERE Field = 'winners'");
		if (!$test->num_rows) {
			$upgrade3 = "ALTER TABLE `" . DB_PREFIX . "competition` ADD `winners` INT( 3 ) NOT NULL DEFAULT '1' AFTER `newsletter`";
			$this->db->query($upgrade3);
		}
		
		$test = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "competition` WHERE Field = 'auto'");
		if (!$test->num_rows) {
			$upgrade4 = "ALTER TABLE `" . DB_PREFIX . "competition` ADD `auto` TINYINT( 1 ) NOT NULL AFTER `winners`";
			$this->db->query($upgrade4);
		}
		$test = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "competition` WHERE Field = 'auto_entry_value'");
		if (!$test->num_rows) {
			$upgrade5 = "ALTER TABLE `" . DB_PREFIX . "competition` ADD `auto_entry_value` DECIMAL(15, 4) NOT NULL AFTER `auto`";
			$this->db->query($upgrade5);
		}
		$test = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "competition_description` WHERE Field = 'auto_entry_message'");
		if (!$test->num_rows) {
			$upgrade6 = "ALTER TABLE `" . DB_PREFIX . "competition_description` ADD `auto_entry_message` varchar(255) NULL AFTER `meta_description`";
			$this->db->query($upgrade6);
		}
		
		$test = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "competition_winners` WHERE Field = 'notified'");
		if (!$test->num_rows) {
			$upgrade7 = "ALTER TABLE `" . DB_PREFIX . "competition_winners` ADD `notified` TINYINT( 1 ) NOT NULL AFTER `name`";
			$this->db->query($upgrade7);
		}
		
		$test = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "competition_entrants` WHERE Field = 'answer'");
		if (!$test->num_rows) {
			$upgrade8 = "ALTER TABLE `" . DB_PREFIX . "competition_entrants` ADD `answer` VARCHAR(4) NOT NULL DEFAULT '0' AFTER `name`";
			$this->db->query($upgrade8);
		}
		
		$test = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "competition_description` WHERE Field = 'answer'");
		if ($test->num_rows) {
			$upgrade9 = "ALTER TABLE `" . DB_PREFIX . "competition_description` DROP `answer`";
			$this->db->query($upgrade9);
		}
		
		$test = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "competition_description` WHERE Field = 'answer1'");
		if (!$test->num_rows) {
			$upgrade10 = "ALTER TABLE `" . DB_PREFIX . "competition_description` ADD `answer1` VARCHAR( 255 ) NOT NULL, ADD `answer2` VARCHAR( 255 ) NOT NULL, ADD `answer3` VARCHAR( 255 ) NOT NULL, ADD `answer4` VARCHAR( 255 ) NOT NULL";
			$this->db->query($upgrade10);
			$upgrade11 = "ALTER TABLE `" . DB_PREFIX . "competition` ADD `answer_is` VARCHAR(4) NOT NULL";
			$this->db->query($upgrade11);
		}
		
		$newsletter = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "c_newsletter` (`newsletter_id` int(10) NOT NULL AUTO_INCREMENT, `name` varchar(100) NOT NULL, `email` varchar(120) NOT NULL, PRIMARY KEY (`newsletter_id`)) ENGINE=MyISAM COLLATE=utf8_general_ci;";
		$this->db->query($newsletter);
	}
}
?>