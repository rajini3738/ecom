<?php
// HTTP
define('HTTP_SERVER', 'http://localhost:81/elegance/admin/');
define('HTTP_CATALOG', 'http://localhost:81/elegance');

// HTTPS
define('HTTPS_SERVER', 'http://localhost:81/elegance/admin/');
define('HTTPS_CATALOG', 'http://localhost:81/elegance/');

//define('HTTPS_SERVER', 'https://'.$_SERVER['SERVER_NAME'].':1443/opencart/admin/');
//define('HTTPS_CATALOG', 'https://'.$_SERVER['SERVER_NAME'].':1443/opencart/');

// DIR
define('DIR_APPLICATION', 'D:/xampp/htdocs/elegance/admin/');
define('DIR_SYSTEM', 'D:/xampp/htdocs/elegance/system/');
define('DIR_IMAGE', 'D:/xampp/htdocs/elegance/image/');
define('DIR_LANGUAGE', 'D:/xampp/htdocs/elegance/admin/language/');
define('DIR_TEMPLATE', 'D:/xampp/htdocs/elegance/admin/view/template/');
define('DIR_CONFIG', 'D:/xampp/htdocs/elegance/system/config/');
define('DIR_CACHE', 'D:/xampp/htdocs/elegance/system/cache/');
define('DIR_DOWNLOAD', 'D:/xampp/htdocs/elegance/system/download/');
define('DIR_LOGS', 'D:/xampp/htdocs/elegance/system/logs/');
define('DIR_MODIFICATION', 'D:/xampp/htdocs/elegance/system/modification/');
define('DIR_UPLOAD', 'D:/xampp/htdocs/elegance/system/upload/');
define('DIR_CATALOG', 'D:/xampp/htdocs/elegance/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'eleganceoud');
define('DB_PORT', '3307');
define('DB_PREFIX', 'oc_');