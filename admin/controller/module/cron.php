<?php
class ControllerModuleCron extends Controller {
	private $error = array();  
 
	public function index() {
		$this->load->language('sale/order');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('sale/order');

		$filter_data = array();

		$results = $this->model_sale_order->getOrders($filter_data);
		$date1 = date("Y-m-d H:i:s");
		foreach ($results as $result) {
			$date2 = date("Y-m-d H:i:s", strtotime($result['date_modified']));
			/*$diff = abs(strtotime($date1) - strtotime($date2));	
			$years = floor($diff / (365*60*60*24));
			$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));	
			$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));*/	
			$seconds = strtotime($date1) - strtotime($date2);
			$hours = $seconds / 60 / 60;
			
			if($result['customer_group_id']==1){
				if($hours >=12){
					if($result['payment_code']=="bank_transfer" && $result['proof_of_payment'] == null){
						//$this->delete($result['order_id']);
						$this->model_sale_order->addOrderHistory1($result['order_id'], 7);
					}
				}
			}
			if($result['customer_group_id']==3){
				if($hours >=24){
					if($result['payment_code']=="bank_transfer" && $result['proof_of_payment'] == null){
						//$this->delete($result['order_id']);
						$this->model_sale_order->addOrderHistory1($result['order_id'], 7);
					}
				}
			}
		}
		echo 'cron job successfully run.';
	}
	public function delete($order_id) {
		$curl = curl_init();

		// Set SSL if required
		if (substr(HTTPS_CATALOG, 0, 5) == 'https') {
			curl_setopt($curl, CURLOPT_PORT, 443);
		}

		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLINFO_HEADER_OUT, true);
		curl_setopt($curl, CURLOPT_USERAGENT, $this->request->server['HTTP_USER_AGENT']);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_FORBID_REUSE, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_URL, HTTPS_CATALOG . 'index.php?route=api/order/delete&order_id=' . $order_id);
		curl_setopt($curl, CURLOPT_COOKIE, session_name() . '=' . $this->session->data['cookie'] . ';');

		$json = curl_exec($curl);

		if (!$json) {
			echo sprintf($this->language->get('error_curl'), curl_error($curl), curl_errno($curl));
		} else {
			$response = json_decode($json, true);

			curl_close($curl);

			if (isset($response['error'])) {
				echo $response['error'];
			}
		}
		if (isset($response['success'])) {
			echo $response['success'];
		}
	}
}