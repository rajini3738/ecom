<?php
class ControllerModuleCzEditor extends Controller {
    private $error = array();
    private $moduleName 			= 'cz_editor';
    private $moduleModel 			= 'model_module_cz_editor';
    private $moduleVersion 			= '1.1';

    public function index() {
        $lang_ar = $this->load->language('module/cz_editor');

        foreach($lang_ar as $key => $item){
            $data[$key] = $item;
        }

        $this->load->model('module/cz_editor');

        if(!$this->model_module_cz_editor->getTableExist('country_description')){
            $data['text_install_module'] = $this->language->get('text_install_module');
            $data['href_install_module'] =$this->url->link('extension/module/install', 'token=' . $this->session->data['token'] . '&extension=cz_editor', 'SSL');
            $data['header'] = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');

            if(VERSION > '2.1.0.2'){
                return $this->response->setOutput($this->load->view('module/cz_editor_install', $data));
            } else {
                return $this->response->setOutput($this->load->view('module/cz_editor_install.tpl', $data));
            }
        }

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('cz_editor', $this->request->post);

            $this->cache->delete('country');
            $this->cache->delete('zone');

            $this->session->data['success'] = $this->language->get('text_success');

            if(!$this->request->post['apply']) {
                $this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
            }
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_module'),
            'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('module/cz_editor', 'token=' . $this->session->data['token'], true)
        );

        $data['action'] = $this->url->link('module/cz_editor', 'token=' . $this->session->data['token'], true);

        $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], true);

        $data['country'] = $this->url->link('module/cz_editor/country', 'token=' . $this->session->data['token'], true);

        $data['zone'] = $this->url->link('module/cz_editor/zone', 'token=' . $this->session->data['token'], true);

        $data['clear'] = $this->url->link('module/cz_editor/clearCache', 'token=' . $this->session->data['token'], true);

        $data['token'] = $this->session->data['token'];

        if (isset($this->request->post['cz_editor_country_multilang'])) {
            $data['cz_editor_country_multilang'] = $this->request->post['cz_editor_country_multilang'];
        } else {
            $data['cz_editor_country_multilang'] = $this->config->get('cz_editor_country_multilang');
        }

        if (isset($this->request->post['cz_editor_zone_multilang'])) {
            $data['cz_editor_zone_multilang'] = $this->request->post['cz_editor_zone_multilang'];
        } else {
            $data['cz_editor_zone_multilang'] = $this->config->get('cz_editor_zone_multilang');
        }

        if (isset($this->request->post['cz_editor_sort_country'])) {
            $data['cz_editor_sort_country'] = $this->request->post['cz_editor_sort_country'];
        } else {
            $data['cz_editor_sort_country'] = $this->config->get('cz_editor_sort_country');
        }

        if (isset($this->request->post['cz_editor_sort_zone'])) {
            $data['cz_editor_sort_zone'] = $this->request->post['cz_editor_sort_zone'];
        } else {
            $data['cz_editor_sort_zone'] = $this->config->get('cz_editor_sort_zone');
        }

        if (isset($this->request->post['cz_editor_order_id'])) {
            $data['cz_editor_order_id'] = $this->request->post['cz_editor_order_id'];
        } else {
            $data['cz_editor_order_id'] = $this->config->get('cz_editor_order_id');
        }

        if (isset($this->request->post['cz_editor_url'])) {
            $data['cz_editor_url'] = $this->request->post['cz_editor_url'];
        } else {
            $data['cz_editor_url'] = $this->config->get('cz_editor_url');
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        
        if(VERSION > '2.1.0.2'){
            $this->response->setOutput($this->load->view('module/cz_editor', $data));
        } else {
            $this->response->setOutput($this->load->view('module/cz_editor.tpl', $data));
        }
    }

    protected function validate() {
        if (!$this->user->hasPermission('modify', 'module/cz_editor')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    public function clearCache() {

        $this->load->language('module/cz_editor');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('module/cz_editor');
        
        $this->model_module_cz_editor->clearCache();
    
        $this->response->redirect($this->url->link('module/cz_editor', 'token=' . $this->session->data['token'], true));

    }

    public function country() {
        $this->load->language('module/cz_editor');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('module/cz_editor');

        $this->getListCountry();
    }

    public function addCountry() {
        
        $this->load->language('module/cz_editor');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('module/cz_editor');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateFormCountry()) {
            $this->model_module_cz_editor->addCountry($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';
            
            if (isset($this->request->get['filter_country_id'])) {
                $url .= '&filter_country_id=' . $this->request->get['filter_country_id'];
            }

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_iso_code_2'])) {
                $url .= '&filter_iso_code_2=' . urlencode(html_entity_decode($this->request->get['filter_iso_code_2'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_iso_code_3'])) {
                $url .= '&filter_iso_code_3=' . urlencode(html_entity_decode($this->request->get['filter_iso_code_3'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_postcode_required'])) {
                $url .= '&filter_postcode_required=' . $this->request->get['filter_postcode_required'];
            }

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('module/cz_editor/country', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getFormCountry();
    }

    public function editCountry() {
        
        $this->load->language('module/cz_editor');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('module/cz_editor');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateFormCountry()) {
            $this->model_module_cz_editor->editCountry($this->request->get['country_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';
            
            if (isset($this->request->get['filter_country_id'])) {
                $url .= '&filter_country_id=' . $this->request->get['filter_country_id'];
            }

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_iso_code_2'])) {
                $url .= '&filter_iso_code_2=' . urlencode(html_entity_decode($this->request->get['filter_iso_code_2'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_iso_code_3'])) {
                $url .= '&filter_iso_code_3=' . urlencode(html_entity_decode($this->request->get['filter_iso_code_3'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_postcode_required'])) {
                $url .= '&filter_postcode_required=' . $this->request->get['filter_postcode_required'];
            }

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('module/cz_editor/country', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getFormCountry();
    }

    public function deleteCountry() {
        
        $this->load->language('module/cz_editor');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('module/cz_editor');

        if (isset($this->request->post['selected']) && $this->validateDeleteCountry()) {
            foreach ($this->request->post['selected'] as $country_id) {
                $this->model_module_cz_editor->deleteCountry($country_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['filter_country_id'])) {
                $url .= '&filter_country_id=' . $this->request->get['filter_country_id'];
            }

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_iso_code_2'])) {
                $url .= '&filter_iso_code_2=' . urlencode(html_entity_decode($this->request->get['filter_iso_code_2'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_iso_code_3'])) {
                $url .= '&filter_iso_code_3=' . urlencode(html_entity_decode($this->request->get['filter_iso_code_3'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_postcode_required'])) {
                $url .= '&filter_postcode_required=' . $this->request->get['filter_postcode_required'];
            }

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('module/cz_editor/country', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getListCountry();
    }

    public function enableCountry() {
        
        $this->load->language('module/cz_editor');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('module/cz_editor');

        if (isset($this->request->post['selected'])) {
            foreach ($this->request->post['selected'] as $country_id) {
                $this->model_module_cz_editor->statusCountry($country_id, 1);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';
            
            if (isset($this->request->get['filter_country_id'])) {
                $url .= '&filter_country_id=' . $this->request->get['filter_country_id'];
            }

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_iso_code_2'])) {
                $url .= '&filter_iso_code_2=' . urlencode(html_entity_decode($this->request->get['filter_iso_code_2'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_iso_code_3'])) {
                $url .= '&filter_iso_code_3=' . urlencode(html_entity_decode($this->request->get['filter_iso_code_3'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_postcode_required'])) {
                $url .= '&filter_postcode_required=' . $this->request->get['filter_postcode_required'];
            }

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('module/cz_editor/country', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getListCountry();
    }

    public function disableCountry() {
        
        $this->load->language('module/cz_editor');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('module/cz_editor');

        if (isset($this->request->post['selected'])) {
            foreach ($this->request->post['selected'] as $country_id) {
                $this->model_module_cz_editor->statusCountry($country_id, 0);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['filter_country_id'])) {
                $url .= '&filter_country_id=' . $this->request->get['filter_country_id'];
            }

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_iso_code_2'])) {
                $url .= '&filter_iso_code_2=' . urlencode(html_entity_decode($this->request->get['filter_iso_code_2'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_iso_code_3'])) {
                $url .= '&filter_iso_code_3=' . urlencode(html_entity_decode($this->request->get['filter_iso_code_3'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_postcode_required'])) {
                $url .= '&filter_postcode_required=' . $this->request->get['filter_postcode_required'];
            }

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('module/cz_editor/country', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getListCountry();
    }

    protected function getListCountry() {

        $lang_ar = $this->load->language('module/cz_editor');

        if (isset($this->request->get['filter_country_id'])) {
            $filter_country_id = $this->request->get['filter_country_id'];
        } else {
            $filter_country_id = null;
        }

        if (isset($this->request->get['filter_name'])) {
            $filter_name = $this->request->get['filter_name'];
        } else {
            $filter_name = null;
        }

        if (isset($this->request->get['filter_iso_code_2'])) {
            $filter_iso_code_2 = $this->request->get['filter_iso_code_2'];
        } else {
            $filter_iso_code_2 = null;
        }

        if (isset($this->request->get['filter_iso_code_3'])) {
            $filter_iso_code_3 = $this->request->get['filter_iso_code_3'];
        } else {
            $filter_iso_code_3 = null;
        }

        if (isset($this->request->get['filter_postcode_required'])) {
            $filter_postcode_required = $this->request->get['filter_postcode_required'];
        } else {
            $filter_postcode_required = null;
        }

        if (isset($this->request->get['filter_status'])) {
            $filter_status = $this->request->get['filter_status'];
        } else {
            $filter_status = null;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'name';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['filter_country_id'])) {
            $url .= '&filter_country_id=' . $this->request->get['filter_country_id'];
        }

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_iso_code_2'])) {
            $url .= '&filter_iso_code_2=' . urlencode(html_entity_decode($this->request->get['filter_iso_code_2'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_iso_code_3'])) {
            $url .= '&filter_iso_code_3=' . urlencode(html_entity_decode($this->request->get['filter_iso_code_3'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_postcode_required'])) {
            $url .= '&filter_postcode_required=' . $this->request->get['filter_postcode_required'];
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_module'),
            'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'] . $url, true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('module/cz_editor', 'token=' . $this->session->data['token'] . $url, true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_country_list'),
            'href' => $this->url->link('module/cz_editor/country', 'token=' . $this->session->data['token'] . $url, true)
        );

        $data['add'] = $this->url->link('module/cz_editor/addCountry', 'token=' . $this->session->data['token'] . $url, true);
        $data['delete'] = $this->url->link('module/cz_editor/deleteCountry', 'token=' . $this->session->data['token'] . $url, true);
        $data['enable'] = $this->url->link('module/cz_editor/enableCountry', 'token=' . $this->session->data['token'] . $url, true);
        $data['disable'] = $this->url->link('module/cz_editor/disableCountry', 'token=' . $this->session->data['token'] . $url, true);
        $data['setting'] = $this->url->link('module/cz_editor', 'token=' . $this->session->data['token'] , true);
        $data['zone'] = $this->url->link('module/cz_editor/zone', 'token=' . $this->session->data['token'] , true);

        $data['token'] = $this->session->data['token'];

        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();

        $data['cz_editor_country_multilang'] = $this->config->get('cz_editor_country_multilang');

        $data['countries'] = array();

        $filter_data = array(
            'filter_country_id'	       => $filter_country_id,
            'filter_name'	           => $filter_name,
            'filter_iso_code_2'	  	   => $filter_iso_code_2,
            'filter_iso_code_3'	  	   => $filter_iso_code_3,
            'filter_postcode_required' => $filter_postcode_required,
            'filter_status'            => $filter_status,
            'sort'  => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $country_total = $this->model_module_cz_editor->getTotalCountries($filter_data);

        $results = $this->model_module_cz_editor->getCountries($filter_data);

        foreach ($results as $result) {
            $data['countries'][] = array(
                'country_id' => $result['country_id'],
                'name'       => $result['name'] . (($result['country_id'] == $this->config->get('config_country_id')) ? $this->language->get('text_default') : null),
                'name_multi' => $this->config->get('cz_editor_country_multilang') ? $this->model_module_cz_editor->getCountryDescriptions($result['country_id']) : '',
                'iso_code_2' => $result['iso_code_2'],
                'iso_code_3' => $result['iso_code_3'],
                'sort_order' => $result['sort_order'],
                'postcode_required' => $result['postcode_required'],
                'status' => $result['status'],
                'edit'       => $this->url->link('module/cz_editor/editCountry', 'token=' . $this->session->data['token'] . '&country_id=' . $result['country_id'] . $url, true)
            );
        }

        foreach($lang_ar as $key => $item){
            $data[$key] = $item;
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';

        if (isset($this->request->get['filter_country_id'])) {
            $url .= '&filter_country_id=' . $this->request->get['filter_country_id'];
        }

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_iso_code_2'])) {
            $url .= '&filter_iso_code_2=' . urlencode(html_entity_decode($this->request->get['filter_iso_code_2'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_iso_code_3'])) {
            $url .= '&filter_iso_code_3=' . urlencode(html_entity_decode($this->request->get['filter_iso_code_3'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_postcode_required'])) {
            $url .= '&filter_postcode_required=' . $this->request->get['filter_postcode_required'];
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_country_id'] = $this->url->link('module/cz_editor/country', 'token=' . $this->session->data['token'] . '&sort=country_id' . $url, true);
        $data['sort_name'] = $this->url->link('module/cz_editor/country', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
        $data['sort_iso_code_2'] = $this->url->link('module/cz_editor/country', 'token=' . $this->session->data['token'] . '&sort=iso_code_2' . $url, true);
        $data['sort_iso_code_3'] = $this->url->link('module/cz_editor/country', 'token=' . $this->session->data['token'] . '&sort=iso_code_3' . $url, true);
        $data['sort_sort_order'] = $this->url->link('module/cz_editor/country', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);
        $data['sort_postcode_required'] = $this->url->link('module/cz_editor/country', 'token=' . $this->session->data['token'] . '&sort=postcode_required' . $url, true);
        $data['sort_status'] = $this->url->link('module/cz_editor/country', 'token=' . $this->session->data['token'] . '&sort=status' . $url, true);

        $url = '';

        if (isset($this->request->get['filter_country_id'])) {
            $url .= '&filter_country_id=' . $this->request->get['filter_country_id'];
        }

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_iso_code_2'])) {
            $url .= '&filter_iso_code_2=' . urlencode(html_entity_decode($this->request->get['filter_iso_code_2'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_iso_code_3'])) {
            $url .= '&filter_iso_code_3=' . urlencode(html_entity_decode($this->request->get['filter_iso_code_3'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_postcode_required'])) {
            $url .= '&filter_postcode_required=' . $this->request->get['filter_postcode_required'];
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $country_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('module/cz_editor/country', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($country_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($country_total - $this->config->get('config_limit_admin'))) ? $country_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $country_total, ceil($country_total / $this->config->get('config_limit_admin')));

        $data['filter_country_id'] = $filter_country_id;
        $data['filter_name'] = $filter_name;
        $data['filter_iso_code_2'] = $filter_iso_code_2;
        $data['filter_iso_code_3'] = $filter_iso_code_3;
        $data['filter_postcode_required'] = $filter_postcode_required;
        $data['filter_status'] = $filter_status;

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if(VERSION > '2.1.0.2'){
            $this->response->setOutput($this->load->view('module/cz_editor_country_list', $data));
        } else {
            $this->response->setOutput($this->load->view('module/cz_editor_country_list.tpl', $data));
        }
    }

    protected function getFormCountry() {
        $lang_ar = $this->load->language('module/cz_editor');

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_form'] = !isset($this->request->get['country_id']) ? $this->language->get('text_add_country') : $this->language->get('text_edit_country');

        foreach($lang_ar as $key => $item){
            $data[$key] = $item;
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['default_name'])) {
            $data['error_default_name'] = $this->error['default_name'];
        } else {
            $data['error_default_name'] = '';
        }

        if ($this->config->get('cz_editor_country_multilang') && isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = '';
        }

        $url = '';

        if (isset($this->request->get['filter_country_id'])) {
            $url .= '&filter_country_id=' . $this->request->get['filter_country_id'];
        }

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_iso_code_2'])) {
            $url .= '&filter_iso_code_2=' . urlencode(html_entity_decode($this->request->get['filter_iso_code_2'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_iso_code_3'])) {
            $url .= '&filter_iso_code_3=' . urlencode(html_entity_decode($this->request->get['filter_iso_code_3'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_postcode_required'])) {
            $url .= '&filter_postcode_required=' . $this->request->get['filter_postcode_required'];
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_module'),
            'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'] . $url, true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('module/cz_editor', 'token=' . $this->session->data['token'] . $url, true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_country_list'),
            'href' => $this->url->link('module/cz_editor/country', 'token=' . $this->session->data['token'] . $url, true)
        );

        if (!isset($this->request->get['country_id'])) {
            $data['action'] = $this->url->link('module/cz_editor/addCountry', 'token=' . $this->session->data['token'] . $url, true);
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_add_country'),
                'href' => $this->url->link('module/cz_editor/addCountry', 'token=' . $this->session->data['token'] . $url, true)
            );
        } else {
            $data['action'] = $this->url->link('module/cz_editor/editCountry', 'token=' . $this->session->data['token'] . '&country_id=' . $this->request->get['country_id'] . $url, true);
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_edit_country'),
                'href' => $this->url->link('module/cz_editor/editCountry', 'token=' . $this->session->data['token'] . '&country_id=' . $this->request->get['country_id'] . $url, true)
            );
        }

        $data['cancel'] = $this->url->link('module/cz_editor/country', 'token=' . $this->session->data['token'] . $url, true);

        $data['cz_editor_country_multilang'] = $this->config->get('cz_editor_country_multilang');

        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();

        if (isset($this->request->get['country_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $country_info = $this->model_module_cz_editor->getCountry($this->request->get['country_id']);
        }

        if (isset($this->request->post['country_description'])) {
            $data['country_description'] = $this->request->post['country_description'];
        } elseif (isset($this->request->get['country_id'])) {
            $data['country_description'] = $this->model_module_cz_editor->getCountryDescriptions($this->request->get['country_id']);
        } else {
            $data['country_description'] = array();
        }

        if (isset($this->request->post['name'])) {
            $data['name'] = $this->request->post['name'];
        } elseif (!empty($country_info)) {
            $data['name'] = $country_info['name'];
        } else {
            $data['name'] = '';
        }

        if (isset($this->request->post['iso_code_2'])) {
            $data['iso_code_2'] = $this->request->post['iso_code_2'];
        } elseif (!empty($country_info)) {
            $data['iso_code_2'] = $country_info['iso_code_2'];
        } else {
            $data['iso_code_2'] = '';
        }

        if (isset($this->request->post['iso_code_3'])) {
            $data['iso_code_3'] = $this->request->post['iso_code_3'];
        } elseif (!empty($country_info)) {
            $data['iso_code_3'] = $country_info['iso_code_3'];
        } else {
            $data['iso_code_3'] = '';
        }

        if (isset($this->request->post['address_format'])) {
            $data['address_format'] = $this->request->post['address_format'];
        } elseif (!empty($country_info)) {
            $data['address_format'] = $country_info['address_format'];
        } else {
            $data['address_format'] = '';
        }

        if (isset($this->request->post['postcode_required'])) {
            $data['postcode_required'] = $this->request->post['postcode_required'];
        } elseif (!empty($country_info)) {
            $data['postcode_required'] = $country_info['postcode_required'];
        } else {
            $data['postcode_required'] = 0;
        }

        if (isset($this->request->post['sort_order'])) {
            $data['sort_order'] = $this->request->post['sort_order'];
        } elseif (!empty($country_info)) {
            $data['sort_order'] = $country_info['sort_order'];
        } else {
            $data['sort_order'] = 0;
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($country_info)) {
            $data['status'] = $country_info['status'];
        } else {
            $data['status'] = '1';
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        
        if(VERSION > '2.1.0.2'){
            $this->response->setOutput($this->load->view('module/cz_editor_country_form', $data));
        } else {
            $this->response->setOutput($this->load->view('module/cz_editor_country_form.tpl', $data));
        }
    }

    protected function validateFormCountry() {

        if (!$this->user->hasPermission('modify', 'module/cz_editor')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 128)) {
			$this->error['default_name'] = $this->language->get('error_country_name');
		}

        if($this->config->get('cz_editor_country_multilang')){
            foreach ($this->request->post['country_description'] as $language_id => $value) {
                if ((utf8_strlen($value['name']) < 1) || (utf8_strlen($value['name']) > 128)) {
                    $this->error['name'][$language_id] = $this->language->get('error_country_name');
                }
            }
        }

        return !$this->error;
    }

    protected function validateDeleteCountry() {
        if (!$this->user->hasPermission('modify', 'module/cz_editor')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        $this->load->model('setting/store');
        $this->load->model('customer/customer');
        $this->load->model('marketing/affiliate');
        $this->load->model('module/cz_editor');
        $this->load->model('localisation/geo_zone');

        foreach ($this->request->post['selected'] as $country_id) {
            if ($this->config->get('config_country_id') == $country_id) {
                $this->error['warning'] = $this->language->get('error_country_default');
            }

            $store_total = $this->model_setting_store->getTotalStoresByCountryId($country_id);

            if ($store_total) {
                $this->error['warning'] = sprintf($this->language->get('error_country_store'), $store_total);
            }

            $address_total = $this->model_customer_customer->getTotalAddressesByCountryId($country_id);

            if ($address_total) {
                $this->error['warning'] = sprintf($this->language->get('error_country_address'), $address_total);
            }

            $affiliate_total = $this->model_marketing_affiliate->getTotalAffiliatesByCountryId($country_id);

            if ($affiliate_total) {
                $this->error['warning'] = sprintf($this->language->get('error_country_affiliate'), $affiliate_total);
            }

            $zone_total = $this->model_module_cz_editor->getTotalZonesByCountryId($country_id);

            if ($zone_total) {
                $this->error['warning'] = sprintf($this->language->get('error_country_zone'), $zone_total);
            }

            $zone_to_geo_zone_total = $this->model_localisation_geo_zone->getTotalZoneToGeoZoneByCountryId($country_id);

            if ($zone_to_geo_zone_total) {
                $this->error['warning'] = sprintf($this->language->get('error_country_zone_to_geo_zone'), $zone_to_geo_zone_total);
            }
        }

        return !$this->error;
    }

    public function zone() {
        $this->load->language('module/cz_editor');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('module/cz_editor');

        $this->getListZone();
    }

    public function addZone() {
        $this->load->language('module/cz_editor');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('module/cz_editor');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateFormZone()) {
            $this->model_module_cz_editor->addZone($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success_zone');

            $url = '';

            if (isset($this->request->get['filter_country_id'])) {
                $url .= '&filter_country_id=' . $this->request->get['filter_country_id'];
            }

            if (isset($this->request->get['filter_country'])) {
                $url .= '&filter_country=' . urlencode(html_entity_decode($this->request->get['filter_country'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_zone_id'])) {
                $url .= '&filter_zone_id=' . $this->request->get['filter_zone_id'];
            }

            if (isset($this->request->get['filter_code'])) {
                $url .= '&filter_code=' . urlencode(html_entity_decode($this->request->get['filter_code'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('module/cz_editor/zone', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getFormZone();
    }

    public function editZone() {
        $this->load->language('module/cz_editor');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('module/cz_editor');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateFormZone()) {
            $this->model_module_cz_editor->editZone($this->request->get['zone_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success_zone');

            $url = '';

            if (isset($this->request->get['filter_country_id'])) {
                $url .= '&filter_country_id=' . $this->request->get['filter_country_id'];
            }

            if (isset($this->request->get['filter_country'])) {
                $url .= '&filter_country=' . urlencode(html_entity_decode($this->request->get['filter_country'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_zone_id'])) {
                $url .= '&filter_zone_id=' . $this->request->get['filter_zone_id'];
            }

            if (isset($this->request->get['filter_code'])) {
                $url .= '&filter_code=' . urlencode(html_entity_decode($this->request->get['filter_code'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('module/cz_editor/zone', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getFormZone();
    }

    public function enableZone() {
        
        $this->load->language('module/cz_editor');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('module/cz_editor');

        if (isset($this->request->post['selected'])) {
            foreach ($this->request->post['selected'] as $zone_id) {
                $this->model_module_cz_editor->statusZone($zone_id, 1);
            }

            $this->session->data['success'] = $this->language->get('text_success_zone');

            $url = '';

            if (isset($this->request->get['filter_country_id'])) {
                $url .= '&filter_country_id=' . $this->request->get['filter_country_id'];
            }

            if (isset($this->request->get['filter_country'])) {
                $url .= '&filter_country=' . urlencode(html_entity_decode($this->request->get['filter_country'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_zone_id'])) {
                $url .= '&filter_zone_id=' . $this->request->get['filter_zone_id'];
            }

            if (isset($this->request->get['filter_code'])) {
                $url .= '&filter_code=' . urlencode(html_entity_decode($this->request->get['filter_code'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('module/cz_editor/zone', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getListZone();
    }

    public function disableZone() {
        
        $this->load->language('module/cz_editor');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('module/cz_editor');

        if (isset($this->request->post['selected'])) {
            foreach ($this->request->post['selected'] as $zone_id) {
                $this->model_module_cz_editor->statusZone($zone_id, 0);
            }

            $this->session->data['success'] = $this->language->get('text_success_zone');

            $url = '';

            if (isset($this->request->get['filter_country_id'])) {
                $url .= '&filter_country_id=' . $this->request->get['filter_country_id'];
            }

            if (isset($this->request->get['filter_country'])) {
                $url .= '&filter_country=' . urlencode(html_entity_decode($this->request->get['filter_country'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_zone_id'])) {
                $url .= '&filter_zone_id=' . $this->request->get['filter_zone_id'];
            }

            if (isset($this->request->get['filter_code'])) {
                $url .= '&filter_code=' . urlencode(html_entity_decode($this->request->get['filter_code'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('module/cz_editor/zone', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getListZone();
    }

    public function deleteZone() {
        
        $this->load->language('module/cz_editor');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('module/cz_editor');

        if (isset($this->request->post['selected']) && $this->validateDeleteZone()) {
            foreach ($this->request->post['selected'] as $zone_id) {
                $this->model_module_cz_editor->deleteZone($zone_id);
            }

            $this->session->data['success'] = $this->language->get('text_success_zone');

            $url = '';

            if (isset($this->request->get['filter_country_id'])) {
                $url .= '&filter_country_id=' . $this->request->get['filter_country_id'];
            }

            if (isset($this->request->get['filter_country'])) {
                $url .= '&filter_country=' . urlencode(html_entity_decode($this->request->get['filter_country'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_zone_id'])) {
                $url .= '&filter_zone_id=' . $this->request->get['filter_zone_id'];
            }

            if (isset($this->request->get['filter_code'])) {
                $url .= '&filter_code=' . urlencode(html_entity_decode($this->request->get['filter_code'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_status'])) {
                $url .= '&filter_status=' . $this->request->get['filter_status'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('module/cz_editor/zone', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getListZone();
    }

    protected function getListZone() {

        $lang_ar = $this->load->language('module/cz_editor');

        foreach($lang_ar as $key => $item){
            $data[$key] = $item;
        }

        if (isset($this->request->get['filter_country_id'])) {
            $filter_country_id = $this->request->get['filter_country_id'];
        } else {
            $filter_country_id = null;
        }

        if (isset($this->request->get['filter_country'])) {
            $filter_country = $this->request->get['filter_country'];
        } else {
            $filter_country = null;
        }

        if (isset($this->request->get['filter_name'])) {
            $filter_name = $this->request->get['filter_name'];
        } else {
            $filter_name = null;
        }

        if (isset($this->request->get['filter_zone_id'])) {
            $filter_zone_id = $this->request->get['filter_zone_id'];
        } else {
            $filter_zone_id = null;
        }

        if (isset($this->request->get['filter_code'])) {
            $filter_code = $this->request->get['filter_code'];
        } else {
            $filter_code = null;
        }

        if (isset($this->request->get['filter_status'])) {
            $filter_status = $this->request->get['filter_status'];
        } else {
            $filter_status = null;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'c.name';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['filter_country_id'])) {
            $url .= '&filter_country_id=' . $this->request->get['filter_country_id'];
        }

        if (isset($this->request->get['filter_country'])) {
            $url .= '&filter_country=' . urlencode(html_entity_decode($this->request->get['filter_country'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_zone_id'])) {
            $url .= '&filter_zone_id=' . $this->request->get['filter_zone_id'];
        }

        if (isset($this->request->get['filter_code'])) {
            $url .= '&filter_code=' . urlencode(html_entity_decode($this->request->get['filter_code'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_module'),
            'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'] . $url, true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('module/cz_editor', 'token=' . $this->session->data['token'] . $url, true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_zone_list'),
            'href' => $this->url->link('module/cz_editor/zone', 'token=' . $this->session->data['token'] . $url, true)
        );

        $data['add'] = $this->url->link('module/cz_editor/addZone', 'token=' . $this->session->data['token'] . $url, true);
        $data['delete'] = $this->url->link('module/cz_editor/deleteZone', 'token=' . $this->session->data['token'] . $url, true);
        $data['enable'] = $this->url->link('module/cz_editor/enableZone', 'token=' . $this->session->data['token'] . $url, true);
        $data['disable'] = $this->url->link('module/cz_editor/disableZone', 'token=' . $this->session->data['token'] . $url, true);
        $data['setting'] = $this->url->link('module/cz_editor', 'token=' . $this->session->data['token'] , true);
        $data['country'] = $this->url->link('module/cz_editor/country', 'token=' . $this->session->data['token'] , true);

        $data['token'] = $this->session->data['token'];

        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();

        $data['cz_editor_zone_multilang'] = $this->config->get('cz_editor_zone_multilang');

        $data['zones'] = array();

        $filter_data = array(
            'filter_country_id'	       => $filter_country_id,
            'filter_country'	       => $filter_country,
            'filter_name'	           => $filter_name,
            'filter_zone_id'	       => $filter_zone_id,
            'filter_code'	  	       => $filter_code,
            'filter_status'            => $filter_status,
            'sort'  => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $zone_total = $this->model_module_cz_editor->getTotalZones($filter_data);

        $results = $this->model_module_cz_editor->getZones($filter_data);

        foreach ($results as $result) {
            $data['zones'][] = array(
                'zone_id' => $result['zone_id'],
                'country_id' => $result['country_id'],
                'country' => $result['country'],
                'name'    => $result['name'] . (($result['zone_id'] == $this->config->get('config_zone_id')) ? $this->language->get('text_default') : null),
                'name_multi' => $this->config->get('cz_editor_zone_multilang') ? $this->model_module_cz_editor->getZoneDescriptions($result['zone_id']) : '',
                'code'    => $result['code'],
                'iso_code_2' => $result['iso_code_2'],
                'sort_order' => $result['sort_order'],
                'status' => $result['status'],
                'edit'    => $this->url->link('module/cz_editor/editZone', 'token=' . $this->session->data['token'] . '&zone_id=' . $result['zone_id'] . $url, true)
            );
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';

        if (isset($this->request->get['filter_country_id'])) {
            $url .= '&filter_country_id=' . $this->request->get['filter_country_id'];
        }

        if (isset($this->request->get['filter_country'])) {
            $url .= '&filter_country=' . urlencode(html_entity_decode($this->request->get['filter_country'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_zone_id'])) {
            $url .= '&filter_zone_id=' . $this->request->get['filter_zone_id'];
        }

        if (isset($this->request->get['filter_code'])) {
            $url .= '&filter_code=' . urlencode(html_entity_decode($this->request->get['filter_code'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_country_id'] = $this->url->link('module/cz_editor/zone', 'token=' . $this->session->data['token'] . '&sort=c.country_id' . $url, true);
        $data['sort_country'] = $this->url->link('module/cz_editor/zone', 'token=' . $this->session->data['token'] . '&sort=c.name' . $url, true);
        $data['sort_zone_id'] = $this->url->link('module/cz_editor/zone', 'token=' . $this->session->data['token'] . '&sort=z.zone_id' . $url, true);
        $data['sort_name'] = $this->url->link('module/cz_editor/zone', 'token=' . $this->session->data['token'] . '&sort=z.name' . $url, true);
        $data['sort_code'] = $this->url->link('module/cz_editor/zone', 'token=' . $this->session->data['token'] . '&sort=z.code' . $url, true);
        $data['sort_sort_order'] = $this->url->link('module/cz_editor/zone', 'token=' . $this->session->data['token'] . '&sort=z.sort_order' . $url, true);
        $data['sort_status'] = $this->url->link('module/cz_editor/zone', 'token=' . $this->session->data['token'] . '&sort=z.status' . $url, true);

        $url = '';

        if (isset($this->request->get['filter_country_id'])) {
            $url .= '&filter_country_id=' . $this->request->get['filter_country_id'];
        }

        if (isset($this->request->get['filter_country'])) {
            $url .= '&filter_country=' . urlencode(html_entity_decode($this->request->get['filter_country'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_zone_id'])) {
            $url .= '&filter_zone_id=' . $this->request->get['filter_zone_id'];
        }

        if (isset($this->request->get['filter_code'])) {
            $url .= '&filter_code=' . urlencode(html_entity_decode($this->request->get['filter_code'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $zone_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('module/cz_editor/zone', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($zone_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($zone_total - $this->config->get('config_limit_admin'))) ? $zone_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $zone_total, ceil($zone_total / $this->config->get('config_limit_admin')));

        $data['filter_country_id'] = $filter_country_id;
        $data['filter_country'] = $filter_country;
        $data['filter_name'] = $filter_name;
        $data['filter_zone_id'] = $filter_zone_id;
        $data['filter_code'] = $filter_code;
        $data['filter_status'] = $filter_status;

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if(VERSION > '2.1.0.2') {
            $this->response->setOutput($this->load->view('module/cz_editor_zone_list', $data));
        } else {
            $this->response->setOutput($this->load->view('module/cz_editor_zone_list.tpl', $data));
        }
    }

    protected function getFormZone() {

        $lang_ar = $this->load->language('module/cz_editor');

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_form'] = !isset($this->request->get['zone_id']) ? $this->language->get('text_add_zone') : $this->language->get('text_edit_zone');

        foreach($lang_ar as $key => $item){
            $data[$key] = $item;
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['default_name'])) {
            $data['error_default_name'] = $this->error['default_name'];
        } else {
            $data['error_default_name'] = '';
        }

        if ($this->config->get('cz_editor_country_multilang') && isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = '';
        }

        $url = '';

        if (isset($this->request->get['filter_country_id'])) {
            $url .= '&filter_country_id=' . $this->request->get['filter_country_id'];
        }

        if (isset($this->request->get['filter_country'])) {
            $url .= '&filter_country=' . urlencode(html_entity_decode($this->request->get['filter_country'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_zone_id'])) {
            $url .= '&filter_zone_id=' . $this->request->get['filter_zone_id'];
        }

        if (isset($this->request->get['filter_code'])) {
            $url .= '&filter_code=' . urlencode(html_entity_decode($this->request->get['filter_code'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_status'])) {
            $url .= '&filter_status=' . $this->request->get['filter_status'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_module'),
            'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('module/cz_editor', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_zone_list'),
            'href' => $this->url->link('module/cz_editor/zone', 'token=' . $this->session->data['token'], true)
        );

        if (!isset($this->request->get['zone_id'])) {
            $data['action'] = $this->url->link('module/cz_editor/addZone', 'token=' . $this->session->data['token'] . $url, true);
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_add_zone'),
                'href' => $this->url->link('module/cz_editor/addZone', 'token=' . $this->session->data['token'] . $url, true)
            );
        } else {
            $data['action'] = $this->url->link('module/cz_editor/editZone', 'token=' . $this->session->data['token'] . '&zone_id=' . $this->request->get['zone_id'] . $url, true);
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_edit_zone'),
                'href' => $this->url->link('module/cz_editor/editZone', 'token=' . $this->session->data['token'] . '&zone_id=' . $this->request->get['zone_id'] . $url, true)
            );
        }

        $data['cancel'] = $this->url->link('module/cz_editor/zone', 'token=' . $this->session->data['token'] . $url, true);

        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();

        $data['cz_editor_zone_multilang'] = $this->config->get('cz_editor_zone_multilang');

        if (isset($this->request->get['zone_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $zone_info = $this->model_module_cz_editor->getZone($this->request->get['zone_id']);
        }

        if (isset($this->request->post['zone_description'])) {
            $data['zone_description'] = $this->request->post['zone_description'];
        } elseif (isset($this->request->get['zone_id'])) {
            $data['zone_description'] = $this->model_module_cz_editor->getZoneDescriptions($this->request->get['zone_id']);
        } else {
            $data['zone_description'] = array();
        }

        if (isset($this->request->post['sort_order'])) {
            $data['sort_order'] = $this->request->post['sort_order'];
        } elseif (!empty($zone_info)) {
            $data['sort_order'] = $zone_info['sort_order'];
        } else {
            $data['sort_order'] = 0;
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($zone_info)) {
            $data['status'] = $zone_info['status'];
        } else {
            $data['status'] = '1';
        }

        if (isset($this->request->post['name'])) {
            $data['name'] = $this->request->post['name'];
        } elseif (!empty($zone_info)) {
            $data['name'] = $zone_info['name'];
        } else {
            $data['name'] = '';
        }

        if (isset($this->request->post['code'])) {
            $data['code'] = $this->request->post['code'];
        } elseif (!empty($zone_info)) {
            $data['code'] = $zone_info['code'];
        } else {
            $data['code'] = '';
        }

        if (isset($this->request->post['country_id'])) {
            $data['country_id'] = $this->request->post['country_id'];
        } elseif (!empty($zone_info)) {
            $data['country_id'] = $zone_info['country_id'];
        } else {
            $data['country_id'] = '';
        }
        $data['country_id'] = '184';    
        $this->load->model('module/cz_editor');

        $data['countries'] = $this->model_module_cz_editor->getCountries();

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        
        if(VERSION > '2.1.0.2'){
            $this->response->setOutput($this->load->view('module/cz_editor_zone_form', $data));
        } else {
            $this->response->setOutput($this->load->view('module/cz_editor_zone_form.tpl', $data));
        }
    }

    protected function validateFormZone() {
        if (!$this->user->hasPermission('modify', 'module/cz_editor')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
            $this->error['default_name'] = $this->language->get('error_zone_name');
        }

        if($this->config->get('cz_editor_zone_multilang')){
            foreach ($this->request->post['zone_description'] as $language_id => $value) {
                if ((utf8_strlen($value['name']) < 1) || (utf8_strlen($value['name']) > 128)) {
                    $this->error['name'][$language_id] = $this->language->get('error_zone_name');
                }
            }
        }

        return !$this->error;
    }

    protected function validateDeleteZone() {
        if (!$this->user->hasPermission('modify', 'module/cz_editor')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        $this->load->model('setting/store');
        $this->load->model('customer/customer');
        $this->load->model('marketing/affiliate');
        $this->load->model('localisation/geo_zone');

        foreach ($this->request->post['selected'] as $zone_id) {
            if ($this->config->get('config_zone_id') == $zone_id) {
                $this->error['warning'] = $this->language->get('error_zone_default');
            }

            $store_total = $this->model_setting_store->getTotalStoresByZoneId($zone_id);

            if ($store_total) {
                $this->error['warning'] = sprintf($this->language->get('error_zone_store'), $store_total);
            }

            $address_total = $this->model_customer_customer->getTotalAddressesByZoneId($zone_id);

            if ($address_total) {
                $this->error['warning'] = sprintf($this->language->get('error_zone_address'), $address_total);
            }

            $affiliate_total = $this->model_marketing_affiliate->getTotalAffiliatesByZoneId($zone_id);

            if ($affiliate_total) {
                $this->error['warning'] = sprintf($this->language->get('error_zone_affiliate'), $affiliate_total);
            }

            $zone_to_geo_zone_total = $this->model_localisation_geo_zone->getTotalZoneToGeoZoneByZoneId($zone_id);

            if ($zone_to_geo_zone_total) {
                $this->error['warning'] = sprintf($this->language->get('error_zone_zone_to_geo_zone'), $zone_to_geo_zone_total);
            }
        }

        return !$this->error;
    }

    public function install() {
        $this->load->model('module/'.$this->moduleName);
        $this->{$this->moduleModel}->install();
    }

    public function uninstall() {
        $this->load->model('module/'.$this->moduleName);
        $this->{$this->moduleModel}->uninstall();
    }

    public function getNotifications() {
        sleep(1);
        $this->load->model('module/' . $this->moduleName);
        $this->load->language('module/' . $this->moduleName);
        $response = $this->{$this->moduleModel}->getNotifications($this->moduleVersion);
        $json = array();
        $json['version'] = $this->moduleVersion;
        if ($response===false) {
            $json['message'] = '';
            $json['error'] = $this->language->get('error_message');
        } else {
            $json['message'] = $response;
            $json['error'] = '';
        }
        $this->response->setOutput(json_encode($json));
    }
}