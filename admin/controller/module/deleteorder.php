<?php
class ControllerModuleDeleteOrder extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('sale/order');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('sale/order');

		$filter_data = array();

		$results = $this->model_sale_order->getOrders($filter_data);

		echo 'sfsdfl';

		//$this->response->addHeader('Content-Type: application/json');
		//$this->response->setOutput($json);
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/deleteorder')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}