<?php
class ControllerModuleJosSendSMS extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('module/jossendsms');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$message = $this->request->post['message'];
			$this->load->model('customer/customer');
			$this->load->model('customer/customer_group');
			$this->load->model('marketing/affiliate');
			$this->load->model('sale/order');

			switch ($this->request->post['to']) {
				case 'nohp':
					$numbers[] = $this->request->post['nohp'].','.'null';
                    break;
				case 'newsletter':
					$customer_data = array(
							'filter_newsletter' => 1
					);

					$email_total = $this->model_customer_customer->getTotalCustomers($customer_data);
					$results = $this->model_customer_customer->getCustomers($customer_data);
					foreach ($results as $result) {
						$query = $this->db->query("SELECT `country_id` FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$result['customer_id'] . "'");
                        if( count($query->rows) > 0 ){
                            $cid = $query->row['country_id'];
                            $query2 = $this->db->query("SELECT `iso_code_2` FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$cid . "'");
                            $isoc = $query2->row['iso_code_2'];
                            $numbers[] = $result['telephone'].','.$isoc;
                        }
					}
                    break;
				case 'customer_all':
					$customer_data = array();

					$email_total = $this->model_customer_customer->getTotalCustomers($customer_data);
					$results = $this->model_customer_customer->getCustomers($customer_data);

					foreach ($results as $result) {
						$query = $this->db->query("SELECT `country_id` FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$result['customer_id'] . "'");
                        if( count($query->rows) > 0 ){
                            $cid = $query->row['country_id'];
                            $query2 = $this->db->query("SELECT `iso_code_2` FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$cid . "'");
                            $isoc = $query2->row['iso_code_2'];
                            $numbers[] = $result['telephone'].','.$isoc;
                        }
					}
                    break;
				case 'customer_group':
					$customer_data = array(
							'filter_customer_group_id' => $this->request->post['customer_group_id']
					);

					$email_total = $this->model_customer_customer->getTotalCustomers($customer_data);
					$results = $this->model_customer_customer->getCustomers($customer_data);
					foreach ($results as $result) {
						$query = $this->db->query("SELECT `country_id` FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$result['customer_id'] . "'");
                        if( count($query->rows) > 0 ){
                            $cid = $query->row['country_id'];
                            $query2 = $this->db->query("SELECT `iso_code_2` FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$cid . "'");
                            $isoc = $query2->row['iso_code_2'];
                            $numbers[] = $result['telephone'].','.$isoc;
                        }
					}
                    break;
				case 'customer':
					if (!empty($this->request->post['customer'])) {
						foreach ($this->request->post['customer'] as $customer_id) {
							$customer_info = $this->model_customer_customer->getCustomer($customer_id);

							if ($customer_info) {
								$query = $this->db->query("SELECT `country_id` FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$customer_id . "'");
                                if( count($query->rows) > 0 ){
                                    $cid = $query->row['country_id'];
                                    $query2 = $this->db->query("SELECT `iso_code_2` FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$cid . "'");
                                    $isoc = $query2->row['iso_code_2'];
                                    $numbers[] = $customer_info['telephone'].','.$isoc;
                                }
							}
						}
					}
                    break;
				case 'affiliate_all':
					$affiliate_data = array();

					$email_total = $this->model_marketing_affiliate->getTotalAffiliates($affiliate_data);
					$results = $this->model_marketing_affiliate->getAffiliates($affiliate_data);
					foreach ($results as $result) {
						$query = $this->db->query("SELECT `iso_code_2` FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$result['country_id'] . "'");
						$isoc = $query->row['iso_code_2'];
						$numbers[] = $result['telephone'].','.$isoc;
					}
                    break;
				case 'affiliate':
					if (!empty($this->request->post['affiliate'])) {
						foreach ($this->request->post['affiliate'] as $affiliate_id) {
							$affiliate_info = $this->model_marketing_affiliate->getAffiliate($affiliate_id);

							if ($affiliate_info) {
								$query = $this->db->query("SELECT `iso_code_2` FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$affiliate_info['country_id'] . "'");
								$isoc = $query->row['iso_code_2'];
								$numbers[] = $affiliate_info['telephone'].','.$isoc;
							}
						}
					}
                    break;
				case 'product':
					if (isset($this->request->post['product'])) {
						$results = $this->model_sale_order->getNohpByProductsOrdered($this->request->post['product']);

						foreach ($results as $result) {
							$query2 = $this->db->query("SELECT `iso_code_2` FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$result['payment_country_id'] . "'");
							$isoc = $query2->row['iso_code_2'];
							$numbers[] = $result['telephone'].','.$isoc;
						}
					}
                    break;
				case 'peding_orders_all':
                    $filter_date_start = $this->request->post['filter_date_added'];
                    $filter_date_end = $this->request->post['filter_date_modified'];
                    $filter_data = array(
                            'filter_date_start'	     => $filter_date_start,
                            'filter_date_end'	     => $filter_date_end,
                        );
                    $results = $this->model_sale_order->getTotalPendingOrders($filter_data);
                    foreach ($results as $result) {
                        $numbers[] = $result['telephone'].'$$'.$result['invoice_prefix'].$result['invoice_no'];
                    }
                    break;
			}
			if ($numbers) {
				$this->load->model('libraries/jossms');

				$gateway = $this->config->get('jossms_gateway');
				$storename = $this->config->get('config_name');

				$parsing = array ( '{storename}' );
				$replace = array ( $storename );
				$pesan = str_replace( $parsing, $replace, $this->request->post['message'] );
				foreach ($numbers as $destination) {
					if($this->request->post['to']=='peding_orders_all'){
                        $parsing = array ( '{storename}' );
                        $replace = array ( $storename );
                        $pesan = str_replace( $parsing, $replace, $this->request->post['message'] );
                        $break = explode('$$',$destination);
                        $destination = $break[0];//$this->model_libraries_jossms->getConvertPhonePrefix($break[0],$break[1]);
                        $parsing = array ( '{order_no}' );
                        $replace = array ( $break[1] );
                        $pesan = str_replace( $parsing, $replace, $pesan );
                        $getresponse = $this->model_libraries_jossms->send_message($destination, $pesan, $gateway);
					}
					else{
						$break = explode(',',$destination);
                        $destination = $this->model_libraries_jossms->getConvertPhonePrefix($break[0],$break[1]);
                        $getresponse = $this->model_libraries_jossms->send_message($destination, $pesan, $gateway);
					}
				}

				$status = $getresponse;
			}

            if($status == "Success"){
				$this->session->data['success'] = $this->language->get('text_success_sent');
			}else{
				$this->session->data['error'] = $this->language->get('text_error').'<b><i>'.$status.'</i></b>)';
			}

			$this->response->redirect($this->url->link('module/jossendsms', 'token=' . $this->session->data['token'], 'SSL'));
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->session->data['error'])) {
			$data['error'] = $this->session->data['error'];

			unset($this->session->data['error']);
		} else {
			$data['error'] = '';
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$defaultgateway = $this->config->get('jossms_gateway');
		if($defaultgateway == "amd"){
			$data['gateway'] = "AMD Telecom";
		}elseif($defaultgateway == "smsglobal"){
			$data['gateway'] = "Bulk SMS Global";
		}elseif($defaultgateway == "clickatell"){
			$data['gateway'] = "Clickatell";
		}elseif($defaultgateway == "liveall"){
			$data['gateway'] = "LiveAll";
		}elseif($defaultgateway == "malath"){
			$data['gateway'] = "Malath SMS";
		}elseif($defaultgateway == "mobily"){
			$data['gateway'] = "mobily.ws";
		}elseif($defaultgateway == "msegat"){
			$data['gateway'] = "Msegat.com";
		}elseif($defaultgateway == "msg91"){
			$data['gateway'] = "MSG91";
		}elseif($defaultgateway == "mvaayoo"){
			$data['gateway'] = "mVaayoo";
		}elseif($defaultgateway == "mysms"){
			$data['gateway'] = "MySms.com.gr";
		}elseif($defaultgateway == "nexmo"){
			$data['gateway'] = "Nexmo";
		}elseif($defaultgateway == "netgsm"){
			$data['gateway'] = "Netgsm.com.tr";
		}elseif($defaultgateway == "oneway"){
			$data['gateway'] = "One Way SMS";
		}elseif($defaultgateway == "openhouse"){
			$data['gateway'] = "Openhouse IMI Mobile";
		}elseif($defaultgateway == "redsms"){
			$data['gateway'] = "Red SMS";
		}elseif($defaultgateway == "routesms"){
			$data['gateway'] = "Routesms";
		}elseif($defaultgateway == "smsboxcom"){
			$data['gateway'] = "SMSBox.com";
		}elseif($defaultgateway == "smsgatewayhub"){
			$data['gateway'] = "SMS GATEWAYHUB";
		}elseif($defaultgateway == "smslane"){
			$data['gateway'] = "SMS Lane";
		}elseif($defaultgateway == "smslaneg"){
			$data['gateway'] = "SMSLane Global SMS";
		}elseif($defaultgateway == "smsnetgr"){
			$data['gateway'] = "SMS.net.gr";
		}elseif($defaultgateway == "topsms"){
			$data['gateway'] = "TOP SMS";
		}elseif($defaultgateway == "velti"){
			$data['gateway'] = "Velti";
		}elseif($defaultgateway == "zenziva"){
			$data['gateway'] = "Zenziva";
		}else{
			$data['gateway'] = "Not found!";
		}


		$data['text_none'] = $this->language->get('text_none');
		$data['text_nohp'] = $this->language->get('text_nohp');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		$data['text_customer_all'] = $this->language->get('text_customer_all');
		$data['text_customer'] = $this->language->get('text_customer');
		$data['text_customer_group'] = $this->language->get('text_customer_group');
		$data['text_affiliate_all'] = $this->language->get('text_affiliate_all');
		$data['text_affiliate'] = $this->language->get('text_affiliate');
		$data['text_product'] = $this->language->get('text_product');
        $data['text_order'] = 'All Pending Orders';//$this->language->get('text_product');

		$data['entry_nohp'] = $this->language->get('entry_nohp');
		$data['entry_message'] = $this->language->get('entry_message');
		$data['entry_or'] = $this->language->get('entry_or');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$data['entry_customer'] = $this->language->get('entry_customer');
		$data['entry_affiliate'] = $this->language->get('entry_affiliate');
		$data['entry_product'] = $this->language->get('entry_product');

		$data['button_send'] = $this->language->get('button_send');
		$data['button_setting'] = $this->language->get('button_setting');

		$data['token'] = $this->session->data['token'];

        if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['nohp'])) {
			$data['error_nohp'] = $this->error['nohp'];
		} else {
			$data['error_nohp'] = '';
		}

		if (isset($this->error['message'])) {
			$data['error_message'] = $this->error['message'];
		} else {
			$data['error_message'] = '';
		}

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
         'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
           'separator' => false
        );

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_module'),
         'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
           'separator' => ' :: '
        );

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
         'href'      => $this->url->link('module/jossendsms', 'token=' . $this->session->data['token'], 'SSL'),
           'separator' => ' :: '
        );

		$data['action'] = $this->url->link('module/jossendsms', 'token=' . $this->session->data['token'], 'SSL');
		$data['setting'] = $this->url->link('module/jossms', 'token=' . $this->session->data['token'], 'SSL');

		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$model_file  = DIR_APPLICATION . 'model/customer/customer_group.php';
		if (file_exists($model_file)) {
			$this->load->model('customer/customer_group');
			$data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups(0);
		} else {
			$this->load->model('sale/customer_group');
			$data['customer_groups'] = $this->model_sale_customer_group->getCustomerGroups(0);
		}

		$this->template = 'module/jossendsms.tpl';
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		//$this->response->setOutput($this->render());
		$this->response->setOutput($this->load->view('module/jossendsms.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/jossendsms')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if (!$this->request->post['to']) {
			$this->error['nohp'] = $this->language->get('error_nohp');
		}

        switch ($this->request->post['to']) {
			case 'nohp':
				if (!$this->request->post['nohp']) {
					$this->error['nohp'] = $this->language->get('error_nohp');
				}
                break;
			case 'customer':
				if (!$this->request->post['customer']) {
					$this->error['nohp'] = $this->language->get('error_nohp');
				}
                break;
			case 'affiliate':
				if (!$this->request->post['affiliate']) {
					$this->error['nohp'] = $this->language->get('error_nohp');
				}
                break;
			case 'product':
				if (!$this->request->post['product']) {
					$this->error['nohp'] = $this->language->get('error_nohp');
				}
                break;
		}

		if (!$this->request->post['message']) {
			$this->error['message'] = $this->language->get('error_message');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
?>