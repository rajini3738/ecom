<?php
class ControllerCompetitionCompetitionList extends Controller {
	private $error = array();

	public function index() {
	
		$this->load->language('competition/competition');
		$this->load->model('competition/competition');
	
		$this->document->setTitle($this->language->get('heading_title_competitions'));

		$this->getList();
	}

	public function insert() {
	
		$this->load->language('competition/competition');
		$this->load->model('competition/competition');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validateForm())) {
			$this->model_competition_competition->addCompetition($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success_competitions');

			$this->response->redirect($this->url->link('competition/competition_list', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
	
		$this->load->language('competition/competition');
		$this->load->model('competition/competition');
	
		$this->document->setTitle($this->language->get('heading_title_edit_competitions'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validateForm())) {
			$this->model_competition_competition->editCompetition($this->request->get['competition_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success_competitions');

			$this->response->redirect($this->url->link('competition/competition_list', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
	
		$this->load->language('competition/competition');
		$this->load->model('competition/competition');

		$this->document->setTitle($this->language->get('heading_title_competitions'));

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $competition_id) {
				$this->model_competition_competition->deleteCompetition($competition_id);
			}

			$this->session->data['success'] = $this->language->get('text_success_competitions');

			$this->response->redirect($this->url->link('competition/competition_list', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		$this->load->language('competition/competition');
		$this->load->model('competition/competition');

		$data['heading_title'] = $this->language->get('heading_title_competitions');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['text_edit'] = $this->language->get('text_edit');

		$data['column_title'] = $this->language->get('column_title');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_end_date'] = $this->language->get('column_end_date');
		$data['column_active'] = $this->language->get('column_active');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_auto_enabled'] = $this->language->get('column_auto_enabled');
		$data['column_auto_value'] = $this->language->get('column_auto_value');
		$data['column_entrants'] = $this->language->get('column_entrants');
		$data['column_action'] = $this->language->get('column_action');		

		$data['button_module'] = $this->language->get('button_module');
		$data['button_insert'] = $this->language->get('button_insert');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title_competitions'),
			'href'      => $this->url->link('competition/competition_list', 'token=' . $this->session->data['token'], 'SSL')
		);
		
		$data['module'] = $this->url->link('module/competition', 'token=' . $this->session->data['token'], 'SSL');
		$data['insert'] = $this->url->link('competition/competition_list/insert', 'token=' . $this->session->data['token'], 'SSL');
		$data['delete'] = $this->url->link('competition/competition_list/delete', 'token=' . $this->session->data['token'], 'SSL');

		$data['competitions'] = array();

		$competition_total = $this->model_competition_competition->getTotalCompetitions();
		$today = strtotime(date("d-m-Y"));
		$results = $this->model_competition_competition->getCompetition();

    	foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('competition/competition_list/update', 'token=' . $this->session->data['token'] . '&competition_id=' . $result['competition_id'], 'SSL')
			);
			
			$action[] = array(
				'text' => $this->language->get('text_view_entrants'),
				'href' => $this->url->link('competition/competition_entrants/', 'token=' . $this->session->data['token'] . '&competition_id=' . $result['competition_id'], 'SSL')
			);

			$data['competitions'][] = array(
				'competition_id'    => $result['competition_id'],
				'title'       		=> $result['title'],
				'auto_enabled'		=> ($result['auto'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
				'auto_value'       	=> $result['auto_entry_value'],
				'status'      		=> ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
				'active'       		=> (strtotime($result['end_date']) > $today && $result['status'] ? 'Yes' : 'No'),
				'date_added'  		=> date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'end_date' 	  		=> date($this->language->get('date_format_short'), strtotime($result['end_date'])),
				'status'      		=> ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
				'selected'    		=> isset($this->request->post['selected']) && in_array($result['competition_id'], $this->request->post['selected']),
				'entrants'			=> $this->model_competition_competition->getTotalEntrants($result['competition_id']),
				'action'      		=> $action
			);
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('competition/comp_list.tpl', $data));
	}

	protected function getForm() {
		$this->load->language('competition/competition');
		$this->load->model('competition/competition');

		$data['heading_title'] = $this->language->get('heading_title_competitions');
		
    	$data['text_form'] = $this->language->get('heading_title_insert_competitions');
    	$data['text_enabled'] = $this->language->get('text_enabled');
    	$data['text_disabled'] = $this->language->get('text_disabled');
    	$data['text_default'] = $this->language->get('text_default');
		$data['text_image_manager'] = $this->language->get('text_image_manager');
		$data['text_browse'] = $this->language->get('text_browse');
		$data['text_clear'] = $this->language->get('text_clear');				

		$data['entry_title'] = $this->language->get('entry_title');
		$data['entry_winners'] = $this->language->get('entry_winners');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_end_date'] = $this->language->get('entry_end_date');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_question'] = $this->language->get('entry_question');
		$data['entry_answer'] = $this->language->get('entry_answer');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_login'] = $this->language->get('entry_login');
		$data['entry_newsletter'] = $this->language->get('entry_newsletter');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_image_size'] = $this->language->get('entry_image_size');
		$data['entry_auto'] = $this->language->get('entry_auto');
		$data['entry_auto_value'] = $this->language->get('entry_auto_value');
		$data['entry_auto_message'] = $this->language->get('entry_auto_message');
		$data['entry_answer1'] = $this->language->get('entry_answer1');
		$data['entry_answer2'] = $this->language->get('entry_answer2');
		$data['entry_answer3'] = $this->language->get('entry_answer3');
		$data['entry_answer4'] = $this->language->get('entry_answer4');
		$data['entry_answer_is'] = $this->language->get('entry_answer_is');
		
		$data['answerlists'] = array('A', 'B', 'C', 'D');

		$data['button_save_competition'] = $this->language->get('button_save_competition');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');

		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['title'])) {
			$data['error_title'] = $this->error['title'];
		} else {
			$data['error_title'] = '';
		}

		if (isset($this->error['description'])) {
			$data['error_description'] = $this->error['description'];
		} else {
			$data['error_description'] = '';
		}
	
		if (isset($this->error['end_date'])) {
			$data['error_end_date'] = $this->error['end_date'];
		} else {
			$data['error_end_date'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => FALSE
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title_competitions'),
			'href'      => $this->url->link('competition/competition_list', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);
		
		if (!isset($this->request->get['competition_id'])) {
			$data['action'] = $this->url->link('competition/competition_list/insert', 'token=' . $this->session->data['token'], 'SSL');
		} else {
			$data['action'] = $this->url->link('competition/competition_list/update', 'token=' . $this->session->data['token'] . '&competition_id=' . $this->request->get['competition_id'], 'SSL');
		}

		$data['cancel'] = $this->url->link('competition/competition_list', 'token=' . $this->session->data['token'], 'SSL');

		if ((isset($this->request->get['competition_id'])) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$competition_info = $this->model_competition_competition->getCompetitionDetails($this->request->get['competition_id']);
		}

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['competition_data'])) {
			$data['competition_data'] = $this->request->post['competition_data'];
		} elseif (isset($this->request->get['competition_id'])) {
			$data['competition_data'] = $this->model_competition_competition->getCompetitionData($this->request->get['competition_id']);
		} else {
			$data['competition_data'] = array();
		}

		$this->load->model('setting/store');

		$data['stores'] = $this->model_setting_store->getStores();

		if (isset($this->request->post['competition_store'])) {
			$data['competition_store'] = $this->request->post['competition_store'];
		} elseif (isset($competition_info)) {
			$data['competition_store'] = $this->model_competition_competition->getCompetitionStores($this->request->get['competition_id']);
		} else {
			$data['competition_store'] = array(0);
		}			

		if (isset($this->request->post['image'])) {
			$data['image'] = $this->request->post['image'];
		} elseif (!empty($competition_info)) {
			$data['image'] = $competition_info['image'];
		} else {
			$data['image'] = '';
		}
		
		$this->load->model('tool/image');

		if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($competition_info) && is_file(DIR_IMAGE . $competition_info['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($competition_info['image'], 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}
		
		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		
		if (isset($this->request->post['width'])) {
			$data['width'] = $this->request->post['width'];
		} elseif (isset($competition_info)) {
			$data['width'] = $competition_info['width'];
		} else {
			$data['width'] = '130';
		}
		
		if (isset($this->request->post['height'])) {
			$data['height'] = $this->request->post['height'];
		} elseif (isset($competition_info)) {
			$data['height'] = $competition_info['height'];
		} else {
			$data['height'] = '130';
		}

		if (isset($this->request->post['keyword'])) {
			$data['keyword'] = $this->request->post['keyword'];
		} elseif (isset($competition_info)) {
			$data['keyword'] = $competition_info['keyword'];
		} else {
			$data['keyword'] = '';
		}

		if (isset($this->request->post['winners'])) {
			$data['winners'] = $this->request->post['winners'];
		} elseif (isset($competition_info)) {
			$data['winners'] = $competition_info['winners'];
		} else {
			$data['winners'] = '1';
		}
		
		if (isset($this->request->post['auto'])) {
			$data['auto'] = $this->request->post['auto'];
		} elseif (isset($competition_info)) {
			$data['auto'] = $competition_info['auto'];
		} else {
			$data['auto'] = '';
		}
		
		if (isset($this->request->post['auto_entry_value'])) {
			$data['auto_entry_value'] = $this->request->post['auto_entry_value'];
		} elseif (isset($competition_info)) {
			$data['auto_entry_value'] = $competition_info['auto_entry_value'];
		} else {
			$data['auto_entry_value'] = '';
		}
		
		if (isset($this->request->post['answer_is'])) {
			$data['answer_is'] = $this->request->post['answer_is'];
		} elseif (isset($competition_info)) {
			$data['answer_is'] = $competition_info['answer_is'];
		} else {
			$data['answer_is'] = '';
		}
		
		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (isset($competition_info)) {
			$data['status'] = $competition_info['status'];
		} else {
			$data['status'] = '';
		}
		
		if (isset($this->request->post['login'])) {
			$data['login'] = $this->request->post['login'];
		} elseif (isset($competition_info)) {
			$data['login'] = $competition_info['login'];
		} else {
			$data['login'] = '';
		}
		
		if (isset($this->request->post['newsletter'])) {
			$data['newsletter'] = $this->request->post['newsletter'];
		} elseif (isset($competition_info)) {
			$data['newsletter'] = $competition_info['newsletter'];
		} else {
			$data['newsletter'] = '';
		}
		
		if (isset($this->request->post['end_date'])) {
			$data['end_date'] = $this->request->post['end_date'];
		} elseif (isset($competition_info)) {
			$data['end_date'] = $competition_info['end_date'];
		} else {
			$data['end_date'] = '';
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('competition/comp_form.tpl', $data));
	}

	public function upgrade() {
	
		$this->load->model('competition/competition');
		$this->model_competition_competition->doUpgrade();
		
		$this->response->redirect($this->url->link('competition/competition_list', 'token=' . $this->session->data['token'], 'SSL'));
		
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'competition/competition_list')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'competition/competition_list')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		foreach ($this->request->post['competition_data'] as $language_id => $value) {
			if ((strlen($value['title']) < 3)) {
				$this->error['title'][$language_id] = $this->language->get('error_title');
			}

			if (strlen($value['description']) < 3) {
				$this->error['description'][$language_id] = $this->language->get('error_description');
			}
		}
		
			if ($this->request->post['end_date'] < 1) {
				$this->error['end_date'] = $this->language->get('error_end_date');
			}
			
		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'competition/competition_list')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
?>