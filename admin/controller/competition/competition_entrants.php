<?php
class ControllerCompetitionCompetitionEntrants extends Controller {
	private $error = array();
 
	public function index() {
		$this->load->language('competition/competition');
		$this->load->model('competition/competition');
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->getList();
	}

	public function insert() {
		$this->load->language('competition/competition');
		$this->load->model('competition/competition');
		
		$this->document->setTitle($this->language->get('heading_title_insert_entrant'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_competition_competition->addEmail($this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success_entrants');
			
			$this->response->redirect($this->url->link('competition/competition_entrants', 'token=' . $this->session->data['token'] . '&competition_id=' . $this->request->post['competition_id'], 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->load->language('competition/competition');
		$this->load->model('competition/competition');

		$this->document->setTitle($this->language->get('heading_title_edit_entrant'));
		$this->session->data['template'] = 1;
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateUpdateForm()) {
			$this->model_competition_competition->editEmail($this->request->get['id'], $this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success_entrants');
			unset($this->session->data['template']);
			
			$this->response->redirect($this->url->link('competition/competition_entrants', 'token=' . $this->session->data['token'] . '&competition_id=' . $this->request->post['competition_id'], 'SSL'));
		}

		$this->getForm();
	}

	public function delete() { 
		$this->load->language('competition/competition');
		$this->load->model('competition/competition');
		
		$this->document->setTitle($this->language->get('heading_title'));		
		
		if (isset($this->request->post['selected'])) {
      		foreach ($this->request->post['selected'] as $id) {
				$this->model_competition_competition->deleteEmail($id);	
			}
						
			$this->session->data['success'] = $this->language->get('text_success_entrants');
			
			$this->response->redirect($this->url->link('competition/competition_entrants', 'token=' . $this->session->data['token'] . '&competition_id=' . $this->request->post['competition_id'], 'SSL'));
		}

		$this->getList();
	}
	
	public function delete_incorrect() { 
		$this->load->model('competition/competition');
		
		if (isset($this->request->get['competition_id'])) {
			$competition_id = $this->request->get['competition_id'];
		} else {
			$competition_id = '0';
		}

		$this->model_competition_competition->deleteIncorrect($competition_id);
		
		$this->response->redirect($this->url->link('competition/competition_entrants', 'token=' . $this->session->data['token'] . '&competition_id=' . $competition_id, 'SSL'));
	}

	public function deleteWinner() { 
		$this->load->language('competition/competition');
		$this->load->model('competition/competition');

		$this->document->setTitle($this->language->get('heading_title'));
		if (isset($this->request->post['selected'])) {
      		foreach ($this->request->post['selected'] as $id) {
				$this->model_competition_competition->deleteWinner($id);	
			}
						
			$this->session->data['success'] = $this->language->get('text_success_delete_winner');
			
			$this->response->redirect($this->url->link('competition/competition_entrants', 'token=' . $this->session->data['token'] . '&competition_id=' . $this->request->post['competition_id'], 'SSL'));
		
		} else {
		
		$this->response->redirect($this->url->link('competition/competition_entrants/view_winner', 'token=' . $this->session->data['token'] . '&competition_id=' . $this->request->get['competition_id'], 'SSL'));
		
		}

		$this->getList();
	}

	protected function getList() {
	
  		$data['breadcrumbs'] = array();

  		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
       		'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL')
   		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title_competitions'),
			'href'      => $this->url->link('competition/competition_list', 'token=' . $this->session->data['token'], 'SSL')
		);
		
		$data['insert'] = $this->url->link('competition/competition_entrants/insert', 'token=' . $this->session->data['token'] . '&competition_id=' . $this->request->get['competition_id'], 'SSL');
		$data['delete'] = $this->url->link('competition/competition_entrants/delete', 'token=' . $this->session->data['token'] . '&competition_id=' . $this->request->get['competition_id'], 'SSL');
		$data['view_winner'] = $this->url->link('competition/competition_entrants/view_winner', 'token=' . $this->session->data['token'] . '&competition_id=' . $this->request->get['competition_id'], 'SSL');
		
		if(isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['competition_id'])) {
			$competition_id = $this->request->get['competition_id'];
		} else {
			$competition_id = '0';
		}

		$data['competition_id'] = $competition_id;

		$data['check_winner'] = $this->model_competition_competition->checkWinner($competition_id);
		$data['button_winner'] = $this->language->get('button_view_winner');
		
		$data['emails'] = array();
		
		$filter_data = array(
			'competition_id'          => $competition_id,
			'start'                   => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'                   => $this->config->get('config_admin_limit')
		);
		
		$email_total = $this->model_competition_competition->getTotalEmails($filter_data);
		
		$results = $this->model_competition_competition->getEmails($filter_data);

		foreach ($results as $result) {
			$action = array();
			
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('competition/competition_entrants/update', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . '&competition_id=' . $this->request->get['competition_id'], 'SSL'),
			);		
		
			$data['emails'][] = array(
				'id' 		=> $result['id'],
				'name' 		=> $result['name'],
				'email' 	=> $result['email_id'],
				'answer' 	=> $result['answer'],
				'action'    => $action
			);
		}
		
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['name'] = $this->model_competition_competition->getName($competition_id);

		$data['column_name'] = $this->language->get('column_name');
		$data['column_email'] = $this->language->get('column_email');
		$data['column_answer'] = $this->language->get('column_answer');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_insert'] = $this->language->get('button_insert');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_go'] = $this->language->get('button_go');
		
		$data['dropdown'] = $this->getDropdown();
 
 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		$pagination = new Pagination();
		$pagination->total = $email_total;
		$pagination->page  = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text  = $this->language->get('text_pagination');
		$pagination->url   = $this->url->link('competition/competition_entrants/', 'token=' . $this->session->data['token'] . '&competition_id=' . $competition_id . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('competition/competition_entrants.tpl', $data));
 	}

	protected function getForm() {
			
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_answer_is'] = $this->language->get('entry_answer_is');
		
		$data['answerlists'] = array('A', 'B', 'C', 'D');
		
		$data['button_save_entrant'] = $this->language->get('button_save_entrant');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['text_entrant_form'] = $this->language->get('text_entrant_form');

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

 		if (isset($this->error['error_email_name'])) {
			$data['error_email_name'] = $this->error['error_email_name'];
		} else {
			$data['error_email_name'] = '';
		}
		
 		if (isset($this->error['error_email_exist'])) {
			$data['error_email_exist'] = $this->error['error_email_exist'];
		} else {
			$data['error_email_exist'] = '';
		}

  		$data['breadcrumbs'] = array();

  		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
       		'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => FALSE
   		);

  		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
       		'href'      => $this->url->link('competition/competition_entrants/', 'token=' . $this->session->data['token'] . '&competition_id=' . $this->request->get['competition_id'], 'SSL'),
      		'separator' => ' :: '
   		);
			
		if (!isset($this->request->get['id'])) {
			$data['action'] = $this->url->link('competition/competition_entrants/insert', 'token=' . $this->session->data['token'] . '&competition_id=' . $this->request->get['competition_id'], 'SSL');
		} else {
			$data['action'] = $this->url->link('competition/competition_entrants/update', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . '&competition_id=' . $this->request->get['competition_id'], 'SSL');
		}
		
		$data['token'] = $this->session->data['token'];
		  
		$data['cancel'] = $this->url->link('competition/competition_entrants/', 'token=' . $this->session->data['token'] . '&competition_id=' . $this->request->get['competition_id'], 'SSL');

		$competition_id = $this->request->get['competition_id'];
		$data['compid'] = $competition_id;
		
		$data['name'] = $this->model_competition_competition->getName($competition_id);

		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$email_info = $this->model_competition_competition->getEmail($this->request->get['id']);
		}

		if (isset($this->request->post['email_name'])) {
			$data['email_name'] = $this->request->post['email_name'];
		} elseif (isset($email_info)) {
			$data['email_name'] = $email_info['name'];
		} else {
			$data['email_name'] = '';
		}
		
		if (isset($this->request->post['email_id'])) {
			$data['email_id'] = $this->request->post['email_id'];
		} elseif (isset($email_info)) {
			$data['email_id'] = $email_info['email_id'];
		} else {
			$data['email_id'] = '';
		}
		
		if (isset($this->request->post['answer_is'])) {
			$data['answer_is'] = $this->request->post['answer_is'];
		} elseif (isset($email_info)) {
			$data['answer_is'] = $email_info['answer'];
		} else {
			$data['answer_is'] = '';
		}
		
		if (isset($this->request->post['competition_id'])) {
			$data['competition_id'] = $this->request->post['competition_id'];
		} elseif (isset($email_info)) {
			$data['competition_id'] = $email_info['competition_id'];
		} else {
			$data['competition_id'] = '';
		}
		
		if (isset($this->session->data['template'])) {	
			$template = 'competition/comp_entrant_update_form.tpl';
			$data['heading_title'] = $this->language->get('heading_title_edit_entrant');
		} else {
			$template = 'competition/comp_entrant_form.tpl';
			$data['heading_title'] = $this->language->get('heading_title_insert_entrant');

		}
		
		unset($this->session->data['template']);
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view($template, $data));
	}

	protected function validateForm() {
		
	$this->load->model('competition/competition');
		
		if (!$this->user->hasPermission('modify', 'competition/competition_entrants')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

	 	if(!filter_var($this->request->post['email_id'],FILTER_VALIDATE_EMAIL)){
			$this->error['error_email_name'] = $this->language->get('error_email');
		}
		
		if($this->model_competition_competition->checkmail($this->request->post)) {
			$this->error['error_email_exist'] = $this->language->get('error_email_exist');
		}

		return !$this->error;
	}

	protected function validateUpdateForm() {
		
	$this->load->model('competition/competition');
		
		if (!$this->user->hasPermission('modify', 'competition/competition_entrants')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function winner() {
	
		$this->load->language('competition/competition');
		$this->load->model('competition/competition');
	
		if (isset($this->request->get['competition_id'])) {
			$competition_id = $this->request->get['competition_id'];
		} else {
			$competition_id = '0';
		}
		
		$check_winner = $this->model_competition_competition->checkWinner($competition_id);

		if(!$check_winner) {
			if (isset($this->request->get['type'])) {
				if ($this->request->get['type'] == 'all') {
					$getWinners = $this->model_competition_competition->getWinnersAll($competition_id);
				} elseif ($this->request->get['type'] == 'correct') {
					$getWinners = $this->model_competition_competition->getWinnersCorrectOnly($competition_id);
				}
			}

			foreach ($getWinners as $getWinner) {
				$savedata = array(
				  'name' 			=> $getWinner['name'],
				  'email' 			=> $getWinner['email_id'],
				 'competition_id' 	=> $competition_id
				);
				
				$this->model_competition_competition->saveWinners($savedata);
			}
			
			$winners = $this->model_competition_competition->checkWinner($competition_id);
		}
		
		$this->response->redirect($this->url->link('competition/competition_entrants/view_winner', 'token=' . $this->session->data['token'] . '&competition_id=' . $competition_id, 'SSL'));
	}
	
	public function view_winner() {
	
		$this->load->language('competition/competition');
		$this->load->model('competition/competition');

		$this->document->setTitle($this->language->get('heading_title_winner'));

	
  		$data['breadcrumbs'] = array();

  		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
       		'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => FALSE
   		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title_competitions'),
			'href'      => $this->url->link('competition/competition_list', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

  		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
       		'href'      => $this->url->link('competition/competition_entrants/', 'token=' . $this->session->data['token'] . '&competition_id=' . $this->request->get['competition_id'], 'SSL'),
      		'separator' => ' :: '
   		);
							
		$data['cancel'] = $this->url->link('competition/competition_entrants/', 'token=' . $this->session->data['token'] . '&competition_id=' . $this->request->get['competition_id'], 'SSL');
		$data['save'] = $this->url->link('competition/competition_entrants/save', 'token=' . $this->session->data['token'] . '&competition_id=' . $this->request->get['competition_id'], 'SSL');
		$data['delete'] = $this->url->link('competition/competition_entrants/deleteWinner', 'token=' . $this->session->data['token'] . '&competition_id=' . $this->request->get['competition_id'], 'SSL');
		$data['text_email'] = $this->language->get('text_email_winner');
		$data['button_delete'] = $this->language->get('button_delete');
	
		$data['emails'] = array();
		
		if (isset($this->request->get['competition_id'])) {
			$competition_id = $this->request->get['competition_id'];
		} else {
			$competition_id = '0';
		}

		$check_winner = $this->model_competition_competition->checkWinner($competition_id);

		if($check_winner) {
			$results = $this->model_competition_competition->checkWinner($competition_id);

			foreach ($results as $result) {
				$action = array();
			
				$action[] = array(
					'text' => $this->language->get('text_email_winner'),
					'href' => $this->url->link('competition/competition_entrants/emailWinner', 'token=' . $this->session->data['token'] . '&competition_id=' . $competition_id . '&email_id=' . $result['email_id'] . '&email_name=' . $result['name'], 'SSL')
				);		
		
				$data['emails'][] = array(
					'id' 		=> $result['id'],
					'name' 		=> $result['name'],
					'email' 	=> $result['email_id'],
					'notified'	=> ($result['notified'] ? 'Yes' : 'No'),
					'action'    => $action
				);
			}
		}
	
		$data['heading_title'] = $this->language->get('text_winner');
		$data['name'] = $this->model_competition_competition->getName($competition_id);
		
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_list'] = $this->language->get('text_winners_list');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_email'] = $this->language->get('column_email');
		$data['column_notified'] = $this->language->get('column_notified');
		
		$data['column_action'] = $this->language->get('column_action');

		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['competition_id'] = $competition_id;
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('competition/competition_winners.tpl', $data));
	}
	
	public function emailWinner() { 
		$this->load->language('competition/competition');
		$this->load->model('competition/competition');
		
		if (isset($this->request->get['competition_id'])) {
			$competition_id = $this->request->get['competition_id'];
		} else {
			$competition_id = '0';
		}
		
		if (isset($this->request->get['email_id'])) {
			$email_id = $this->request->get['email_id'];
		} else {
			$email_id = '';
		}
		
		if (isset($this->request->get['email_name'])) {
			$email_name = $this->request->get['email_name'];
		} else {
			$email_name = '';
		}
		
//		$addresses = $this->model_competition_competition->checkWinner($competition_id);

//		foreach ($addresses as $address) {
//		
//			$data['emails'][] = array(
//				'id' 		=> $address['id'],
//				'name' 		=> $address['name'],
//				'email' 	=> $address['email_id'],
//			);
//		}

		$message = sprintf($this->language->get('text_email_intro'), $this->model_competition_competition->getName($competition_id)) . "\n\n";
		$message .= sprintf($this->language->get('text_email_body'), $this->config->get('config_name')) . "\n\n";
		$message .= sprintf($this->language->get('text_email_signoff')) . "\n\n";
		$message .= sprintf($this->language->get('text_email_end'), $this->config->get('config_name'));

		$subject = sprintf($this->language->get('text_email_subject'));
		$mail = new Mail($this->config->get('config_mail'));
		$mail->setReplyTo($this->config->get('config_email'));
		$mail->setTo($email_id);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender($this->config->get('config_name'));
		$mail->setSubject($subject);
		$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
		$mail->send();
		
		$this->model_competition_competition->notify($competition_id, $email_id);
		
		$this->session->data['success'] = $this->language->get('text_email_sent');
		
		$this->response->redirect($this->url->link('competition/competition_entrants/view_winner&competition_id=' . $this->request->get['competition_id'], 'token=' . $this->session->data['token'], 'SSL'));
	
	}
	
	public function getDropdown() {
	
	$dropdown = array (
		array (
			'link' => $this->url->link('competition/competition_entrants/winner', 'token=' . $this->session->data['token'] . '&competition_id=' . $this->request->get['competition_id'] . '&type=correct', 'SSL'),
			'name' => $this->language->get('button_choose_correct'),
		),
		array (
			'link' => $this->url->link('competition/competition_entrants/winner', 'token=' . $this->session->data['token'] . '&competition_id=' . $this->request->get['competition_id'] . '&type=all', 'SSL'),
			'name'  => $this->language->get('button_choose_all'),
		),
		array (
			'link' => $this->url->link('competition/competition_entrants/delete_incorrect', 'token=' . $this->session->data['token'] . '&competition_id=' . $this->request->get['competition_id'], 'SSL'),
			'name'  => $this->language->get('button_delete_incorrect'),
		),
	);
	
	return $dropdown;
	}
}
?>