<?php
/*-----------------------------------------------------------------------------
 | File index.php
 |     Author:  Pete Allison
 |
 |    Purpose:  Example CRON task using CLI.  To enable easy debugging, if you
 |       define a constant called "DEVELOPMENT" with some value (e.g. true),
 |       you can run this via http.
 |
 +---------------------------------------------------------------------------*/

class ControllerExampleCron extends Controller {
	public function index() {
		$start = microtime(true);

		$response = array();
		if (php_sapi_name() != 'cli' && !defined('DEVELOPMENT') && !DEVELOPMENT) {
			if (function_exists('cliProblem')) {
				cliProblem('Cannot be run using HTTP');
			} else {
				die('Cannot be run using HTTP');
			}
		} elseif (php_sapi_name() != 'cli') {
			// Purely for http development, we push out a pre tag
			$response[] = '<pre>';
		}


		$end = microtime(true);

		$response[] = "Total Time: " . (($end - $start) * 1000) . "ms";

		// Log if it took more than 1 second to execute
		if (($end - $start) - 1000 > 1000) {
			$this->log->write("WARNING: " . __FILE__ . " Cron task took " . (($end - $start) * 1000) . "ms to complete");
		}

		$this->response->setOutput(implode(PHP_EOL, $response) . PHP_EOL);
	}
}

?>