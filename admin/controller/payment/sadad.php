<?php
ob_start();
class ControllerPaymentSadad extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('payment/sadad');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
		session_start();


		$this->model_setting_setting->editSetting('sadad', $this->request->post);
		$_SESSION['website_merchant'] = $this->request->post['website_merchant'];
		$website = $_SESSION['website_merchant'];
		//$this->config->get('website_merchant') = $website;

//echo $_SESIION['website_merchant'];

print_r($_SESSION['website_merchant']);//die;

			$this->session->data['success'] = $this->language->get('text_success');



			$this->response->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');

		$data['entry_merchant'] = $this->language->get('entry_merchant');
		$data['entry_password'] = $this->language->get('entry_password');
		$data['entry_website'] = $this->language->get('entry_website');
		
		$data['entry_callback'] = $this->language->get('entry_callback');
		$data['entry_total'] = $this->language->get('entry_total');
		$data['entry_order_status'] = $this->language->get('entry_order_status');
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');

		$data['help_callback'] = $this->language->get('help_callback');
		$data['help_total'] = $this->language->get('help_total');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['merchant'])) {
			$data['error_merchant'] = $this->error['merchant'];
		} else {
			$data['error_merchant'] = '';
		}

		if (isset($this->error['website'])) {
			$data['error_website'] = $this->error['website'];
			} else {
				$data['error_website'] = '';
			}

		if (isset($this->error['security'])) {
			$data['error_security'] = $this->error['security'];
		} else {
			$data['error_security'] = '';
		}

		


		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_payment'),
			'href' => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('payment/sadad', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('payment/sadad', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['sadad_merchant'])) {
			$data['sadad_merchant'] = $this->request->post['sadad_merchant'];
		} else {
			$data['sadad_merchant'] = $this->config->get('sadad_merchant');
		}


		if (isset($this->request->post['sadad_password'])) {
			$data['sadad_password'] = $this->request->post['sadad_password'];
		} else {
			$data['sadad_password'] = $this->config->get('sadad_password');
		}


		if (isset($this->request->post['website_merchant'])) {

			$data['website_merchant'] = $this->request->post['website_merchant'];
		} else {
			$data['website_merchant'] = $this->config->get('website_merchant');//$_SESSION['website_merchant'];  //$this->config->get('website_merchant');
		}


		if (isset($this->request->post['sadad_live'])) {
			$data['sadad_live'] = $this->request->post['sadad_live'];
		} else {
			$data['sadad_live'] = $this->config->get('sadad_live');
		}


		$data['callback'] = HTTP_CATALOG . 'index.php?route=payment/sadad/callback';

		if (isset($this->request->post['sadad_total'])) {
			$data['sadad_total'] = $this->request->post['sadad_total'];
		} else {
			$data['sadad_total'] = $this->config->get('sadad_total');
		}




		if (isset($this->request->post['sadad_order_status_id'])) {
			$data['sadad_order_status_id'] = $this->request->post['sadad_order_status_id'];
		} else {
			$data['sadad_order_status_id'] = $this->config->get('sadad_order_status_id');
		}

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		if (isset($this->request->post['sadad_geo_zone_id'])) {
			$data['sadad_geo_zone_id'] = $this->request->post['sadad_geo_zone_id'];
		} else {
			$data['sadad_geo_zone_id'] = $this->config->get('sadad_geo_zone_id');
		}

		$this->load->model('localisation/geo_zone');

		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		if (isset($this->request->post['sadad_status'])) {
			$data['sadad_status'] = $this->request->post['sadad_status'];
		} else {
			$data['sadad_status'] = $this->config->get('sadad_status');
		}

		if (isset($this->request->post['sadad_sort_order'])) {
			$data['sadad_sort_order'] = $this->request->post['sadad_sort_order'];
		} else {
			$data['sadad_sort_order'] = $this->config->get('sadad_sort_order');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		//echo "<pre>";
			//print_r($data);//die;
		
		if (isset($this->request->post['sadad_website'])) {

			$data['sadad_website'] = $this->request->post['sadad_website'];
			} else {
			$data['sadad_website'] = $this->config->get('sadad_website');
			}

		$this->response->setOutput($this->load->view('payment/sadad.tpl', $data));
	}


	protected function validate() {
		if (!$this->user->hasPermission('modify', 'payment/sadad')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->request->post['sadad_merchant']) {
			$this->error['merchant'] = $this->language->get('error_merchant');
		}


		if (!$this->request->post['sadad_password']) {
		$this->error['security'] = $this->language->get('error_security');
		}


		if (!$this->request->post['sadad_live']) {
			$this->error['security'] = $this->language->get('error_security');
		}

		return !$this->error;
	}
}