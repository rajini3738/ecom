<?php
class ControllerCatalogBids extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/bids');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['column_customer'] = $this->language->get('column_customer');
		$data['column_bid'] = $this->language->get('column_bid');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_list'] = $this->language->get('text_list');
		$data['text_loading'] = $this->language->get('text_loading');
		$data['button_approve'] = $this->language->get('button_approve');
		$data['button_disapprove'] = $this->language->get('button_disapprove');
		$data['text_selectbid'] = $this->language->get('text_selectbid');
		$data['text_selectedbid'] = $this->language->get('text_selectedbid');
		$data['text_disapproveallbid'] = $this->language->get('text_disapproveallbid');

		$this->load->model('catalog/category');

		 if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
    
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/product', 'token=' . $this->session->data['token'], true)
		);

		$this->load->model('catalog/bids');

		$product_id = $this->request->get['product_id'];
		$data['product_id'] = $product_id;
		$data['token'] = $this->session->data['token'];
		$data['bids'] = array();
		$bids_info = $this->model_catalog_bids->getBids($this->request->get['product_id']);    
		foreach ($bids_info as $bid_info) {
			$this->load->model('customer/customer');
			$customer_info = $this->model_customer_customer->getCustomer($bid_info['customer_id']);
			$customer = $customer_info['firstname'] . '&nbsp;' . $customer_info['lastname'];
			$bid = $this->currency->format($bid_info['bid'], $this->session->data['currency']);
			$data['bids'][] = array(
			 'customer'      => $customer,
			 'customer_id'   => $bid_info['customer_id'],
			 'bid_id'        => $bid_info['bid_id'],
			 'bid'           => $bid,
			 'pbid'          => $bid_info['bid'],
			 'bid_status'    => $bid_info['bid_status'],
			 'approve'       => $bid_info['bid_status'],
			 'date_added'    => $bid_info['date_added']
			);
		}

		$bids_total = $this->model_catalog_bids->getTotalBids($this->request->get['product_id']);

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

    $pagination = new Pagination();
		$pagination->total = $bids_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/bids', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($bids_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($bids_total - $this->config->get('config_limit_admin'))) ? $bids_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $bids_total, ceil($bids_total / $this->config->get('config_limit_admin')));

		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/bids_list.tpl', $data));
	}
	/*public function approve() {
		$this->load->language('catalog/bids');
		$json = array();
		$product_id = $this->request->get['product_id'];
		$bid = $this->request->get['bid'];
		if (!$this->user->hasPermission('modify', 'catalog/bids')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if (!isset($json['error'])) {
			$this->load->model('catalog/bids');
			$this->model_catalog_bids->selecktBid($this->request->get['customer_id'], $product_id, $bid);
			$json['success'] = $this->language->get('text_approved');
		}
		print_r($json);
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}*/

	public function disapprove() {
		$this->load->language('catalog/bids');
		$json = array();
		$product_id = $this->request->get['product_id'];
		$this->load->model('catalog/product');
		$product_info = $this->model_catalog_product->getProduct($this->request->get['product_id']);
		$bn_price = $product_info['bn_price'];
		if (!$this->user->hasPermission('modify', 'catalog/bids')) {
			$json['error'] = $this->language->get('error_permission');
		}
		if (!isset($json['error'])) {
			$this->load->model('catalog/bids');
			$this->model_catalog_bids->unselecktBid($product_id, $bn_price);
			$json['success'] = $this->language->get('text_unapproved');
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function approve() {
		$this->load->language('catalog/bids');
		$json = array();
		$product_id = $this->request->get['product_id'];
		$bid = $this->request->get['bid'];
		//print_r('ddd');
		if (!$this->user->hasPermission('modify', 'catalog/bids')) {
			$json['error'] = $this->language->get('error_permission');
		}
		if (!isset($json['error'])) {
			$this->load->model('catalog/bids');
			$this->model_catalog_bids->selecktBid($this->request->get['customer_id'], $product_id, $bid);
			$json['success'] = $this->language->get('text_approved');
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
