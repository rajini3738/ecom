<?php
class ControllerCatalogStockManager extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/stockmanager');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/stockmanager');

		$this->getList();
		//$this->response->setOutput($this->load->view('catalog/stock_list.tpl', null));
	}

	public function edit() {
		$this->load->language('catalog/stockmanager');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/stockmanager');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_stockmanager->editStock($this->request->get['stock_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/stockmanager', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_quantity'])) {
			$filter_quantity = $this->request->get['filter_quantity'];
		} else {
			$filter_quantity = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'product_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/stockmanager', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$data['stocks'] = array();

		$filter_data = array(
			'filter_name'	  => $filter_name,
			'filter_quantity' => $filter_quantity,
			'sort'            => $sort,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'           => $this->config->get('config_limit_admin')
		);

		$stock_total = $this->model_catalog_stockmanager->getTotalStocks($filter_data);

		$results = $this->model_catalog_stockmanager->getStocks($filter_data);

		foreach ($results as $result) {

			$data['stocks'][] = array(
				'stock_id'			=> $result['stock_id'],
				'product_id'		=> $result['product_id'],
				'product_name'		=> $result['product_name'],
				'quantity'			=> $result['quantity'],
				'orginalquantity'   => $result['orginalquantity'],
                'adminquantity'		=> $result['adminquantity'],
				'edit'				=> $this->url->link('catalog/stockmanager/edit', 'token=' . $this->session->data['token'] . '&stock_id=' . $result['stock_id'] . $url, 'SSL')
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['column_name'] = $this->language->get('column_name');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_oquantity'] = $this->language->get('column_oquantity');
		$data['column_aquantity'] = $this->language->get('column_aquantity');
		$data['column_action'] = $this->language->get('column_action');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_quantity'] = $this->language->get('entry_quantity');

		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_filter'] = $this->language->get('button_filter');

		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/stockmanager', 'token=' . $this->session->data['token'] . '&sort=product_name' . $url, 'SSL');
		$data['sort_quantity'] = $this->url->link('catalog/stockmanager', 'token=' . $this->session->data['token'] . '&sort=quantity' . $url, 'SSL');
		$data['sort_oquantity'] = $this->url->link('catalog/stockmanager', 'token=' . $this->session->data['token'] . '&sort=oquantity' . $url, 'SSL');
		$data['sort_aquantity'] = $this->url->link('catalog/stockmanager', 'token=' . $this->session->data['token'] . '&sort=aquantity' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $stock_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/stockmanager', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($stock_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($stock_total - $this->config->get('config_limit_admin'))) ? $stock_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $stock_total, ceil($stock_total / $this->config->get('config_limit_admin')));

		$data['filter_name'] = $filter_name;
		$data['filter_quantity'] = $filter_quantity;

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/stock_list.tpl', $data));
	}

	protected function getForm() {

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['stock_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_quantity'] = $this->language->get('entry_quantity');
		$data['entry_orginalquantity'] = 'Orginal '.$this->language->get('entry_quantity');
        $data['entry_adminquantity'] = 'Admin '.$this->language->get('entry_quantity');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		if (isset($this->error['quantity'])) {
			$data['error_quantity'] = $this->error['quantity'];
		} else {
			$data['error_quantity'] = array();
		}

		if (isset($this->error['orginalquantity'])) {
			$data['error_orginalquantity'] = $this->error['orginalquantity'];
		} else {
			$data['error_orginalquantity'] = array();
		}

		if (isset($this->error['adminquantity'])) {
			$data['error_adminquantity'] = $this->error['adminquantity'];
		} else {
			$data['error_adminquantity'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/stockmanager', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		if (isset($this->request->get['stock_id'])) {		
			$data['action'] = $this->url->link('catalog/stockmanager/edit', 'token=' . $this->session->data['token'] . '&stock_id=' . $this->request->get['stock_id'] . $url, 'SSL');
		}

		$data['cancel'] = $this->url->link('catalog/stockmanager', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['stock_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$product_info = $this->model_catalog_stockmanager->getStock($this->request->get['stock_id']);
		}

		$data['token'] = $this->session->data['token'];

		if (isset($this->request->post['product_name'])) {
			$data['product_name'] = $this->request->post['product_name'];
		} elseif (!empty($product_info)) {
			$data['product_name'] = $product_info['product_name'];
		} else {
			$data['product_name'] = '';
		}

		if (isset($this->request->post['quantity'])) {
			$data['quantity'] = $this->request->post['quantity'];
		} elseif (!empty($product_info)) {
			$data['quantity'] = $product_info['quantity'];
		} else {
			$data['quantity'] = 1;
		}

		if (isset($this->request->post['orginalquantity'])) {
			$data['orginalquantity'] = $this->request->post['orginalquantity'];
		} elseif (!empty($product_info)) {
			$data['orginalquantity'] = $product_info['orginalquantity'];
		} else {
			$data['orginalquantity'] = 1;
		}

        if (isset($this->request->post['adminquantity'])) {
			$data['adminquantity'] = $this->request->post['adminquantity'];
		} elseif (!empty($product_info)) {
			$data['adminquantity'] = $product_info['adminquantity'];
		} else {
			$data['adminquantity'] = 0;
		}


		$this->load->model('design/layout');

		$data['layouts'] = $this->model_design_layout->getLayouts();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/stock_form.tpl', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/stockmanager')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}