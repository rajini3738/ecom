<?php    
class ControllerCatalogVideoGalleryalbum extends Controller { 
	private $error = array();
  
  	public function index() {
		$this->language->load('catalog/videoGallery_album');
		
		$this->document->setTitle($this->language->get('heading_title'));
				 
		$this->load->model('catalog/videoGallery_album');
		
    	$this->getList();
  	}
  
  	public function insert() {
		$this->load->language('catalog/videoGallery_album');

    	$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/videoGallery_album');
			
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_videoGallery_album->addAlbum($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');
			
			$url = '';
			
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
		    $this->response->redirect($this->url->link('catalog/videoGallery_album', 'token=' . $this->session->data['token'], 'SSL'));
		}
    
    	$this->getForm();
  	} 
   
  	public function update() {
		$this->load->language('catalog/videoGallery_album');

    	$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/videoGallery_album');
		
    	if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_videoGallery_album->editAlbum($this->request->get['album_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			//$this->response->setOutput($this->load->view('catalog/videoGallery_album.tpl', $data));

			// $this->response->setOutput($this->load->view('catalog/videoGallery_album_list.tpl', $data));

			$this->response->redirect($this->url->link('catalog/videoGallery_album', 'token=' . $this->session->data['token'] . $url, 'SSL'));

		}
    
    	$this->getForm();
  	}   

  	public function delete() {
		$this->load->language('catalog/videoGallery_album');

    	$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/videoGallery_album');
			
    	if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $album_id) {
				$this->model_catalog_videoGallery_album->deleteAlbum($album_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');
			
			$url = '';
			
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
    		$this->response->redirect($this->url->link('catalog/videoGallery_album', 'token=' . $this->session->data['token'], 'SSL'));
    	}
	
    	$this->getList();
  	}  
    
  	private function getList() {
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		$url = '';
			
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$data['albums'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);
		
		$album_total = $this->model_catalog_videoGallery_album->getTotalAlbums();
	
		$results = $this->model_catalog_videoGallery_album->getAlbums($data);
 
		$this->load->model('tool/image');
		

		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$image = $this->model_tool_image->resize($result['image'], 40, 40);
			} else {
				$image = $this->model_tool_image->resize('no_image.png', 40, 40);
			}
				

			$data['albums'][] = array(
				'album_id'        => $result['album_id'],
				'name'            => $result['name'],
				'viewed'		      => $result['viewed'],
				'sort_order'      => $result['sort_order'],
			  'thumb'           => $this->model_tool_image->resize($result['image'], 50, 50),
        'videos'          => $this->model_catalog_videoGallery_album->getTotalVideos($result['album_id']),
				'date_added'      => $result['date_added'],
				'selected'        => isset($this->request->post['selected']) && in_array($result['album_id'], $this->request->post['selected']),
				'edit'            => $this->url->link('catalog/videoGallery_album/update', 'token=' . $this->session->data['token'] . '&album_id=' . $result['album_id'], 'SSL')
			);
		}	
	
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_no_results'] = $this->language->get('text_no_results');

		$data['text_viewed'] = $this->language->get('text_viewed');

		
		$data['text_enabled'] = $this->language->get('text_enabled');
    	$data['text_disabled'] = $this->language->get('text_disabled');
    	$data['text_image'] = $this->language->get('text_image');

    	$data['text_confirm'] = $this->language->get('text_confirm');

    	$data['text_list'] = $this->language->get('text_list');
		
		$data['column_name'] = $this->language->get('column_name');
		$data['column_image'] = $this->language->get('column_image');		
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');		
		
		$data['button_add'] = $this->language->get('button_add');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_edit'] = $this->language->get('button_edit');
		
		$data['text_browse'] = $this->language->get('text_browse');
        $data['text_clear'] = $this->language->get('text_clear');	
 		
        $data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/videoGallery_album', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$data['add'] = $this->url->link('catalog/videoGallery_album/insert', 'token=' . $this->session->data['token'], 'SSL');
		$data['delete'] = $this->url->link('catalog/videoGallery_album/delete', 'token=' . $this->session->data['token'], 'SSL');



        $data['token'] = $this->session->data['token'];

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/videoGallery_album', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');

		$data['sort_sort_order'] = $this->url->link('catalog/videoGallery_album', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, 'SSL');


		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $album_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->url = $this->url->link('catalog/videoGallery_album', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($album_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($album_total - $this->config->get('config_limit_admin'))) ? $album_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $album_total, ceil($album_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/videoGallery_album_list.tpl', $data));

	}
  
  	private function getForm() {
    	$data['heading_title'] = $this->language->get('heading_title');

    	$data['text_enabled'] = $this->language->get('text_enabled');
    	$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_default'] = $this->language->get('text_default');
    	$data['text_image'] = $this->language->get('text_image');
		
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
    	$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
  
    	$data['button_save'] = $this->language->get('button_save');
    	$data['button_cancel'] = $this->language->get('button_cancel');
		
		$data['text_browse'] = $this->language->get('text_browse');
        $data['text_clear'] = $this->language->get('text_clear');	

        $data['text_form'] = $this->language->get('text_form');
	  
 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

 		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}
		    
		$url = '';
			
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

   		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/videoGallery_album', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);
							
		if (!isset($this->request->get['album_id'])) {
			$data['action'] = $this->url->link('catalog/videoGallery_album/insert', 'token=' . $this->session->data['token'], 'SSL');
		} else {	
			$data['action'] = $this->url->link('catalog/videoGallery_album/update', 'token=' . $this->session->data['token'] . '&album_id=' . $this->request->get['album_id'], 'SSL');
		}

		$data['cancel'] = $this->url->link('catalog/videoGallery_album', 'token=' . $this->session->data['token'] . $url, 'SSL');


		$data['token'] = $this->session->data['token'];
		
    	if (isset($this->request->get['album_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
      		$album_info = $this->model_catalog_videoGallery_album->getAlbum($this->request->get['album_id']);
    	}

    	if (isset($this->request->post['name'])) {
      		$data['name'] = $this->request->post['name'];
    	} elseif (isset($album_info)) {
			$data['name'] = $album_info['name'];
		} else {	
      		$data['name'] = '';
    	}

		if (isset($this->request->post['image'])) {
			$data['image'] = $this->request->post['image'];
		} elseif (isset($album_info)) {
			$data['image'] = $album_info['image'];
		} else {
			$data['image'] = '';
		}
	
		$this->load->model('tool/image');

		if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($album_info) && is_file(DIR_IMAGE . $album_info['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($album_info['image'], 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}

		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		
		if (isset($this->request->post['sort_order'])) {
      		$data['sort_order'] = $this->request->post['sort_order'];
    	} elseif (isset($album_info)) {
			$data['sort_order'] = $album_info['sort_order'];
		} else {
      		$data['sort_order'] = '';
    	}
		
	
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		
		$this->response->setOutput($this->load->view('catalog/videoGallery_album_form.tpl', $data));
	}  
	 
  	private function validateForm() {
    	if (!$this->user->hasPermission('modify', 'catalog/videoGallery_album')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	}

    	if ((strlen(utf8_decode($this->request->post['name'])) < 3) || (strlen(utf8_decode($this->request->post['name'])) > 64)) {
      		$this->error['name'] = $this->language->get('error_name');
    	}
		
		if (!$this->error) {
	  		return TRUE;
		} else {
	  		return FALSE;
		}
  	}    

  	private function validateDelete() {
    	if (!$this->user->hasPermission('modify', 'catalog/videoGallery_album')) {
			$this->error['warning'] = $this->language->get('error_permission');
    	}	
				
		if (!$this->error) {
	  		return TRUE;
		} else {
	  		return FALSE;
		}  
  	}
}
?>