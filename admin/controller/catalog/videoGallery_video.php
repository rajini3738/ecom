<?php    
class ControllerCatalogVideoGalleryvideo extends Controller { 
	private $error = array();
  
  	public function index() {
			$this->language->load('catalog/videoGallery_video');
		
		$this->document->setTitle($this->language->get('heading_title'));
				 
		$this->load->model('catalog/videoGallery_video');
		
    	$this->getList();
  	}
  
  	public function insert() {
		$this->load->language('catalog/videoGallery_video');

    	$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/videoGallery_video');
			
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_videoGallery_video->addVideo($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');
			
			$url = '';
			
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
		
			$this->response->redirect($this->url->link('catalog/videoGallery_video', 'token=' . $this->session->data['token'], 'SSL'));

		}
    
    	$this->getForm();
  	} 
   
  	public function update() {
		$this->load->language('catalog/videoGallery_video');

    	$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/videoGallery_video');
		
    	if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_videoGallery_video->editVideo($this->request->get['video_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			
			$this->response->redirect($this->url->link('catalog/videoGallery_video', 'token=' . $this->session->data['token'], 'SSL'));
		}
    
    	$this->getForm();
  	}   

  	public function delete() {
		$this->load->language('catalog/videoGallery_video');

    	$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('catalog/videoGallery_video');
			
    	if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $video_id) {
				$this->model_catalog_videoGallery_video->deleteVideo($video_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');
			
			$url = '';
			
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			
			$this->response->redirect($this->url->link('catalog/videoGallery_video', 'token=' . $this->session->data['token'], 'SSL'));
    	}
	
    	$this->getList();
  	}  
    
  	private function getList() {
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		$url = '';
			
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/videoGallery_video', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
							
		$data['add'] = $this->url->link('catalog/videoGallery_video/insert', 'token=' . $this->session->data['token'], 'SSL');
		$data['delete'] = $this->url->link('catalog/videoGallery_video/delete', 'token=' . $this->session->data['token'], 'SSL');

		$data['videos'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
		
		$video_total = $this->model_catalog_videoGallery_video->getTotalVideos();
	
		$results = $this->model_catalog_videoGallery_video->getVideos($data);
 
		$this->load->model('tool/image');

		

		foreach ($results as $result) {

			if (is_file(DIR_IMAGE . $result['image'])) {
				$image = $this->model_tool_image->resize($result['image'], 100, 100);
			} else {
				$image = $result['image'];
			}
    	
						
			$data['videos'][] = array(
				'video_id'        => $result['video_id'],
				'name'            => $result['name'],
				'description'     => $result['description'],
				'code'            => $result['code'],
				'source'          => $result['source'],
				'date_added'      => $result['date_added'],
			    'thumb'           => $image,
				'sort_order'      => $result['sort_order'],
				'selected'        => isset($this->request->post['selected']) && in_array($result['video_id'], $this->request->post['selected']),
				'edit'            => $this->url->link('catalog/videoGallery_video/update', 'token=' . $this->session->data['token'] . '&video_id=' . $result['video_id'], 'SSL')
			);
		}	
	
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_no_results'] = $this->language->get('text_no_results');

		$data['text_viewed'] = $this->language->get('text_viewed');

		
		$data['text_enabled'] = $this->language->get('text_enabled');
    	$data['text_disabled'] = $this->language->get('text_disabled');
    	$data['text_image'] = $this->language->get('text_image');

    	$data['text_confirm'] = $this->language->get('text_confirm');

    	$data['text_list'] = $this->language->get('text_list');
		
		$data['column_name'] = $this->language->get('column_name');
		$data['column_image'] = $this->language->get('column_image');		
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');		
		
		$data['button_add'] = $this->language->get('button_add');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_edit'] = $this->language->get('button_edit');
		
		$data['text_browse'] = $this->language->get('text_browse');
        $data['text_clear'] = $this->language->get('text_clear');	
 
 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$data['sort_name'] = $this->url->link('catalog/videoGallery_video', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');

		$data['sort_sort_order'] = $this->url->link('catalog/videoGallery_video', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
	
		$pagination = new Pagination();
		$pagination->total = $video_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->url = $this->url->link('catalog/videoGallery_video', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($video_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($video_total - $this->config->get('config_limit_admin'))) ? $video_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $video_total, ceil($video_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/videoGallery_video_list.tpl', $data));


	}
  
  	private function getForm() {
    	$data['heading_title'] = $this->language->get('heading_title');

    	$data['text_enabled'] = $this->language->get('text_enabled');
    	$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_default'] = $this->language->get('text_default');
    	$data['text_image'] = $this->language->get('text_image');
		
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_album'] = $this->language->get('entry_album');
		$data['entry_source'] = $this->language->get('entry_source');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
    	$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
  
    	$data['button_save'] = $this->language->get('button_save');
    	$data['button_cancel'] = $this->language->get('button_cancel');
		
		$data['text_browse'] = $this->language->get('text_browse');
        $data['text_clear'] = $this->language->get('text_clear');	

        $data['text_form'] = $this->language->get('text_form');
	  
 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

 		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->error['description'])) {
			$data['error_description'] = $this->error['description'];
		} else {
			$data['error_description'] = '';
		}
		    
		$url = '';
			
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
  		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/videoGallery_video', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);
						
							
		if (!isset($this->request->get['video_id'])) {
			$data['action'] = $this->url->link('catalog/videoGallery_video/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$data['action'] = $this->url->link('catalog/videoGallery_video/update', 'token=' . $this->session->data['token'] . '&video_id=' . $this->request->get['video_id'] . $url, 'SSL');
		}

		$data['cancel'] = $this->url->link('catalog/videoGallery_video', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$data['token'] = $this->session->data['token'];
		
    	if (isset($this->request->get['video_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
      		$video_info = $this->model_catalog_videoGallery_video->getVideo($this->request->get['video_id']);
    	}

    	if (isset($this->request->post['name'])) {
      		$data['name'] = $this->request->post['name'];
    	} elseif (isset($video_info)) {
			$data['name'] = $video_info['name'];
		} else {	
      		$data['name'] = '';
    	}


    	if (isset($this->request->post['image'])) {
			$data['image'] = $this->request->post['image'];
		} elseif (isset($video_info)) {
			$data['image'] = $video_info['image'];
		} else {
			$data['image'] = '';
		}
	
		$this->load->model('tool/image');
        
		if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($video_info) && is_file(DIR_IMAGE . $video_info['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($video_info['image'], 100, 100);
		} else {
			if (!isset($video_info['image'])){
				$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
			}else {

			   $data['thumb'] = $video_info['image'];
            }
		}

		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		
		if (isset($this->request->post['source'])) {
      		$data['source'] = $this->request->post['source'];
    	} elseif (isset($video_info)) {
			$data['source'] = $video_info['source'];
		} else {	
      		$data['source'] = '';
    	}
		
		
    	if (isset($this->request->post['code'])) {
      		$data['code'] = $this->request->post['code'];
    	} elseif (isset($video_info)) {
			$data['code'] = $video_info['code'];
		} else {	
      		$data['code'] = '';
    	}
		
		if (isset($this->request->post['description'])) {
      		$data['description'] = $this->request->post['description'];
    	} elseif (isset($video_info)) {
			$data['description'] = $video_info['description'];
		} else {	
      		$data['description'] = '';
    	}
    			
		$data['albums'] = $this->model_catalog_videoGallery_video->getAlbums();
		
		if (isset($this->request->post['video_album'])) {
			$data['video_album'] = $this->request->post['video_album'];
		} elseif (isset($video_info)) {
			$data['video_album'] = $this->model_catalog_videoGallery_video->getVideoAlbums($this->request->get['video_id']);
		} else {
			$data['video_album'] = array(0);
		}	
		
		if (isset($this->request->post['sort_order'])) {
      		$data['sort_order'] = $this->request->post['sort_order'];
    	} elseif (isset($video_info)) {
			$data['sort_order'] = $video_info['sort_order'];
		} else {
      		$data['sort_order'] = '';
    	}
		
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		
		$this->response->setOutput($this->load->view('catalog/videoGallery_video_form.tpl', $data));
	}  
	 
  	private function validateForm() {
    	if (!$this->user->hasPermission('modify', 'catalog/videoGallery_video')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	}

    	if ((strlen(utf8_decode($this->request->post['name'])) < 3) || (strlen(utf8_decode($this->request->post['name'])) > 300)) {
      		$this->error['name'] = $this->language->get('error_name');
    	}
      
    	/*if (strlen(utf8_decode($this->request->post['description'])) < 3) {
      		$this->error['description'] = $this->language->get('error_description');
    	}*/
		
		if (!$this->error) {
	  		return TRUE;
		} else {
	  		return FALSE;
		}
  	}    

  	private function validateDelete() {
    	if (!$this->user->hasPermission('modify', 'catalog/videoGallery_video')) {
			$this->error['warning'] = $this->language->get('error_permission');
    	}	
				
		if (!$this->error) {
	  		return TRUE;
		} else {
	  		return FALSE;
		}  
  	}
}
?>