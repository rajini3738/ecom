<?php
class ControllerShippingDHL extends Controller { 
	private $error = array();
	
	public function index() {  
		$this->load->language('shipping/dhl');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				 
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			
			$this->model_setting_setting->editSetting('dhl', $this->request->post);	

			$this->session->data['success'] = $this->language->get('text_success');
									
			$this->response->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));
		}
		
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_all_zones'] = $this->language->get('text_select');
		
		$data['entry_rate'] = $this->language->get('entry_rate');
		$data['entry_tax_class'] = $this->language->get('entry_tax_class');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_country'] = $this->language->get('entry_country');
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['site_id'])) {
			$data['error_site_id'] = $this->error['site_id'];
		} else {
			$data['error_site_id'] = '';
		}

		if (isset($this->error['site_pwd'])) {
			$data['error_site_pwd'] = $this->error['site_pwd'];
		} else {
			$data['error_site_pwd'] = '';
		}

		if (isset($this->error['country'])) {
			$data['error_country'] = $this->error['country'];
		} else {
			$data['error_country'] = '';
		}

		if (isset($this->error['city'])) {
			$data['error_city'] = $this->error['city'];
		} else {
			$data['error_city'] = '';
		}

		if (isset($this->error['postcode'])) {
			$data['error_postcode'] = $this->error['postcode'];
		} else {
			$data['error_postcode'] = '';
		}

		if (isset($this->error['geo_zone_id'])) {
			$data['dhl_geo_zone_id'] = $this->error['geo_zone_id'];
		} else {
			$data['dhl_geo_zone_id'] = '';
		}

  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_shipping'),
			'href'      => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL')
   		);
		
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('shipping/dhl', 'token=' . $this->session->data['token'], 'SSL')
   		);
		
		$data['action'] = $this->url->link('shipping/dhl', 'token=' . $this->session->data['token'], 'SSL');
		
		$data['cancel'] = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'); 

		if (isset($this->request->post['dhl_account_number'])) {
			$data['dhl_account_number'] = $this->request->post['dhl_account_number'];
		} else {
			$data['dhl_account_number'] = $this->config->get('dhl_account_number');
		}
		
		if (isset($this->request->post['dhl_site_id'])) {
			$data['dhl_site_id'] = $this->request->post['dhl_site_id'];
		} else {
			$data['dhl_site_id'] = $this->config->get('dhl_site_id');
		}

		if (isset($this->request->post['dhl_site_pwd'])) {
			$data['dhl_site_pwd'] = $this->request->post['dhl_site_pwd'];
		} else {
			$data['dhl_site_pwd'] = $this->config->get('dhl_site_pwd');
		}
		
		if (isset($this->request->post['dhl_status'])) {
			$data['dhl_status'] = $this->request->post['dhl_status'];
		} else {
			$data['dhl_status'] = $this->config->get('dhl_status');
		}
		
		if (isset($this->request->post['dhl_sort_order'])) {
			$data['dhl_sort_order'] = $this->request->post['dhl_sort_order'];
		} else {
			$data['dhl_sort_order'] = $this->config->get('dhl_sort_order');
		}	

		if (isset($this->request->post['dhl_country'])) {
			$data['dhl_country'] = $this->request->post['dhl_country'];
		} else {
			$data['dhl_country'] = $this->config->get('dhl_country');
		}

		if (isset($this->request->post['dhl_city'])) {
			$data['dhl_city'] = $this->request->post['dhl_city'];
		} else {
			$data['dhl_city'] = $this->config->get('dhl_city');
		}

		if (isset($this->request->post['dhl_postcode'])) {
			$data['dhl_postcode'] = $this->request->post['dhl_postcode'];
		} else {
			$data['dhl_postcode'] = $this->config->get('dhl_postcode');
		}

		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/geo_zone');

		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		$this->load->model('localisation/country');

		$data['countries'] = $this->model_localisation_country->getCountries();

	if ($data['dhl_site_pwd'] != '' && $data['dhl_site_id'] != '' && $data['dhl_country'] != '') {
			
		$url = 'https://xmlpi-ea.dhl.com/XMLShippingServlet';		
		$mailingDate = date('Y-m-d', time());

		foreach($data['countries'] as $cnt){
			if($cnt['country_id'] == $data['dhl_country']){
				$country = $cnt['iso_code_2'];
				break;
			}
		}

		$xmlRequest = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
	<p:DCTRequest xmlns:p="http://www.dhl.com" xmlns:p1="http://www.dhl.com/datatypes" xmlns:p2="http://www.dhl.com/DCTRequestdatatypes" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dhl.com DCT-req.xsd ">
		<GetQuote>
			<Request>
				<ServiceHeader>
					<SiteID>{$data['dhl_site_id']}</SiteID>
					<Password>{$data['dhl_site_pwd']}</Password>
				</ServiceHeader>
			</Request>
			<From>
				<CountryCode>{$country}</CountryCode>
				<Postalcode>{$data['dhl_postcode']}</Postalcode>
				<City>{$data['dhl_city']}</City>
			</From>
			<BkgDetails>
				<PaymentCountryCode>{$country}</PaymentCountryCode>
				<Date>{$mailingDate}</Date>
				<ReadyTime>PT10H21M</ReadyTime>
				<DimensionUnit>CM</DimensionUnit>
				<WeightUnit>KG</WeightUnit>
				<IsDutiable>N</IsDutiable>
			</BkgDetails>
			<To>
				<CountryCode>{$country}</CountryCode>
			</To>
		</GetQuote>
	</p:DCTRequest>
XML;

		$curl = curl_init($url);

		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $xmlRequest);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_TIMEOUT, 60);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

		$xml = curl_exec($curl);

		curl_close($curl);

		$result = simplexml_load_string($xml);

		if(isset($result->Response->Status->Condition->ConditionData[0])) {
			$data['error_message'] = '<small style="color:red">' . $result->Response->Status->Condition->ConditionData[0] . '</small>';

		} else {
			$data['error_message'] = '<small style="color:green">Success</small>';
			$data['validation'] = true;
		}

	} elseif($data['dhl_site_id'] == '') {
		$data['error_message'] = '<small style="color:red">Please fill in site id</small>';
	} elseif($data['dhl_site_pwd'] == '') {
		$data['error_message'] = '<small style="color:red">Please fill in site password</small>';
	} else {
		$data['error_message'] = '<small style="color:red">Please select country</small>';
	}


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('shipping/dhl.tpl', $data));
	}
		
	private function validate() {
		if (!$this->user->hasPermission('modify', 'shipping/dhl')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->request->post['dhl_site_id']) {
			$this->error['site_id'] = $this->language->get('error_site_id');
		}

		if (!$this->request->post['dhl_site_pwd']) {
			$this->error['site_pwd'] = $this->language->get('error_site_pwd');
		}

		if (!$this->request->post['dhl_country']) {
			$this->error['country'] = $this->language->get('error_country');
		}

		if (!$this->request->post['dhl_city']) {
			$this->error['city'] = $this->language->get('error_city');
		}

		if (!$this->request->post['dhl_postcode']) {
			$this->error['postcode'] = $this->language->get('error_postcode');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
	public function validation(){

		$site_id = $this->request->post['site_id'];
		$site_pwd = $this->request->post['site_pwd'];
		$country_id = $this->request->post['country_id'];
		$postcode = $this->request->post['postcode'];		
		$city = $this->request->post['city'];
		$url = 'https://xmlpi-ea.dhl.com/XMLShippingServlet';
		$mailingDate = date('Y-m-d', time());

	if ($site_id != '' && $site_pwd != '' && $country_id != '') {

		$this->load->model('localisation/country');

		$countries = $this->model_localisation_country->getCountries();

		foreach($countries as $cnt){
			if($cnt['country_id'] == $country_id){
				$country = $cnt['iso_code_2'];
				break;
			}
		}

		$xmlRequest = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
	<p:DCTRequest xmlns:p="http://www.dhl.com" xmlns:p1="http://www.dhl.com/datatypes" xmlns:p2="http://www.dhl.com/DCTRequestdatatypes" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dhl.com DCT-req.xsd ">
		<GetQuote>
			<Request>
				<ServiceHeader>
					<SiteID>{$site_id}</SiteID>
					<Password>{$site_pwd}</Password>
				</ServiceHeader>
			</Request>
			<From>
				<CountryCode>{$country}</CountryCode>
				<Postalcode>{$postcode}</Postalcode>
				<City>{$city}</City>
			</From>
			<BkgDetails>
				<PaymentCountryCode>{$country}</PaymentCountryCode>
				<Date>{$mailingDate}</Date>
				<ReadyTime>PT10H21M</ReadyTime>
				<DimensionUnit>CM</DimensionUnit>
				<WeightUnit>KG</WeightUnit>
				<IsDutiable>N</IsDutiable>
			</BkgDetails>
			<To>
				<CountryCode>{$country}</CountryCode>
			</To>
		</GetQuote>
	</p:DCTRequest>
XML;

		$curl = curl_init($url);

		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $xmlRequest);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_TIMEOUT, 60);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

		$xml = curl_exec($curl);

		curl_close($curl);

		$result = simplexml_load_string($xml);

		if(isset($result->Response->Status->Condition->ConditionData[0])) {
			$data['error_message'] = '<small style="color:red">' . $result->Response->Status->Condition->ConditionData[0] . '</small>';

		} else {
			$data['error_message'] = '<small style="color:green">Success</small>';
			$data['validation'] = true;
		}

	} elseif($site_id == '') {
		$data['error_message'] = '<small style="color:red">Please fill in site id</small>';
	} elseif($site_pwd == '') {
		$data['error_message'] = '<small style="color:red">Please fill in site password</small>';
	} else {
		$data['error_message'] = '<small style="color:red">Please select country</small>';
	}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));

	}
}
