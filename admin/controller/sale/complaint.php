<?php
class ControllerSaleComplaint extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('sale/complaint');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('sale/complaint');

		$this->getList();
	}

    public function edit() {
        $this->load->language('sale/complaint');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/complaint');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $upload_info = $this->model_sale_complaint->editComplaint($this->request->get['complaint_id'], $this->request->post);

            if ($upload_info->row && is_file(DIR_UPLOAD . $upload_info->row['filename'])) {
                unlink(DIR_UPLOAD . $upload_info->row['filename']);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['filter_complaint_id'])) {
                $url .= '&filter_complaint_id=' . $this->request->get['filter_complaint_id'];
            }

            if (isset($this->request->get['filter_order_id'])) {
                $url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
            }

            if (isset($this->request->get['filter_customer'])) {
                $url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_product'])) {
                $url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_model'])) {
                $url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_date_added'])) {
                $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
            }

            if (isset($this->request->get['filter_date_modified'])) {
                $url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('sale/complaint', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function delete() {
        $this->load->language('sale/complaint');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/complaint');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $complaint_id) {
                $this->model_sale_complaint->deleteComplaint($complaint_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['filter_complaint_id'])) {
                $url .= '&filter_complaint_id=' . $this->request->get['filter_complaint_id'];
            }

            if (isset($this->request->get['filter_order_id'])) {
                $url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
            }

            if (isset($this->request->get['filter_customer'])) {
                $url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_product'])) {
                $url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_date_added'])) {
                $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
            }

            if (isset($this->request->get['filter_date_modified'])) {
                $url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('sale/complaint', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getList();
    }

	protected function getList() {
		if (isset($this->request->get['filter_complaint_id'])) {
			$filter_complaint_id = $this->request->get['filter_complaint_id'];
		} else {
			$filter_complaint_id = null;
		}

		if (isset($this->request->get['filter_order_id'])) {
			$filter_order_id = $this->request->get['filter_order_id'];
		} else {
			$filter_order_id = null;
		}

		if (isset($this->request->get['filter_customer'])) {
			$filter_customer = $this->request->get['filter_customer'];
		} else {
			$filter_customer = null;
		}

		if (isset($this->request->get['filter_product'])) {
			$filter_product = $this->request->get['filter_product'];
		} else {
			$filter_product = null;
		}

		if (isset($this->request->get['filter_date_added'])) {
			$filter_date_added = $this->request->get['filter_date_added'];
		} else {
			$filter_date_added = null;
		}

		if (isset($this->request->get['filter_date_modified'])) {
			$filter_date_modified = $this->request->get['filter_date_modified'];
		} else {
			$filter_date_modified = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'complaint_id';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_complaint_id'])) {
			$url .= '&filter_complaint_id=' . $this->request->get['filter_complaint_id'];
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
		}

		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_product'])) {
			$url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['filter_date_modified'])) {
			$url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('sale/complaint', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		$data['add'] = $this->url->link('sale/complaint/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$data['delete'] = $this->url->link('sale/complaint/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$data['complaints'] = array();

		$filter_data = array(
			'filter_complaint_id'           => $filter_complaint_id,
			'filter_order_id'               => $filter_order_id,
			'filter_customer'               => $filter_customer,
			'filter_product'                => $filter_product,
			'filter_date_added'             => $filter_date_added,
			'filter_date_modified'          => $filter_date_modified,
			'sort'                          => $sort,
			'order'                         => $order,
			'start'                         => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                         => $this->config->get('config_limit_admin')
		);

		$complaint_total = $this->model_sale_complaint->getTotalComplaints($filter_data);

		$results = $this->model_sale_complaint->getComplaints($filter_data);

		foreach ($results as $result) {
            if($result['uploadphoto']){
                if($this->model_sale_complaint->getUploadedPhoto($result['uploadphoto']))
                    $file = HTTP_CATALOG. 'system/upload/' . $this->model_sale_complaint->getUploadedPhoto($result['uploadphoto']);
                else
                    $file="#";
			}
			else{
				$file="#";
			}
			$data['complaints'][] = array(
				'complaint_id'      => $result['complaint_id'],
				'order_id'          => $result['order_id'],
				'customer'          => $result['customer'],
				'product'           => $result['product'],
				'model'             => $result['model'],
				'status'            => $result['opened'],
                'upload'            => $file,
				'date_added'        => $result['date_added'],//date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'date_modified'     => $result['date_modified'],//date($this->language->get('date_format_short'), strtotime($result['date_modified'])),
				'edit'              => $this->url->link('sale/complaint/edit', 'token=' . $this->session->data['token'] . '&complaint_id=' . $result['complaint_id'] . $url, 'SSL')
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_complaint_id'] = $this->language->get('column_complaint_id');
		$data['column_order_id'] = $this->language->get('column_order_id');
		$data['column_customer'] = $this->language->get('column_customer');
		$data['column_product'] = $this->language->get('column_product');
		$data['column_model'] = $this->language->get('column_model');
		$data['column_status'] = $this->language->get('column_status');

		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_date_modified'] = $this->language->get('column_date_modified');
		$data['column_action'] = $this->language->get('column_action');

		$data['entry_complaint_id'] = $this->language->get('entry_complaint_id');
		$data['entry_order_id'] = $this->language->get('entry_order_id');
		$data['entry_customer'] = $this->language->get('entry_customer');
		$data['entry_product'] = $this->language->get('entry_product');
		$data['entry_model'] = $this->language->get('entry_model');
		$data['entry_complaint_status'] = $this->language->get('entry_complaint_status');
		$data['entry_date_added'] = $this->language->get('entry_date_added');
		$data['entry_date_modified'] = $this->language->get('entry_date_modified');
        $data['entry_telephone'] = $this->language->get('entry_telephone');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_filter'] = $this->language->get('button_filter');

		$data['token'] = $this->session->data['token'];

		if (isset($this->session->data['error'])) {
			$data['error_warning'] = $this->session->data['error'];

			unset($this->session->data['error']);
		} elseif (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_complaint_id'])) {
			$url .= '&filter_complaint_id=' . $this->request->get['filter_complaint_id'];
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
		}

		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_product'])) {
			$url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['filter_date_modified'])) {
			$url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_complaint_id'] = $this->url->link('sale/complaint', 'token=' . $this->session->data['token'] . '&sort=r.complaint_id' . $url, 'SSL');
		$data['sort_order_id'] = $this->url->link('sale/complaint', 'token=' . $this->session->data['token'] . '&sort=r.order_id' . $url, 'SSL');
		$data['sort_customer'] = $this->url->link('sale/complaint', 'token=' . $this->session->data['token'] . '&sort=customer' . $url, 'SSL');
		$data['sort_product'] = $this->url->link('sale/complaint', 'token=' . $this->session->data['token'] . '&sort=product' . $url, 'SSL');
		$data['sort_status'] = $this->url->link('sale/complaint', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');
		$data['sort_date_added'] = $this->url->link('sale/complaint', 'token=' . $this->session->data['token'] . '&sort=r.date_added' . $url, 'SSL');
		$data['sort_date_modified'] = $this->url->link('sale/complaint', 'token=' . $this->session->data['token'] . '&sort=r.date_modified' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['filter_complaint_id'])) {
			$url .= '&filter_complaint_id=' . $this->request->get['filter_complaint_id'];
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
		}

		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_product'])) {
			$url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['filter_date_modified'])) {
			$url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $complaint_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('sale/complaint', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($complaint_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($complaint_total - $this->config->get('config_limit_admin'))) ? $complaint_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $complaint_total, ceil($complaint_total / $this->config->get('config_limit_admin')));

		$data['filter_complaint_id'] = $filter_complaint_id;
		$data['filter_order_id'] = $filter_order_id;
		$data['filter_customer'] = $filter_customer;
		$data['filter_product'] = $filter_product;
		$data['filter_date_added'] = $filter_date_added;
		$data['filter_date_modified'] = $filter_date_modified;

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('sale/complaint_list.tpl', $data));
	}

    protected function getForm() {
        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_form'] = !isset($this->request->get['complaint_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['text_select'] = $this->language->get('text_select');
        $data['text_opened'] = $this->language->get('text_opened');
        $data['text_unopened'] = $this->language->get('text_unopened');
        $data['text_order'] = $this->language->get('text_order');
        $data['text_product'] = $this->language->get('text_product');
        $data['text_history'] = $this->language->get('text_history');
        $data['text_loading'] = $this->language->get('text_loading');

        $data['entry_customer'] = $this->language->get('entry_customer');
        $data['entry_order_id'] = $this->language->get('entry_order_id');
        $data['entry_date_ordered'] = $this->language->get('entry_date_ordered');
        $data['entry_firstname'] = $this->language->get('entry_firstname');
        $data['entry_lastname'] = $this->language->get('entry_lastname');
        $data['entry_middlename'] = $this->language->get('entry_middlename');
        $data['entry_email'] = $this->language->get('entry_email');
        $data['entry_telephone'] = $this->language->get('entry_telephone');
        $data['entry_product'] = $this->language->get('entry_product');
        $data['entry_model'] = $this->language->get('entry_model');
        $data['entry_opened'] = $this->language->get('entry_opened');
        $data['entry_comment'] = $this->language->get('entry_comment');
        $data['entry_admin_comment'] = "Admin Comment";//$this->language->get('entry_comment');
        $data['entry_notify'] = $this->language->get('entry_notify');
        $data['entry_reason'] = $this->language->get('entry_reason');

        $data['help_product'] = $this->language->get('help_product');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['button_history_add'] = $this->language->get('button_history_add');

        $data['tab_general'] = $this->language->get('tab_general');
        $data['tab_history'] = $this->language->get('tab_history');

        $data['token'] = $this->session->data['token'];

        if (isset($this->request->get['complaint_id'])) {
            $data['complaint_id'] = $this->request->get['complaint_id'];
        } else {
            $data['complaint_id'] = 0;
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['order_id'])) {
            $data['error_order_id'] = $this->error['order_id'];
        } else {
            $data['error_order_id'] = '';
        }

        if (isset($this->error['firstname'])) {
            $data['error_firstname'] = $this->error['firstname'];
        } else {
            $data['error_firstname'] = '';
        }

        if (isset($this->error['middlename'])) {
            $data['error_middlename'] = $this->error['middlename'];
        } else {
            $data['error_middlename'] = '';
        }

        if (isset($this->error['lastname'])) {
            $data['error_lastname'] = $this->error['lastname'];
        } else {
            $data['error_lastname'] = '';
        }

        if (isset($this->error['email'])) {
            $data['error_email'] = $this->error['email'];
        } else {
            $data['error_email'] = '';
        }

        if (isset($this->error['telephone'])) {
			$data['error_telephone'] = $this->error['telephone'];
		} else {
			$data['error_telephone'] = '';
		}

        if (isset($this->error['reason'])) {
			$data['error_reason'] = $this->error['reason'];
		} else {
			$data['error_reason'] = '';
		}

        $url = '';

        if (isset($this->request->get['filter_complaint_id'])) {
            $url .= '&filter_complaint_id=' . $this->request->get['filter_complaint_id'];
        }

        if (isset($this->request->get['filter_order_id'])) {
            $url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
        }

        if (isset($this->request->get['filter_customer'])) {
            $url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_product'])) {
            $url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_date_added'])) {
            $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
        }

        if (isset($this->request->get['filter_date_modified'])) {
            $url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('sale/complaint', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        if (!isset($this->request->get['complaint_id'])) {
            $data['action'] = $this->url->link('sale/complaint/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        } else {
            $data['action'] = $this->url->link('sale/complaint/edit', 'token=' . $this->session->data['token'] . '&complaint_id=' . $this->request->get['complaint_id'] . $url, 'SSL');
        }

        $data['cancel'] = $this->url->link('sale/complaint', 'token=' . $this->session->data['token'] . $url, 'SSL');

        if (isset($this->request->get['complaint_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $complaint_info = $this->model_sale_complaint->getComplaint($this->request->get['complaint_id']);
        }

        if (isset($this->request->post['order_id'])) {
            $data['order_id'] = $this->request->post['order_id'];
        } elseif (!empty($complaint_info)) {
            $data['order_id'] = $complaint_info['order_id'];
        } else {
            $data['order_id'] = '';
        }

        if (isset($this->request->post['firstname'])) {
            $data['firstname'] = $this->request->post['firstname'];
        } elseif (!empty($complaint_info)) {
            $data['firstname'] = $complaint_info['firstname'];
        } else {
            $data['firstname'] = '';
        }

        if (isset($this->request->post['lastname'])) {
            $data['lastname'] = $this->request->post['lastname'];
        } elseif (!empty($complaint_info)) {
            $data['lastname'] = $complaint_info['lastname'];
        } else {
            $data['lastname'] = '';
        }

        if (isset($this->request->post['middlename'])) {
            $data['middlename'] = $this->request->post['middlename'];
        } elseif (!empty($complaint_info)) {
            $data['middlename'] = $complaint_info['middlename'];
        } else {
            $data['middlename'] = '';
        }

        if (isset($this->request->post['email'])) {
            $data['email'] = $this->request->post['email'];
        } elseif (!empty($complaint_info)) {
            $data['email'] = $complaint_info['email'];
        } else {
            $data['email'] = '';
        }

        if (isset($this->request->post['telephone'])) {
			$data['telephone'] = $this->request->post['telephone'];
		} elseif (!empty($complaint_info)) {
			$data['telephone'] = $complaint_info['telephone'];
		} else {
			$data['telephone'] = '';
		}

        if (isset($this->request->post['opened'])) {
            $data['opened'] = $this->request->post['opened'];
        } elseif (!empty($complaint_info)) {
            $data['opened'] = $complaint_info['opened'];
        } else {
            $data['opened'] = '';
        }

        if (isset($this->request->post['reason_id'])) {
			$data['reason_id'] = $this->request->post['reason_id'];
		} elseif (!empty($complaint_info)) {
			$data['reason_id'] = $complaint_info['reason_id'];
		} else {
			$data['reason_id'] = '';
		}

        if (isset($this->request->post['comment'])) {
            $data['comment'] = $this->request->post['comment'];
        } elseif (!empty($complaint_info)) {
            $data['comment'] = $complaint_info['comment'];
        } else {
            $data['comment'] = '';
        }

        if (isset($this->request->post['admin_comment'])) {
            $data['admin_comment'] = $this->request->post['admin_comment'];
        } elseif (!empty($complaint_info)) {
            $data['admin_comment'] = $complaint_info['admin_comment'];
        } else {
            $data['admin_comment'] = '';
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('sale/complaint_form.tpl', $data));
    }

    protected function validateForm() {
        if (!$this->user->hasPermission('modify', 'sale/complaint')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        //if (empty($this->request->post['order_id'])) {
        //    $this->error['order_id'] = $this->language->get('error_order_id');
        //}

        if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
            $this->error['firstname'] = $this->language->get('error_firstname');
        }

        if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
            $this->error['lastname'] = $this->language->get('error_lastname');
        }

        if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
            $this->error['email'] = $this->language->get('error_email');
        }

        if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
			$this->error['telephone'] = $this->language->get('error_telephone');
		}

        if (empty($this->request->post['reason_id'])) {
			$this->error['reason'] = $this->language->get('error_reason');
		}

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;
    }

    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'sale/complaint')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }
}