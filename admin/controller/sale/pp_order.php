<?php

class ControllerSalePpOrder extends Controller {

  private $error = array();

  public function index() {
    $this->checkdb();

    $this->language->load('sale/pp_order');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('sale/pp_order');

    $this->getList();
  }

  protected function getList() {
    if (isset($this->request->get['filter_order_id'])) {
      $filter_order_id = $this->request->get['filter_order_id'];
    } else {
      $filter_order_id = null;
    }

    if (isset($this->request->get['filter_customer'])) {
      $filter_customer = $this->request->get['filter_customer'];
    } else {
      $filter_customer = null;
    }

    if (isset($this->request->get['filter_order_status'])) {
      $filter_order_status = $this->request->get['filter_order_status'];
    } else {
      $filter_order_status = null;
    }

    if (isset($this->request->get['filter_total'])) {
      $filter_total = $this->request->get['filter_total'];
    } else {
      $filter_total = null;
    }

    if (isset($this->request->get['filter_date_added'])) {
      $filter_date_added = $this->request->get['filter_date_added'];
    } else {
      $filter_date_added = null;
    }

    if (isset($this->request->get['filter_date_modified'])) {
      $filter_date_modified = $this->request->get['filter_date_modified'];
    } else {
      $filter_date_modified = null;
    }

    if (isset($this->request->get['sort'])) {
      $sort = $this->request->get['sort'];
    } else {
      $sort = 'o.order_id';
    }

    if (isset($this->request->get['order'])) {
      $order = $this->request->get['order'];
    } else {
      $order = 'DESC';
    }

    if (isset($this->request->get['page'])) {
      $page = $this->request->get['page'];
    } else {
      $page = 1;
    }

    $url = '';

    if (isset($this->request->get['filter_order_id'])) {
      $url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
    }

    if (isset($this->request->get['filter_su_order_id'])) {
      $url .= '&filter_su_order_id=' . $this->request->get['filter_su_order_id'];
    }

    if (isset($this->request->get['filter_customer'])) {
      $url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
    }

    if (isset($this->request->get['filter_order_status_id'])) {
      $url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
    }

    if (isset($this->request->get['filter_order_date_added'])) {
      $url .= '&filter_order_date_added=' . $this->request->get['filter_order_date_added'];
    }

    if (isset($this->request->get['filter_su_date_added'])) {
      $url .= '&filter_su_date_added=' . $this->request->get['filter_su_date_added'];
    }

    if (isset($this->request->get['sort'])) {
      $url .= '&sort=' . $this->request->get['sort'];
    }

    if (isset($this->request->get['order'])) {
      $url .= '&order=' . $this->request->get['order'];
    }

    if (isset($this->request->get['page'])) {
      $url .= '&page=' . $this->request->get['page'];
    }

    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      'separator' => false
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('heading_title'),
      'href' => $this->url->link('sale/pp_order', 'token=' . $this->session->data['token'] . $url, 'SSL'),
      'separator' => ' :: '
    );


    $data['action_submit_su'] = $this->url->link('sale/pp_order/submit_su', 'token=' . $this->session->data['token'], 'SSL');
    $data['action_label'] = $this->url->link('sale/pp_order/label', 'token=' . $this->session->data['token'], 'SSL');
    $data['action_colli_update'] = $this->url->link('sale/pp_order/updateColli', 'token=' . $this->session->data['token'], 'SSL');


    $data['orders'] = array();

    $filter_data = array(
      'filter_order_id' => $filter_order_id,
      'filter_customer' => $filter_customer,
      'filter_order_status' => $filter_order_status,
      'filter_total' => $filter_total,
      'filter_date_added' => $filter_date_added,
      'filter_date_modified' => $filter_date_modified,
      'sort' => $sort,
      'order' => $order,
      'start' => ($page - 1) * $this->config->get('config_limit_admin'),
      'limit' => $this->config->get('config_limit_admin')
    );

    $data['filter_order_id'] = $filter_order_id;
    $data['filter_customer'] = $filter_customer;
    $data['filter_date_modified'] = $filter_date_modified;
    $data['filter_total'] = $filter_total;
    $data['filter_date_added'] = $filter_date_added;


    $order_total = $this->model_sale_pp_order->getTotalOrders($filter_data);

    $results = $this->model_sale_pp_order->getOrders($filter_data);
    foreach ($results as $result) {
      $action = array();

      if ((int) $result['su_order_id'] == '0') {
        $action[] = array(
          'span' => 'submit' . $result['order_id'],
          'text' => $this->language->get('text_submit_su'),
          'blank' => false,
          'action' => '',
          'href' => $this->url->link('sale/pp_order/submit_su', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, 'SSL')
        );
      }

      if ($result['su_order_id'] and $result['su_label_printed'] == '0') {
        if ($result['su_url_label']) {
          $action[] = array(
            'span' => 'label' . $result['order_id'],
            'text' => $this->language->get('text_show_label'),
            'blank' => true,
            'action' => 'onclick="$(\'.label' . $result['order_id'] . '\').remove();"',
            'href' => $this->url->link('sale/pp_order/label', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'])
          );
        }
      }

      if ($result['su_url_tracking']) {
        $action[] = array(
          'span' => 'tracking' . $result['order_id'],
          'text' => $this->language->get('text_show_tracking'),
          'blank' => true,
          'action' => '',
          'href' => $result['su_url_tracking']
        );
      }

      $action[] = array(
        'span' => 'view' . $result['order_id'],
        'text' => $this->language->get('text_view'),
        'blank' => false,
        'action' => '',
        'href' => $this->url->link('sale/order/info', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, 'SSL')
      );

      $shipping_code = substr($result['shipping_code'], 13);
      $shipping_code = explode('_', $shipping_code);
      $shipping_code = 'text_type_id_' . $shipping_code[4];

      $data['orders'][] = array(
        'order_id' => $result['order_id'],
        'su_order_id' => $result['su_order_id'],
        'customer' => $result['customer'],
        'status' => $result['status'],
        'su_colli' => $result['su_colli'],
        'su_barcode' => $result['su_barcode'],
        'su_shipping_method' => $this->language->get($shipping_code),
        'su_weight' => round($result['su_weight']),
        'order_date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
        'su_date_added' => ((int) $result['su_date_added'] == '0' ? '' : date($this->language->get('date_format_short'), strtotime($result['su_date_added']))),
        'selected' => isset($this->request->post['selected']) && in_array($result['order_id'], $this->request->post['selected']),
        'action' => $action
      );
    }

    $data['heading_title'] = $this->language->get('heading_title');

    $data['text_no_results'] = $this->language->get('text_no_results');
    $data['text_missing'] = $this->language->get('text_missing');
    $data['text_copyright'] = $this->language->get('text_copyright');

    $data['column_order_id'] = $this->language->get('column_order_id');
    $data['column_customer'] = $this->language->get('column_customer');
    $data['column_status'] = $this->language->get('column_status');
    $data['column_total'] = $this->language->get('column_total');
    $data['column_date_added'] = $this->language->get('column_order_date_added');
    $data['column_date_modified'] = $this->language->get('column_su_date_added');
    $data['column_su_order_id'] = $this->language->get('column_su_order_id');
    $data['column_weight'] = $this->language->get('column_weight');
    $data['column_shipping_method'] = $this->language->get('column_shipping_method');
    $data['column_colli'] = $this->language->get('column_colli');
    $data['column_barcode'] = $this->language->get('column_barcode');
    $data['column_order_date_added'] = $this->language->get('column_order_date_added');
    $data['column_su_date_added'] = $this->language->get('column_su_date_added');
    $data['column_action'] = $this->language->get('column_action');

    $data['button_submit_su'] = $this->language->get('button_submit_su');
    $data['button_label'] = $this->language->get('button_label');
    $data['button_filter'] = $this->language->get('button_filter');
    $data['button_update'] = $this->language->get('button_update');

    $data['token'] = $this->session->data['token'];

    if (isset($this->session->data['warning'])) {
      $data['error_warning'] = $this->session->data['warning'];

      unset($this->session->data['warning']);
    } else {
      $data['error_warning'] = '';
    }

    if (isset($this->session->data['success'])) {
      $data['success'] = $this->session->data['success'];

      unset($this->session->data['success']);
    } else {
      $data['success'] = '';
    }

    $url = '';

    if (isset($this->request->get['filter_order_id'])) {
      $url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
    }

    if (isset($this->request->get['filter_su_order_id'])) {
      $url .= '&filter_su_order_id=' . $this->request->get['filter_su_order_id'];
    }

    if (isset($this->request->get['filter_customer'])) {
      $url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
    }

    if (isset($this->request->get['filter_order_status_id'])) {
      $url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
    }

    if (isset($this->request->get['filter_order_date_added'])) {
      $url .= '&filter_order_date_added=' . $this->request->get['filter_order_date_added'];
    }

    if (isset($this->request->get['filter_su_date_added'])) {
      $url .= '&filter_su_date_added=' . $this->request->get['filter_su_date_added'];
    }

    if ($order == 'ASC') {
      $url .= '&order=DESC';
    } else {
      $url .= '&order=ASC';
    }

    if (isset($this->request->get['page'])) {
      $url .= '&page=' . $this->request->get['page'];
    }

    $data['sort_order'] = $this->url->link('sale/pp_order', 'token=' . $this->session->data['token'] . '&sort=o.order_id' . $url, 'SSL');
    $data['sort_su_order'] = $this->url->link('sale/pp_order', 'token=' . $this->session->data['token'] . '&sort=o.su_order_id' . $url, 'SSL');
    $data['sort_customer'] = $this->url->link('sale/pp_order', 'token=' . $this->session->data['token'] . '&sort=customer' . $url, 'SSL');
    $data['sort_status'] = $this->url->link('sale/pp_order', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');
    $data['sort_order_date_added'] = $this->url->link('sale/pp_order', 'token=' . $this->session->data['token'] . '&sort=o.date_added' . $url, 'SSL');
    $data['sort_su_date_added'] = $this->url->link('sale/pp_order', 'token=' . $this->session->data['token'] . '&sort=o.su_date_added' . $url, 'SSL');

    $url = '';

    if (isset($this->request->get['filter_order_id'])) {
      $url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
    }

    if (isset($this->request->get['filter_customer'])) {
      $url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
    }

    if (isset($this->request->get['filter_order_status'])) {
      $url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
    }

    if (isset($this->request->get['filter_total'])) {
      $url .= '&filter_total=' . $this->request->get['filter_total'];
    }

    if (isset($this->request->get['filter_date_added'])) {
      $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
    }

    if (isset($this->request->get['filter_date_modified'])) {
      $url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
    }

    if (isset($this->request->get['sort'])) {
      $url .= '&sort=' . $this->request->get['sort'];
    }

    if (isset($this->request->get['order'])) {
      $url .= '&order=' . $this->request->get['order'];
    }

    $pagination = new Pagination();
    $pagination->total = $order_total;
    $pagination->page = $page;
    $pagination->limit = $this->config->get('config_admin_limit');
    $pagination->text = $this->language->get('text_pagination');
    $pagination->url = $this->url->link('sale/pp_order', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

    $data['pagination'] = $pagination->render();

    $this->load->model('localisation/order_status');

    $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

    $data['sort'] = $sort;
    $data['order'] = $order;

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $data['text_list'] = $this->language->get('text_list');
    $data['text_view'] = $this->language->get('text_view');

    $data['entry_return_id'] = $this->language->get('entry_return_id');
    $data['entry_order_id'] = $this->language->get('entry_order_id');
    $data['entry_customer'] = $this->language->get('entry_customer');
    $data['entry_order_status'] = $this->language->get('entry_order_status');
    $data['entry_total'] = $this->language->get('entry_total');
    $data['entry_date_added'] = $this->language->get('entry_date_added');
    $data['entry_date_modified'] = $this->language->get('entry_date_modified');


    $this->response->setOutput($this->load->view('sale/pp_order_list.tpl', $data));
  }

  public function submit_su() {
    $this->language->load('sale/pp_order');

    $this->load->model('sale/pp_order');

    if ((isset($this->request->post['selected']) or isset($this->request->get['order_id'])) && ($this->validateSubmitSu())) {

      if (isset($this->request->get['order_id']) and ! isset($this->request->post['selected'])) {
        $this->request->post['selected'][] = $this->request->get['order_id'];
      }

      if (isset($this->session->data['success'])) {
        unset($this->session->data['success']);
      }

      if (isset($this->session->data['warning'])) {
        unset($this->session->data['warning']);
      }

      $count_right = 0;
      $count_wrong = 0;
      $error_system = false;
      $error_order = false;

      $gebruiker_id = $this->config->get('parcelpro_Id');
      $apiKey = $this->config->get('parcelpro_ApiKey');
      $rekeningnummer = $this->config->get('parcel_pro_iban');
      $naam_afzender = $this->config->get('parcel_pro_name');
      $straat_afzender = $this->config->get('parcel_pro_street');
      $nummer_afzender = $this->config->get('parcel_pro_number');
      $postcode_afzender = $this->config->get('parcel_pro_postcode');
      $plaats_afzender = $this->config->get('parcel_pro_city');
      $land_afzender = $this->config->get('parcel_pro_country');

      $shipping_methodes = $this->getShippingMethods();

      if (!$shipping_methodes) {
        if (isset($this->session->data['warning'])) {
          $this->session->data['warning'] .= '<br / >Not able to retrieve shipping methodes from Parcel Pro server';
        } else {
          $this->session->data['warning'] = 'Not able to retrieve shipping methodes from Parcel Pro server';
        }

        $error_system = true;
      }
      if (!$error_system) {
        $errorMessages = array();

        foreach ($this->request->post['selected'] as $order_id) {
          $error_order = false;

          $order_info = $this->model_sale_pp_order->getOrder($order_id);

          if (!$order_info) {
            if (isset($this->session->data['warning'])) {
              $this->session->data['warning'] .= '<br / >No order info found for order: ' . $order_id;
            } else {
              $this->session->data['warning'] = 'No order info found for order: ' . $order_id;
            }

            $count_wrong++;
            $error_order = true;
          }

          if (!$error_order) {

            $order_referentie = $order_info['order_id'] . ' - ' . $order_info['invoice_prefix'] . $order_info['invoice_no'];

            $error_order = true;

            $shipping_code = substr($order_info['shipping_code'], 13);
            $shipping_code = explode('_', $shipping_code);



            $order_shipping_type_id = $shipping_code[4];
            $order_shipping_type = 'parcel_pro_type_id_' . $shipping_code[4];
            $order_shipping_rule_id = $shipping_code[5];

            $order_shipping_rules = $this->config->get($order_shipping_type . '_rule');

            if (is_array($order_shipping_rules)) {
              if (isset($order_shipping_rules[$order_shipping_rule_id])) {
                $order_shipping_rules = $order_shipping_rules[$order_shipping_rule_id];
                $error_order = false;
              }
            }

            if ($error_order) {
              if (isset($this->session->data['warning'])) {
                $this->session->data['warning'] .= '<br / >No shippingrule set in shop administration for order: ' . $order_referentie;
              } else {
                $this->session->data['warning'] = 'No shippingrule set in shop administration for order: ' . $order_referentie;
              }

              $count_wrong++;
            }
          }

          if (!$error_order) {
            //                        var_dump($order_shipping_type_id);
            //                        echo '<pre>';
            //                        var_dump($shipping_methodes);
            //                        exit;

            foreach ($shipping_methodes as $shipping_methode) {

              if ($shipping_code[4] !== 'Parcelshop') {
                if ($shipping_methode['Code'] == $order_shipping_type_id || $shipping_methode['Type'] == $order_shipping_type_id) {
                  $type = $shipping_methode['Type'];
                  $carrier = $shipping_methode['Carrier'];
                  break;
                } else {
                  $type = false;
                  $carrier = false;
                }
              } else {
                if ($shipping_methode['Type'] == 'DFY' && $shipping_methode['ServicePoint'] == 1) {
                  $type = $shipping_methode['Type'];
                  $carrier = $shipping_methode['Carrier'];
                  break;
                } else {
                  $type = false;
                  $carrier = false;
                }
              }
            }
            if (!$type or ! $carrier) {

              if (isset($this->session->data['warning'])) {
                $this->session->data['warning'] .= '<br / >Shipping methode not found at Parcel Pro for order: ' . $order_referentie;
              } else {
                $this->session->data['warning'] = 'Shipping methode not found at Parcel Pro for order: ' . $order_referentie;
              }

              $count_wrong++;
              $error_order = true;
            }
          }

          if (!$error_order) {
            // general information
            $referentie = $order_info['order_id'] . ' - ' . $order_info['invoice_prefix'] . $order_info['invoice_no'];
            $naam = $order_info['shipping_firstname'] . ' ' . $order_info['shipping_lastname'];
            $tav = false;
            $straat = $order_info['shipping_address_1'];
            $nummer = false;
            $postcode = $order_info['shipping_postcode'];
            $plaats = $order_info['shipping_city'];
            $land = $order_info['shipping_country_iso_2'];
            $email = $order_info['email'];
            $telefoonnummer = $order_info['telephone'];
            $opmerking = 'Goederen';
            $date = date('Y\-m\-d G:i:s');

            $hmacSha256 = hash_hmac("sha256", $gebruiker_id . $date . $postcode_afzender . $postcode, $apiKey);

            if ($rekeningnummer != '') {
              $data['Rekeningnummer'] = $rekeningnummer;
            }

            // order information
            $order_total = $order_info['total'];
            $gewicht = $order_info['su_weight'];

            // shipping information
            $handtekening_bij_aflevering = false;
            $niet_leveren_bij_de_buren = false;
            $directe_avond_levering = false;
            $zaterdag_levering = false;
            $extrazeker = false;
            $rembours = false;
            $verzekerd_bedrag = false;
            $aantal_pakketten = $order_info['su_colli'];

            //PostNL, Pakket + rembours
            if ($order_shipping_type == 'parcel_pro_type_id_2') {
              $rembours = $order_total;
            }

            //PostNL, Pakket + verzekerd bedrag
            if ($order_shipping_type == 'parcel_pro_type_id_3') {
              $verzekerd_bedrag = false;
            }

            //PostNL, Pakket + handtekening voor ontvangst
            if ($order_shipping_type == 'parcel_pro_type_id_4') {

            }

            //PostNL, Rembours + Verhoogd aansprakelijk
            if ($order_shipping_type == 'parcel_pro_type_id_195') {
              $rembours = $order_total;
              $verzekerd_bedrag = false;
            }

            //DHL, DFY
            if ($order_shipping_type == 'parcel_pro_type_id_DFY') {
              if (isset($order_shipping_rules['handtekening'])) {
                if ($order_shipping_rules['handtekening'] == '1') {
                  $handtekening_bij_aflevering = true;
                }
              }

              if (isset($order_shipping_rules['nietbijburen'])) {
                if ($order_shipping_rules['nietbijburen'] == '1') {
                  $niet_leveren_bij_de_buren = true;
                }
              }

              if (isset($order_shipping_rules['avond'])) {
                if ($order_shipping_rules['avond'] == '1') {
                  $directe_avond_levering = true;
                }
              }
              if (isset($order_shipping_rules['extrazeker'])) {
                if ($order_shipping_rules['extrazeker'] == '1') {
                  $extrazeker = true;
                }
              }
            }
            //DHL, EUROPLUS
            if ($order_shipping_type == 'parcel_pro_type_id_Europlus') {
              if (isset($order_shipping_rules['handtekening'])) {
                if ($order_shipping_rules['handtekening'] == '1') {
                  $handtekening_bij_aflevering = true;
                }
              }

              if (isset($order_shipping_rules['nietbijburen'])) {
                if ($order_shipping_rules['nietbijburen'] == '1') {
                  $niet_leveren_bij_de_buren = true;
                }
              }

              if (isset($order_shipping_rules['avond'])) {
                if ($order_shipping_rules['avond'] == '1') {
                  $directe_avond_levering = true;
                }
              }
              if (isset($order_shipping_rules['extrazeker'])) {
                if ($order_shipping_rules['extrazeker'] == '1') {
                  $extrazeker = true;
                }
              }
            }

            //DPD, Rembours
            if ($order_shipping_type == 'parcel_pro_type_id_28') {
              $rembours = $order_total;
            }

            //DPD, Verzekerd
            if ($order_shipping_type == 'parcel_pro_type_id_35') {
              $verzekerd_bedrag = '';
            }

            //DPD, Verzekerd, rembours
            if ($order_shipping_type == 'parcel_pro_type_id_36') {
              $rembours = $order_total;
              $verzekerd_bedrag = '';
            }

            //'DPD, DPD 12:00, rembours';
            if ($order_shipping_type == 'parcel_pro_type_id_41') {
              $rembours = $order_total;
            }


            //DPD, 8:30, rembours';
            if ($order_shipping_type == 'parcel_pro_type_id_43') {
              $rembours = $order_total;
            }

            // Create data array
            $data['GebruikerId'] = $gebruiker_id;
            $data['Datum'] = $date;

            $data['Carrier'] = $carrier;

            $data['Type'] = $type;


            if ($handtekening_bij_aflevering)
            $data['HandtekeningBijAflevering'] = '1';

            if ($niet_leveren_bij_de_buren)
            $data['NietLeverenBijDeBuren'] = '1';

            if ($directe_avond_levering)
            $data['DirecteAvondlevering'] = '1';

            if ($zaterdag_levering)
            $data['Zaterdaglevering'] = '1';

            if ($extrazeker)
            $data['Extrazeker'] = '1';

            if ($rembours) {
              $data['Rembours'] = $rembours;
            }

            if ($verzekerd_bedrag) {
              $data['VerzekerdBedrag'] = $verzekerd_bedrag;
            }

            $data['order_id'] = $order_info['order_id'];
            $data['payment_firstname'] = $order_info['payment_firstname'];
            $data['payment_lastname'] = $order_info['payment_lastname'];
            $data['payment_company'] = $order_info['payment_company'];
            $data['payment_address_1'] = $order_info['payment_address1'];
            $data['payment_address_2'] = $order_info['payment_address2'];
            $data['payment_postcode'] = $order_info['payment_postcode'];
            $data['payment_city'] = $order_info['payment_city'];
            $data['shipping_firstname'] = $order_info['shipping_firstname'];
            $data['shipping_lastname'] = $order_info['shipping_lastname'];

            $data['shipping_company'] = $order_info['shipping_company'];
            if($order_shipping_type == 'parcel_pro_type_id_3533' || $order_shipping_type == 'parcel_pro_type_id_Parcelshop'){
              $data['shipping_company'] = $order_info['shipping_company'];
            }
            $data['shipping_address_1'] = $order_info['shipping_address_1'];
            $data['shipping_address_2'] = $order_info['shipping_address_2'];
            $data['shipping_postcode'] = $order_info['shipping_postcode'];
            $data['shipping_city'] = $order_info['shipping_city'];
            $data['shipping_country'] = $order_info['shipping_country'];
            $data['shipping_iso_code_2'] = $order_info['shipping_country_iso_2'];
            $data['shipping_code'] = $order_info['shipping_code'];
            $data['shipping_method'] = $order_shipping_type;

            $data['email'] = $order_info['email'];
            $data['telephone'] = $order_info['telephone'];
            $data['date_added'] = $order_info['date_added'];

            //Huisnummer
            if(empty($data['shipping_address_2']) || ctype_alpha($data['shipping_address_2'])){
               preg_match("/^(\\D{1,})[\\s\\r\\n]{1,}(\\d{1,}[\\s\\S]*)$/", $data['shipping_address_1'], $matches);
               if(!empty($matches[2])){
                 $data['nummer'] = $matches[2];
                 $data['toevoeging'] = $data['shipping_address_2'];
               }
            }else{
              $data['nummer'] = $data['shipping_address_2'];
            }

            $data['Naam'] = $naam;

            if ($tav) {
              $data['Tav'] = $tav;
            }

            $data['Straat'] = $straat;

            if ($nummer) {
              $data['Nummer'] = $nummer;
            }

            $data['Postcode'] = $postcode;
            $data['Plaats'] = $plaats;

            if ($land) {
              $data['Land'] = $land;
            }

            if ($email) {
              $data['Email'] = $email;
            }

            if ($telefoonnummer) {
              $data['Telefoonnummer'] = $telefoonnummer;
            }

            $data['AantalPakketten'] = $aantal_pakketten;
            $data['Gewicht'] = $gewicht;

            if ($opmerking) {
              $data['Opmerking'] = $opmerking;
            }

            $data['HmacSha256'] = $hmacSha256;
            $data['API'] = $apiKey;

            //Check before submitting
            if(
            !empty($data['shipping_firstname'])
            && !empty($data['shipping_lastname'])
            && !empty($data['shipping_address_1'])
            && !empty($data['nummer'])
            && !empty($data['shipping_postcode'])
            && !empty($data['shipping_city'])
            && !empty($data['shipping_country'])

            ){
              if(empty($order_info['su_order_id'])){
                $submit_result = $this->submitShipping($data);
              }else{
                array_push($errorMessages, $this->language->get('text_already_submitted'). $order_referentie);
                $submit_result = null;
              }
            }else{
              array_push($errorMessages, $this->language->get('text_missing_data') . $order_referentie);
              $submit_result = null;
            }


            if (!$submit_result) {
              $count_wrong++;
              $error_order = true;
            } else {
              if (isset($submit_result['level'])) {
                if ($submit_result['level'] = 'error') {
                  if (isset($this->session->data['warning'])) {
                    $this->session->data['warning'] .= '<br / >Parcel Pro error for order: ' . $order_referentie . ' | ErrorCode: ' . $submit_result['code'] . ' | Omschrijving: ' . $submit_result['omschrijving'];
                  } else {
                    $this->session->data['warning'] = 'Parcel Pro error for order: ' . $order_referentie . ' | ErrorCode: ' . $submit_result['code'] . ' | Omschrijving: ' . $submit_result['omschrijving'];
                  }

                  $count_wrong++;
                  $error_order = true;
                }
              }
            }
          }

          if (!$error_order) {

            $saving_data = array(
              'su_order_id' => $submit_result['Id'],
              'su_url_tracking' => $submit_result['TrackingUrl'],
              'su_url_label' => $submit_result['LabelUrl'],
              'su_barcode' => $submit_result['Barcode'],
              'su_barcodes' => '',
              'su_colli' => $aantal_pakketten,
            );

            $this->model_sale_pp_order->saveSuData($order_id, $saving_data);

            $count_right++;
          }
        }
      }

      if ($count_right) {
        if (isset($this->session->data['success'])) {
          $this->session->data['success'] .= '<br / >' . $this->language->get('text_count_right') . $count_right;
          ;
        } else {
          $this->session->data['success'] = $this->language->get('text_count_right') . $count_right;
          ;
        }
      }

      if ($count_wrong) {
        if (isset($this->session->data['warning'])) {
          $this->session->data['warning'] .= '<br / >' . $this->language->get('text_count_wrong') . $count_wrong;
        } else {
          $this->session->data['warning'] = $this->language->get('text_count_wrong') . $count_wrong;
        }
        for($i = 0; $i < count($errorMessages); $i++){
          $this->session->data['warning'] .= '<br / >' . $errorMessages[$i];
        }
      }
    } elseif (!isset($this->request->post['selected']) && ($this->validateSubmitSu())) {
      $this->session->data['warning'] = $this->language->get('error_nothing_selected');
    }

    $url = '';

    if (isset($this->request->get['filter_order_id'])) {
      $url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
    }

    if (isset($this->request->get['filter_su_order_id'])) {
      $url .= '&filter_su_order_id=' . $this->request->get['filter_su_order_id'];
    }

    if (isset($this->request->get['filter_customer'])) {
      $url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
    }

    if (isset($this->request->get['filter_order_status_id'])) {
      $url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
    }

    if (isset($this->request->get['filter_order_date_added'])) {
      $url .= '&filter_order_date_added=' . $this->request->get['filter_order_date_added'];
    }

    if (isset($this->request->get['filter_su_date_added'])) {
      $url .= '&filter_su_date_added=' . $this->request->get['filter_su_date_added'];
    }

    if (isset($this->request->get['sort'])) {
      $url .= '&sort=' . $this->request->get['sort'];
    }

    if (isset($this->request->get['order'])) {
      $url .= '&order=' . $this->request->get['order'];
    }

    if (isset($this->request->get['page'])) {
      $url .= '&page=' . $this->request->get['page'];
    }

    $this->response->redirect($this->url->link('sale/pp_order', 'token=' . $this->session->data['token'] . $url, 'SSL'));
  }

  protected function submitShipping($data) {
    $url = $this->config->get('parcelpro_Webhook');
    $url = 'http://login.parcelpro.nl/api/opencart/order-created.php';
    $gebruiker_id = $this->config->get('parcelpro_Id');
    $apiKey = $this->config->get('parcelpro_ApiKey');

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json',
      sprintf('X-Oc-Webhook-Accountid: %s', $gebruiker_id),
      sprintf('X-Oc-Webhook-Signature: %s', hash_hmac("sha256", json_encode($data), $apiKey)),
      sprintf('X-Oc-Webhook-Referer: %s', $_SERVER['HTTP_HOST']),
    ));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($curl, CURLOPT_VERBOSE, 1);
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

    $response = curl_exec($curl);
    curl_close($curl);

    if ($response) {
      $result = json_decode($response, true);
    } else {
      $result = array();
    }

    return $result;
  }

  protected function getShippingMethods() {
    $gebruiker_id = $this->config->get('parcelpro_Id');
    $apiKey = $this->config->get('parcelpro_ApiKey');
    $date = date('Y\-m\-d G:i:s');

    $hmacSha256 = hash_hmac("sha256", $gebruiker_id . $date, $apiKey);
    $url = 'http://login.parcelpro.nl/api/type.php';

    $data = array(
      'GebruikerId' => $gebruiker_id,
      'Datum' => $date,
      'HmacSha256' => hash_hmac("sha256", $gebruiker_id . $date, $apiKey),
    );

    $data = http_build_query($data);
    $data = str_replace('+', '%20', $data);
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_VERBOSE, 1);

    $response = curl_exec($curl);

    curl_close($curl);

    if ($response) {
      $result = json_decode($response, true);
    } else {
      $result = array();
    }
    return $result;
  }

  public function label() {

    $this->language->load('sale/pp_order');
    $this->load->model('sale/pp_order');
    $label_queue = array();
    $order_ids = array();

    if ((isset($this->request->post['selected']) or isset($this->request->get['order_id'])) && ($this->validateSubmitSu())) {
      if (isset($this->request->get['order_id']) and ! isset($this->request->post['selected'])) {
        $this->request->post['selected'][] = $this->request->get['order_id'];
      }


      foreach ($this->request->post['selected'] as $order_id) {
        $order_info = $this->model_sale_pp_order->getOrder($order_id);

        if ($order_info) {
          $label_queue[] = $order_info['su_order_id'];
          $order_ids[] = $order_id;
        }
      }

      if (!empty($label_queue)) {
        $label_url = $this->getLabelUrl($label_queue);

        if ($label_url) {
          foreach ($order_ids as $order_id) {
            $this->model_sale_pp_order->disableLabelPrint($order_id);
          }

          header('Location: ' . $label_url);
        } else {
          $this->session->data['warning'] = $this->language->get('error_action');
          $this->response->redirect($this->url->link('sale/pp_order', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }
      }
    } else {
      $this->session->data['warning'] = $this->language->get('error_nothing_selected') . $count_wrong;
      $this->response->redirect($this->url->link('sale/pp_order', 'token=' . $this->session->data['token'] . $url, 'SSL'));
    }
  }

  protected function getLabelUrl($label_queue = array()) {
    $su_order_id = $label_queue['0'];
    if (is_null($su_order_id)) {
      return null;
    }
    $gebruiker_id = $this->config->get('parcelpro_Id');
    $apiKey = $this->config->get('parcelpro_ApiKey');
    $hmacSha256 = hash_hmac("sha256", $gebruiker_id . $su_order_id, $apiKey);
    $data = array(
      'GebruikerId' => $gebruiker_id,
      'ZendingId' => $su_order_id,
      'HmacSha256' => $hmacSha256
    );

    if (count($label_queue) >= 2) {
      $count = 0;

      foreach ($label_queue as $label) {
        $data['selected[' . $count . ']'] = $label;

        $count++;
      }
    }

    $queryData = http_build_query($data);

    $label_url = $this->_url . 'http://login.parcelpro.nl/api/label.php?' . $queryData;

    return $label_url;
  }

  protected function validateSubmitSu() {
    if (!$this->user->hasPermission('modify', 'sale/order')) {
      $this->session->data['warning'] = $this->language->get('error_permission');
    }

    if (!$this->error) {
      return true;
    } else {
      return false;
    }
  }

  public function updateColli() {
    if (!empty($this->request->post['colli'])) {
      $this->load->model('sale/pp_order');

      foreach ($this->request->post['colli'] as $key => $value) {
        $this->model_sale_pp_order->updateColli($key, $value);
      }
    }

    $url = '';

    if (isset($this->request->get['filter_order_id'])) {
      $url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
    }

    if (isset($this->request->get['filter_su_order_id'])) {
      $url .= '&filter_su_order_id=' . $this->request->get['filter_su_order_id'];
    }

    if (isset($this->request->get['filter_customer'])) {
      $url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
    }

    if (isset($this->request->get['filter_order_status_id'])) {
      $url .= '&filter_order_status_id=' . $this->request->get['filter_order_status_id'];
    }

    if (isset($this->request->get['filter_order_date_added'])) {
      $url .= '&filter_order_date_added=' . $this->request->get['filter_order_date_added'];
    }

    if (isset($this->request->get['filter_su_date_added'])) {
      $url .= '&filter_su_date_added=' . $this->request->get['filter_su_date_added'];
    }

    if (isset($this->request->get['sort'])) {
      $url .= '&sort=' . $this->request->get['sort'];
    }

    if (isset($this->request->get['order'])) {
      $url .= '&order=' . $this->request->get['order'];
    }

    if (isset($this->request->get['page'])) {
      $url .= '&page=' . $this->request->get['page'];
    }

    $this->response->redirect($this->url->link('sale/pp_order', 'token=' . $this->session->data['token'] . $url, 'SSL'));
  }

  private function checkdb() {
    $table = '`' . DB_PREFIX . 'order`';
    $column = '`su_date_added`';
    $sql = "DESC $table $column";

    $query = $this->db->query($sql);

    if (!$query->num_rows) {
      $sql = "alter table $table add column $column datetime NOT NULL after `date_modified`";

      $this->db->query($sql);
    }

    $table = '`' . DB_PREFIX . 'order`';
    $column = '`su_weight`';
    $sql = "DESC $table $column";

    $query = $this->db->query($sql);

    if (!$query->num_rows) {
      $sql = "alter table $table add column $column varchar(16) NULL after `date_modified`";

      $this->db->query($sql);
    }

    $table = '`' . DB_PREFIX . 'order`';
    $column = '`su_colli`';
    $sql = "DESC $table $column";

    $query = $this->db->query($sql);

    if (!$query->num_rows) {
      $sql = "alter table $table add column $column int(11) NOT NULL DEFAULT '1' after `date_modified`";

      $this->db->query($sql);
    }

    $table = '`' . DB_PREFIX . 'order`';
    $column = '`su_barcodes`';
    $sql = "DESC $table $column";

    $query = $this->db->query($sql);

    if (!$query->num_rows) {
      $sql = "alter table $table add column $column text NOT NULL after `date_modified`";

      $this->db->query($sql);
    }

    $table = '`' . DB_PREFIX . 'order`';
    $column = '`su_barcode`';
    $sql = "DESC $table $column";

    $query = $this->db->query($sql);

    if (!$query->num_rows) {
      $sql = "alter table $table add column $column varchar(128) NOT NULL after `date_modified`";

      $this->db->query($sql);
    }

    $table = '`' . DB_PREFIX . 'order`';
    $column = '`su_url_label`';
    $sql = "DESC $table $column";

    $query = $this->db->query($sql);

    if (!$query->num_rows) {
      $sql = "alter table $table add column $column varchar(255) NOT NULL after `date_modified`";

      $this->db->query($sql);
    }

    $table = '`' . DB_PREFIX . 'order`';
    $column = '`su_label_printed`';
    $sql = "DESC $table $column";

    $query = $this->db->query($sql);

    if (!$query->num_rows) {
      $sql = "alter table $table add column $column varchar(255) NOT NULL after `date_modified`";

      $this->db->query($sql);
    }

    $table = '`' . DB_PREFIX . 'order`';
    $column = '`su_url_tracking`';
    $sql = "DESC $table $column";

    $query = $this->db->query($sql);

    if (!$query->num_rows) {
      $sql = "alter table $table add column $column  varchar(255) NOT NULL after `date_modified`";

      $this->db->query($sql);
    }

    $table = '`' . DB_PREFIX . 'order`';
    $column = '`su_order_id`';
    $sql = "DESC $table $column";

    $query = $this->db->query($sql);

    if (!$query->num_rows) {
      $sql = "alter table $table add column $column int(11) NULL after `date_modified`";

      $this->db->query($sql);
    }
  }

}

?>
