<?php
// Heading
$_['heading_title']           = 'Country & Zone Editor';

// Text
$_['text_success']            = 'Success: You have modified module settings!';
$_['text_success_country']    = 'Success: You have modified countries!';
$_['text_success_zone']       = 'Success: You have modified zones!';
$_['text_module']             = 'Modules';
$_['text_edit']               = 'Edit Settings';
$_['text_country_list']       = 'Country List';
$_['text_zone_list']          = 'Zone List';
$_['text_country']            = 'Country';
$_['text_add_country']        = 'Add Country';
$_['text_edit_country']       = 'Edit Country';
$_['text_add_zone']           = 'Add Zone';
$_['text_edit_zone']          = 'Edit Zone';

$_['text_name_asc']           = 'Name (A - Z)';
$_['text_name_desc']          = 'Name (Z - A)';
$_['text_sort_order_asc']     = 'Sort Order (Low &gt; High) Ex. 0 1 2 3 4 5';
$_['text_sort_order_desc']    = 'Sort Order (High &gt; Low) Ex. 5 4 3 2 1 0';
$_['text_sort_order_zero']    = 'Sort Order (Low &gt; High, zero value is ignored and is shown last) Ex. 1 2 3 4 5 0';

$_['text_load_message']       = 'Getting messages';
$_['text_no_message']         = 'No new messages';
$_['text_retry']              = 'Retry';

$_['text_url_note']           = 'Leave empty to activate the extension for this site.';
$_['text_install_module']     = 'Install Country & Zone Editor';

// Column
$_['column_country_id']       = 'Country ID';
$_['column_zone_id']          = 'Zone ID';
$_['column_country_name']     = 'Country Name';
$_['column_zone_name']        = 'Zone Name';
$_['column_name_multi']       = 'Multilingual';
$_['column_code']             = 'Zone Code';
$_['column_country']          = 'Country';
$_['column_iso_code_2']       = 'ISO Code (2)';
$_['column_iso_code_3']       = 'ISO Code (3)';
$_['column_sort_order']       = 'Sort Order';
$_['column_postcode']         = 'Postcode Required';
$_['column_status']           = 'Status';
$_['column_action']           = 'Action';

// Entry
$_['entry_country_multilang'] = 'Multilingual Countries';
$_['entry_zone_multilang']    = 'Multilingual Zones';
$_['entry_sort_country']      = 'Sort Countries By';
$_['entry_sort_zone']         = 'Sort Zones By';
$_['entry_country_id']        = 'Country ID';
$_['entry_zone_id']           = 'Zone ID';
$_['entry_country_dname']     = 'Default Country Name';
$_['entry_zone_dname']        = 'Default Zone Name';
$_['entry_country_name']      = 'Country Name';
$_['entry_zone_name']         = 'Zone Name';
$_['entry_country']           = 'Country';
$_['entry_code']              = 'Zone Code';
$_['entry_iso_code_2']        = 'ISO Code (2)';
$_['entry_iso_code_3']        = 'ISO Code (3)';
$_['entry_address_format']    = 'Address Format';
$_['entry_sort_order']        = 'Sort Order';
$_['entry_postcode_required'] = 'Postcode Required';
$_['entry_status']            = 'Status';

$_['entry_activation']        = 'Extension Activation';
$_['entry_order_id']          = 'Order ID';
$_['entry_url']               = 'Site URL';

// Button
$_['button_add']              = 'Add New';
$_['button_edit']             = 'Edit';
$_['button_apply']            = 'Apply';
$_['button_country']          = 'Countries';
$_['button_zone']             = 'Zones';
$_['button_setting']          = 'Module Settings';
$_['button_clear']            = 'Clear';
$_['button_clear_cache']      = 'Clear Cache';

// Tab
$_['tab_general']             = 'General';
$_['tab_developer']           = 'Support';

// Help
$_['help_default_name']       = 'If the option \'Multilingual Countries\' is disabled used this name value.';
$_['help_name']               = 'If the option \'Multilingual Countries\' is enabled used this name values.';
$_['help_default_zone_name']  = 'If the option \'Multilingual Zones\' is disabled used this name value.';
$_['help_zone_name']          = 'If the option \'Multilingual Zones\' is enabled used this name values.';
$_['help_address_format']     = 'First Name = {firstname}<br />Last Name = {lastname}<br />Company = {company}<br />Address 1 = {address_1}<br />Address 2 = {address_2}<br />City = {city}<br />Postcode = {postcode}<br />Zone = {zone}<br />Zone Code = {zone_code}<br />Country = {country}';
$_['help_activation']         = 'You can activate as many times as unit of extension you have purchased in your order, after successful activation for one unit from the order will be assigned to the specified site url, and more this information cannot be changed!';

$_['help_order_id']           = 'Your order number from openсart.com or vanstudio.co.ua, you can see order number in your account (orders list) or in email message after purchase extension';
$_['help_url']                = 'Not required field, used only to activate the extension. Main URL of site where you want used extension, if you use multi-store then set URL for default store, for example http://your-site-name.com';

// Error
$_['error_permission']                = 'Warning: You do not have permission to modify module \'Country & Zone Editor\'!';
$_['error_country_name']              = 'Country Name must be between 3 and 128 characters!';
$_['error_zone_name']                 = 'Zone Name must be between 3 and 128 characters!';
$_['error_country_default']           = 'Warning: This country cannot be deleted as it is currently assigned as the default store country!';
$_['error_country_store']             = 'Warning: This country cannot be deleted as it is currently assigned to %s stores!';
$_['error_country_address']           = 'Warning: This country cannot be deleted as it is currently assigned to %s address book entries!';
$_['error_country_affiliate']         = 'Warning: This country cannot be deleted as it is currently assigned to %s affiliates!';
$_['error_country_zone']              = 'Warning: This country cannot be deleted as it is currently assigned to %s zones!';
$_['error_country_zone_to_geo_zone']  = 'Warning: This country cannot be deleted as it is currently assigned to %s zones to geo zones!';
$_['error_zone_default']              = 'Warning: This zone cannot be deleted as it is currently assigned as the default store zone!';
$_['error_zone_store']                = 'Warning: This zone cannot be deleted as it is currently assigned to %s stores!';
$_['error_zone_address']              = 'Warning: This zone cannot be deleted as it is currently assigned to %s address book entries!';
$_['error_zone_affiliate']            = 'Warning: This zone cannot be deleted as it is currently assigned to %s affiliates!';
$_['error_zone_zone_to_geo_zone']     = 'Warning: This zone cannot be deleted as it is currently assigned to %s zones to geo zones!';
$_['error_message']                   = 'Could not load messages from vanstudio.co.ua';