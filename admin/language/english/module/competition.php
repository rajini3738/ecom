<?php
// Heading
$_['heading_title']          = 'Competition Module';

// Text
$_['text_module']            = 'Module';
$_['text_success']           = 'Success: You have modified module competition!';
$_['text_default']           = 'Default';
$_['text_edit']        		 = 'Edit Compeitition Module';


// Entry
$_['entry_status']          = 'Status';
$_['entry_width']           = 'Image Width';
$_['entry_height']          = 'Image Height';
$_['entry_name']			= 'Name';

// Button

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify module competition!';
$_['error_width']      = 'Width required!';
$_['error_height']     = 'Height required!';
?>
