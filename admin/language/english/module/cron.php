<?php
// Heading
$_['heading_title']    = 'CRON (Automatic Start Scripts)';

// Text
$_['text_success']     = 'Success: You have modified CRON!';

// Column
$_['column_file']      = 'Path / Filename';
$_['column_datetime']  = 'Datetime';
$_['column_every']     = 'Every';
$_['column_status']    = 'Status';
$_['column_action']    = 'Action';

// Entry
$_['entry_file']       = 'Fullpath to PHP script:<br /><span class="help">system/helper/bulkmail_cron.php</span>';
$_['entry_every']      = 'Interval (every):<br /><span class="help">Value 900 = 15min.</span>';
$_['entry_status']     = 'Status:';

// Error
$_['error_permission'] = 'Warning: Nemáte oprávnění na změnu CRONu!';

?>