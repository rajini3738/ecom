<?php
// Heading
$_['heading_title']       = 'jOS Send SMS';

// Text
$_['text_module']          = 'Modules';
$_['text_success_sent']    = 'Success: Message has been sent!';
$_['text_error']        	 = 'Failed: Message not sent! (error message: ';
$_['text_none']      			 = '-- Select Destination --';
$_['text_nohp']      			 = 'Input Number';
$_['text_newsletter']      = 'All Newsletter Subscribers';
$_['text_customer_all']    = 'All Customers';
$_['text_customer_group']  = 'Customer Group';
$_['text_customer']        = 'Customers';
$_['text_affiliate_all']   = 'All Affiliates';
$_['text_affiliate']       = 'Affiliates';
$_['text_product']         = 'Products';

// Entry
$_['entry_nohp']    			 = '<span title="Send to one destination number.<br />Input mobile number with Country code." data-toggle="tooltip">Mobile Number</span>';
$_['entry_message']    		 = '<span title="Accept variable: <i>{storename}{order_no}</i>" data-toggle="tooltip">Message</span>';
$_['entry_or']    				 = '<span title="Select message destination by category or group. You can also input single number destination." data-toggle="tooltip">To</span>';
$_['entry_customer_group'] = 'Customer Group:';
$_['entry_customer']       = '<span title="Autocomplete" data-toggle="tooltip">Customer</span>';
$_['entry_affiliate']      = '<span title="Autocomplete" data-toggle="tooltip">Affiliate</span>';
$_['entry_product']        = '<span title="Send only to customers who have ordered products in the list. (Autocomplete)" data-toggle="tooltip">Products</span>';

// Button
$_['button_send']					 = 'Send Message';
$_['button_setting']			 = 'jOS SMS Setting';

// Error
$_['error_permission']     = 'Warning: You do not have permission to modify module jOS SMS!';
$_['error_nohp']       		 = 'Destination number required';
$_['error_message']        = 'Text message required';
$_['error_gateway_null']   = 'Gateway not found! Please set the default gateway';

?>