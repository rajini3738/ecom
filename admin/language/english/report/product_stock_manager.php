<?php
// Heading
$_['heading_title']			= 'Products Stock Manager Report';

$_['text_all_status']		= 'All Statuses';
$_['text_list']				= 'Products Stock Manager List';

// Column
$_['column_date_start']		= 'Date Start';
$_['column_date_end']		= 'Date End';
$_['column_name']			= 'Product Name';
$_['column_model']			= 'Category';
$_['column_quantity']		= 'Quantity Sold';
$_['column_leftquantity']   = 'Quantity Left';
$_['column_total']			= 'Total';

// Entry
$_['entry_date_start']		= 'Date Start';
$_['entry_date_end']		= 'Date End';
$_['entry_status']			= 'Order Status';
$_['entry_name']			= 'Product Name';