﻿<?php
// Heading
$_['heading_title']       = 'Complaint';

// Text
$_['text_success']        = 'Success: You have modified complaint!';
$_['text_list']           = 'Complaint List';
$_['text_add']            = 'Add Complaint';
$_['text_edit']           = 'Edit Complaint';
$_['text_opened']         = 'Opened';
$_['text_unopened']       = 'Closed';
$_['text_order']          = 'Order Information';
$_['text_product']        = 'Product Information &amp; Reason for Complaint';
$_['text_history']        = 'Add Return History';

// Column
$_['column_complaint_id']     = 'Complaint ID';
$_['column_order_id']      = 'Order ID';
$_['column_customer']      = 'Customer';
$_['column_product']       = 'Product';
$_['column_model']         = 'Model';
$_['column_status']        = 'Status';
$_['column_date_added']    = 'Date Added';
$_['column_date_modified'] = 'Date Modified';
$_['column_comment']       = 'Comment';
$_['column_notify']        = 'Customer Notified';
$_['column_action']        = 'Action';

// Entry
$_['entry_customer']      = 'Customer';
$_['entry_order_id']      = 'Order ID';
$_['entry_date_ordered']  = 'Order Date';
$_['entry_firstname']     = 'First Name';
$_['entry_lastname']      = 'Last Name';
$_['entry_middlename']    = 'Middle Name';
$_['entry_email']         = 'E-Mail';
$_['entry_telephone']     = 'Telephone';
$_['entry_product']       = 'Product';
$_['entry_model']         = 'Category';
$_['entry_opened']        = 'Opened';
$_['entry_comment']       = 'Comment';
$_['entry_notify']        = 'Notify Customer';
$_['entry_complaint_id']     = 'Complaint ID';
$_['entry_date_added']    = 'Date Added';
$_['entry_date_modified'] = 'Date Modified';
$_['entry_reason']       = 'Complaint Type';

// Help
$_['help_product']        = '(Autocomplete)';

// Error
$_['error_warning']       = 'Warning: Please check the form carefully for errors!';
$_['error_permission']    = 'Warning: You do not have permission to modify returns!';
$_['error_order_id']      = 'Order ID required!';
$_['error_firstname']     = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']      = 'Last Name must be between 1 and 32 characters!';
$_['error_email']         = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']     = 'Telephone must be between 3 and 32 characters!';
$_['error_product']       = 'Product Name must be greater than 3 and less than 255 characters!';
$_['error_model']         = 'Product Model must be greater than 3 and less than 64 characters!';
$_['error_reason']       = 'Please select complaint type';