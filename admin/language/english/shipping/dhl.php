<?php
// Heading
$_['heading_title']    		= 'DHL';

// Text
$_['text_shipping']    		= 'Shipping';
$_['text_edit']        		= 'Edit';
$_['text_success']     		= 'Success: You have modified DHL!';

// Entry
$_['entry_rate']      		= 'Rates:<br /><span class="help">Example: 5:10.00,7:12.00 Weight:Cost,Weight:Cost, etc..</span>';
$_['entry_tax_class']  		= 'Tax Class:';
$_['entry_geo_zone']   		= 'Geo Zone:';
$_['entry_status']     		= 'Status:';
$_['entry_sort_order'] 		= 'Sort Order:';
$_['entry_country']    		= 'Country';

//Error
$_['error_id']              = 'Warning: You do not have permission to modify Fedex shipping!';
$_['error_site_pwd']        = 'Key required!';
$_['error_country']         = 'Password required!';
$_['error_city']            = 'Account required!';
$_['error_geo_zone_id']     = 'Meter required!';
$_['error_postcode']        = 'Post Code required!';