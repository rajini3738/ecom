<?php
// Heading
$_['heading_title']    = 'Dutch DHL4You Shipping';

// Text
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified Dutch DHL4You shipping!';
$_['text_edit']        = 'Edit Free Shipping';

// Entry
$_['entry_small_cost'] = 'Small Parcel Netherlands:';
$_['entry_large_cost'] = 'Large Parcel Netherlands:';
$_['text_help_small']  = 'Max box size 380x260x32 mm and 10 Kilogram';
$_['text_help_large']  = 'Max box size 800x400x600 mm and 10 Kilogram';
$_['entry_use_freeshipping'] = 'Is Free Shipping enabled?';
$_['entry_tax_class']  = 'Tax Class';
$_['entry_geo_zone']   = 'Geo Zone';
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

$_['entry_germany_cost'] = 'Parcel to Germany';
$_['entry_westeurope_cost']  = 'Parcel to Western Europe';
$_['entry_europe_cost']  = 'Parcel to Rest of Europe';
$_['entry_world_cost']   = 'Parcel to Rest of World';

$_['text_help_germany']  = 'Max box size 800x400x400 mm and 10 Kilogram';
$_['text_help_europe']   = 'Max box size 800x400x400 mm and 10 Kilogram';
$_['text_help_world']    = 'Max box size 800x400x400 mm and 10 Kilogram';

// Help
$_['help_use_freeshipping'] = 'Do not activate this unless Free Shipping is enabled!!';

// Countries
$_['text_countries_westeurope']  = 'Zone 2 - Verzending mogelijk naar:</br>
									&Aring;land*, Belearen*, Belgi&euml;, Campione d\'Italia*, Ceuta*, Denemarken, Finland, Frankrijk,</br>
									Groot Britanni&euml;, Ierland, Itali&euml;, Kanaal Eilanden*, Luxemburg, Noord Ierland,</br>
									Oostenrijk, Polen, Portugal, Spanje, Zweden.</br></br>';
$_['text_countries_europe']  = 'Zone 3 - Verzending mogelijk naar:</br>
									Albani&euml;*, Andorra*, Belarus*, Bosni&euml; en Herzegovina*, Bulgarije*, Canarische Eilanden*,</br>
									Corsica*, Cyprus, Estland, Gibraltar*, Griekenland, Groenland*, Hongarije, Kosovo*, Kroati&euml;*,</br>
									Letland, Liechtenstein<*, Litouwen, Macedoni&euml;*, Madeira*, Malta, Mellila*, Moldavi&euml;*, Monaco*,</br>
									Montenegro*, Noorwegen*, Roemeni&euml;, San Marino*, Servi&euml;*, Sloveni&euml;, Slowakije,</br>
									Spitsbergen en Jan Mans*, Tjechi&euml;, Turkije*, Vaticaanstad*, Zwitserland*.</br></br>';
$_['text_countries_world']  = 'Zone 4 - Verzending mogelijk naar:</br>
									Alle andere landen<sup style="color:#FF0000">*</sup> niet genoemd.';

$_['text_countries_customs']  = '<span class="help"><sup style="color:#FF0000">*</sup>&nbsp;These countries need Custums Decleration Forms !</span>';

// tabs
$_['tab_netherlands'] = 'Netherlands <img src="view/image/flags/nl.png" align="absmiddle" />';
$_['tab_germany'] 	  = 'Germany <img src="view/image/flags/de.png" align="absmiddle" />';
$_['tab_europe']	  = 'Europe <img src="view/image/flags/eu.png" align="absmiddle" />';
$_['tab_world'] 	  = 'World <img src="view/image/flags/world.png" align="absmiddle" />';



// Error
$_['error_permission'] = 'Warning: You do not have permission to modify DHL4You shipping!';
?>