<?php
// Heading
$_['heading_title']             = 'Newsletter Subscribers';
$_['heading_title_competitions']= 'Competitions';

// Text
$_['text_success']              = 'Success: You have modified newsletters!';
$_['text_list']              	= 'Competition Newsletter Subscriber List';
$_['text_form']         		= 'Competition Newsletter Subscriber Form';
$_['text_edit']         		= 'Edit';
$_['text_confirm']         		= 'Are your Sure?';

// Column
$_['column_name']               = 'Newsletter Name';
$_['column_email']              = 'E-Mail';
$_['column_action']             = 'Action';

// Button
$_['button_insert']             = 'Insert';

// Entry
$_['entry_name']           		= 'Name:';
$_['entry_email']               = 'E-Mail:';

// Error
$_['error_permission']          = 'Warning: You do not have permission to modify newsletters!';
$_['error_exists']              = 'Warning: E-Mail Address is already registered!';
$_['error_name']    	        = 'First Name must be between 1 and 32 characters!';
$_['error_email']               = 'E-Mail Address does not appear to be valid!';
?>