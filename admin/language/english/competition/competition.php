<?php
// Heading
$_['heading_title']    					= 'Competition Entrants';
$_['heading_title_competitions']    	= 'Competitions';
$_['heading_title_edit_competitions']   = 'Edit Competition';
$_['heading_title_insert_competitions'] = 'Insert / Edit Competition';
$_['heading_title_insert']    			= 'Insert / Update';
$_['heading_title_insert_entrant'] 		= 'Insert New Entrant';
$_['heading_title_edit_entrant']   		= 'Edit Existing Entrant';
$_['heading_title_winner']   			= 'Competition Winner';

// Text
$_['text_success_entrants']     	= 'Success: You have modified Competition Entrants!';
$_['text_success_delete_winner']    = 'Success: You have deleted the current winner!';
$_['text_success_competitions']		= 'Success: You have modified Competitions!';
$_['text_email_sent']      			= 'Competition winnner has been emailed.';
$_['text_email_winner']      		= 'Email Winner';
$_['text_winner']      				= 'Competition Winner';
$_['text_view_entrants']      		= 'View Entrants';
$_['text_default']           		= 'Default';
$_['text_image_manager']     		= 'Image Manager';
$_['text_browse']            		= 'Browse Files';
$_['text_clear']             		= 'Clear Image';
$_['text_list']             		= 'Competitions List';
$_['text_confirm']					= 'Are You Sure?';
$_['text_edit']						= 'Edit';
$_['text_winners_list']				= 'Competition Winners List';
$_['text_entrant_form']				= 'Insert / Edit New Entrant Form';

// Winners Email Text
$_['text_email_subject']      		= 'Competition Entry - Update';
$_['text_email_intro']      		= 'Congratulations, you\'re the winner of our %s competition.';
$_['text_email_body']      			= 'Please reply to this email with your name and address and we\'ll arrange delivery of your prize.';
$_['text_email_signoff']      		= 'Kind Regards,';
$_['text_email_end']      			= '%s.';

// Column
$_['column_name']        = 'Name';
$_['column_email']       = 'Email';
$_['column_title']       = 'Competition Name';
$_['column_date_added']  = 'Date Added';
$_['column_end_date']  	 = 'End Date';
$_['column_active']  	 = 'Active?';
$_['column_status']      = 'Status';
$_['column_auto_enabled']= 'Auto Entry';
$_['column_auto_value']  = 'Auto Entry Value';
$_['column_entrants']  	 = 'No. of Entrants';
$_['column_notified']    = 'Winner Notified?';
$_['column_answer']   	 = 'Answer';
$_['column_action']      = 'Action';

// Entry
$_['entry_name']       		 = 'Name';
$_['entry_email']       	 = 'Email';
$_['entry_layout']           = 'Layout:';
$_['entry_position']         = 'Position:';
$_['entry_status']           = 'Status:';
$_['entry_sort_order']       = 'Sort Order:';
$_['entry_title']            = 'Competition Name:';
$_['entry_keyword']          = 'SEO Keyword:';
$_['entry_meta_description'] = 'Meta Tag Description:';
$_['entry_question']      	 = 'Question:';
$_['entry_answer1']      	 = 'Answer A:';
$_['entry_answer2']      	 = 'Answer B:';
$_['entry_answer3']      	 = 'Answer C:';
$_['entry_answer4']      	 = 'Answer D:';
$_['entry_answer_is']      	 = 'Answer is:';
$_['entry_description']      = 'Competition Details:';
$_['entry_store']            = 'Stores:';
$_['entry_end_date']         = 'End Date:';
$_['entry_login']        	 = 'Require Login to store to Enter:';
$_['entry_newsletter']     	 = 'Force Newsletter Subscription?:';
$_['entry_image']            = 'Thubmnail Image:';
$_['entry_image_size']       = 'Thubmnail Image Size:';
$_['entry_winners']      	 = 'Number of Winners:';
$_['entry_auto']      		 = 'Enable Auto Entry?:';
$_['entry_auto_value']   	 = 'Auto Entry Order Value:';
$_['entry_auto_message']   	 = 'Auto Entry Message:';

//Buttons
$_['button_module']          	= 'Module Settings';
$_['button_choose_all']         = 'Select Winner - All Entries';
$_['button_choose_correct']		= 'Select Winner - Correct Entries';
$_['button_save']          		= 'Save Winner';
$_['button_save_competition']   = 'Save';
$_['button_save_entrant']       = 'Save';
$_['button_view_winner']        = 'View Winner';
$_['button_go']       		    = 'Go';
$_['button_delete_incorrect']	= 'Delete Incorrect Entries';
$_['button_insert']          	= 'Insert';


// Error
$_['error_email_exist']= 'Email already exists';
$_['error_email']      = 'Invalid Email Format';
$_['error_permission'] = 'Warning: You do not have permission to modify Competition!';
$_['error_title']      = 'Title required';
$_['error_description']= 'Description Required';
$_['error_end_date']   = 'End Date Required';


?>