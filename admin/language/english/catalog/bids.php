<?php
// Heading
$_['heading_title']     = 'Bids';

// Text
$_['text_success']      = 'Success: You have modified bids!';
$_['text_list']         = 'Bids List';
$_['text_add']          = 'Add Bid';
$_['text_edit']         = 'Edit Bid';

$_['text_selectbid']         = 'Select Bid';
$_['text_selectedbid']         = 'Selected Bid';
$_['text_disapproveallbid']         = 'Disapprove And continue the auction';
$_['text_unapproved']         = 'Bid Disapprove And auction continue';
$_['text_approved']         = 'Selected';

// Column
$_['column_customer']   = 'Customer';
$_['column_bid']        = 'Bid';
$_['column_date_added'] = 'Date Added';
$_['column_action']     = 'Action';
