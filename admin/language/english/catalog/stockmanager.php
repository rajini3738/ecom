<?php
// Heading
$_['heading_title']          = 'Stock Manager';

// Text
$_['text_success']           = 'Success: You have modified stocks!';
$_['text_list']              = 'Stock List';
// Column
$_['column_name']            = 'Product Name';
$_['column_quantity']        = 'Quantity';
$_['column_oquantity']       = 'Original Quantity';
$_['column_aquantity']       = 'Admin Quantity';
$_['column_action']          = 'Action';

// Entry
$_['entry_name']             = 'Product Name';
$_['entry_quantity']         = 'Quantity';
$_['entry_oquantity']        = 'Original Quantity';
$_['entry_aquantity']        = 'Admin Quantity';
$_['entry_minimum']          = 'Minimum Quantity';


// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify products!';
$_['error_name']             = 'Product Name must be greater than 3 and less than 255 characters!';
$_['error_meta_title']       = 'Meta Title must be greater than 3 and less than 255 characters!';
$_['error_model']            = 'Product Model must be greater than 1 and less than 64 characters!';
$_['error_keyword']          = 'SEO keyword already in use!';