<?php
// Heading
$_['heading_title']      = 'Video Gallery Video';

// Text
$_['text_success']       = 'Success: You have modified Video Gallery Video!';
$_['text_default']       = 'Default';
$_['text_image']         = 'Image';
$_['text_viewed']        = 'Viewed';
$_['text_browse']        = 'Browse Files';
$_['text_clear']         = 'Clear Image';
$_['text_list']          = 'Video List';
$_['text_form']          = 'Video Form';

// Column
$_['column_name']        = 'Video Name';
$_['column_image']       = 'Image';
$_['column_description'] = 'Description';
$_['column_source']      = 'Source';
$_['column_sort_order']  = 'Sort Order';
$_['column_action']      = 'Action';

// Entry
$_['entry_name']         = 'Video Name:';
$_['entry_album']        = 'Album:';
$_['entry_keyword']      = 'SEO Keyword:';
$_['entry_image']        = 'Image:';
$_['entry_description']  = 'Description:';
$_['entry_source']       = 'Source:';
$_['entry_sort_order']   = 'Sort Order:';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify Gallery Video!';
$_['error_name']         = 'Video Name must be between 3 and 300 characters!';
$_['error_description']  = 'Video Description must be higher than 3 characters!';
$_['error_product']      = 'Warning: This manufacturer cannot be deleted as it is currently assigned to %s products!';
?>
