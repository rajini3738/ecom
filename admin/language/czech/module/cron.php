<?php
// Heading
$_['heading_title']    = 'CRON (Automatizovane spousteni skriptu)';

// Text
$_['text_success']     = 'Úspěch: Změnili jste CRON!';

// Column
$_['column_file']      = 'Cesta k PHP skriptu';
$_['column_datetime']  = 'Datetime';
$_['column_every']     = 'Interval (sec)';
$_['column_status']    = 'Stav';
$_['column_action']    = 'Akce';

// Entry
$_['entry_file']       = 'Cesta k PHP skriptu:<br /><span class="help">system/helper/bulkmail_cron.php</span>';
$_['entry_every']      = 'Interval spouštění:<br /><span class="help">Hodnota 900 = 15min.</span>';
$_['entry_status']     = 'Stav:';

// Error
$_['error_permission'] = 'Varování: Nemáte oprávnění na změnu CRONu!';

?>