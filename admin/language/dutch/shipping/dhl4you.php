<?php
// Heading
$_['heading_title']    = 'Verzendkosten DHL for You';

// Text
$_['text_shipping']    = 'Verzendmethode';
$_['text_success']     = 'Succes: Instellingen gewijzigd!';
$_['text_edit']        = 'Instellingen DHL for you wijzigen';
$_['text_help_small']  = 'Maximaal formaat 380x260x32 mm en 10 Kilogram';
$_['text_help_large']  = 'Maximaal formaat 800x400x600 mm en 10 Kilogram';
$_['text_help_germany']  = 'Maximaal formaat 800x400x400 mm en 10 Kilogram';
$_['text_help_europe']  = 'Maximaal formaat 800x400x400 mm en 10 Kilogram';
$_['text_help_world']  = 'Maximaal formaat 800x400x400 mm en 10 Kilogram';

// Countries								
$_['text_countries_westeurope']  = 'Zone 2 - Verzending mogelijk naar:</br>
									&Aring;land*, Belearen*, Belgi&euml;, Campione d\'Italia*, Ceuta*, Denemarken, Finland, Frankrijk,</br>
									Groot Britanni&euml;, Ierland, Itali&euml;, Kanaal Eilanden*, Luxemburg, Noord Ierland,</br>
									Oostenrijk, Polen, Portugal, Spanje, Zweden.</br></br>';
$_['text_countries_europe']  = 'Zone 3 - Verzending mogelijk naar:</br>
									Albani&euml;*, Andorra*, Belarus*, Bosni&euml; en Herzegovina*, Bulgarije*, Canarische Eilanden*,</br>
									Corsica*, Cyprus, Estland, Gibraltar*, Griekenland, Groenland*, Hongarije, Kosovo*, Kroati&euml;*,</br>
									Letland, Liechtenstein<*, Litouwen, Macedoni&euml;*, Madeira*, Malta, Mellila*, Moldavi&euml;*, Monaco*,</br>
									Montenegro*, Noorwegen*, Roemeni&euml;, San Marino*, Servi&euml;*, Sloveni&euml;, Slowakije,</br>
									Spitsbergen en Jan Mans*, Tjechi&euml;, Turkije*, Vaticaanstad*, Zwitserland*.</br></br>';
$_['text_countries_world']  = 'Zone 4 - Verzending mogelijk naar:</br>
									Alle andere landen<sup style="color:#FF0000">*</sup> niet genoemd.';

$_['text_countries_customs']  = '<sup style="color:#FF0000">*</sup>&nbsp; Voor verzending naar dit land is een douaneformulier vereist !';

// Entry
$_['entry_small_cost'] = 'Klein Pakket Nederland';
$_['entry_large_cost'] = 'Groot Pakket Nederland';
$_['entry_germany_cost'] = 'Pakket Duitsland';
$_['entry_westeurope_cost']  = 'Pakket West Europe';
$_['entry_europe_cost'] = 'Pakket rest van Europa';
$_['entry_world_cost'] = 'Pakket rest van de Wereld';


//$_['entry_use_freeshipping'] = 'Maak je gebruik van<br />Gratis Verzending uit OpenCart?<br /><span class="help">Alleen activeren bij gebruik van Gratis Verzenden!!.</span>';
$_['entry_use_freeshipping'] = 'Maak je gebruik van gratis verzending uit OpenCart?';
$_['entry_tax_class']  = 'Belastinggroep';
$_['entry_geo_zone']   = 'Geo Zone';
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sorteervolgorde';

// Help
$_['help_use_freeshipping'] = 'Alleen activeren bij gebruik van Gratis Verzenden!!';

// tabs
$_['tab_netherlands'] = 'Nederland <img src="view/image/flags/nl.png" align="absmiddle" />';
$_['tab_germany'] 	  = 'Duitsland <img src="view/image/flags/de.png" align="absmiddle" />';
$_['tab_europe']	  = 'Europa <img src="view/image/flags/eu.png" align="absmiddle" />';
$_['tab_world'] 	  = 'Wereld <img src="view/image/flags/world.png" align="absmiddle" />';


// Error
$_['error_permission'] = 'Waarschuwing: U heeft geen rechten deze instellingen te wijzigen!';
?>