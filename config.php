<?php
// HTTP
define('HTTP_SERVER', 'http://localhost:81/elegance/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost:81/elegance/');

define('DIR_APPLICATION', 'D:/xampp/htdocs/elegance/catalog/');
define('DIR_SYSTEM', 'D:/xampp/htdocs/elegance/system/');
define('DIR_IMAGE', 'D:/xampp/htdocs/elegance/image/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/theme/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_SYSTEM . 'cache/');
define('DIR_DOWNLOAD', DIR_SYSTEM . 'download/');
define('DIR_LOGS', DIR_SYSTEM . 'logs/');
define('DIR_MODIFICATION', DIR_SYSTEM . 'modification/');
define('DIR_UPLOAD', DIR_SYSTEM . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'eleganceoud');
define('DB_PORT', '3307');
define('DB_PREFIX', 'oc_');
