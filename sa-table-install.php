<?php
// Configuration
if (file_exists('config.php')) {
	require_once('config.php');
}

// Startup
require_once(DIR_SYSTEM . 'startup.php');
// Registry
$registry = new Registry();

// Loader
$loader = new Loader($registry);
$registry->set('load', $loader);

// Config
$config = new Config();
$registry->set('config', $config);

// Database
$db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
$registry->set('db', $db);

$sql = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "customer_bid` ( ";
	  $sql .= "`bid_id` int(22) NOT NULL AUTO_INCREMENT, ";
    $sql .= "`product_id` int(11) NOT NULL, ";
	  $sql .= "`customer_id` int(11) NOT NULL, ";
	  $sql .= "`bid` decimal(15,4) NOT NULL DEFAULT '0.0000', ";
    $sql .= "`bid_status` tinyint(1) NOT NULL DEFAULT '0', ";
    $sql .= "`date_added` datetime NOT NULL, ";
	  $sql .= "PRIMARY KEY (`bid_id`) ";
	  $sql .= ") ENGINE=MyISAM DEFAULT CHARSET=utf8 ; ";
	  $db->query($sql);

$query = $db->query( "SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
		AND COLUMN_NAME='auc_status' AND TABLE_NAME='".DB_PREFIX."product'");
if(count($query->rows) <= 0){
$db->query("ALTER TABLE `" . DB_PREFIX . "product` ADD COLUMN `auc_status` tinyint(1) NOT NULL DEFAULT '0' AFTER `status`");
echo '1Installed';
} else {
echo '1Installed';
}

$query = $db->query( "SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
		AND COLUMN_NAME='auc_customer_id' AND TABLE_NAME='".DB_PREFIX."product'");
if(count($query->rows) <= 0){
$db->query("ALTER TABLE `" . DB_PREFIX . "product` ADD COLUMN `auc_customer_id` int(11) NOT NULL DEFAULT '0' AFTER `product_id`");
echo '2Installed';
} else {
echo '2Installed';
}

$query = $db->query( "SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
		AND COLUMN_NAME='start_bid' AND TABLE_NAME='".DB_PREFIX."product'");
if(count($query->rows) <= 0){
$db->query("ALTER TABLE `" . DB_PREFIX . "product` ADD COLUMN `start_bid` decimal(15,4) NOT NULL DEFAULT '0.0000' AFTER `price`");
echo '2Installed';
} else {
echo '2Installed';
}

$query = $db->query( "SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
		AND COLUMN_NAME='krok_bid' AND TABLE_NAME='".DB_PREFIX."product'");
if(count($query->rows) <= 0){
$db->query("ALTER TABLE `" . DB_PREFIX . "product` ADD COLUMN `krok_bid` decimal(15,4) NOT NULL DEFAULT '0.0000' AFTER `points`");
echo '3Installed';
} else {
echo '3Installed';
}

$query = $db->query( "SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
		AND COLUMN_NAME='date_ende' AND TABLE_NAME='".DB_PREFIX."product'");
if(count($query->rows) <= 0){
$db->query("ALTER TABLE `" . DB_PREFIX . "product` ADD COLUMN `date_ende` datetime NOT NULL AFTER `date_added`");
echo '4Installed';
} else {
echo '4Installed';
}

$query = $db->query( "SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
		AND COLUMN_NAME='bn_price' AND TABLE_NAME='".DB_PREFIX."product'");
if(count($query->rows) <= 0){
$db->query("ALTER TABLE `" . DB_PREFIX . "product` ADD COLUMN `bn_price` decimal(15,4) NOT NULL DEFAULT '0.0000' AFTER `price`");
echo '5Installed';
} else {
echo '5Installed';
}

$query = $db->query( "SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
		AND COLUMN_NAME='buynow_status' AND TABLE_NAME='".DB_PREFIX."product'");
if(count($query->rows) <= 0){
$db->query("ALTER TABLE `" . DB_PREFIX . "product` ADD COLUMN `buynow_status` tinyint(1) NOT NULL DEFAULT '0' AFTER `status`");
echo '6Installed';
} else {
echo '6Installed';
}

 


